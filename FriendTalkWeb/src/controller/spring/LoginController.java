package controller.spring;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
//import javax.mail.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.EditProfileManager;
import Manager.IndexManager;
import Manager.UserManager;
import bean.*;
import conn.HibernateConnection;
import conn.PasswordUtil;
import conn.ResponseObj;

@Controller
public class LoginController {
	private static String SALT = "123456";
	
    private static String USER_NAME = "idearyoujei@gmail.com";  // GMail user name (just the part before "@gmail.com")
    private static String PASSWORD = "29072560"; // GMail password
    private static String RECIPIENT = "aooza12370@gmail.com";
    
	@RequestMapping(value = "/openHome", method = RequestMethod.GET)
	public String openHome(HttpServletRequest request,HttpSession session) {
		EditProfileManager epm = new EditProfileManager();

		
		
		return "home";
	}
	@RequestMapping(value = "/openindex", method = RequestMethod.GET)
	public String openindex(HttpServletRequest request,HttpSession session) throws ParseException {
		
		return "index";
	}


	@RequestMapping(value = "/openlogin", method = RequestMethod.GET)
	public String loadloinpage() {
		HibernateConnection conn = new HibernateConnection();
		conn.doHibernateConnection();
		return "Login";
	}

	@RequestMapping(value = "/openregister", method = RequestMethod.GET)
	public String openregister(HttpServletRequest request, Model md, HttpSession session) {
		try {
			String Mtype = request.getParameter("Mtype");
			
			System.out.println(Mtype);
			
			if(Mtype.equals("consultant")) {
				session.setAttribute("mtype", Mtype);
				return "register";
			}else if(Mtype.equals("psychologist")) {
				session.setAttribute("mtype", Mtype);
				return "registerPsy";
			}else {
				session.setAttribute("mtype", Mtype);
				return "registerOrg";
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "Error";
	}

	@RequestMapping(value = "/verifylogin", method = RequestMethod.POST)
	public String verifylogin(HttpServletRequest request, Model md, HttpSession session) throws ParseException {
		
		String username = request.getParameter("uname");
		String password = request.getParameter("pwd");
		String type = request.getParameter("type");
		String result ;

		
//		try {
//			password = PasswordUtil.getInstance().createPassword(password, SALT);
//		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
		
		Login login = new Login(username, password, type);
		
		UserManager rm = new UserManager();
		result = rm.doHibernateLogin(login);
		
		System.out.println(result);
		
		IndexManager idm = new IndexManager();
	    List<MakeAppointment> listmake = idm.listAllMakeAppointment();
	    
	    if(listmake.size() > 0){
	    	for(int i=0 ; i<listmake.size() ; i++){ 
	    		String notify = listmake.get(i).getNotify();
	    		String sub = "เรียนคุณ"+listmake.get(i).getConsultant().getFristname()+" "+listmake.get(i).getConsultant().getLastname();
	    		
	    		String ds = listmake.get(i).getTime_start().toInstant().toString().substring(0, 10);
				int h = listmake.get(i).getTime_start().getTime().getHours();
				int m = listmake.get(i).getTime_start().getTime().getMinutes();
				int s = listmake.get(i).getTime_start().getTime().getSeconds();
				
			    
			    String time = String.valueOf(h)+":"+String.valueOf(m)+":"+String.valueOf(s);
			    DateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			    Date date = sdf.parse(time);
			    
			    String[] d = ds.split("-");
			    int day = Integer.parseInt(d[2]);
			    int month = Integer.parseInt(d[1]);
			    int year = Integer.parseInt(d[0]);
			    
			    String dd = d[2]+"-"+d[1]+"-"+d[0];
			    
			    DateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
			    Date ndate = new Date();
			    String di = sdf2.format(ndate);
			    String[] d2 = di.split("-");
			    int day2 = Integer.parseInt(d2[0]);
			    //datee
			    String subtime = date.toString() ;
			    String strT1 = subtime.replaceAll("Thu Jan 01 ", "");
			    String strT2 = strT1.replaceAll(":41 ICT 1970", "");
			    if(d[1].equals(d2[1])) {
			    	if(day - day2 == 1) {
			    		String ps = "วันพรุ้งนี้คึณมีนัด กับคุณ"+listmake.get(i).getPsychologist().getFristname()+" "+listmake.get(i).getPsychologist().getLastname()+" เวลา :"+ strT2;
	    			    if(notify.equals("รับการแจ้งเตือน")) {
				     
					        String from1 = USER_NAME;
					        String pass1 = PASSWORD;
					        String[] to1 = { RECIPIENT }; // list of recipient email addresses
					        String subject1 = sub ;
					        String body = ps;

					        try {
								sendFromGMail(from1, pass1, to1, subject1, body);
							} catch (MessagingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					        MakeAppointment ma = new MakeAppointment();
					        String mid = listmake.get(i).getAppointment_id();
					        ma = idm.getMakeAppointment(mid);
					        
					        ma.setNotify("แจ้งเตือนเสร็จสิ้น");
					        
					        String r = idm.insertMakeAppointment(ma);
					        System.out.println("R = "+r);
	    			    }
			    	}
			    }
	    	}
	    }

		
		if ("login success".equals(result)) {
			session.setAttribute("Mtype", login.getType());
			session.setAttribute("uname", login.getUsername());
			return "home";
		} else {
			md.addAttribute("error_msg", result);
			return "Login";
		}

	}

	@RequestMapping(value = "/verifylogin", method = RequestMethod.GET)
	public String verifyloginFail() {

		return "login";
	}
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String getLogout(HttpSession session ){
		String user = (String) session.getAttribute("user");
		session.removeAttribute("user");
		return "index";
	}
	private void sendFromGMail(String from1, String pass1, String[] to1, String subject1, String body) throws MessagingException {
		Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from1);
        props.put("mail.smtp.password", pass1);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from1));
            InternetAddress[] toAddress = new InternetAddress[to1.length];

            // To get the array of addresses
            for( int i = 0; i < to1.length; i++ ) {
                toAddress[i] = new InternetAddress(to1[i]);
            }

            for( int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject1);
            message.setText(body);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from1, pass1);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            
        }
        catch (AddressException ae) {
            ae.printStackTrace();

        }
		
	}
}
