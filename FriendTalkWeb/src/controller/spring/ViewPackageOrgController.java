package controller.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.*;
import bean.*;
import bean.Psychologist;
import bean.Redeem;
import conn.HibernateConnection;

@Controller
public class ViewPackageOrgController {
	@RequestMapping(value = "/openViewPackageOrg", method = RequestMethod.GET)
	public String openRedeem(HttpServletRequest request, Model md, HttpSession session) {
		
		String user = request.getParameter("user");
		ViewPackageOrgManager vpom = new ViewPackageOrgManager();
		
			Organization org = vpom.getOrganization(user);
			
			request.setAttribute("org", org);

		return "ViewPackageOrganization";
	}

}
