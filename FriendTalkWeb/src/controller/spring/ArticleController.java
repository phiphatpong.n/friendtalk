package controller.spring;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import Manager.*;
import bean.*;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class ArticleController {
	
	@RequestMapping(value = "/openlistArticle", method = RequestMethod.GET)
	public String openlistArticle(HttpServletRequest request, Model md, HttpSession session) {
		
		String user = request.getParameter("user");
		
		ArticleManager am = new ArticleManager();
		EditProfileManager epm = new EditProfileManager();
		List<Article> arList = new ArrayList<Article>();
		
		String type = epm.oooo(user);
		if(type != null) {
		if(type.toLowerCase().equals("psychologist".toLowerCase())) {
			 arList = am.ListArticle(user);
		}else {
			arList = am.listAllarticle();
		}
		}else {
			arList = am.listAllarticle();
		}
		session.setAttribute("user", user);
		session.setAttribute("type", type);
		request.setAttribute("arList", arList);
		
		return "ListArticle";
	}
	
	@RequestMapping(value = "/openAddArticle", method = RequestMethod.GET)
	public String openAddArticle(HttpServletRequest request, Model md, HttpSession session) {
		
		ArticleManager am = new ArticleManager();
		int maxid = am.getMaxarticleId();
		
		String user = request.getParameter("user");
		String Aid = "A" + String.valueOf(maxid) ;
		
		Psychologist Pid = new Psychologist();
		Pid = am.getPsychologist(user);
		
		session.setAttribute("Pid", Pid);
		session.setAttribute("Aid", Aid);
		
		return "AddArticle";
	}
	@RequestMapping(value = "/openEditArticle", method = RequestMethod.GET)
	public String openEditArticle(HttpServletRequest request, Model md, HttpSession session) {
		
		ArticleManager am = new ArticleManager();
		
		String Aid = request.getParameter("Aid");
		
		Article article = new Article();
		
		article = am.getArticle(Aid);
		
		session.setAttribute("article", article);
		
		return "EditArticle";
	}
	@RequestMapping(value="/addArticle", method=RequestMethod.POST)
	public String AddArticle(HttpServletRequest request ,HttpSession session) throws UnsupportedEncodingException {
		request.setCharacterEncoding("UTF-8");
		
		if(ServletFileUpload.isMultipartContent(request)) {
			
			try {
				List<FileItem> data = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
				String article_id = data.get(0).getString("UTF-8");
				String article_name = data.get(1).getString("UTF-8");
				String article_detail = data.get(2).getString("UTF-8");
				String add_date = data.get(3).getString("UTF-8");
				String filename = new File(data.get(4).getName()).getName();
				String username = data.get(5).getString("UTF-8");
				
				SimpleDateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
				Date date = dfm.parse(add_date);
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				
				ArticleManager am = new ArticleManager();
				Psychologist Pid = new Psychologist();
				Pid = am.getPsychologist(username);
				
				String path = request.getSession().getServletContext().getRealPath("/") + "WEB-INF\\images\\" ;
				System.out.println(path);
				String f = filename.substring(filename.indexOf("."));
				data.get(4).write(new File(path + File.separator + article_name + f ));
				String img_A = article_name + f ;
				
				Article article = new Article(article_id,article_name,article_detail,cal,img_A);
				article.setPsychologist(Pid);
				
				String result = am.insertAddArticle(article);
				
				System.out.println(result);
				

				
				if(result.equals("successfully saved")) {
					  List<Article> arList = am.ListArticle(username);
					  request.setAttribute("arList", arList);
					  session.setAttribute("user", username);	
					  return "ListArticle" ;
				}else {
					 return "AddArticle" ;
				}
			} catch (Exception e) {e.printStackTrace();}
		}
		return null;
		
	}
	@RequestMapping(value="/EditArticle", method=RequestMethod.POST)
	public ModelAndView EditArticle(HttpServletRequest request , HttpSession session) throws UnsupportedEncodingException {
		ModelAndView mav = new ModelAndView("ListArticle");
		request.setCharacterEncoding("UTF-8");
		
		if(ServletFileUpload.isMultipartContent(request)) {
			
			try {
				List<FileItem> data = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
				String article_id = data.get(0).getString("UTF-8");
				String article_name = data.get(2).getString("UTF-8");
				String article_detail = data.get(1).getString("UTF-8");
				String add_date = data.get(3).getString("UTF-8");
				String filename = new File(data.get(4).getName()).getName();
				String username = data.get(5).getString("UTF-8");
				
				SimpleDateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
				Date date = dfm.parse(add_date);
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				
				Article art =new  Article();
				ArticleManager am = new ArticleManager();
				String result = "";
				
				art = am.getArticle(article_id);
				
				Psychologist Pid = new Psychologist();
				Pid =(Psychologist) session.getAttribute("mid");
				
				if(!filename.equals("null")) {
					Article article = new Article(article_id,article_detail,article_name,cal,art.getImg_articles());
					article.setPsychologist(Pid);
					result = am.editArticle(article);
					
				}else {
					String path = request.getSession().getServletContext().getRealPath("/") + "WEB-INF\\images\\" ;
					String f = filename.substring(filename.indexOf("."));
					data.get(4).write(new File(path + File.separator + article_name + f ));
					String img_a = article_name + f ;
					
					Article article = new Article(article_id,article_detail,article_name,cal,img_a);
					article.setPsychologist(Pid);
					result = am.editArticle(article);
					
				}
				
				System.out.println(result);
				
				  List<Article> arList = am.ListArticle(username);
				  request.setAttribute("arList", arList);
				
				if(result.equals("successfully saved")) {
					
					mav.addObject("msg","Insert sucess!!");					
				}else {
					mav.addObject("msg","Insert fail!!");
				}
			} catch (Exception e) {}
		}
		return mav ;
	}
	@RequestMapping(value = "/openArticleDetils", method = RequestMethod.GET)
	public String openArticleDetils(HttpServletRequest request, Model md, HttpSession session) {
		
		ArticleManager am = new ArticleManager();
		Article article = new Article();
		
		String Aid = request.getParameter("Aid");
		article = am.getArticle(Aid);
		
		
		session.setAttribute("article",article );
		
		return "ArticleDetails";
	}
	@RequestMapping(value = "/DeleteArticle", method = RequestMethod.GET)
	public String DeleteArticle(HttpServletRequest request, Model md, HttpSession session) throws SQLException {
		
		ArticleManager am = new ArticleManager();
		Article article = new Article();
		
		String id = request.getParameter("Aid");
		
		Article art = am.getArticle(id);
		String user = art.getPsychologist().getUsername();
		
		article.setArticle_id(id);
		int result =am.deleteArticle(article);
		int alt = 5 ;
		if(result == 1) {
			alt = 1 ;
			request.setAttribute("alt", alt);
		}else {
			alt = 0 ;
			request.setAttribute("alt", alt);
		}
		
		List<Article> arList = am.ListArticle(user);
		request.setAttribute("arList", arList);
		 
		
		return "ListArticle";
	}
}
