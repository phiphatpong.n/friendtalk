package controller.spring;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.*;
import bean.*;

@Controller
public class ViewAppointmentController {
	@RequestMapping(value = "/openViewAppointment", method = RequestMethod.GET)
	public String openViewAppointment(HttpServletRequest request, Model md, HttpSession session) {
		
		String user = request.getParameter("user");
		
		MakeAppointmentManager mam = new MakeAppointmentManager();
		ViewAppointmentManager vam = new ViewAppointmentManager();
		
		List<MakeAppointment> listMA = new ArrayList<>();
		
		listMA = vam.listAllMakeAppointment();
		
		Date datenow = new Date();
		SimpleDateFormat df = new SimpleDateFormat("dd-M-yyyy");
		String dd = df.format(datenow);
		String[]  strdate = dd.split("-");
		int dnow = Integer.parseInt(strdate[0]);
		int mnow = Integer.parseInt(strdate[1]);
		System.out.println("Date/Now = "+dnow + " = " + mnow);
		String result = "" ;
		for(int i=0 ; i<listMA.size() ; i++) {
			String ds = listMA.get(i).getTime_start().toInstant().toString().substring(0, 10);
			String[]  strds = ds.split("-");
			String dates = strds[2] +"-"+ strds[1] +"-"+ strds[0] ;
			int dapp = Integer.parseInt(strds[2]);
			int mapp = Integer.parseInt(strds[1]);
			System.out.println(i + " = " + dapp + "  " + mapp );
			 
			if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")) {
				if(mnow == mapp) {
					if(dapp - dnow < 3) {
						MakeAppointment ma = new MakeAppointment();
						String id = listMA.get(i).getAppointment_id();
						ma = mam.getMakeAppointment(id);
						ma.setStatus_appoint("ยกเลิกนัด");
						result = mam.insertMakeAppointment(ma);
						System.out.println("ยกเลิกนัด");
					}
				}
			}
		}
		
		String Mtype = vam.GettypeMember(user);
		if(Mtype.equals("consultant")) {
			listMA = mam.listAllMakeAppointmentconsultant(user);
		}else {
			listMA = mam.listAllMakeAppointmentpsychologist(user);
		}
		int alt = 0 ;
		if(result.equals("successfully saved")) {
			
			List<Advice> listadvice = vam.ListAdvice();
			List<Payment> listpay = vam.ListPayment();
			
			List<PsychologistExpertise> listPEK = mam.listPsychologistExpertise();
			System.out.println("List size = "+listMA.size());
			session.setAttribute("user", user);
			request.setAttribute("listMA", listMA);
			request.setAttribute("listPEK", listPEK);
			request.setAttribute("Mtype", Mtype);
			request.setAttribute("listadvice", listadvice);
			request.setAttribute("listpay", listpay);
			
			return "ViewAppointment";
		}else {
			
			List<Advice> listadvice = vam.ListAdvice();
			List<Payment> listpay = vam.ListPayment();
			
			List<PsychologistExpertise> listPEK = mam.listPsychologistExpertise();
			System.out.println("List size = "+listMA.size());
			session.setAttribute("user", user);
			request.setAttribute("listMA", listMA);
			request.setAttribute("listPEK", listPEK);
			request.setAttribute("Mtype", Mtype);
			request.setAttribute("listadvice", listadvice);
			request.setAttribute("listpay", listpay);
			
			return "ViewAppointment";
		}
		
		
		
	}
}
