package controller.spring;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import Manager.MakeAppointmentManager;
import Manager.SearchManager;
import Manager.ViewAppointmentManager;
import bean.*;

@Controller
public class SearchController {
	
	@RequestMapping(value = "/searchArticle", method = RequestMethod.POST)
	public ModelAndView getSearchArticle(HttpServletRequest request, Model md, HttpSession session) {
		String message = "";
		ModelAndView mav = new ModelAndView("ListArticle");
		try {

			request.setCharacterEncoding("UTF-8");

			String ArticleName = request.getParameter("keyword");

			SearchManager sea = new SearchManager();

			List<Article> arList = sea.searchArticleName(ArticleName);

			if (arList != null) {

				request.setAttribute("arList", arList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			message = "Please try again....";
		}
		return mav;
	}
	@RequestMapping(value = "/searchpsychologist", method = RequestMethod.POST)
	public ModelAndView getSearchPsychologist(HttpServletRequest request, Model md, HttpSession session) {
		String message = "";
		ModelAndView mav = new ModelAndView("ListPsychologist");
		try {

			request.setCharacterEncoding("UTF-8");

			String keyword = request.getParameter("keyword");

			SearchManager sea = new SearchManager();

			List<Psychologist> lsitP = sea.ListPsychologistName();
			List<PsychologistExpertise> listPE = sea.ListPsychologistExpertise();
			List<Expertise> listE = sea.ListExpertise(keyword);
			
			List<Psychologist> list = new ArrayList<Psychologist>();
			Psychologist psy = new Psychologist();
			for(int i=0 ; i<listE.size() ; i++) {
				for(int j=0 ; j<listPE.size() ; j++) {
					if(listE.get(i).getExpertise_id() == listPE.get(j).getPk().getExpertise().getExpertise_id()) {
						for(int k=0 ; k<lsitP.size() ; k++) {
							if(lsitP.get(k).getUsername().equals(listPE.get(j).getPk().getPsychologist().getUsername())) {
								String user = lsitP.get(k).getUsername();
								psy = sea.getProfilePsychologist(user);
								list.add(psy);
				 			}
							System.out.println("Size list =>"+list.size());
						}
					}
					
				}
			}
			MakeAppointmentManager mam = new MakeAppointmentManager();
			ViewAppointmentManager vam = new ViewAppointmentManager();
			if (list != null) {
				List<PsychologistExpertise> listPEK = sea.listPsychologistExpertise();
				List<SetWorkingTime>  listswt = mam.listAllSetWorkingTime();
				List<Review> listreReviews = mam.listReview();
				List<Advice> liAdvices = mam.listAdvice();
				
				request.setAttribute("listreReviews", listreReviews);
				request.setAttribute("liAdvices", liAdvices);
				
				request.setAttribute("listswt", listswt);
				request.setAttribute("list", list);
				request.setAttribute("listPEK", listPEK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			message = "Please try again....";
		}
		return mav;
	}
}
