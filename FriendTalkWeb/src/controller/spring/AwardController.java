package controller.spring;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import Manager.AwardManager;
import Manager.EditProfileManager;
import Manager.UserManager;
import bean.Article;
import bean.Award;
import bean.Consultant;
import bean.Login;
import bean.Organization;
import bean.Psychologist;
import bean.Redeem;

@Controller
public class AwardController {
	
	@RequestMapping(value = "/openListAward", method = RequestMethod.GET)
	public String openListAward(HttpServletRequest request, Model md, HttpSession session) {
		try {
		
		String user = request.getParameter("user");
		AwardManager awm = new AwardManager();
		 
		List<Award> awList = awm.listAllAward();
		List<Redeem> listredeem = awm.getlistredeem();
		
		session.setAttribute("user", user);
		request.setAttribute("awList", awList);
		request.setAttribute("listredeem", listredeem);
		
		} catch (Exception e) {
		// TODO: handle exception
		}
		return "ListAward";
	}
	
	@RequestMapping(value = "/openAddAward", method = RequestMethod.GET)
	public String openAddAward(HttpServletRequest request, Model md, HttpSession session) {
		try {
		
		String user = request.getParameter("user");
		AwardManager awm = new AwardManager();
		
		int maxid = awm.getMaxAwardId();
		String Awid = "AW" + String.valueOf(maxid) ;

		session.setAttribute("user", user);
		session.setAttribute("Awid", Awid);
		
		} catch (Exception e) {
		// TODO: handle exception
		}
		return "AddAward";
	}
	@RequestMapping(value = "/openEditAward", method = RequestMethod.GET)
	public String openEditAward(HttpServletRequest request, Model md, HttpSession session) {
		try {
		AwardManager awm = new AwardManager();
		Award award =new Award();
		
		//String user = request.getParameter("user");
		String awid = request.getParameter("Awid");
		
		award = awm.getAward(awid);
		
		session.setAttribute("award", award);
		
		} catch (Exception e) {
		// TODO: handle exception
		}
		return "EditAward";
	}
	@RequestMapping(value="/AddAward", method=RequestMethod.POST)
	public ModelAndView AddAward(HttpServletRequest request,HttpSession session) throws UnsupportedEncodingException {
		ModelAndView mav = new ModelAndView("ListAward");
		request.setCharacterEncoding("UTF-8");
		
		if(ServletFileUpload.isMultipartContent(request)) {
			
			try {
				List<FileItem> data = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
				String award_id = data.get(0).getString("UTF-8");
				String award_name = data.get(1).getString("UTF-8");
				String award_detail = data.get(2).getString("UTF-8");
				int score = Integer.parseInt(data.get(3).getString("UTF-8"));
				String Available_from = data.get(4).getString("UTF-8");
				String Expiration_date = data.get(5).getString("UTF-8");
				String filename = new File(data.get(6).getName()).getName();
				
				
				AwardManager awm = new AwardManager();
				String result = "" ;
				
				String path = request.getSession().getServletContext().getRealPath("/") + "WEB-INF\\images\\" ;
				System.out.println(path);
				String f = filename.substring(filename.indexOf("."));
				data.get(6).write(new File(path + File.separator + award_id + f ));
				
				String img_aw = award_id + f ;
				
				Calendar cal2 = Calendar.getInstance();
				Calendar cal1 = Calendar.getInstance(); 
				if(!Available_from.equals("") && !Expiration_date.equals("")) {
					SimpleDateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
					Date date = dfm.parse(Available_from);
					cal1.setTime(date);
					
					SimpleDateFormat dfm1 = new SimpleDateFormat("yyyy-MM-dd");
					Date date1 = dfm1.parse(Expiration_date);
					cal2.setTime(date1);
					
					Award awa = new Award(award_id,award_name,award_detail,score,img_aw,cal1,cal2);
					result = awm.insertAddAward(awa);
				}else {
					Award awa = new Award(award_id,award_name,award_detail,score,img_aw,null,null);
					result = awm.insertAddAward(awa);
				}

				
				
				
				
				
				
				System.out.println(result);
				
				if(result.equals("successfully saved")) {
					
					List<Award> awList = awm.listAllAward();
					List<Redeem> listredeem = awm.getlistredeem();
					
					request.setAttribute("listredeem", listredeem);
					request.setAttribute("awList", awList);	
					
					mav.addObject("msg","Insert sucess!!");					
				}else {
					mav.addObject("msg","Insert fail!!");
				}
			} catch (Exception e) {}
		}
		return mav ;
	}
	@RequestMapping(value="/EditAward", method=RequestMethod.POST)
	public ModelAndView EditAward(HttpServletRequest request ,HttpSession session) throws UnsupportedEncodingException {
		ModelAndView mav = new ModelAndView("ListAward");
		request.setCharacterEncoding("UTF-8");
		
		if(ServletFileUpload.isMultipartContent(request)) {
			
			try {
				List<FileItem> data = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
				
				String award_id = data.get(0).getString("UTF-8");
				String award_name = data.get(1).getString("UTF-8");
				String award_detail = data.get(2).getString("UTF-8");
				int score = Integer.parseInt(data.get(3).getString("UTF-8"));
				String Available_from = data.get(4).getString("UTF-8");
				String Expiration_date = data.get(5).getString("UTF-8");
				String filename = new File(data.get(6).getName()).getName();
				
				AwardManager awm = new AwardManager();
				Award award = new Award();
				String result = "" ;
				award  = awm.getAward(award_id);
				
				Calendar cal2 = Calendar.getInstance();
				Calendar cal1 = Calendar.getInstance();
				if(!Available_from.equals("") || !Expiration_date.equals("")) {
				SimpleDateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
				Date date = dfm.parse(Available_from);
				cal1.setTime(date);
				
				SimpleDateFormat dfm1 = new SimpleDateFormat("yyyy-MM-dd");
				Date date1 = dfm1.parse(Expiration_date);
				cal2.setTime(date1);
				}
				if(filename == null) {
					Award awa = new Award(award_id,award_name,award_detail,score,award.getImg_award(),cal1,cal2);
					result = awm.insertAddAward(awa);
				}else {
					
					String path = request.getSession().getServletContext().getRealPath("/") + "WEB-INF\\images\\" ;
					String f = filename.substring(filename.indexOf("."));
					data.get(6).write(new File(path + File.separator + award_id + f ));
					
					String img_aw = award_id + f ;
					
					Award awa = new Award(award_id,award_name,award_detail,score,award.getImg_award(),cal1,cal2);
					 result = awm.insertAddAward(awa);
				}
				
				
				
				System.out.println(result);
				
				if(result.equals("successfully saved")) {
					List<Award> awList = awm.listAllAward();
					List<Redeem> listredeem = awm.getlistredeem();
					
					request.setAttribute("listredeem", listredeem);
					request.setAttribute("awList", awList);	
					mav.addObject("msg","Insert sucess!!");					
				}else {
					mav.addObject("msg","Insert fail!!");
				}
			} catch (Exception e) {}
		}
		return mav ;
	}
	@RequestMapping(value = "/DeleteAward", method = RequestMethod.GET)
	public String DeleteAward(HttpServletRequest request, Model md, HttpSession session) throws SQLException {
		
		AwardManager awm = new AwardManager();
		Award aw = new Award();
		
		String id = request.getParameter("Awid");

		aw.setAward_id(id);
		int result =awm.deleteAward(aw);
		int alt = 5 ;
		if(result < 1) {
			alt = 1 ;
			request.setAttribute("alt", alt);
		}else {
			alt = 0 ;
			request.setAttribute("alt", alt);
		}
		
		List<Award> awList = awm.listAllAward();
		List<Redeem> listredeem = awm.getlistredeem();
		
		request.setAttribute("listredeem", listredeem);
		request.setAttribute("awList", awList);
		 
		
		return "ListAward";
	}
}
