package controller.spring;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import Manager.EditProfileManager;
import Manager.ExpertiseManager;
import Manager.UserManager;
import bean.Consultant;
import bean.Expertise;
import bean.Login;
import bean.Organization;
import bean.Psychologist;
import bean.PsychologistExpertise;
import bean.PsychologistExpertiseKey;

@Controller
public class EditProfileController {
	private static String SALT = "123456";
	
	@RequestMapping(value="/openEditprofile", method=RequestMethod.GET)
	public String loadEdit(HttpServletRequest request, Model md, HttpSession session) {
		try {
		Consultant consultant = new Consultant();
		Psychologist psychologist = new Psychologist();
		Organization organization = new Organization();
		
		String user = request.getParameter("user");
 
		EditProfileManager epm = new EditProfileManager();
		String type = epm.oooo(user);
		 
		if(type.equals("consultant")){
			consultant  = epm.getProfileConsultant(user);
			request.setAttribute("Cid", consultant);
			System.out.println(consultant.getUsername());
			return "EditProfileCon";
		}else if(type.equals("psychologist")){
			psychologist = epm.getProfilePsychologist(user);
			request.setAttribute("Pid", psychologist);
			System.out.println(psychologist.getUsername());
			return "EditProfilePsy";
		}else{
			organization = epm.getProfileOrganization(user);
			request.setAttribute("Oid", organization);
			System.out.println(organization.getUsername()); 
			return "EditProfileOrg";
		}
		} catch (Exception e) {
			// TODO: handle exception
			}
		return "home";
		
	}
	

	
	@RequestMapping(value="/editprofilecon", method=RequestMethod.POST)
	public ModelAndView RegisterCon(HttpServletRequest request,HttpSession session) throws UnsupportedEncodingException {
		ModelAndView mav = new ModelAndView("EditProfileCon");
		request.setCharacterEncoding("UTF-8");
		
		if(ServletFileUpload.isMultipartContent(request)) {
			
			try {
				List<FileItem> data = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
				String title = data.get(0).getString("UTF-8");
				String fristname = data.get(1).getString("UTF-8");
				String lastname = data.get(2).getString("UTF-8");
				String gender = data.get(3).getString("UTF-8");
				String Brihtday =  data.get(4).getString("UTF-8");
				String email = data.get(5).getString("UTF-8");
				String phone = data.get(6).getString("UTF-8");
				String address  = data.get(7).getString("UTF-8");
				String congenital_disease  = data.get(8).getString("UTF-8");
				String drug_allergy  = data.get(9).getString("UTF-8");
				String food_allergy  = data.get(10).getString("UTF-8");
				String username = data.get(11).getString("UTF-8");
				String password = data.get(12).getString("UTF-8");
				String Mtype = data.get(13).getString("UTF-8");
				String filename = new File(data.get(14).getName()).getName();

				if(title.equals("Mr")) {
					title = "นาย";
				}else if(title.equals("Mrs")){
					title = "นาง";
				}else {
					title = "นางสาว";
				}
				
				SimpleDateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				
				Date date = dfm.parse(Brihtday);
				cal.setTime(date);
				System.out.println("==>"+date);
				System.out.println("=>"+cal);
				UserManager um = new UserManager();
				EditProfileManager epm = new EditProfileManager();
				Consultant consultant = new Consultant();
				String result = "" ;
				consultant  = epm.getProfileConsultant(username);
				
				if(!filename.equals(consultant.getImg_c())) {
					Consultant con = new Consultant(username,title,fristname,lastname,gender,cal,phone,email,address,food_allergy,drug_allergy,congenital_disease,consultant.getImg_c());
					 result = um.insertConsultant(con);
				}else {
					String path = request.getSession().getServletContext().getRealPath("/") + "WEB-INF\\images\\" ;
					String f = filename.substring(filename.indexOf("."));
					data.get(14).write(new File(path + File.separator + username + f ));
					String img_c = username + f ;
					
					Consultant con = new Consultant(username,title,fristname,lastname,gender,cal,phone,email,address,food_allergy,drug_allergy,congenital_disease,img_c);
					result = um.insertConsultant(con);
				}
				Login login = new Login(username,password,Mtype);
				login = um.insertLogin(login);
				
				System.out.println(result);
				
				if(result.equals("Sign Up Successfully")) {
					consultant  = epm.getProfileConsultant(username);
					request.setAttribute("Cid", consultant);
					mav.addObject("msg","Insert sucess!!");					
				}else {
					mav.addObject("msg","Insert fail!!");
				}
			} catch (Exception e) {}
		}
		return mav ;
	}
	@RequestMapping(value="/editprofilepsy", method=RequestMethod.POST)
	public ModelAndView RegisterPsy(HttpServletRequest request,HttpSession session) throws UnsupportedEncodingException {
		ModelAndView mav = new ModelAndView("EditProfilePsy");
		request.setCharacterEncoding("UTF-8");
		
		if(ServletFileUpload.isMultipartContent(request)) {
			
			try {
				List<FileItem> data = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
	
				String title = data.get(0).getString("UTF-8");
				String fristname = data.get(1).getString("UTF-8");
				String lastname = data.get(2).getString("UTF-8");
				String gender = data.get(3).getString("UTF-8");
				String Brihtday =  data.get(4).getString("UTF-8");
				String email = data.get(5).getString("UTF-8");
				String phone =data.get(6).getString("UTF-8");
				String address  = data.get(7).getString("UTF-8");
				String educationl_history = data.get(8).getString("UTF-8");
				String working_history = data.get(9).getString("UTF-8");
				String mentor_type = data.get(10).getString("UTF-8");
				String username = data.get(11).getString("UTF-8");
				String password = data.get(12).getString("UTF-8");
				String Mtype = data.get(13).getString("UTF-8");
				String certificate = new File(data.get(15).getName()).getName();
				String filename = new File(data.get(14).getName()).getName();
				
				if(title.equals("Mr")) {
					title = "นาย";
				}else if(title.equals("Mrs")){
					title = "นาง";
				}else {
					title = "นางสาว";
				}
				
				System.out.println(Brihtday);
				
				SimpleDateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				Date date = dfm.parse(Brihtday);
				cal.setTime(date);
				
				UserManager um = new UserManager();
				EditProfileManager epm = new EditProfileManager();
				Psychologist psychologist = new Psychologist();
				String result = "" ;
				
				psychologist  = epm.getProfilePsychologist(username);
				String img_p ="" ;
				String img_certificat = "" ;
				
				String path = request.getSession().getServletContext().getRealPath("/") + "WEB-INF\\images\\" ;
				System.out.println(path);
				
				if(!filename.equals("")) {
					String f = filename.substring(filename.indexOf("."));
					data.get(14).write(new File(path + File.separator + username + f ));
					img_p = username + f ;
				}
				if(!certificate.equals("")) {
					String c = certificate.substring(certificate.indexOf("."));
					data.get(15).write(new File(path + File.separator + fristname + c ));
					img_certificat = fristname + c ;
				}
				
				 
				
				if(filename.equals("")) {
					if(certificate.equals("")) {
						Psychologist psy = new Psychologist(username,title,fristname,lastname,gender,cal,phone,email,address,educationl_history,working_history,mentor_type,psychologist.getCertificate(),psychologist.getImg_p(),psychologist.getGoodness_score(),password);
						result = um.insertPsychologist(psy);
					}else {
						Psychologist psy = new Psychologist(username,title,fristname,lastname,gender,cal,phone,email,address,educationl_history,working_history,mentor_type,img_certificat,psychologist.getImg_p(),psychologist.getGoodness_score(),password);
						 result = um.insertPsychologist(psy);
					}
				}else {
					if(certificate.equals("")) {
						Psychologist psy = new Psychologist(username,title,fristname,lastname,gender,cal,phone,email,address,educationl_history,working_history,mentor_type,psychologist.getCertificate(),img_p,psychologist.getGoodness_score(),password);
						result = um.insertPsychologist(psy);
					}else {
						Psychologist psy = new Psychologist(username,title,fristname,lastname,gender,cal,phone,email,address,educationl_history,working_history,mentor_type,img_certificat,img_p,psychologist.getGoodness_score(),password);
						 result = um.insertPsychologist(psy);
					}
				}
				Login login = new Login(username,password,Mtype);
				login = um.insertLogin(login);
				
				psychologist = epm.getProfilePsychologist(username);
				
				System.out.println(result);
				
				if(result.equals("Sign Up Successfully")) {
					session.setAttribute("uname", username);
					request.setAttribute("Pid", psychologist);
					mav.addObject("msg","Insert sucess!!");
				}else {
					mav.addObject("msg","Insert fail!!");
				}
			} catch (Exception e) {}
		}
		return mav ;
	}
	@RequestMapping(value="/editprofileorg", method=RequestMethod.POST)
	public ModelAndView RegisterOrg(HttpServletRequest request,HttpSession session) throws UnsupportedEncodingException {
		ModelAndView mav = new ModelAndView("EditProfileOrg");
		request.setCharacterEncoding("UTF-8");
		if(ServletFileUpload.isMultipartContent(request)) {
			
			try {
				List<FileItem> data = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
	
				String cname = data.get(0).getString("UTF-8");
				String email = data.get(1).getString("UTF-8");
				String phone = data.get(2).getString("UTF-8");
				String address = data.get(3).getString("UTF-8");
				String employee_number = data.get(4).getString("UTF-8");
				
				String username = data.get(5).getString("UTF-8");
				String password = data.get(6).getString("UTF-8");
				String Mtype = data.get(7).getString("UTF-8");
				String crc = new File(data.get(8).getName()).getName();
				int Tcall = Integer.parseInt(data.get(9).getString("UTF-8"));
				int Tchat = Integer.parseInt(data.get(10).getString("UTF-8"));
				
				UserManager um = new UserManager();
				EditProfileManager epm = new EditProfileManager();
				
				Organization org = new Organization();
				String result = "" ;
				
				org  = epm.getProfileOrganization(username);
				
				String path = request.getSession().getServletContext().getRealPath("/") + "WEB-INF\\images\\" ;
				System.out.println(path);
				String f = crc.substring(crc.indexOf("."));
				data.get(8).write(new File(path + File.separator + username + f ));
				
				String img_crc = username + f ;
				
				Login login = new Login(username,password,Mtype);
				if(crc.equals("")) {
					Organization organization = new Organization(username,cname,phone,email,address,employee_number,org.getCompany_registration_certificate(),Tcall,Tchat,org.getMinute_code_call(),org.getMinute_code_chat());
					result = um.insertOrganization(organization);
				}else {
					Organization organization = new Organization(username,cname,phone,email,address,employee_number,img_crc,Tcall,Tchat,org.getMinute_code_call(),org.getMinute_code_chat());
					result = um.insertOrganization(organization);
				}
				
				login = um.insertLogin(login);
				
				System.out.println(result);
				
				if(result.equals("Sign Up Successfully")) {
					request.setAttribute("uname", username);
					mav.addObject("msg","Insert sucess!!");
				}else {
					mav.addObject("msg","Insert fail!!");
				}
			} catch (Exception e) {}
		}
		return mav ;
	}
}
