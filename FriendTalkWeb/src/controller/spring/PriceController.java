package controller.spring;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.*;
import bean.*;

@Controller
public class PriceController {
	@RequestMapping(value = "/openPrice", method = RequestMethod.GET)
	public String openPaymantCode(HttpServletRequest request, Model md, HttpSession session) {
		try {

			PriceManager pm = new PriceManager();
			
			List<Price> prices = pm.ListPrice();
			
			request.setAttribute("prices", prices); 

		} catch (Exception e) {
			// TODO: handle exception
		}
		return "UpdateServicePrice";
	}
	
	@RequestMapping(value = "/UpdatePrice", method = RequestMethod.POST)
	public String AddReview(HttpServletRequest request, Model md, HttpSession session) throws ParseException, UnsupportedEncodingException {
		request.setCharacterEncoding("UTF-8");
		
		String type1 = request.getParameter("type1");
		String call30 = request.getParameter("call30");
		String Tcall30 = request.getParameter("Tcall30");
		
		Double c1_30 = Double.valueOf(call30);
		int T1c30 = Integer.parseInt(Tcall30);
		
		String type2 = request.getParameter("type2");
		String call60 = request.getParameter("call60");
		String Tcall60 = request.getParameter("Tcall60");
		
		Double c2_60 = Double.valueOf(call60);
		int T2c60 = Integer.parseInt(Tcall60);
		
		String type3 = request.getParameter("type3");
		String chat30 = request.getParameter("chat30");
		String Tchat30 = request.getParameter("Tchat30");
		
		Double c3_30 = Double.valueOf(chat30);
		int T3c30 = Integer.parseInt(Tchat30);
		
		String type4 = request.getParameter("type4");
		String chat60 = request.getParameter("chat60");
		String Tchat60 = request.getParameter("Tchat60");
		
		Double c4_60 = Double.valueOf(chat60);
		int T4c60 = Integer.parseInt(Tchat60);
		
		PriceManager pm = new PriceManager();
		
		Price price1 = new Price(type1,c1_30,T1c30);
		Price price2 = new Price(type2,c2_60,T2c60);
		Price price3 = new Price(type3,c3_30,T3c30);
		Price price4 = new Price(type4,c4_60,T4c60);
		
		String result1 = pm.UpdatePrice(price1) ;
		String result2 = pm.UpdatePrice(price2) ;
		String result3 = pm.UpdatePrice(price3) ;
		String result4 = pm.UpdatePrice(price4) ;
		
		int alt = 0 ;
		if(result1.equals("Successfully saved") && result2.equals("Successfully saved") && result3.equals("Successfully saved") && result4.equals("Successfully saved")) {	
			List<Price> prices = pm.ListPrice();
			alt = 1 ;
			request.setAttribute("prices", prices); 
			request.setAttribute("alt", alt); 
			
			return "UpdateServicePrice";
		}else {
			alt = 0 ;
			request.setAttribute("alt", alt);
			return "UpdateServicePrice";
		}

	}
}
