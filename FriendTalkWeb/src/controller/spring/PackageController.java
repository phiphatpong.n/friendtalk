package controller.spring;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import Manager.EditProfileManager;
import Manager.PackageManager;
import Manager.UserManager;
import bean.Consultant;
import bean.Login;
import bean.packages;

@Controller
public class PackageController {
	@RequestMapping(value = "/openPackage", method = RequestMethod.GET)
	public String openPaymantCode(HttpServletRequest request, Model md, HttpSession session) {
		try {

			String user = request.getParameter("user"); 
			PackageManager pm = new PackageManager();
			
			List<packages> list_pack = pm.listAllpackages();
			String type = pm.gettype(user);
			
			session.setAttribute("list_pack", list_pack);
			session.setAttribute("type", type); 

		} catch (Exception e) {
			// TODO: handle exception
		}
		return "Package";
	}
	@RequestMapping(value = "/openEditPackage", method = RequestMethod.GET)
	public String openEditPackage(HttpServletRequest request, Model md, HttpSession session) {
		try {
			String size = request.getParameter("size");
			PackageManager pm = new PackageManager();
			packages pack = new packages();
			
			pack = pm.getPackage(size);
			
			session.setAttribute("pack", pack);

		} catch (Exception e) {
			// TODO: handle exception
		}
		return "UpdatePackage";
	}
	@RequestMapping(value="/editPackage", method=RequestMethod.POST)
	public ModelAndView editPackage(HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException {
		ModelAndView mav = new ModelAndView("Package");
		request.setCharacterEncoding("UTF-8");
		
		if(ServletFileUpload.isMultipartContent(request)) {
			
			try {
				List<FileItem> data = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
				String package_size = data.get(0).getString("UTF-8");
				String call_hr = data.get(1).getString("UTF-8");
				String chat_hr = data.get(2).getString("UTF-8");
				String price = data.get(3).getString("UTF-8");
				
				
				int Tcall = Integer.parseInt(call_hr);
				int Tchat = Integer.parseInt(chat_hr);
				double Tprice = Double.parseDouble(price);
				PackageManager pm = new PackageManager();
				
				
				packages pack = new packages(package_size,Tprice,Tcall,Tchat);
				String result = pm.insertPackage(pack);
				
				if(result.equals("successfully saved")) {
					List<packages> list_pack = pm.listAllpackages();
					
					session.setAttribute("list_pack", list_pack);
					mav.addObject("msg","Insert sucess!!");					
				}else {
					mav.addObject("msg","Insert fail!!");
				}
			} catch (Exception e) {}
		}
		return mav ;
	}

}
