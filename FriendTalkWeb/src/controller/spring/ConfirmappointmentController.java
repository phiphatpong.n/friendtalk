package controller.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.*;
import bean.*;

@Controller
public class ConfirmappointmentController {
	
	@RequestMapping(value = "/confrimApp", method = RequestMethod.GET)
	public String confrimAppointment(HttpServletRequest request, Model md, HttpSession session) {
		
		String id = request.getParameter("Mid");
		
		ConfirmappointmentManager cam = new ConfirmappointmentManager();
		MakeAppointment ma = new MakeAppointment();
		
		ma = cam.getMakeAppointment(id);
		
		String user = ma.getPsychologist().getUsername();
		System.out.println("User ="+ user);
		
		ma.setStatus_appoint("ยืนยันการนัด");
		
		String result = cam.UpdateMakeAppointment(ma);
		int alt = 0 ;
		if(result.equals("successfully saved")) {
			MakeAppointmentManager mam = new MakeAppointmentManager();
			ViewAppointmentManager vam = new ViewAppointmentManager();
			
			List<MakeAppointment> listMA = mam.listAllMakeAppointmentpsychologist(user);
			List<PsychologistExpertise> listPEK = mam.listPsychologistExpertise();
			String Mtype = vam.GettypeMember(user);
			alt = 1 ;
			
			List<Advice> listadvice = vam.ListAdvice();
			List<Payment> listpay = vam.ListPayment();
			
			System.out.println("List size = "+listMA.size());
			session.setAttribute("user", user);
			request.setAttribute("listMA", listMA);
			request.setAttribute("listPEK", listPEK);
			request.setAttribute("Mtype", Mtype);
			request.setAttribute("altC", alt);
			request.setAttribute("listadvice", listadvice);
			request.setAttribute("listpay", listpay);
			
			return "ViewAppointment";
		}else {
			request.setAttribute("alt", alt);
			return "ViewAppointment";
		}
	}
}
