package controller.spring;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import Manager.ExpertiseManager;
import Manager.UserManager;
import bean.Consultant;
import bean.Expertise;
import bean.Login;
import bean.Organization;
import bean.Psychologist;
import bean.PsychologistExpertise;
import bean.PsychologistExpertiseKey;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@Controller
public class RegisterController {
	private static String SALT = "123456";
	
	@RequestMapping(value="/registercon", method=RequestMethod.POST)
	public ModelAndView RegisterCon(HttpServletRequest request,HttpSession session) throws UnsupportedEncodingException {
		request.setCharacterEncoding("UTF-8");
		ModelAndView mav = new ModelAndView("home");
		if(ServletFileUpload.isMultipartContent(request)) {
			
			try {
				List<FileItem> data = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
				String title = data.get(0).getString("UTF-8");
				String fristname = data.get(1).getString("UTF-8");
				String lastname = data.get(2).getString("UTF-8");
				String gender = data.get(3).getString("UTF-8");
				String Brihtday =  data.get(4).getString("UTF-8");
				String email = data.get(5).getString("UTF-8");
				String phone =data.get(6).getString("UTF-8");
				String address  = data.get(7).getString("UTF-8");
				String congenital_disease  = data.get(8).getString("UTF-8");
				String drug_allergy  = data.get(9).getString("UTF-8");
				String food_allergy  = data.get(10).getString("UTF-8");
				String username = data.get(11).getString("UTF-8");
				String password = data.get(12).getString("UTF-8");
				String Mtype = data.get(13).getString("UTF-8");
				String filename = new File(data.get(14).getName()).getName();
				
				if(title.equals("Mr")) {
					title = "นาย";
				}else if(title.equals("Mrs")){
					title = "นาง";
				}else {
					title = "นางสาว";
				}
				if(gender.equals("male")) {
					gender = "ชาย"; 
				}else {
					gender = "หญิง"; 
				}
				
			    SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
			    String[] d = Brihtday.split("-");
			    int day = Integer.parseInt(d[0]);
			    int month = Integer.parseInt(d[1]);
			    int year = Integer.parseInt(d[2]);
			    System.out.println("day = "+day+"month = "+month+"year = "+year);
			    Calendar calendar = new GregorianCalendar(year,month,day);
			    System.out.println(sdf.format(calendar.getTime()));
				
				
				String path = request.getSession().getServletContext().getRealPath("/") + "WEB-INF\\images\\" ;
				//String path2 = request.getSession().getServletContext().getRealPath("/") + "WEB-INF\\images\\" ;
				System.out.println(path);
				String f = filename.substring(filename.indexOf("."));
				data.get(14).write(new File(path + File.separator + username + f ));
				String img_c = username + f ;
				System.out.println(img_c);
				
				Login login = new Login(username,password,Mtype);
				Consultant con = new Consultant(username,title,fristname,lastname,gender,calendar,phone,email,address,food_allergy,drug_allergy,congenital_disease,img_c);
				UserManager um = new UserManager();
				String result = um.insertConsultant(con);
				login = um.insertLogin(login);
				
				System.out.println(result);
				
				if(result.equals("Sign Up Successfully")) {
					session.setAttribute("Mtype", Mtype);
					session.setAttribute("uname", username);
					mav.addObject("msg","Insert sucess!!");	
					
				}else {
					mav.addObject("msg","Insert fail!!");
				}
			} catch (Exception e) {}
		}
		return mav ;
	}
	@RequestMapping(value="/registerpsy", method=RequestMethod.POST)
	public ModelAndView RegisterPsy(HttpServletRequest request,HttpSession session) throws UnsupportedEncodingException {
		ModelAndView mav = new ModelAndView("index");
		request.setCharacterEncoding("UTF-8");
		
		if(ServletFileUpload.isMultipartContent(request)) {
			
			try {
				List<FileItem> data = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
	
				String title = data.get(0).getString("UTF-8");
				String fristname = data.get(1).getString("UTF-8");
				String lastname = data.get(2).getString("UTF-8");
				String gender = data.get(3).getString("UTF-8");
				String Brihtday =  data.get(4).getString("UTF-8");
				String email = data.get(5).getString("UTF-8");
				String phone = data.get(6).getString("UTF-8");
				String address  = data.get(7).getString("UTF-8");
				String educationl_history = data.get(8).getString("UTF-8");
				String working_history = data.get(9).getString("UTF-8");
				String mentor_type = data.get(10).getString("UTF-8");
				String username = data.get(11).getString("UTF-8");
				String password = data.get(12).getString("UTF-8");
				String Mtype = data.get(13).getString("UTF-8");
				String certificate = new File(data.get(15).getName()).getName();
				String filename = new File(data.get(14).getName()).getName();
				
				if(title.equals("Mr")) {
					title = "นาย";
				}else if(title.equals("Mrs")){
					title = "นาง";
				}else {
					title = "นางสาว";
				}
				
				if(gender.equals("male")) {
					gender = "ชาย"; 
				}else {
					gender = "หญิง"; 
				}
				
//				String expertise = data.get(16).getString("UTF-8");
//				String expertise1 = data.get(17).getString("UTF-8");
//				String expertise2 = data.get(18).getString("UTF-8");
				
				SimpleDateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
				Date date = dfm.parse(Brihtday);
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				
				String path = request.getSession().getServletContext().getRealPath("/") + "WEB-INF\\images\\" ;
				System.out.println(path);
				String f = filename.substring(filename.indexOf("."));
				data.get(14).write(new File(path + File.separator + username + f ));
				String c = certificate.substring(certificate.indexOf("."));
				data.get(15).write(new File(path + File.separator + fristname + c ));
				
				String img_p = username + f ;
				String img_certificat = fristname + c ;
				System.out.println(img_p+"="+img_certificat);
				UserManager um = new UserManager();
				
				
				Psychologist psychologist = new Psychologist(username,title,fristname,lastname,gender,cal,phone,email,address,educationl_history,working_history,mentor_type,img_certificat,img_p,0.0,password);
				String result = um.insertPsychologist(psychologist);
				
				System.out.println(result);
				
				ExpertiseManager em = new ExpertiseManager();
				Expertise exp = new Expertise();

				for(int i=16 ; i<=20 ; i++) {
					String expertise = data.get(i).getString("UTF-8");
					System.out.println("ประเภท = "+expertise);
					if(!expertise.equals("")) {
						int maxid = em.getMaxExpertiseId();
						exp.setExpertise_id(maxid);
						exp.setExpertise_type(expertise);
						String Resultexp = em.insertExpertise(exp);
						System.out.println(i+"="+Resultexp);
						
						PsychologistExpertiseKey pek = new PsychologistExpertiseKey(psychologist,exp);
						PsychologistExpertise pe = new PsychologistExpertise(pek);
						String resultpe = em.insertPsychologistExpertise(pe);
						System.out.println("Save key:"+i+"="+resultpe);
					}
				}
				
				
				if(result.equals("Sign Up Successfully")) {
					session.setAttribute("Mtype", Mtype);
					session.setAttribute("uname", username);
					mav.addObject("msg","Insert sucess!!");
				}else {
					mav.addObject("msg","Insert fail!!");
				}
			} catch (Exception e) {}
		}
		return mav ;
	}
	@RequestMapping(value="/registerorg", method=RequestMethod.POST)
	public ModelAndView RegisterOrg(HttpServletRequest request ,HttpSession session) throws UnsupportedEncodingException {
		ModelAndView mav = new ModelAndView("home");
		request.setCharacterEncoding("UTF-8");
		if(ServletFileUpload.isMultipartContent(request)) {
			
			try {
				List<FileItem> data = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
	
				String cname = data.get(0).getString("UTF-8");
				String email = data.get(1).getString("UTF-8");
				String phone = data.get(2).getString("UTF-8");
				String address = data.get(3).getString("UTF-8");
				String employee_number = data.get(4).getString("UTF-8");
				
				String username = data.get(5).getString("UTF-8");
				String password = data.get(6).getString("UTF-8");
				String Mtype = data.get(7).getString("UTF-8");
				String crc = new File(data.get(8).getName()).getName();
				
				String path = request.getSession().getServletContext().getRealPath("/") + "WEB-INF\\images\\" ;
				System.out.println(path);
				String f = crc.substring(crc.indexOf("."));
				data.get(8).write(new File(path + File.separator + cname + f ));
				String img_crc = cname + f ;
				
				Login login = new Login(username,password,Mtype);
				
				Organization organization = new Organization(username,cname,phone,email,address,employee_number,img_crc,0,0,0,0);
				UserManager um = new UserManager();
				String result = um.insertOrganization(organization);
				login = um.insertLogin(login);
				
				System.out.println(result);
				
				if(result.equals("Sign Up Successfully")) {
					session.setAttribute("Mtype", Mtype);
					session.setAttribute("uname", username);
					mav.addObject("msg","Insert sucess!!");
				}else {
					mav.addObject("msg","Insert fail!!");
				}
			} catch (Exception e) {}
		}
		return mav ;
	}
}
