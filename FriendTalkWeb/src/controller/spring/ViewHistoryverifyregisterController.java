package controller.spring;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.ListRegisterPsyManager;
import Manager.ViewHistoryverifyregisterManager;
import bean.Login;
import bean.Psychologist;
import bean.PsychologistExpertise;
import bean.SetWorkingTime;

@Controller
public class ViewHistoryverifyregisterController {
	@RequestMapping(value = "/ViewHistoryverifyregister", method = RequestMethod.GET)
	public String ViewHistoryverifyregister(HttpServletRequest request, Model md, HttpSession session) {
		
		ViewHistoryverifyregisterManager vhvrm = new ViewHistoryverifyregisterManager();
		
		Psychologist psy = new Psychologist();
		List<Login> listlogin = vhvrm.listLogin();
		List<Psychologist> list = new ArrayList<>();
		List<Psychologist> listpsy = vhvrm.listPsychologist();
		try {
			for(int k=0 ; k<listlogin.size() ; k++) { 
				String users = listlogin.get(k).getUsername();
				psy = vhvrm.getProfilePsychologist(users);
				if(psy != null) {
					list.add(psy);
				}
				
				System.out.println("suer = "+ users); 
			}
	} catch (Exception e) {
		e.printStackTrace();
	}
		System.out.println(list.size());
		request.setAttribute("list", list);
		
		
		return "ViewHistoryverifyregister";
	}
}
