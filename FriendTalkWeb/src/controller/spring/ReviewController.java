package controller.spring;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.*;
import bean.*;

@Controller
public class ReviewController {
	
	@RequestMapping(value = "/AddReview", method = RequestMethod.POST)
	public String AddReview(HttpServletRequest request, Model md, HttpSession session) throws ParseException, UnsupportedEncodingException {
		request.setCharacterEncoding("UTF-8");
		
		String Mid = request.getParameter("mid");
		String comment = request.getParameter("comment");
		String score = request.getParameter("score");
		String review_date = request.getParameter("review_date");
		
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
		Date datee = format.parse(review_date);
		System.out.println("date = "+datee);
		
		double s = Double.parseDouble(score);
		
		ReviewManager rm = new ReviewManager();
		
		int maxid = rm.getmaxid();
		String Rid = "R"+String.valueOf(maxid);
		
		MakeAppointment ma = rm.getMakeAppointment(Mid);
		String user = ma.getConsultant().getUsername();
		System.out.println("user = "+user);
		
		
		Review review = new Review();
		review.setReview_id(Rid);
		review.setReview_date(datee);
		review.setComment(comment);
		review.setSatisfaction_score(s);
		
		String result = rm.insertReview(review);
		
		Psychologist psy = new Psychologist();
		Advice advice = new Advice();
		int alt = 0;
		String result2 = "" ;
		if(result.equals("successfully saved")) {	
			List<Advice> listadvice = rm.ListAdvice();
			for(int i=0 ; i<listadvice.size() ; i++) {
				if(listadvice.get(i).getMake_appointment().getAppointment_id().equals(Mid)) {
					String Aid = listadvice.get(i).getAdvice_id();
					System.out.println("Adice id = "+Aid);
					
					advice = rm.getAdvice(Aid);
					advice.setReview(review);
					result2 = rm.insertAdvice(advice);
					break;
				}
			}
		}
		if(result2.equals("successfully saved")) {
			MakeAppointmentManager mam = new MakeAppointmentManager();
			ViewAppointmentManager vam = new ViewAppointmentManager();
			
			List<MakeAppointment> listMA = new ArrayList<>();
			
			String Mtype = vam.GettypeMember(user);
			if(Mtype.equals("consultant")) {
				listMA = mam.listAllMakeAppointmentconsultant(user);
			}else {
				listMA = mam.listAllMakeAppointmentpsychologist(user);
			}
			List<PsychologistExpertise> listPEK = mam.listPsychologistExpertise();
			System.out.println("List size = "+listMA.size());
			
			alt = 1 ;
			
			List<Review> listreview = rm.ListReview();
			List<Advice> listadvice = vam.ListAdvice();
			session.setAttribute("user", user);
			request.setAttribute("altR", alt);
			request.setAttribute("listMA", listMA);
			request.setAttribute("listPEK", listPEK);
			request.setAttribute("Mtype", Mtype);
			request.setAttribute("listadvice", listadvice);
			
			return "ViewAppointment";
		}else {
			return "ViewAppointment";
		}
	}
}
