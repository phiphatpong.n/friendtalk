package controller.spring;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.*;
import bean.*;

@Controller
public class StressTestController {
	@RequestMapping(value = "/openStressQuestion", method = RequestMethod.GET)
	public String openStressQuestion(HttpServletRequest request, Model md, HttpSession session) {
		try {

			String user = request.getParameter("user");
			StressTestManager stm = new StressTestManager();
			
			List<StressQuestion> liststress = stm.getListStressQuestion();
			Consultant con = stm.getProfileConsultant(user);
			session.setAttribute("liststress", liststress);
			session.setAttribute("con", con);
			session.setAttribute("user", user);

		} catch (Exception e) {
			// TODO: handle exception
		}
		return "StressTest";
	}
	@RequestMapping(value = "/AddStressTestScore", method = RequestMethod.POST)
	public String AddStressTestScore(HttpServletRequest request, Model md, HttpSession session) {
		try {

			String user = request.getParameter("user");
			int sum = 0 ;
			for(int i=0 ; i<55 ; i++) {
				String ch1 = request.getParameter("ch"+i);
				int ch = Integer.parseInt(ch1);
				sum = sum+ch ;
			}
			StressTestManager stm = new StressTestManager();
			if(user != null) {
			Consultant con = new Consultant();
			
			int maxid = stm.getMaxStressTest();
			con = stm.getProfileConsultant(user);
			
			  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			  Date date = new Date();
			  String date1 = dateFormat.format(date);
			  Date day = dateFormat.parse(date1);
			  
			  System.out.println(dateFormat.format(date));  
			  System.out.println(day); 
			
			StressTest st =new StressTest(maxid,day,sum);
			st.setConsultant(con);
			String result = stm.insertStressTest(st);
			
			if(result.equals("successfully saved")) {
				List<ResultSuggestion> lsitRS = stm.getListResultSuggestion();
				
				session.setAttribute("sum",sum );
				session.setAttribute("con",con );
				session.setAttribute("lsitRS",lsitRS );
				
				return "ResultSuggestion";
			}
			}else {
				List<ResultSuggestion> lsitRS = stm.getListResultSuggestion();
				session.setAttribute("sum",sum );
				session.setAttribute("lsitRS",lsitRS);
				return "ResultSuggestion";
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "StressTest";
	}
	@RequestMapping(value = "/openHistoryStressTest", method = RequestMethod.GET)
	public String openHistoryStressTest(HttpServletRequest request, Model md, HttpSession session) {
		try {

			String user = request.getParameter("user");
			StressTestManager stm = new StressTestManager();
			
			List<StressTest> stest = stm.getListStressTest(user);
			List<ResultSuggestion> lsitRS = stm.getListResultSuggestion();
			
			request.setAttribute("lsitRS", lsitRS);
			request.setAttribute("stest", stest);
			
			System.out.println("list Stree = " + stest.size());
			System.out.println("lsitRS = " + lsitRS.size());
		} catch (Exception e) {
		e.printStackTrace();
		}
		return "HistoryStressTest";
	}
}
