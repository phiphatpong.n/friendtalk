package controller.spring;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.*;
import bean.*;

@Controller
public class CreatePaymantCodeController {

	@RequestMapping(value = "/openPaymantCode", method = RequestMethod.GET)
	public String openPaymantCode(HttpServletRequest request, Model md, HttpSession session) {
		try {

			String user = request.getParameter("user");
			PaymantCodeManager pcm = new PaymantCodeManager();
			Organization org = new Organization();
			
			org = pcm.getProfileOrganization(user);
			session.setAttribute("org", org);

		} catch (Exception e) { 
		}
		return "PaymantCode";
	}

	@RequestMapping(value = "/getListGiveAdvice", method = RequestMethod.GET)
	public String getListGiveAdvice(HttpServletRequest request, Model md, HttpSession session) {
		try {
			String user = request.getParameter("user");
			
			PaymantCodeManager pcm = new PaymantCodeManager();
			
			List<PaymentCode> listpay = pcm.getListPaymentCode(user);
			
			for(int i=0 ; i<listpay.size() ; i++) {
				if(listpay.get(i).getUsage_status().equals("ใช้งาน")) {
					Organization org = new Organization();
					org = pcm.getProfileOrganization(user); 
					
					int chat = Integer.parseInt(listpay.get(i).getChat_minute());
					int call = Integer.parseInt(listpay.get(i).getCall_minute());
					int totalchat = org.getTotal_chat();
					int totalcall = org.getTotal_call();
					int codecall =org.getMinute_code_call();
					int codechat =org.getMinute_code_chat();
					
					String c = listpay.get(i).getCode();
					PaymentCode pc = pcm.getPaymantcode(c);
					
					if(chat != 0) {
						totalchat = totalchat - chat ;
						codechat = codechat - chat ;
						
						org.setTotal_chat(totalchat);
						org.setMinute_code_chat(codechat);
						pc.setUsage_status("ชำระเงินเรียบร้อย");
						String result = pcm.insertOrganization(org);
						String result2 = pcm.insertPaymentCode(pc);
					}else if(call != 0) {
						totalcall = totalcall - call ;
						codecall = codecall - call ;
						
						org.setTotal_call(totalcall);
						org.setMinute_code_call(codecall);
						pc.setUsage_status("ชำระเงินเรียบร้อย");
						pc.setOrganization(org);
						
						String result = pcm.insertOrganization(org);
						String result2 = pcm.insertPaymentCode(pc); 
						
						System.out.println("result = "+result );
						System.out.println("result2 ="+result2 );
					}
				}
			}
			
			List<PaymentCode> listpay2 = pcm.getListPaymentCode(user);
			session.setAttribute("user", user);
			request.setAttribute("listpay", listpay2);

		} catch (Exception e) {
			// TODO: handle exception
		}
		return "ListGiveAdvice";
	}

	@RequestMapping(value = "/AddPaymentcode", method = RequestMethod.POST)
	public String AddPaymentcode(HttpServletRequest request, Model md, HttpSession session) {
		try {

			String user = request.getParameter("user");
			String typecontect = request.getParameter("typecontect");
			String minute = request.getParameter("minute");
			String issue_date = request.getParameter("issue_date");

			int minutes = Integer.parseInt(minute);
			
			int length = 5;
			boolean useLetters = true;
			boolean useNumbers = false;
			String code = RandomStringUtils.random(length, useLetters, useNumbers);
			
			System.out.println(code);
			
			PaymantCodeManager pcm = new PaymantCodeManager();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
			Date date = format.parse(issue_date);

			System.out.println(date);
			
			PaymentCode paymentCode = new PaymentCode();
			Organization organization = new Organization();
			organization = pcm.getProfileOrganization(user);
			List<PaymentCode> list = pcm.getListPaymentCode(user);
			
			if(list.size() != 0) {
				for(int i=0 ; i<list.size() ; i++) {
					if(list.get(i).getCode().equals(code)) {
						
						code = RandomStringUtils.random(length, useLetters, useNumbers);
						System.out.println(code);
						if(typecontect.toLowerCase().equals("chat".toLowerCase())) {
							paymentCode.setCode(code);
							paymentCode.setCall_minute("0");
							paymentCode.setChat_minute(minute);
							paymentCode.setIssue_date(date);
							paymentCode.setUsage_status("ยังไม่ได้ใช้งาน");
							paymentCode.setOrganization(organization);
						}else {
							paymentCode.setCode(code);
							paymentCode.setCall_minute(minute);
							paymentCode.setChat_minute("0");
							paymentCode.setIssue_date(date);
							paymentCode.setOrganization(organization);
							paymentCode.setUsage_status("ยังไม่ได้ใช้งาน");
						}
						
					}else {
						if(typecontect.toLowerCase().equals("chat".toLowerCase())) {
							paymentCode.setCode(code);
							paymentCode.setCall_minute("0");
							paymentCode.setChat_minute(minute);
							paymentCode.setIssue_date(date);
							paymentCode.setOrganization(organization);
							paymentCode.setUsage_status("ยังไม่ได้ใช้งาน");
						}else {
							paymentCode.setCode(code);
							paymentCode.setCall_minute(minute);
							paymentCode.setChat_minute("0");
							paymentCode.setIssue_date(date);
							paymentCode.setOrganization(organization);
							paymentCode.setUsage_status("ยังไม่ได้ใช้งาน");
						}
					}
				}
			}else {
				if(typecontect.toLowerCase().equals("chat".toLowerCase())) {
					paymentCode.setCode(code);
					paymentCode.setCall_minute("0");
					paymentCode.setChat_minute(minute);
					paymentCode.setIssue_date(date);
					paymentCode.setOrganization(organization);
					paymentCode.setUsage_status("ยังไม่ได้ใช้งาน");
				}else {
					paymentCode.setCode(code);
					paymentCode.setCall_minute(minute);
					paymentCode.setChat_minute("0");
					paymentCode.setIssue_date(date);
					paymentCode.setOrganization(organization);
					paymentCode.setUsage_status("ยังไม่ได้ใช้งาน");
				}
			}

			

			int sumcall = organization.getTotal_call();
			int sumchat = organization.getTotal_chat();
			int codeMinutes_call = organization.getMinute_code_call();
			int codeMinutes_chat = organization.getMinute_code_chat();	
			int alertPC = 0;
			
			if(typecontect.toLowerCase().equals("call".toLowerCase())) {
				if (sumcall > codeMinutes_call ) {
					int ans1 = codeMinutes_call + minutes;
					organization.setMinute_code_call(ans1);
					organization.setMinute_code_chat(codeMinutes_chat);

					String result = pcm.insertOrganization(organization);
					String result2 = pcm.insertPaymentCode(paymentCode);

					alertPC = 1;

					System.out.println("1=" + result);
					System.out.println("2=" + result2);
				} else {
					alertPC = 0;
					session.setAttribute("org", organization);
					session.setAttribute("alertPC", alertPC);
					return "PaymantCode";
				}
			}else {
				if (sumchat > codeMinutes_chat ) {
					int ans2 = codeMinutes_chat + minutes;

					organization.setMinute_code_call(codeMinutes_call);
					organization.setMinute_code_chat(ans2);

					String result = pcm.insertOrganization(organization);
					String result2 = pcm.insertPaymentCode(paymentCode);

					alertPC = 1;

					System.out.println("1=" + result);
					System.out.println("2=" + result2);
				} else {
					alertPC = 0;
					session.setAttribute("org", organization);
					session.setAttribute("alertPC", alertPC);
					return "PaymantCode";
				}
			}
			paymentCode = pcm.getPaymantcode(code);
			
			session.setAttribute("paymentCode", paymentCode);
			session.setAttribute("alertPC", alertPC);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "printcode";
	}
}
