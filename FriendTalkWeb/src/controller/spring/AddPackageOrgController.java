package controller.spring;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.*;
import Manager.MakeAppointmentManager;
import bean.*;

@Controller
public class AddPackageOrgController{
	@RequestMapping(value = "/openAppPackage", method = RequestMethod.GET)
	public String openAppPackage(HttpServletRequest request, Model md, HttpSession session) {
		
		AddPackageOrgManager apom = new AddPackageOrgManager();
		 
		List<Organization> list = apom.ListOrganization();
		System.out.println(list.size());
		request.setAttribute("list", list);
		 
		return "AddPackageOrg";
//		new AddPackageOrganizationManager()
	}
	@RequestMapping(value = "/UpdatetotalHR", method = RequestMethod.POST)
	public String UpdatetotalHR(HttpServletRequest request, Model md, HttpSession session) {
		
		AddPackageOrgManager apom = new AddPackageOrgManager();
		
		String companyname = request.getParameter("companyname");
		String totalcall = request.getParameter("totalcall");
		String totalchat = request.getParameter("totalchat");
		
		Organization org = new Organization();
		List<Organization> listO = apom.ListOrganization();
		
		int sumcall = 0 ;
		int sumchat = 0 ;
		String cname ="";
		String user ="";
		for(int i=0 ; i<listO.size() ; i++) {
			if(companyname.equals(String.valueOf(i))) {
				System.out.println("Codde = "+companyname);
				cname = listO.get(i).getCompanyname();
				user = listO.get(i).getUsername();
				System.out.println("username ="+user);
			}
		}
		org = apom.getOrganization(cname);

		int call = Integer.parseInt(totalcall);
		int chat = Integer.parseInt(totalchat);
		int c1 = org.getTotal_call();
		int c2 = org.getTotal_chat();
		sumcall = call + c1 ;
		sumchat = chat + c2;
		
		org.setTotal_call(sumcall);
		org.setTotal_chat(sumchat);
		
		
		Date d =new Date();
		System.out.println("date = "+d);
		int maxid = apom.maxidBuyPackageHistory();
		String Bid = "B" + String.valueOf(maxid) ;
		System.out.println("Bid = "+ Bid);
		BuyPackageHistory bphistory = new BuyPackageHistory(Bid,d,call,chat);
		bphistory.setOrganization(org);
		
		String result = apom.UpdateOrganization(org);
		String result2 = apom.insertBuyPackageHistory(bphistory);
		System.out.println("R1 = "+result + " \nR2 = "+result2);
		
		List<BuyPackageHistory> listb = apom.ListBuyPackageHistory();
		List<Organization> list = apom.ListOrganization();
		if(result.equals("successfully saved") && result2.equals("successfully saved")) {
			int alert = 1;
			
			request.setAttribute("listb", listb);
			request.setAttribute("alert", alert);
			return "ViewUpdatePackageOrganization"; 
		}else {
			int alert = 0;
			request.setAttribute("list", list);
			request.setAttribute("alert", alert);
			return "AddPackageOrg";
		}	
	}

}
