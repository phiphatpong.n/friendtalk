package controller.spring;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.*;
import bean.*;

@Controller
public class CancelappointmentController {
	@RequestMapping(value = "/Cancelapppsy", method = RequestMethod.GET)
	public String Cancelapp(HttpServletRequest request, Model md, HttpSession session) throws SQLException {
		
		CancelappointmentManager lrp = new CancelappointmentManager();
		MakeAppointmentManager mam = new MakeAppointmentManager();
		ViewAppointmentManager vam = new ViewAppointmentManager();
		
		String mid = request.getParameter("Mid");
		MakeAppointment maak = mam.getMakeAppointment(mid);
		String user = maak.getPsychologist().getUsername();
		
		maak.setStatus_appoint("ยกเลิกการนัด");
		String result = lrp.insertMakeAppointment(maak);
		
		int alt = 5 ;
		if(result.equals("successfully saved")) {
			alt = 1 ;
			request.setAttribute("altCan", alt);
		}else {
			alt = 0 ;
			request.setAttribute("altCan", alt);
		}
		

		
		List<MakeAppointment> listMA = mam.listAllMakeAppointmentpsychologist(user);
		List<PsychologistExpertise> listPEK = mam.listPsychologistExpertise();
		String Mtype = vam.GettypeMember(user);
		
		System.out.println("List size = "+listMA.size());
		session.setAttribute("user", user);
		request.setAttribute("listMA", listMA);
		request.setAttribute("listPEK", listPEK);
		request.setAttribute("Mtype", Mtype);
		
		return "ViewAppointment";
	}
	@RequestMapping(value = "/Cancelappcon", method = RequestMethod.GET)
	public String Cancelappcon(HttpServletRequest request, Model md, HttpSession session) throws SQLException {
		
		CancelappointmentManager lrp = new CancelappointmentManager();
		MakeAppointmentManager mam = new MakeAppointmentManager();
		ViewAppointmentManager vam = new ViewAppointmentManager();
		
		MakeAppointment ma = new MakeAppointment();
		
		String mid = request.getParameter("Mid");
		MakeAppointment maak = mam.getMakeAppointment(mid);
		String user = maak.getConsultant().getUsername();
		
		ma.setAppointment_id(mid);
		ma.setStatus_appoint("ยกเลิกการนัด");
		String result = lrp.insertMakeAppointment(ma);
		
		int alt = 5 ;
		if(result.equals("successfully saved")) {
			alt = 1 ;
			request.setAttribute("altCan", alt);
		}else {
			alt = 0 ;
			request.setAttribute("altCan", alt);
		}
		

		
		List<MakeAppointment> listMA = mam.listAllMakeAppointmentconsultant(user);
		List<PsychologistExpertise> listPEK = mam.listPsychologistExpertise();
		String Mtype = vam.GettypeMember(user);
		
		System.out.println("List size = "+listMA.size());
		session.setAttribute("user", user);
		request.setAttribute("listMA", listMA);
		request.setAttribute("listPEK", listPEK);
		request.setAttribute("Mtype", Mtype);
		
		return "ViewAppointment";
	}
	
}
