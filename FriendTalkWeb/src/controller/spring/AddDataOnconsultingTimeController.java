package controller.spring;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.*;
import bean.*;

@Controller
public class AddDataOnconsultingTimeController {
	
	@RequestMapping(value = "/openGiveAdvice", method = RequestMethod.GET)
	public String openGiveAdvice(HttpServletRequest request, Model md, HttpSession session) {
		
		String Mid = request.getParameter("Mid");
		
		AddDataOnconsultingTimeManager adoctm = new AddDataOnconsultingTimeManager();
		MakeAppointment ma = adoctm.getMakeAppointment(Mid);
		
		request.setAttribute("Makeapp", ma);
		
		
		return "AddDataOnConsultingTime";
	}
	
	@RequestMapping(value="/addGiveAdvice", method=RequestMethod.POST)
	public String addGiveAdvice(HttpServletRequest request ,HttpSession session) throws UnsupportedEncodingException {
		request.setCharacterEncoding("UTF-8");
		
			try {
				String StartTime = request.getParameter("StartTime");
				String EndTime = request.getParameter("EndTime");
				String mid = request.getParameter("mid");
				
				
				AddDataOnconsultingTimeManager adoctm = new AddDataOnconsultingTimeManager();
				
				int maxid = adoctm.getmaxidAdvice();
				String Aid = "A" + String.valueOf(maxid) ;
				
				MakeAppointment Makeapp = adoctm.getMakeAppointment(mid);
				String user = Makeapp.getPsychologist().getUsername();
				System.out.println("user = "+user);
				
				Advice advice = new Advice(Aid,StartTime,EndTime);
				advice.setMake_appointment(Makeapp);
				
				MakeAppointment ma = adoctm.getMakeAppointment(mid);
				ma.setStatus_appoint("ค้างชำระ");
				
				String result = adoctm.insertGiveAdvice(advice);
				String result2 = adoctm.UpdateMakeAppointment(ma);
				
				if(result.equals("successfully saved") || result2.equals("successfully saved")) {
					
					MakeAppointmentManager mam = new MakeAppointmentManager();
					ViewAppointmentManager vam = new ViewAppointmentManager();
					
					List<MakeAppointment> listMA = new ArrayList<>();
					
					String Mtype = vam.GettypeMember(user);
					if(Mtype.equals("consultant")) {
						listMA = mam.listAllMakeAppointmentconsultant(user);
					}else {
						listMA = mam.listAllMakeAppointmentpsychologist(user);
					}
					List<PsychologistExpertise> listPEK = mam.listPsychologistExpertise();
					System.out.println("List size = "+listMA.size());
					session.setAttribute("user", user);
					request.setAttribute("listMA", listMA);
					request.setAttribute("listPEK", listPEK);
					request.setAttribute("Mtype", Mtype);
					
					  return "ViewAppointment" ;
				}
			} catch (Exception e) {e.printStackTrace();}
			
		return null;
		
	}
}
