package controller.spring;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.*;
import bean.*;

@Controller
public class ViewUpdatePackageOrgController {
	@RequestMapping(value = "/openViewUpdatePackageOrganization", method = RequestMethod.GET)
	public String openViewUpdatePackageOrganization(HttpServletRequest request, Model md, HttpSession session) {
		
		ViewUpdatePackageOrgManager vupo = new ViewUpdatePackageOrgManager();
		List<BuyPackageHistory> listb = vupo.ListBuyPackageHistory();
		List<Organization> listorg = vupo.ListOrganization();
		request.setAttribute("listb", listb);
		request.setAttribute("listorg", listorg);
		return "ViewUpdatePackageOrganization";
	}
}
