package controller.spring;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import Manager.*;
import bean.*;


@Controller
public class MakeAppointmentCobtroller {
	@RequestMapping(value = "/openListPsychologistent", method = RequestMethod.GET)
	public String openListPsychologistent(HttpServletRequest request, Model md, HttpSession session) {
		
		String usercon = request.getParameter("user");
		
		MakeAppointmentManager mam = new MakeAppointmentManager();
		ListRegisterPsyManager lrp  = new ListRegisterPsyManager();
		
		List<PsychologistExpertise> listPEK = mam.listPsychologistExpertise();
		List<Login> listlogin = lrp.listLogin();
		List<SetWorkingTime>  day = mam.listAllSetWorkingTime();
		Psychologist psy = new Psychologist();
		List<Psychologist> list = new ArrayList<>();
		try {
				for(int k=0 ; k<listlogin.size() ; k++) { 
					String users = listlogin.get(k).getUsername();
					psy = lrp.getProfilePsychologist(users);
					list.add(psy);
					System.out.println("suer = "+ users); 
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<SetWorkingTime> listswt = new ArrayList<>();

				listswt.add(day.get(0));
				listswt.add(day.get(4));
				listswt.add(day.get(2));
				listswt.add(day.get(1));
				listswt.add(day.get(3));
				listswt.add(day.get(6));
				listswt.add(day.get(5));

		List<Review> listreReviews = mam.listReview();
		List<Advice> liAdvices = mam.listAdvice();
		 
		System.out.println("listswt ="+listswt.size());
		System.out.println("list ="+list.size());
		System.out.println("listPEK ="+listPEK.size());
		
		request.setAttribute("listreReviews", listreReviews);
		request.setAttribute("liAdvices", liAdvices);
		
		request.setAttribute("listswt", listswt);
		request.setAttribute("list", list);
		request.setAttribute("listPEK", listPEK);
		session.setAttribute("user", usercon);
		
		return "ListPsychologist";
	}
	@RequestMapping(value = "/openMakeAppointment", method = RequestMethod.POST)
	public String openMakeAppointment(HttpServletRequest request, Model md, HttpSession session) {
		
		String user = request.getParameter("user");
		String userpsy = request.getParameter("userpsy");
		
		MakeAppointmentManager mam = new MakeAppointmentManager();
		Psychologist psy = new Psychologist();
		List<SetWorkingTime>  listswt = mam.listAllSetWorkingTime();
		
		psy = mam.getProfilePsychologist(userpsy);
		int maxid = mam.getMaxMakeAppointmentId();
		String Mid = "M" + String.valueOf(maxid) ;
		List<Price> list_Price = mam.listPrice();
		
		
		
		
		request.setAttribute("maxid", Mid);
		request.setAttribute("psy", psy);
		session.setAttribute("user", user);
		request.setAttribute("list_Price", list_Price);
		request.setAttribute("listswt", listswt);
		System.out.println("list_Price"+list_Price.size());
		
		return "MakeAppointment";
	}
	@RequestMapping(value = "/openTime", method = RequestMethod.POST)
	public String openTime(HttpServletRequest request, Model md, HttpSession session) throws UnsupportedEncodingException {
		request.setCharacterEncoding("UTF-8");
		
		String selectday = request.getParameter("selectday");
		String Mid = request.getParameter("appid");
		String user = request.getParameter("conid");
		String userpsy = request.getParameter("psyid");
		String contact = request.getParameter("butt");
		String AppDate = request.getParameter("appointment_date");
		
		String minute = contact.substring(5, 7);
		System.out.println("===>"+minute);
		MakeAppointmentManager mam = new MakeAppointmentManager();
		Psychologist psy = new Psychologist();
		
		List<Price> list_Price = mam.listPrice();
		List<SetWorkingTime> Twork = mam.getSetWorkingTime(userpsy);
		psy = mam.getProfilePsychologist(userpsy);
		List<SetWorkingTime>  day1 = mam.listAllSetWorkingTime();
		
		String day = "";
		if(selectday.equals("วันจันทร์") || selectday.equals("Monday")) {
			day ="วันจันทร์";
		}else if(selectday.equals("วันอังคาร") || selectday.equals("Thursday")) {
			day ="วันอังคาร";
		}else if(selectday.equals("วันพุธ") || selectday.equals("Wednesday")) {
			day ="วันพุธ";
		}else if(selectday.equals("วันพฤหัสบดี") || selectday.equals("Friday")) {
			day ="วันอังคาร";
		}else if(selectday.equals("วันศุกร์") || selectday.equals("Tuesday")) {
			day ="วันศุกร์";
		}else if(selectday.equals("วันเสาร์") || selectday.equals("Sunday")) {
			day ="วันเสาร์";
		}else if(selectday.equals("วันอาทิตย์") || selectday.equals("Saturday")) {
			day ="วันอาทิตย์";
		}
		System.out.println("วัน =>"+day);
		System.out.println("จำนวนนาที =>"+minute);
		
		List<SetWorkingTime> listswt = new ArrayList<>();

		listswt.add(day1.get(0));
		listswt.add(day1.get(4));
		listswt.add(day1.get(2));
		listswt.add(day1.get(1));
		listswt.add(day1.get(3));
		listswt.add(day1.get(6));
		listswt.add(day1.get(5));
		
		List<MakeAppointment> listtimeapp = mam.listMakeAppointmentPsychologist(userpsy);
		request.setAttribute("listtimeapp", listtimeapp);
		session.setAttribute("user", user);
		request.setAttribute("psy", psy);
		request.setAttribute("Twork", Twork);
		request.setAttribute("maxid", Mid);
		request.setAttribute("day", day);
		request.setAttribute("minute", minute);
		request.setAttribute("AppDate", AppDate);
		request.setAttribute("contact", contact);
		request.setAttribute("list_Price", list_Price);
		request.setAttribute("listswt", listswt);
		
		return "MakeAppointment";
	}
	
	@RequestMapping(value="/MakeAppointment", method=RequestMethod.POST)
	public ModelAndView MakeAppointment(HttpServletRequest request , HttpSession session) throws UnsupportedEncodingException {
		ModelAndView mav = new ModelAndView("ViewAppointment");
		request.setCharacterEncoding("UTF-8");
			
			try {
				MakeAppointmentManager mam = new MakeAppointmentManager();
				
				String appid = request.getParameter("appid");
				String psyid = request.getParameter("psyid");
				String conid = request.getParameter("conid");
				String anonymous = request.getParameter("anonymous");
				String notify = request.getParameter("notify");
				String timeS = request.getParameter("timeS");
				String appdate = request.getParameter("appdate");
				String contact = request.getParameter("contact");
				String cancel_appointment = request.getParameter("cancel_appointment");
				String status_appoint = request.getParameter("status_appoint");
				
				boolean anon = false ;
				String noti= "" ;
				boolean cancel = false ;
				
				if(cancel_appointment.equals("1")) {
					cancel = true ;
				}else {
					cancel = false ;
				}
				if(anonymous != null) {
					anon = true ;
				}else {
					anon = false ;
				}
				if(notify != null) {
					noti = "รับการแจ้งเตือน" ;
				}else {
					noti = "ไม่รับรับการแจ้งเตือน"  ;
				}
				Psychologist psy = new Psychologist();
				Consultant con = new Consultant();
				Price price = new Price();
				
				psy = mam.getProfilePsychologist(psyid);
				con = mam.getProfileConsultant(conid);
				price = mam.getPrice(contact);
				
				String[] tEnd = timeS.split(":") ;
				int HourS = Integer.parseInt(tEnd[0]);
				int MinS = Integer.parseInt(tEnd[1]);
				
				int HourE = 0 ;
				int MinE = 0 ;
				
				System.out.println(tEnd[0]+" Min= "+tEnd[1]);
				
				String t = price.getContact_type();
				String subT = t.substring(4);
				System.out.println(" call time = "+subT);
				
				if(subT.equals("30")) {
					int sum = MinS + 30 ;
					if(sum / 60 == 1) {
						HourE = HourS + 1;
						MinE = 0 ;
					}else {
						HourE = HourS ;
						MinE = 30 ;
					}
				}else {
					int sum = MinS + 60 ;
					if(sum / 60 == 1.5) {
						HourE = HourS + 1;
						MinE = 30 ;
					}else if(sum / 60 == 1) {
						HourE = HourS + 1 ;
						MinE = 0 ;
					}
				}
				System.out.println("Hour = "+HourE+ " Min = "+MinE);
				String timeEnd = String.valueOf(HourE) +":"+ String.valueOf(MinE) ;
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
			    String[] d = appdate.split("-");
			    int day = Integer.parseInt(d[0]);
			    int month = Integer.parseInt(d[1])-1;
			    int year = Integer.parseInt(d[2]);
			    
			    System.out.println("day = "+day+"month = "+month+"year = "+year);
			    Calendar calendarE = Calendar.getInstance();
			    calendarE.set(year, month, day, HourE, MinE);
			    Calendar calendarS = Calendar.getInstance();
			    calendarS.set(year, month, day, HourS, MinS);
			    
			    System.out.println(sdf.format(calendarS.getTime()));
			    System.out.println(sdf.format(calendarE.getTime()));
			    
			    
			    Calendar calAppdate = Calendar.getInstance();
				MakeAppointment ma = new MakeAppointment(appid,status_appoint,calendarS,calendarE,anon,cancel,noti,calAppdate);
				ma.setPrice(price);
				ma.setConsultant(con);
				ma.setPsychologist(psy);
				
				String result = mam.insertMakeAppointment(ma);
				System.out.println("result = " + result);
				if(result.equals("successfully saved")) {
					List<MakeAppointment> listMA = mam.listAllMakeAppointmentconsultant(conid);
					List<PsychologistExpertise> listPEK = mam.listPsychologistExpertise();
					ViewAppointmentManager vam = new ViewAppointmentManager();
					List<Advice> listadvice = vam.ListAdvice();
					List<Payment> listpay = vam.ListPayment();
					
					request.setAttribute("listpay", listpay);
					request.setAttribute("listadvice", listadvice);
					request.setAttribute("listMA", listMA);
					request.setAttribute("listPEK", listPEK);
					session.setAttribute("conid", con);
					request.setAttribute("Mtype", "consultant");
					
					mav.addObject("msg","Insert sucess!!");					
				}else {
					mav.addObject("msg","Insert fail!!");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		return mav ;
	}
}
