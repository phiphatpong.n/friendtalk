package controller.spring;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import Manager.SetWorkingTimeManager;
import Manager.UserManager;
import bean.Consultant;
import bean.Login;
import bean.Psychologist;
import bean.PsychologistWorkingKey;
import bean.SetWorkingTime;

@Controller
public class SetWorkingTimeController {

	@RequestMapping(value = "/openTimeWorking", method = RequestMethod.GET)
	public String openUpdateTimeWorking(HttpServletRequest request, HttpSession session) {

		String user = request.getParameter("user");
		SetWorkingTimeManager swtm = new SetWorkingTimeManager();
		Psychologist Pid = new Psychologist();
		Pid = swtm.getProfilePsychologist(user);
		
		List<SetWorkingTime> listT = swtm.getWorkingtime(user);
		
		String ds1 = "" ;
		String ds2 = "" ;
		String ds3 = "" ;
		String ds4 = "" ; 
		String ds5 = "" ;
		String ds6 = "" ;
		String ds7 = "" ;
		
		String de1 = "" ;
		String de2 = "" ;
		String de3 = "" ;
		String de4 = "" ;
		String de5 = "" ;
		String de6 = "" ;
		String de7 = "" ;
		

				ds1 = listT.get(0).getTime_start();
				de1 =  listT.get(0).getTime_end();

				ds2 =  listT.get(4).getTime_start();
				de2 =  listT.get(4).getTime_end();

				ds3 =  listT.get(2).getTime_start();
				de3 =  listT.get(2).getTime_end();

				ds4 =  listT.get(1).getTime_start();
				de4 =  listT.get(1).getTime_end();

				ds5 =  listT.get(3).getTime_start();
				de5 =  listT.get(3).getTime_end();

				ds6 =  listT.get(6).getTime_start();
				de6 =  listT.get(6).getTime_end();

				ds7 =  listT.get(5).getTime_start();
				de7 =  listT.get(5).getTime_end();

		session.setAttribute("ds1", ds1);
		session.setAttribute("ds2", ds2);
		session.setAttribute("ds3", ds3);
		session.setAttribute("ds4", ds4);
		session.setAttribute("ds5", ds5);
		session.setAttribute("ds6", ds6);
		session.setAttribute("ds7", ds7);
		
		session.setAttribute("de1", de1);
		session.setAttribute("de2", de2);
		session.setAttribute("de3", de3);
		session.setAttribute("de4", de4);
		session.setAttribute("de5", de5);
		session.setAttribute("de6", de6);
		session.setAttribute("de7", de7);
		
		session.setAttribute("Pid", Pid);

		return "SetWorkingTime";
	}

	@RequestMapping(value = "/SetWorkingTime", method = RequestMethod.POST)
	public String SetWorkingTime(HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException {
		request.setCharacterEncoding("UTF-8");

			try {
				String day1 = request.getParameter("day1");
				String day2 = request.getParameter("day2");
				String day3 = request.getParameter("day3");
				String day4 = request.getParameter("day4");
				String day5 = request.getParameter("day5");
				String day6 = request.getParameter("day6");
				String day7 = request.getParameter("day7");

				String day1start = request.getParameter("day1start");
				String day2start = request.getParameter("day2start");
				String day3start = request.getParameter("day3start");
				String day4start = request.getParameter("day4start");
				String day5start = request.getParameter("day5start");
				String day6start = request.getParameter("day6start");
				String day7start = request.getParameter("day7start");

				String day1end = request.getParameter("day1end");
				String day2end = request.getParameter("day2end");
				String day3end = request.getParameter("day3end");
				String day4end = request.getParameter("day4end");
				String day5end = request.getParameter("day5end");
				String day6end = request.getParameter("day6end");
				String day7end = request.getParameter("day7end");

				String allday = request.getParameter("allday");
				String user = request.getParameter("user");
				
				SetWorkingTimeManager swtm = new SetWorkingTimeManager();		
				Psychologist psy = new Psychologist();
				
				psy =(Psychologist) session.getAttribute("Pid");
				
				String time = "09.00";
				String time2 = "18.00";
				
				String result1 = "";
				String result2 = "";
				String result3 = "";
				String result4 = "";
				String result5 = "";
				String result6 = "";
				String result7 = "";
				
				if (allday == null) {
						
						PsychologistWorkingKey pwk7 = new PsychologistWorkingKey(psy,day1);
						PsychologistWorkingKey pwk1 = new PsychologistWorkingKey(psy,day2);
						PsychologistWorkingKey pwk2 = new PsychologistWorkingKey(psy,day3);
						PsychologistWorkingKey pwk3 = new PsychologistWorkingKey(psy,day4);
						PsychologistWorkingKey pwk4 = new PsychologistWorkingKey(psy,day5);
						PsychologistWorkingKey pwk5 = new PsychologistWorkingKey(psy,day6);
						PsychologistWorkingKey pwk6 = new PsychologistWorkingKey(psy,day7);
						
						SetWorkingTime setwork1 = new SetWorkingTime(pwk1,day1start,day1end);
						SetWorkingTime setwork2 = new SetWorkingTime(pwk2,day2start,day2end);
						SetWorkingTime setwork3 = new SetWorkingTime(pwk3,day3start,day3end);
						SetWorkingTime setwork4 = new SetWorkingTime(pwk4,day4start,day4end);
						SetWorkingTime setwork5 = new SetWorkingTime(pwk5,day5start,day5end);
						SetWorkingTime setwork6 = new SetWorkingTime(pwk6,day6start,day6end);
						SetWorkingTime setwork7 = new SetWorkingTime(pwk7,day7start,day7end);
						
							if(!day1start.equals("") ||  !day1end.equals("")) {
								result1 = swtm.insertSetWokingTime(setwork1);
							}
							if(!day2start.equals("") ||  !day2end.equals("")) {
								result2 = swtm.insertSetWokingTime(setwork2);
							}
							if(!day3start.equals("") ||  !day3end.equals("")) {
								result3 = swtm.insertSetWokingTime(setwork3);
							}
							if(!day4start.equals("") ||  !day4end.equals("")) {
								result4 = swtm.insertSetWokingTime(setwork4);
							}
							if(!day5start.equals("") ||  !day5end.equals("")) {
								result5 = swtm.insertSetWokingTime(setwork5);
							}
							if(!day6start.equals("") ||  !day6end.equals("")) {
								result6 = swtm.insertSetWokingTime(setwork6);
							}
							if(!day7start.equals("") ||  !day7end.equals("")) {
								result7 = swtm.insertSetWokingTime(setwork7);
							} 
					
				} else {
					PsychologistWorkingKey pwk1 = new PsychologistWorkingKey(psy,day1);
					PsychologistWorkingKey pwk2 = new PsychologistWorkingKey(psy,day2);
					PsychologistWorkingKey pwk3 = new PsychologistWorkingKey(psy,day3);
					PsychologistWorkingKey pwk4 = new PsychologistWorkingKey(psy,day4);
					PsychologistWorkingKey pwk5 = new PsychologistWorkingKey(psy,day5);
					PsychologistWorkingKey pwk6 = new PsychologistWorkingKey(psy,day6);
					PsychologistWorkingKey pwk7 = new PsychologistWorkingKey(psy,day7);
					
					SetWorkingTime setwork1 = new SetWorkingTime(pwk1,time,time2);
					SetWorkingTime setwork2 = new SetWorkingTime(pwk2,time,time2);
					SetWorkingTime setwork3 = new SetWorkingTime(pwk3,time,time2);
					SetWorkingTime setwork4 = new SetWorkingTime(pwk4,time,time2);
					SetWorkingTime setwork5 = new SetWorkingTime(pwk5,time,time2);
					SetWorkingTime setwork6 = new SetWorkingTime(pwk6,time,time2);
					SetWorkingTime setwork7 = new SetWorkingTime(pwk7,time,time2);
					
					 result1 = swtm.insertSetWokingTime(setwork1);
					 result2 = swtm.insertSetWokingTime(setwork2);
					 result3 = swtm.insertSetWokingTime(setwork3);
					 result4 = swtm.insertSetWokingTime(setwork4);
					 result5 = swtm.insertSetWokingTime(setwork5);
					 result6 = swtm.insertSetWokingTime(setwork6);
					 result7 = swtm.insertSetWokingTime(setwork7);
				}
				
				int alertsettime = 0 ; 
				if(result1.equals("save Successfully") || result2.equals("save Successfully") || result3.equals("save Successfully") || result4.equals("save Successfully") || result5.equals("save Successfully") || result6.equals("save Successfully") || result7.equals("save Successfully")) {
					alertsettime = 1 ;
					
					Psychologist Pid = new Psychologist();
					Pid = swtm.getProfilePsychologist(user);
					List<SetWorkingTime> listT = swtm.getWorkingtime(user);
					
					String ds1 = "" ;
					String ds2 = "" ;
					String ds3 = "" ;
					String ds4 = "" ;
					String ds5 = "" ;
					String ds6 = "" ;
					String ds7 = "" ;
					
					String de1 = "" ;
					String de2 = "" ;
					String de3 = "" ;
					String de4 = "" ;
					String de5 = "" ;
					String de6 = "" ;
					String de7 = "" ;
					
					for(int d=0 ; d < listT.size() ; d++) {
						if(listT.get(d).getPwkey().getWorking_date().equals("วันจันทร์")) {
							ds1 = listT.get(d).getTime_start();
							de1 =  listT.get(d).getTime_end();
						}else if(listT.get(d).getPwkey().getWorking_date().equals("วันอังคาร")) {
							ds2 =  listT.get(d).getTime_start();
							de2 =  listT.get(d).getTime_end();
						}else if(listT.get(d).getPwkey().getWorking_date().equals("วันพุธ")) {
							ds3 =  listT.get(d).getTime_start();
							de3 =  listT.get(d).getTime_end();
						}else if(listT.get(d).getPwkey().getWorking_date().equals("วันพฤหัสบดี")) {
							ds4 =  listT.get(d).getTime_start();
							de4 =  listT.get(d).getTime_end();
						}else if(listT.get(d).getPwkey().getWorking_date().equals("วันศุกร์")) {
							ds5 =  listT.get(d).getTime_start();
							de5 =  listT.get(d).getTime_end();
						}else if(listT.get(d).getPwkey().getWorking_date().equals("วันเสาร์")) {
							ds6 =  listT.get(d).getTime_start();
							de6 =  listT.get(d).getTime_end();
						}else if(listT.get(d).getPwkey().getWorking_date().equals("วันอาทิตย์")) {
							ds7 =  listT.get(d).getTime_start();
							de7 =  listT.get(d).getTime_end();
						}
					}
					session.setAttribute("ds1", ds1);
					session.setAttribute("ds2", ds2);
					session.setAttribute("ds3", ds3);
					session.setAttribute("ds4", ds4);
					session.setAttribute("ds5", ds5);
					session.setAttribute("ds6", ds6);
					session.setAttribute("ds7", ds7);
					
					session.setAttribute("de1", de1);
					session.setAttribute("de2", de2);
					session.setAttribute("de3", de3);
					session.setAttribute("de4", de4);
					session.setAttribute("de5", de5);
					session.setAttribute("de6", de6);
					session.setAttribute("de7", de7);
					
					session.setAttribute("Pid", Pid);
				}else {
					alertsettime = 0 ;
				}
				session.setAttribute("alertsettime", alertsettime);
				

			} catch (Exception e) {
				e.printStackTrace();
			}
		return "SetWorkingTime";
	}
}
