package controller.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.PackageManager;
import Manager.ViewMentoringHistoryManager;
import bean.*;

@Controller
public class ViewMentoringHistoryController {
	@RequestMapping(value = "/openViewMentoringHistory", method = RequestMethod.GET)
	public String openViewMentoringHistory(HttpServletRequest request, Model md, HttpSession session) {
		try {

			String user = request.getParameter("user");
			ViewMentoringHistoryManager vmhm = new ViewMentoringHistoryManager();
			
			List<MakeAppointment> list_histroy = vmhm.lsitHistory(user);
			
			System.out.println("list_histroy= "+list_histroy.size());
			request.setAttribute("list_histroy", list_histroy);
			session.setAttribute("user", user);

		} catch (Exception e) {
			// TODO: handle exception
		}
		return "ViewMentoringHistory";
	}
}
