package controller.spring;

import java.sql.SQLException;
import java.util.*;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.*;
import bean.*;

@Controller
public class ListRegisterPsyController {
	
    private static String USER_NAME = "idearyoujei@gmail.com";  // GMail user name (just the part before "@gmail.com")
    private static String PASSWORD = "29072560"; // GMail password
    private static String RECIPIENT = "aooza12370@gmail.com";
    
	@RequestMapping(value = "/openListregisterPsy", method = RequestMethod.GET)
	public String openListregisterPsy(HttpServletRequest request, Model md, HttpSession session) {
		
		ListRegisterPsyManager lrp = new ListRegisterPsyManager();
		
		Psychologist psy = new Psychologist();
		List<Psychologist> chackListpsy = new ArrayList<>();
		 
		List<Psychologist> listpsy  = lrp.listPsychologist();
		List<Login> listlogin = lrp.listLogin();
 
		try {
			boolean ck = false ;
			for(int i=0 ; i<listpsy.size() ; i++) {
				for(int k=0 ; k<listlogin.size() ; k++) { 
					System.out.println("suer = "+ listpsy.get(i).getUsername() +"=="+listlogin.get(k).getUsername()); 
					if(!listpsy.get(i).getUsername().equals(listlogin.get(k).getUsername())) {
						ck = true ;
					}else {
						ck = false ;
						System.out.println("ck = "+ ck); 
						break ;
					}
				} 
				if(ck) {
					String users = listpsy.get(i).getUsername();
					psy = lrp.getProfilePsychologist(users);
					chackListpsy.add(psy);
					System.out.println("suer = "+ users); 
				}
				ck = false ;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(chackListpsy.size());
		request.setAttribute("chackListpsy", chackListpsy);
		
		
		return "ListRegisterPsychologist";
	}
	@RequestMapping(value = "/verifyregisterPsy", method = RequestMethod.POST)
	public String openAddArticle(HttpServletRequest request, Model md, HttpSession session) {
		
		String username = request.getParameter("username");
		ListRegisterPsyManager lrp = new ListRegisterPsyManager();
		
		Psychologist psychologist = new Psychologist();
		psychologist = lrp.getProfilePsychologist(username);
		
		String user = psychologist.getUsername();
		String pass = psychologist.getPasswords(); 
		
		Login login = new Login(user,pass,"psychologist");
		String result = lrp.insertLogin(login);
		
		int alertVertify = 0;
		if(result.equals("successfully saved")) {
			alertVertify = 1 ;
			
			String from1 = USER_NAME;
	        String pass1 = PASSWORD;
	        String[] to1 = { RECIPIENT }; // list of recipient email addresses
	        String subject1 = "ผลการสมัครในการเป็นผู้ให้คำปรึกษาเว็บ  FriendTalk " ;
	        String body = "จากการตรวจสอบข้อมูลจากแอดมิน คุณได้รับเลือกในการเป็นผู้ให้คำปรึกษาเว็บ  FriendTalk " ;

	        try {
				sendFromGMail(from1, pass1, to1, subject1, body);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			alertVertify = 0 ;
		}
		
		Psychologist psy = new Psychologist();
		List<Psychologist> chackListpsy = new ArrayList<>();
		
		List<Psychologist> listpsy = lrp.listPsychologist();
		List<Login> listlogin = lrp.listLogin();
		try {
			boolean ck = false ;
			for(int i=0 ; i<listpsy.size() ; i++) {
				for(int k=0 ; k<listlogin.size() ; k++) { 
					System.out.println("suer = "+ listpsy.get(i).getUsername() +"=="+listlogin.get(k).getUsername()); 
					if(!listpsy.get(i).getUsername().equals(listlogin.get(k).getUsername())) {
						ck = true ;
					}else {
						ck = false ;
						System.out.println("ck = "+ ck); 
						break ;
					}
				}
				if(ck) {
					String users = listpsy.get(i).getUsername();
					psy = lrp.getProfilePsychologist(users);
					chackListpsy.add(psy);
					System.out.println("suer = "+ users); 
				}
				ck = false ;
			}
	} catch (Exception e) {
		e.printStackTrace();
	}

		request.setAttribute("chackListpsy", chackListpsy);
		session.setAttribute("alertVertify", alertVertify);
			
		return "ListRegisterPsychologist";
	}
	@RequestMapping(value = "/cancelconfirm", method = RequestMethod.GET)
	public String DeleteregisterPsy(HttpServletRequest request, Model md, HttpSession session) throws SQLException {
		
		ListRegisterPsyManager lrp = new ListRegisterPsyManager();
		PsychologistManager pm = new PsychologistManager();
		Psychologist psychologist = new Psychologist();
		
		String user = request.getParameter("user");

		psychologist.setUsername(user);
		
		Psychologist psy1 = pm.getPsychologist(user);
		String email = psy1.getEmail();
		
		String from1 = USER_NAME;
        String pass1 = PASSWORD;
        String[] to1 = { email }; // list of recipient email addresses
        String subject1 = "ผลการสมัครในการเป็นผู้ให้คำปรึกษาเว็บ  FriendTalk " ;
        String body = "จากการตรวจสอบข้อมูลจากแอดมิน คุณส่งเอกสารในการสมัครไม่ครบ ฯลฯ" ;

        try {
			sendFromGMail(from1, pass1, to1, subject1, body);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int result =lrp.deleteRegisterPsychologist(psychologist);
		System.out.println("result = "+result);
		int alt = 5 ;
		if(result  == 1) {
			
			alt = 1 ;
			request.setAttribute("alt", alt);
			
		}else {
			alt = 0 ;
			request.setAttribute("alt", alt);
		}
		
		Psychologist psy = new Psychologist();
		List<Psychologist> chackListpsy = new ArrayList<>();
		List<Psychologist> listpsy  = lrp.listPsychologist();
		List<Login> listlogin = lrp.listLogin();

		try {
			boolean ck = false ;
			for(int i=0 ; i<listpsy.size() ; i++) {
				for(int k=0 ; k<listlogin.size() ; k++) { 
					System.out.println("suer = "+ listpsy.get(i).getUsername() +"=="+listlogin.get(k).getUsername()); 
					if(!listpsy.get(i).getUsername().equals(listlogin.get(k).getUsername())) {
						ck = true ;
					}else {
						ck = false ;
						System.out.println("ck = "+ ck); 
						break ;
					}
				}
				if(ck) {
					String users = listpsy.get(i).getUsername();
					psy = lrp.getProfilePsychologist(users);
					chackListpsy.add(psy);
					System.out.println("suer = "+ users); 
				}
				ck = false ;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(chackListpsy.size());
		request.setAttribute("chackListpsy", chackListpsy);
		 
		
		return "ListRegisterPsychologist";
	}
	
	
	private void sendFromGMail(String from1, String pass1, String[] to1, String subject1, String body) throws MessagingException {
		Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from1);
        props.put("mail.smtp.password", pass1);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from1));
            InternetAddress[] toAddress = new InternetAddress[to1.length];

            // To get the array of addresses
            for( int i = 0; i < to1.length; i++ ) {
                toAddress[i] = new InternetAddress(to1[i]);
            }

            for( int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject1);
            message.setText(body);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from1, pass1);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            
        }
        catch (AddressException ae) {
            ae.printStackTrace();

        }
		
	}
}
