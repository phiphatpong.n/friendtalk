package controller.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.*;
import bean.*;

@Controller
public class ViewBuyPackageOrgController {
	@RequestMapping(value = "/openViewBuyPackageOrg", method = RequestMethod.GET)
	public String openViewBuyPackageOrg(HttpServletRequest request, Model md, HttpSession session) {
		
		String user = request.getParameter("user"); 
		ViewBuyPackageOrgManager vbpom = new ViewBuyPackageOrgManager();
		List<BuyPackageHistory> listBPH = vbpom.listBuyPackageHistory(user);
		 
		request.setAttribute("listBPH",listBPH);
		
		
		
		return "ViewBuyPackageOrg";
	}
//	@RequestMapping(value = "/ViewBuyPackageOrganizationHistory", method = RequestMethod.GET)
//	public String openViewPackageOrganization(HttpServletRequest request, Model md, HttpSession session) {
//		
//		AddPackageOrgManager apom = new AddPackageOrgManager();
//		String user = request.getParameter("user");
//		List<BuyPackageHistory> listb = apom.ListBuyPackageHistory(user);
//		
//		request.setAttribute("listb", listb);
//		
//		return "ViewBuyPackageOrganizationHistory";
//	}
}
