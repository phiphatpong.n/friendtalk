package controller.spring;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import Manager.ArticleManager;
import Manager.AwardManager;
import Manager.RedeemManager;
import bean.Article;
import bean.Award;
import bean.Psychologist;
import bean.Redeem;

@Controller
public class RedeemController {
	
	@RequestMapping(value = "/openRedeem", method = RequestMethod.GET)
	public String openRedeem(HttpServletRequest request, Model md, HttpSession session) {
		
		String user = request.getParameter("user");
		RedeemManager rem = new RedeemManager();
		Psychologist psy = new Psychologist();
		
		psy = rem.getProfilePsychologist(user);
		List<Redeem> lredeem = rem.getlistredeem(user);  
		
		List<Award> awList = rem.listAllAward();
		
		session.setAttribute("psy", psy);
		request.setAttribute("awList", awList);
		request.setAttribute("lredeem", lredeem);
		System.out.println("lredeem = " + lredeem.size());
		
		return "Redeem";
	}
	@RequestMapping(value = "/openSeeMore", method = RequestMethod.POST)
	public String openAwardDetails(HttpServletRequest request, HttpSession session) {
		try {
			Award award = new Award();
			Psychologist psy = new Psychologist();
			
			String awid = request.getParameter("awid");
			String psyid = request.getParameter("psyid");
			
			RedeemManager rem = new RedeemManager();

			psy = rem.getProfilePsychologist(psyid);
			award = rem.getAward(awid);
			List<Redeem> lredeem = rem.getlistredeem(psyid); 
			
			
			request.setAttribute("award", award);
			session.setAttribute("psy", psy);
			request.setAttribute("lredeem", lredeem);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "AwardDetails";
	}
	@RequestMapping(value = "/redeem", method = RequestMethod.POST)
	public String redeem(HttpServletRequest request) {
		HttpSession session = request.getSession();
		
		Award aw = new Award();
		Psychologist psy = new Psychologist();
		
		RedeemManager rem = new RedeemManager();
		
		String awid = request.getParameter("awid");
		String psyid = request.getParameter("psyid");
		String s = request.getParameter("Score");
		
		int Score = Integer.parseInt(s) ;
		
		int maxid = rem.getmaxidRedeem();
		String Rid = "R" + String.valueOf(maxid) ;
		
		Date date = new Date();
		date.getTime();
		
		psy = rem.getProfilePsychologist(psyid);
		aw = rem.getAward(awid);
		
		int awsocre = aw.getScore();
		int psyscore = (int) psy.getGoodness_score();
		int ans = 0 ;
		int alertredeem = 0 ; 
		
		List<Award> awList = rem.listAllAward();
		List<Redeem> lredeem = rem.getlistredeem(psyid); 
		
		if(psyscore >= awsocre) {
			ans = psyscore - awsocre ;
			psy.setGoodness_score(ans);

			Redeem redeem = new Redeem(Rid,date,"รอจัดส่ง",Score);
			redeem.setPsychologist(psy);
			redeem.setAward(aw);
			
			String result = rem.insertPsychologist(psy);
			String result2 =rem.insertRedeem(redeem);
			
			if(result.equals("Sign Up Successfully") || result2.equals("Sign Up Successfully")) {
				
				alertredeem = 1 ;
				List<Redeem> listredeem = rem.getlistredeem(psyid);
				
				session.setAttribute("lredeem", listredeem);
				session.setAttribute("psy", psy);
				request.setAttribute("awList", awList);
				request.setAttribute("alertredeem", alertredeem);
				
				return "Redeem";
			}
		
		}
			alertredeem = 0;
			session.setAttribute("psy", psy);
			request.setAttribute("awList", awList);
			request.setAttribute("lredeem", lredeem);
			request.setAttribute("alertredeem", alertredeem);

		return "Redeem";
	}
	@RequestMapping(value = "/openHistoryRedeem", method = RequestMethod.GET)
	public String openHistoryRedeem(HttpServletRequest request, HttpSession session) {
		try {
			String user = request.getParameter("user");
			
			RedeemManager rem = new RedeemManager();
			
			List<Redeem> listredeem = rem.getlistredeem(user);
			
			session.setAttribute("listredeem", listredeem);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "HistoryRedeem";
	}
	
}
