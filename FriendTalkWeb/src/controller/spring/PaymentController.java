package controller.spring;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import Manager.*;
import bean.*;

import co.omise.Client;
import co.omise.ClientException;
import co.omise.models.Balance;
import co.omise.models.Charge;
import co.omise.models.OmiseException;
import co.omise.requests.Request;

@Controller
public class PaymentController {
	
	@RequestMapping(value = "/openMakePayment", method = RequestMethod.GET)
	public String openMakePayment(HttpServletRequest request, Model md, HttpSession session) {
		try {
		
		String Mid = request.getParameter("Mid");
		
		PaymentManager pm = new PaymentManager();
		
		MakeAppointment ma = pm.getMakeAppointment(Mid);
		String usercon = ma.getConsultant().getUsername();
		Consultant con = pm.getConsultant(usercon);
		System.out.println("userCon = "+ usercon);
		
		Advice advice = pm.getAdvice(Mid);
		Price price = new Price();
		double p = 0;
					String status = advice.getMake_appointment().getStatus_appoint();
					if(status.equals("ค้างชำระ")) {
						List<Review> listreview = pm.listReview();
							if(advice.getReview() == null) {
								price = advice.getMake_appointment().getPrice();
								p = price.getPrice();
								System.out.println("Price = "+ p);
							}
						}
		
		request.setAttribute("price", price);
		request.setAttribute("advice", advice);
		request.setAttribute("ma", ma);
		session.setAttribute("con", con);
		
		return "Paymant";
		} catch (Exception e) {
			e.printStackTrace();
			return "Error";
		}
		
	}
	
	@RequestMapping(value="/ChackMakePayment",method=RequestMethod.POST)
	public String doMakePayment(HttpServletRequest request, HttpSession session)  throws SQLException, IOException, ClientException, OmiseException {
		
		String code = request.getParameter("code");
		String Mid = request.getParameter("Mid");
		String adviceid = request.getParameter("adviceid");
		String conid = request.getParameter("conid");
		
		Client client = new Client.Builder()
                .publicKey("pkey_test_5pw5gnd2yz79xvngc89")
                .secretKey("skey_test_5pw53o6qrc4u46vahmw")
                .build();
		
		Request<Balance> rq = new Balance.GetRequestBuilder().build();
		Balance balance = client.sendRequest(rq);
		System.out.println("Code = "+ code);
		
		String omiseToken = request.getParameter("omiseToken");
		System.out.println(request.getParameter("price"));
		String pri = request.getParameter("price");
		double p = Double.parseDouble(pri);
		int prices =  (int) p;
		
		PaymentManager pm = new PaymentManager();
		Advice advice = pm.getAdvice(Mid);
		List<PaymentCode> listpCodes = pm.listPaymentCode();
		int Money = 0 ;
		if(code != null) {
		if(!code.equals("")) {
			for(int i=0 ; i<listpCodes.size() ; i++) {
				if(code.toLowerCase().equals(listpCodes.get(i).getCode().toLowerCase())) {
					if(listpCodes.get(i).getCall_minute().equals("60")) {
						Money = 800 ;
					}else if(listpCodes.get(i).getCall_minute().equals("30")){
						Money = 400 ;
					}else if(listpCodes.get(i).getChat_minute().equals("60")){
						Money = 400;
					}else if(listpCodes.get(i).getChat_minute().equals("30")){
						Money = 200 ;
					}
				}
			}
			prices = Money - prices ;
		}
		}
		String statusPay = "" ;
		
		int id = pm.getMaxidPayment();
		String maxid = "P"+String.valueOf(id);
		
		Date date = new Date();
		date.toInstant();
		System.out.println(date);
		int statusP = 0 ;
		ReviewManager rm = new ReviewManager();
		if(prices != 0) {
			Request<Charge> requests = new Charge.CreateRequestBuilder()
	                    .amount(prices*100)
	                    .currency(balance.getCurrency())
	                    .card(omiseToken)
	                    .build();
			
			Charge charge = client.sendRequest(requests);
			
			System.out.println(charge.getStatus());
			statusPay = ""+charge.getStatus();
			System.out.println(statusPay);
			
		}else {
			PaymentCode pc = pm.getPaymentCode(code);
			statusP = 1;
			double p2 = Double.parseDouble(pri);
			int price =  (int) p2;
			Payment pay = new Payment(maxid,price,date,statusP);
			pay.setAdvice(advice);
			pay.setPaymentCode(pc);
			
			String result = pm.insertPayment(pay);
			if(result.equals("successfully saved")) {
				
				pc.setUsage_status("ใช้งาน");
				String result2 = pm.updatePaymentCode(pc);
				
				if(result2.equals("successfully saved")) {
					
					MakeAppointment ma = rm.getMakeAppointment(Mid);
					ma.setStatus_appoint("สำเร็จ");
					
					String result3 = rm.insertMakeAppointment(ma);
					ma = rm.getMakeAppointment(Mid);
					if(result3.equals("successfully saved")) {
						request.setAttribute("ma", ma);
						return "review";
					}
					
				}
			}
		}
		

		
		if(statusPay.equals("Pending")) {
			double p2 = Double.parseDouble(pri);
			int price =  (int) p2;
			statusP = 0;
			Payment pay = new Payment(maxid,price,date,statusP);
			pay.setAdvice(advice);
			
			MakeAppointment ma = rm.getMakeAppointment(Mid);
			
			String result = pm.insertPayment(pay);
			if(result.equals("successfully saved")) {
				
			ma.setStatus_appoint("สำเร็จ");
			String result2 = rm.insertMakeAppointment(ma);
			ma = rm.getMakeAppointment(Mid);
			if(result2.equals("successfully saved")) {
				request.setAttribute("ma", ma);
				return "review";
			}
			}
		}
		
		return "";
	}
}
