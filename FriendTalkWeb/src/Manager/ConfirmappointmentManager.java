package Manager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.MakeAppointment;
import conn.HibernateConnection;

public class ConfirmappointmentManager {
	public String UpdateMakeAppointment(MakeAppointment  appointment) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(appointment);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			return "failed to save";
		}
	}
	public MakeAppointment getMakeAppointment(String  id) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			MakeAppointment ma = (MakeAppointment) session.createQuery("From MakeAppointment where appointment_id ='" + id+"'").uniqueResult();
			t.commit();
			session.close();
			return ma;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
