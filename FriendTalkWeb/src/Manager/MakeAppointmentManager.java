package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.*;
import bean.MakeAppointment;
import bean.Psychologist;
import bean.PsychologistExpertise;
import bean.PsychologistExpertiseKey;
import bean.SetWorkingTime;
import conn.HibernateConnection;

public class MakeAppointmentManager {
	
	public String insertMakeAppointment(MakeAppointment  appointment) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(appointment);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			return "failed to save";
		}
	}
	public int getMaxMakeAppointmentId() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();

			String maxappointmentid = (String) session.createQuery("select max(appointment_id) from MakeAppointment").uniqueResult();
			session.close();
			
			String id = maxappointmentid.replaceAll("M", "");
			System.out.println(id);
			
			int max = Integer.parseInt(id) + 1 ;
			
			return max;
			
		} catch (Exception e) {
			return 1;
		}
	}
	public List<MakeAppointment> listAllMakeAppointmentconsultant(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<MakeAppointment> list_appointmentid = session.createQuery("From MakeAppointment where user_consultant = '"+user+"'").list();
			session.close();
			
			return list_appointmentid;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<MakeAppointment> listAllMakeAppointmentpsychologist(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<MakeAppointment> list_appointmentid = session.createQuery("From MakeAppointment where username_psychologist = '"+user+"'").list();
			session.close();
			
			return list_appointmentid;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<Price> listPrice() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Price> list_Price = session.createQuery("From Price").list();
			session.close();
			
			return list_Price;
			
		} catch (Exception e) {
			return null;
		}
	}
	public MakeAppointment getMakeAppointment(String id) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			MakeAppointment appointment = (MakeAppointment) session.createQuery("From MakeAppointment where appointment_id ='"+ id+"'").uniqueResult();
			session.close();
			
			return appointment ;
		} catch (Exception e) {
			return null;
		}
		
	}
	public String deleteMakeAppointment(MakeAppointment appointment) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.delete(appointment);
			t.commit();
			session.close();
			return "successfully delete";
		} catch (Exception e) {
			return "failed to delete student";
		}
	}
	public List<Psychologist> listAllPsychologist() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> listPsychologist= session.createQuery("From Psychologist").list();
			session.close();
			
			return listPsychologist;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<PsychologistExpertise> listPsychologistExpertise() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<PsychologistExpertise> listPEK= session.createQuery("From PsychologistExpertise").list();
			session.close();
			
			return listPEK;
			
		} catch (Exception e) {
			return null;
		}
	}
	public Psychologist getProfilePsychologist(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> users = session.createQuery("From Psychologist where username = '" + user +"'").list();
			session.close();
			
			return users.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
	public Consultant getProfileConsultant(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Consultant> users = session.createQuery("From Consultant where username = '" + user +"'").list();
			session.close();
			
			return users.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
	public Price getPrice(String Price) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Price> prices = session.createQuery("From Price where contact_type = '" + Price +"'").list();
			session.close();
			
			return prices.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<SetWorkingTime> getSetWorkingTime(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<SetWorkingTime> Wtime = session.createQuery("From SetWorkingTime where username = '" + user +"'").list();
			session.close();
			
			return Wtime;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<MakeAppointment> listMakeAppointmentPsychologist(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<MakeAppointment> list_appointmentid = session.createQuery("From MakeAppointment where (username_psychologist = '"+user+"' and status_appoint = 'ยืนยันการนัด') or status_appoint = 'รอการยืนยัน'").list();
			session.close();
			
			return list_appointmentid;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<SetWorkingTime> listAllSetWorkingTime() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<SetWorkingTime> listSetWorkingTime= session.createQuery("From SetWorkingTime ").list();
			session.close();
			
			return listSetWorkingTime;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<Review> listReview( ) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Review> list= session.createQuery("From Review ").list();
			session.close();
			
			return list;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<Advice> listAdvice( ) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Advice> list= session.createQuery("From Advice ").list();
			session.close();
			
			return list;
			
		} catch (Exception e) {
			return null;
		}
	}
}
