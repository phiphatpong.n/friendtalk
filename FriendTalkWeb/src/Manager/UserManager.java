package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import conn.HibernateConnection;
import bean.Consultant;
import bean.Login;
import bean.Organization;
import bean.Psychologist;
import bean.User;

public class UserManager {
	private static String SALT = "123456";

	public String insertConsultant(Consultant consultant) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(consultant);
			t.commit();
			session.close();
			return "Sign Up Successfully";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save student";
		}
	}
	public String insertPsychologist(Psychologist psychologist) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(psychologist);
			t.commit();
			session.close();
			return "Sign Up Successfully";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save ";
		}
	}
	public String insertOrganization(Organization organization) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(organization);
			t.commit();
			session.close();
			return "Sign Up Successfully";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save ";
		}
	}
	public Login insertLogin(Login login) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(login);
			t.commit();
			session.close();
			return login;
		} catch (Exception e) {
			e.printStackTrace();
			return login;
		}
	}
	
	
	
	public String doHibernateLogin(Login login) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			List<Login> user = session.createQuery("From Login where username ='" + login.getUsername()+"'").list();
			session.close();
			
			if (user.size() == 1) {
				//String password = PasswordUtil.getInstance().createPassword(user.get(0).getPassword(), SALT);
				if (login.getPassword().equals(user.get(0).getPassword())) {
					if(login.getType().equals(user.get(0).getType())) {
						return "login success";
					}else {
						return "type does't match";
					}
				} else {
					return "username or password does't match";
				}
			} else {
				return "username or password does't match";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "Please try again...";
		}
	}
	
	
}
