package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.Article;
import bean.Organization;
import bean.PaymentCode;
import conn.HibernateConnection;

public class PaymantCodeManager {
	
	public String insertPaymentCode(PaymentCode paymentcode ) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(paymentcode);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			return "failed to save";
		}
	}
	public String insertOrganization(Organization organization) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(organization);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save ";
		}
	}
	public Organization getProfileOrganization(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Organization> users = session.createQuery("From Organization where username = '" + user +"'").list();
			session.close();
			
			return users.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<PaymentCode> getListPaymentCode(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<PaymentCode> listcode = session.createQuery("From PaymentCode where username = '"+ user +"'").list();
			session.close();
			
			return listcode;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public PaymentCode getPaymantcode(String code) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			PaymentCode pc = (PaymentCode)session.createQuery("From PaymentCode where code = '" + code +"'").uniqueResult();
			session.close();
			
			return pc ;
			
		} catch (Exception e) {
			return null;
		}
	}
}
