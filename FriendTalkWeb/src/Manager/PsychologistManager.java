package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.Psychologist;
import bean.User;
import conn.HibernateConnection;

public class PsychologistManager {
	

	public Psychologist getProfilePsychologist(String name) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> psychologists = session.createQuery("From Psychologist where fristname = " + name).list();
			session.close();
			
			return psychologists.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<Psychologist> getlistPsychologist() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> psychologists = session.createQuery("From Psychologist").list();
			session.close();
			
			return psychologists ;
			
		} catch (Exception e) {
			return null;
		}
	} 
	public Psychologist getPsychologist(String name) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> psychologists = session.createQuery("From Psychologist where username = '" + name+"'").list();
			session.close();
			
			return psychologists.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
}
