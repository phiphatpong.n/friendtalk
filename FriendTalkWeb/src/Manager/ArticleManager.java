package Manager;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.User;
import bean.Article;
import bean.Award;
import bean.Psychologist;
import conn.ConnectionDB;
import conn.HibernateConnection;

public class ArticleManager {
	
	public String insertAddArticle(Article addarticle) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(addarticle);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save article";
			
		}
	}
	public int getMaxarticleId() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();

			String Aid = (String) session.createQuery("select max(article_id) From Article").uniqueResult();
			session.close();
			
			String id = Aid.replaceAll("A", "");

			System.out.println(id);
			
			int max = Integer.parseInt(id) + 1 ;
			
			return max;
			
		} catch (Exception e) {
			return 1;
		}
	}
	public List<Article> listAllarticle() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Article> list = session.createQuery("From Article").list();
			session.close();
			
			return list;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<Article> ListArticle(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Article> list = session.createQuery("From Article where username = '"+ user +"'").list();
			session.close();
			
			return list;
			
		} catch (Exception e) {
			return null;
		}
	}
	public Article getArticle(String id) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			Article article = (Article) session.createQuery("From Article where article_id ='" + id+"'").uniqueResult();
			session.close();
			
			return article ;
		} catch (Exception e) {
			return null;
		}
		
	}
	public int deleteArticle(Article article) throws SQLException{
		ConnectionDB condb = new ConnectionDB();
		Connection conn = condb.getConnection();
		int result = 0;
		try {
			
			Statement stmt = conn.createStatement();
			
			String sql = "Delete from Article where article_id ='"+article.getArticle_id()+"'";
			
			System.out.println(sql);
			result = stmt.executeUpdate(sql);
			System.out.println("result= "+result);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		conn.close();
		return result;
	}
	public String editArticle(Article article) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.update(article);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			return "failed to save student";
		}
	}
	public Psychologist getPsychologist(String name) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> psychologists = session.createQuery("From Psychologist where username = '" + name+"'").list();
			session.close();
			
			return psychologists.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}

}
