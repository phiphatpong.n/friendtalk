package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import bean.BuyPackageHistory;
import conn.HibernateConnection;

public class ViewBuyPackageOrgManager {
	public List<BuyPackageHistory> listBuyPackageHistory (String id) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession(); 
			session.beginTransaction();
			
			List<BuyPackageHistory> listBPH = (List<BuyPackageHistory>) session.createQuery("From BuyPackageHistory where username ='" + id+"'").list();
			session.close();
			 
			return listBPH ;
		} catch (Exception e) {
			return null;
		}
		
	}
}
