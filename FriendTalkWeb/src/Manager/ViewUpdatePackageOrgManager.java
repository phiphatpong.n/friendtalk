package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.BuyPackageHistory;
import bean.Organization;
import conn.HibernateConnection;

public class ViewUpdatePackageOrgManager {
	public List<BuyPackageHistory> ListBuyPackageHistory() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			List<BuyPackageHistory> list = session.createQuery("From BuyPackageHistory ").list();
			t.commit();
			session.close();
			return list;
		} catch (Exception e) {
			return null;
		}
	}
	public List<Organization> ListOrganization() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			List<Organization> list = session.createQuery("From Organization ").list();
			t.commit();
			session.close();
			return list;
		} catch (Exception e) {
			return null;
		}
	}
	public Organization getOrganization(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			Organization org =(Organization) session.createQuery("From Organization where username = '"+user+"'").uniqueResult();
			t.commit();
			session.close();
			
			return org;
		} catch (Exception e) {
			return null;
		}
	}
}
