package Manager;

import java.util.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.*;
import conn.HibernateConnection;

public class AddDataOnconsultingTimeManager {
	
	public String insertGiveAdvice(Advice advice) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(advice);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save article";
			
		}
	}
	public String UpdateMakeAppointment(MakeAppointment ma) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(ma);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save article";
			
		}
	}
	public MakeAppointment getMakeAppointment(String id) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();

			MakeAppointment MA = (MakeAppointment) session.createQuery("From MakeAppointment where appointment_id = '"+id+"'").uniqueResult();
			session.close();
			
			return MA;
			
		} catch (Exception e) {
			return null;
		}
	}
	public int getmaxidAdvice() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();

			String Aid = (String) session.createQuery("select max(advice_id) From Advice").uniqueResult();
			session.close();
			
			String id = Aid.replaceAll("A", "");

			System.out.println(id);
			
			int max = Integer.parseInt(id) + 1 ;
			
			return max;
			
		} catch (Exception e) {
			return 1;
		}
	}
}
