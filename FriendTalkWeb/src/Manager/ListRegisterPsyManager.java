package Manager;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.Article;
import bean.Award;
import bean.Login;
import bean.Psychologist;
import conn.ConnectionDB;
import conn.HibernateConnection;

public class ListRegisterPsyManager {
	public String getlogin(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			String username = (String) session.createQuery("Select username From Login where username = '"+user+"'").uniqueResult();
			session.close();
			
			return username;
			
		} catch (Exception e) {
			return null;
		}
	}
	public String insertLogin(Login login) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(login);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "Failed to save";
		}
	}
	public Psychologist getProfilePsychologist(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> users = session.createQuery("From Psychologist where username = '" + user +"'").list();
			session.close();
			
			return users.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<Psychologist> listPsychologist() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> listpsy = session.createQuery("From Psychologist").list();
			session.close();
			
			return listpsy;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<Login> listLogin() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Login> listLogin= session.createQuery("From Login where type = 'psychologist'").list();
			session.close();
			
			return listLogin;
			
		} catch (Exception e) {
			return null;
		}
	}
	public int deleteRegisterPsychologist(Psychologist psy) throws SQLException{
		ConnectionDB condb = new ConnectionDB();
		Connection conn = condb.getConnection();
		int result = 0;
		try {
			 
			Statement stmt = conn.createStatement();
			
			String sql = "Delete from Psychologist where username ='"+psy.getUsername()+"'";

			System.out.println(sql);
			result = stmt.executeUpdate(sql);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		conn.close();
		return result;
	}

}
