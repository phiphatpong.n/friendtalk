package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import bean.*;
import conn.HibernateConnection;

public class ViewHistoryverifyregisterManager {
	public List<Login> listLogin() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Login> listLogin= session.createQuery("From Login ").list();
			session.close();
			
			return listLogin;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<Psychologist> listPsychologist() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> listpsy= session.createQuery("From Psychologist ").list();
			session.close();
			
			return listpsy ;
			
		} catch (Exception e) {
			return null;
		}
	}
	public Psychologist getProfilePsychologist(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> users = session.createQuery("From Psychologist where username = '" + user +"'").list();
			session.close();
			
			return users.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
}
