package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.*;
import conn.HibernateConnection;

public class PaymentManager {

	public Advice getAdvice(String id) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			Advice advice = (Advice)session.createQuery("From Advice where appointment_id = '"+ id +"'").uniqueResult();
			session.close();
			
			return advice;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<Review> listReview() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Review> list = session.createQuery("From Review").list();
			session.close();
			
			return list;
			
		} catch (Exception e) {
			return null;
		}
	}
	public MakeAppointment getMakeAppointment(String id) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			MakeAppointment ma =(MakeAppointment) session.createQuery("From MakeAppointment where appointment_id ='"+id+"'").uniqueResult();
			session.close();
			
			return ma;
			
		} catch (Exception e) {
			return null;
		}
	}
	
	public Consultant getConsultant(String id) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			Consultant con =(Consultant) session.createQuery("From Consultant where username ='"+id+"'").uniqueResult();
			session.close();
			
			return con;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<PaymentCode> listPaymentCode() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<PaymentCode> list =(List<PaymentCode>) session.createQuery("From PaymentCode ").list();
			session.close();
			
			return list;
			
		} catch (Exception e) {
			return null;
		}
	}
	public int getMaxidPayment() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();

			String Pid = (String) session.createQuery("select max(payment_id) From Payment").uniqueResult();
			session.close();
			
			String id = Pid.replaceAll("P", "");

			System.out.println(id);
			
			int max = Integer.parseInt(id) + 1 ;
			
			return max;
			
		} catch (Exception e) {
			return 1;
		}
	}
	public PaymentCode getPaymentCode(String id) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			PaymentCode pc =(PaymentCode) session.createQuery("From PaymentCode where code ='"+id+"'").uniqueResult();
			session.close();
			
			return pc;
			
		} catch (Exception e) {
			return null;
		}
	}
	public String insertPayment(Payment payment) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(payment);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save ";
			
		}
	}
	
	public String updatePaymentCode(PaymentCode code) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(code);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save";
			
		}
	}
}
