package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.Consultant;
import bean.Organization;
import bean.Psychologist;
import bean.User;
import conn.HibernateConnection;

public class EditProfileManager {
	
	public String oooo(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			String type = (String) session.createQuery("Select type From Login where username = '"+user+"'").uniqueResult();
			session.close();
			
			return type;
			
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<Consultant> listConsultant() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Consultant> users = session.createQuery("Select username From Consultant").list();
			session.close();
			
			return users;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<Psychologist> listPsychologist() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> users = session.createQuery("Select username From Psychologist").list();
			session.close();
			
			return users;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<Organization> listOrganization() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Organization> users = session.createQuery("Select username From Organization").list();
			session.close();
			
			return users;
			
		} catch (Exception e) {
			return null;
		}
	}
	
	public Consultant getProfileConsultant(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Consultant> users = session.createQuery("From Consultant where username = '" + user +"'").list();
			session.close();
			
			return users.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
	public Psychologist getProfilePsychologist(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> users = session.createQuery("From Psychologist where username = '" + user +"'").list();
			session.close();
			
			return users.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
	public Organization getProfileOrganization(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Organization> users = session.createQuery("From Organization where username = '" + user +"'").list();
			session.close();
			
			return users.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}

	public String deleteUser(User user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.delete(user);
			t.commit();
			session.close();
			return "successfully delete";
		} catch (Exception e) {
			return "failed to delete student";
		}
	}
}
