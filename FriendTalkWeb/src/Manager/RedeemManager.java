package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.Award;
import bean.Psychologist;
import bean.Redeem;
import conn.HibernateConnection;

public class RedeemManager {
	
	public List<Award> listAllAward() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
 			
			session.beginTransaction();
			List<Award> list_id = session.createQuery("From Award").list();
			session.close();
			
			return list_id;
			
		} catch (Exception e) {
			return null;
		}
	}
	public Psychologist getProfilePsychologist(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> users = session.createQuery("From Psychologist where username = '" + user +"'").list();
			session.close();
			
			return users.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
	public Award getAward(String did) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			Award award = (Award) session.createQuery("From Award where award_id ='" + did+"'").uniqueResult();
			session.close();
			
			return award ;
		} catch (Exception e) {
			return null;
		}
		
	}
	public String insertPsychologist(Psychologist psychologist) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(psychologist);
			t.commit();
			session.close();
			return "Sign Up Successfully";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save";
		}
	}
	public String insertRedeem(Redeem redeem) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(redeem);
			t.commit();
			session.close();
			return "Sign Up Successfully";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save";
		}
	}
	public List<Redeem> getlistredeem(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			List<Redeem> re = (List<Redeem>) session.createQuery("From Redeem where username ='" + user+"'").list();
			session.close();
			
			return re ;
		} catch (Exception e) {
			return null;
		}
		
	}
	public int getmaxidRedeem() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			String Rid = (String) session.createQuery("select redeem_id From Redeem").uniqueResult();
			session.close();
			
			String id = Rid.replaceAll("R", "");

			System.out.println(id);
			
			int max = Integer.parseInt(id) + 1 ;
			
			return max ;
		} catch (Exception e) {
			return 1;
		}
		
	}
}
