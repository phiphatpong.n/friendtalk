package Manager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.Expertise;
import bean.Organization;
import bean.Psychologist;
import bean.PsychologistExpertise;
import conn.HibernateConnection;

public class ExpertiseManager {
	public String insertExpertise(Expertise expertise) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(expertise);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save ";
		}
	}
	public int getMaxExpertiseId() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			int max = 0;
			max = (Integer) session.createQuery("select max(expertise_id) from Expertise").uniqueResult();
			session.close();
			
			return max+1;
			
		} catch (Exception e) {
			return 1;
		}
	}
	
	public String insertPsychologistExpertise(PsychologistExpertise psychologistExpertise) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			
			session.saveOrUpdate(psychologistExpertise);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save ";
		}
	}
}
