package Manager;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import bean.*;
import conn.HibernateConnection;


public class SearchManager {
	public List<Article> searchArticleName(String search){
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			List<Article> list = session.createQuery("From Article WHERE article_name LIKE '%"+search+"%'").list();
			session.close();
			
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public List<Psychologist> ListPsychologistName(){
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			List<Psychologist> list = session.createQuery("From Psychologist").list();
			session.close();
			
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public List<PsychologistExpertise> ListPsychologistExpertise(){
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			List<PsychologistExpertise> listPsyexp = session.createQuery("From PsychologistExpertise").list();
			session.close();
			
			return listPsyexp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public List<Expertise> ListExpertise(String type){
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			List<Expertise> listExp = session.createQuery("From Expertise where expertise_type LIKE '%"+type+"%'").list();
			session.close();
			
			return listExp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public Psychologist getProfilePsychologist(String name) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> psychologists = session.createQuery("From Psychologist where username = '" + name+"'").list();
			session.close();
			
			return psychologists.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<PsychologistExpertise> listPsychologistExpertise() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<PsychologistExpertise> listPEK= session.createQuery("From PsychologistExpertise").list();
			session.close();
			
			return listPEK;
			
		} catch (Exception e) {
			return null;
		}
	}
}
