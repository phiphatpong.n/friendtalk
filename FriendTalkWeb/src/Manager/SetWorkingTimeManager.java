package Manager;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import bean.*;
import bean.SetWorkingTime;
import conn.HibernateConnection;

public class SetWorkingTimeManager {
	public Psychologist getProfilePsychologist(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Psychologist> users = session.createQuery("From Psychologist where username = '" + user +"'").list();
			session.close();
			
			return users.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
	public String insertSetWokingTime(SetWorkingTime workingTime) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(workingTime);
			t.commit();
			session.close();
			return "save Successfully";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save";
		}
	}
	public List<SetWorkingTime> getWorkingtime(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<SetWorkingTime>  list = session.createQuery("From SetWorkingTime where username = '" + user +"'").list();
			session.close();
			
			return list;
			
		} catch (Exception e) {
			return null;
		}
	}
}
