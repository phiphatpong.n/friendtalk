package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.*;
import conn.HibernateConnection;

public class IndexManager {
	public List<packages> listAllpackages() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<packages> list_pack = session.createQuery("From packages").list();
			session.close();
			
			return list_pack;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<MakeAppointment> listAllMakeAppointment() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<MakeAppointment> list = session.createQuery("From MakeAppointment where status_appoint = 'ยืนยันการนัด' ").list();
			session.close();
			
			return list;
			
		} catch (Exception e) {
			return null;
		}
	}
	public MakeAppointment getMakeAppointment(String id) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			MakeAppointment list =(MakeAppointment) session.createQuery("From MakeAppointment where appointment_id = '"+id+"'").uniqueResult();
			session.close();
			
			return list;
			
		} catch (Exception e) {
			return null;
		}
	}
	public String insertMakeAppointment(MakeAppointment  appointment) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(appointment);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			return "failed to save";
		}
	}
}
