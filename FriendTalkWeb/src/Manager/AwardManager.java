package Manager;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.Article;
import bean.Award;
import bean.Redeem;
import bean.User;
import conn.ConnectionDB;
import conn.HibernateConnection;

public class AwardManager {
	
	public String insertAddAward(Award  award) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(award);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			return "failed to save article";
		}
	}
	public int getMaxAwardId() {
		try { 
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			
			String id = (String) session.createQuery("select max(award_id) from Award").uniqueResult();
			session.close();
			
			String AWid = id.replaceAll("AW", "");

			System.out.println(AWid);
			
			int max = Integer.parseInt(AWid) + 1 ;
			
			return max;
			
		} catch (Exception e) {
			return 0;
		}
	}
	public List<Award> listAllAward() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Award> list_id = session.createQuery("From Award").list();
			session.close();
			
			return list_id;
			
		} catch (Exception e) {
			return null;
		}
	}
	public Award getAward(String did) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			Award award = (Award) session.createQuery("From Award where award_id ='" + did+"'").uniqueResult();
			session.close();
			
			return award ;
		} catch (Exception e) {
			return null;
		}
		
	}
	public int deleteAward(Award award) throws SQLException{
		ConnectionDB condb = new ConnectionDB();
		Connection conn = condb.getConnection();
		int result = 0;
		try {
			
			Statement stmt = conn.createStatement();
			
			String sql = "Delete from Award where Award_id ='"+award.getAward_id()+"'";
			
			System.out.println(sql);
			result = stmt.executeUpdate(sql);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		conn.close();
		return result;
	}
	public List<Redeem> getlistredeem() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			List<Redeem> re = (List<Redeem>) session.createQuery("From Redeem ").list();
			session.close();
			
			return re ;
		} catch (Exception e) {
			return null;
		}
		
	}
}
