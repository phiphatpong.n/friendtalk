package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.Article;
import bean.Award;
import bean.packages;
import conn.HibernateConnection;

public class PackageManager {
	
	public String insertPackage(packages packages) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(packages);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) { 
			return "failed to save article";
		} 
	}
	public packages getPackage(String size) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			packages packages = (packages) session.createQuery("From packages where package_size ='" + size+"'").uniqueResult();
			session.close();
			
			return packages ;
		} catch (Exception e) {
			return null;
		}
		
	}
	public List<packages> listAllpackages() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<packages> list_pack = session.createQuery("From packages").list();
			session.close();
			
			return list_pack;
			
		} catch (Exception e) {
			return null;
		}
	}
	public String gettype(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			String type = (String) session.createQuery("Select type From Login where username = '"+user+"'").uniqueResult();
			session.close();
			
			return type;
			
		} catch (Exception e) {
			return null;
		}
	}
}
