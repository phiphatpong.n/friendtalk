package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.*;
import conn.HibernateConnection;

public class ViewPackageOrgManager {
	public Organization getOrganization(String id) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			
			Organization org = (Organization) session.createQuery("From Organization where username ='" + id+"'").uniqueResult();
			session.close();
			 
			return org ;
		} catch (Exception e) {
			return null;
		}
		
	}
	public List<Organization> ListOrganization() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			List<Organization> list = session.createQuery("From Organization ").list();
			t.commit();
			session.close();
			return list;
		} catch (Exception e) {
			return null;
		}
	}
	public String gettype(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			String type = (String) session.createQuery("Select type From Login where username = '"+user+"'").uniqueResult();
			session.close();
			
			return type;
			
		} catch (Exception e) {
			return null;
		}
	}
	
}
