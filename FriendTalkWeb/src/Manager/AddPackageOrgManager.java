package Manager;

import java.util.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.*;
import conn.HibernateConnection;

public class AddPackageOrgManager {
	public List<Organization> ListOrganization() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			List<Organization> list = session.createQuery("From Organization").list();
			t.commit();
			session.close();
			return list;
		} catch (Exception e) {
			return null; 
		}
	}
	public Organization getOrganization(String companyname) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			Organization org = (Organization) session.createQuery("From Organization where Company_name = '"+ companyname +"'").uniqueResult();
			t.commit();
			session.close();
			return org;
		} catch (Exception e) {
			return null;
		}
	}
	public String UpdateOrganization(Organization  organization) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(organization);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			return "failed to save";
		}
	}
	public int maxidBuyPackageHistory() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			String Bid = (String) session.createQuery("select max(buy_package_history_id) From BuyPackageHistory ").uniqueResult();
			session.close();
			
			String id = Bid.replaceAll("B", "");
			
			int max = Integer.parseInt(id) + 1 ;
			System.out.println("max id = "+max);
			return max;
		} catch (Exception e) {
			return 1;
		}
	}
	public String insertBuyPackageHistory(BuyPackageHistory  packageHistory) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(packageHistory);
			t.commit();
			session.close();
			return "successfully saved"; 
		} catch (Exception e) {
			return "failed to save";
		}
	}
	public List<BuyPackageHistory> ListBuyPackageHistory() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			List<BuyPackageHistory> list = session.createQuery("From BuyPackageHistory ").list();
			t.commit();
			session.close(); 
			return list;
		} catch (Exception e) {
			return null;
		}
	}
}
