package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.*;
import conn.HibernateConnection;

public class StressTestManager {
	
	public List<StressQuestion> getListStressQuestion() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<StressQuestion> liststress = session.createQuery("From StressQuestion").list();
			session.close();
			
			return liststress;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<ResultSuggestion> getListResultSuggestion() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<ResultSuggestion> listResultSuggestion = session.createQuery("From ResultSuggestion").list();
			session.close();
			
			return listResultSuggestion;
			
		} catch (Exception e) {
			return null;
		}
	}
	public Consultant getProfileConsultant(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Consultant> users = session.createQuery("From Consultant where username = '" + user +"'").list();
			session.close();
			
			return users.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
	public String insertStressTest(StressTest stressTest) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(stressTest);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			return "failed to save article";
		}
	}
	public int getMaxStressTest() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();

			int sqid = (int) session.createQuery("select max(stress_id) From StressTest").uniqueResult();
			session.close();
			
			return sqid+1;
			
		} catch (Exception e) {
			return 1;
		}
	}
	public StressTest getStressTest(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();

			StressTest  stressTest = (StressTest) session.createQuery("From StressTest where username ='"+user+"'").uniqueResult();
			session.close();
			
			return stressTest;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<StressTest> getListStressTest(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			session.beginTransaction();

			List<StressTest>   stressTest = session.createQuery("From StressTest where username ='"+user+"'").list();
			session.close();
			
			return stressTest;
			
		} catch (Exception e) {
			return null;
		}
	}
}
