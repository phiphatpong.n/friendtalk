package Manager;

import java.util.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.*;
import conn.HibernateConnection;

public class PriceManager {
	public String UpdatePrice(Price price) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(price);
			t.commit();
			session.close();
			return "Successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save";
		}
	}
	public List<Price> ListPrice() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			
			List<Price> list = session.createQuery("From Price ").list();
			t.commit();
			session.close();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
