package Manager;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.*;
import conn.ConnectionDB;
import conn.HibernateConnection;

public class CancelappointmentManager {
	
	public int Cancelappointment(MakeAppointment appointment) throws SQLException{
		ConnectionDB condb = new ConnectionDB();
		Connection conn = condb.getConnection();
		int result = 0;
		try {
			
			Statement stmt = conn.createStatement();
			
			String sql = "Delete from make_appointment where Appointment_id ='"+appointment.getAppointment_id()+"'";
			
			System.out.println(sql);
			result = stmt.executeUpdate(sql);
			System.out.println("result = "+result);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		conn.close();
		return result;
	}
	public String insertMakeAppointment(MakeAppointment  appointment) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(appointment);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			return "failed to save";
		}
	}
}
