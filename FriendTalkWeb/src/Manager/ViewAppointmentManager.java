package Manager;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import bean.Advice;
import bean.MakeAppointment;
import bean.Payment;
import conn.ConnectionDB;
import conn.HibernateConnection;

public class ViewAppointmentManager {
	public String GettypeMember(String user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			String type = (String) session.createQuery("Select type From Login where username = '"+user+"'").uniqueResult();
			session.close();
			
			return type;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<MakeAppointment> listAllMakeAppointment() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<MakeAppointment> list_appointmentid = session.createQuery("From MakeAppointment ").list();
			session.close();
			
			return list_appointmentid;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<Advice> ListAdvice() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Advice> listA = (List<Advice> )session.createQuery(" From Advice").list();
			session.close();
			
			
			return listA;
			
		} catch (Exception e) {
			return null;
		}
	}
	public List<Payment> ListPayment() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Payment> listA = (List<Payment> )session.createQuery(" From Payment").list();
			session.close();
			
			
			return listA;
			
		} catch (Exception e) {
			return null;
		}
	}
}
