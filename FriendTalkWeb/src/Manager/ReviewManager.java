package Manager;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import bean.*;
import conn.HibernateConnection;

public class ReviewManager {
	
	public String insertReview(Review review) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(review);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save article";
			
		}
	}
	public String insertMakeAppointment(MakeAppointment makeAppointment) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(makeAppointment);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save ";
			
		}
	}
	public MakeAppointment getMakeAppointment(String Mid) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			MakeAppointment ma = (MakeAppointment)session.createQuery("From MakeAppointment where appointment_id = '"+Mid+"'").uniqueResult();
			session.close();
			
			return ma;
			
		} catch (Exception e) {
			return null;
		}
	}
	public int getmaxid() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			String Rid = (String)session.createQuery("select max(review_id) From Review").uniqueResult();
			session.close();
			
			String id = Rid.replaceAll("R","");
			
			System.out.println("max id = "+id);
			
			int max = Integer.parseInt(id) + 1 ;
			
			return max;
			
		} catch (Exception e) {
			return 1;
		}
	}
	public List<Advice> ListAdvice() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Advice> listA = (List<Advice> )session.createQuery(" From Advice").list();
			session.close();
			
			
			return listA;
			
		} catch (Exception e) {
			return null;
		}
	}
	public Advice getAdvice(String Mid) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			Advice ad = (Advice)session.createQuery("From Advice where advice_id = '"+Mid+"'").uniqueResult();
			session.close();
			
			return ad;
			
		} catch (Exception e) {
			return null;
		}
	}
	public String insertAdvice(Advice advice) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(advice);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save article";
			
		}
	}
	public List<Review> ListReview() {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Review> listre = (List<Review> )session.createQuery(" From Review").list();
			session.close();
			
			
			return listre;
			
		} catch (Exception e) {
			return null;
		}
	}
	public String UpdateGoodscorePsy(Psychologist psychologist) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.saveOrUpdate(psychologist);
			t.commit();
			session.close();
			return "successfully saved";
		} catch (Exception e) {
			e.printStackTrace();
			return "failed to save ";
			
		}
	}
}
