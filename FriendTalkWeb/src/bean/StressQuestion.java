package bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stress_question")
public class StressQuestion {
	@Id
	@Column(name = "question_id", length = 15)
	private int question_id;
	
	@Column(name = "question_detail", length = 150)
	private String question_detail ;
	
	@Column(name = "max_score", length = 10)
	private int max_score;
	
	@Column(name = "min_score", length = 10)
	private int min_score;

	public StressQuestion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StressQuestion(int question_id, String question_detail, int max_score, int min_score) {
		super();
		this.question_id = question_id;
		this.question_detail = question_detail;
		this.max_score = max_score;
		this.min_score = min_score;
	}

	public int getQuestion_id() {
		return question_id;
	}

	public void setQuestion_id(int question_id) {
		this.question_id = question_id;
	}

	public String getQuestion_detail() {
		return question_detail;
	}

	public void setQuestion_detail(String question_detail) {
		this.question_detail = question_detail;
	}

	public int getMax_score() {
		return max_score;
	}

	public void setMax_score(int max_score) {
		this.max_score = max_score;
	}

	public int getMin_score() {
		return min_score;
	}

	public void setMin_score(int min_score) {
		this.min_score = min_score;
	}
	
}
