package bean;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "make_appointment")
public class MakeAppointment {
	@Id
	@Column(name = "appointment_id", length = 16 )
	private String appointment_id ;
	
	@Column(name = "status_appoint" )
	private String status_appoint ;
	
	@Column(name = "time_start" )
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar time_start ;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "time_end")
	private Calendar time_end ;
	
	@Column(name = "anonymous" )
	private boolean anonymous ;
	
	@Column(name = "cancel_appointment", length = 20)
	private boolean cancel_appointment ;
	
	@Column(name = "notify", length = 16)
	private String notify ;
	
	@Column(name = "appointment_date")
	private Calendar appointment_date ;
	

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="user_consultant", nullable = false)
	private Consultant consultant ;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="username_psychologist" , nullable = false)
	private Psychologist psychologist ;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="contact_type", unique = false, nullable = false)
	private Price price ; 

	public MakeAppointment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MakeAppointment(String appointment_id, String status_appoint, Calendar time_start, Calendar time_end,
			boolean anonymous, boolean cancel_appointment, String notify, Calendar appointment_date) {
		super();
		this.appointment_id = appointment_id;
		this.status_appoint = status_appoint;
		this.time_start = time_start;
		this.time_end = time_end;
		this.anonymous = anonymous;
		this.cancel_appointment = cancel_appointment;
		this.notify = notify;
		this.appointment_date = appointment_date;
	}

	public String getAppointment_id() {
		return appointment_id;
	}

	public void setAppointment_id(String appointment_id) {
		this.appointment_id = appointment_id;
	}

	public String getStatus_appoint() {
		return status_appoint;
	}

	public void setStatus_appoint(String status_appoint) {
		this.status_appoint = status_appoint;
	}

	public Calendar getTime_start() {
		return time_start;
	}

	public void setTime_start(Calendar time_start) {
		this.time_start = time_start;
	}

	public Calendar getTime_end() {
		return time_end;
	}

	public void setTime_end(Calendar time_end) {
		this.time_end = time_end;
	}

	public boolean isAnonymous() {
		return anonymous;
	}

	public void setAnonymous(boolean anonymous) {
		this.anonymous = anonymous;
	}

	public boolean isCancel_appointment() {
		return cancel_appointment;
	}

	public void setCancel_appointment(boolean cancel_appointment) {
		this.cancel_appointment = cancel_appointment;
	}

	public String getNotify() {
		return notify;
	}

	public void setNotify(String notify) {
		this.notify = notify;
	}

	public Calendar getAppointment_date() {
		return appointment_date;
	}

	public void setAppointment_date(Calendar appointment_date) {
		this.appointment_date = appointment_date;
	}

	public Consultant getConsultant() {
		return consultant;
	}

	public void setConsultant(Consultant consultant) {
		this.consultant = consultant;
	}

	public Psychologist getPsychologist() {
		return psychologist;
	}

	public void setPsychologist(Psychologist psychologist) {
		this.psychologist = psychologist;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}
	


	
	
}
