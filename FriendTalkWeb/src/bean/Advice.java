package bean;

import java.sql.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "advice")
public class Advice {
	
	@Id
	@Column(name = "advice_id" , length = 20 , nullable = false)
	private String advice_id ;
	
	@Column(name = "time_start" )
	private String time_start ;

	@Column(name = "time_end"  )
	private String time_end ;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="appointment_id" ,  nullable = false)
	private MakeAppointment make_appointment ;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="review_id" )
	private Review review ;
	

	public Review getReview() {
		return review;
	}


	public void setReview(Review review) {
		this.review = review;
	}


	public Advice() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Advice(String advice_id, String time_start, String time_end) {
		super();
		this.advice_id = advice_id;
		this.time_start = time_start;
		this.time_end = time_end;
	}


	public String getAdvice_id() {
		return advice_id;
	}


	public void setAdvice_id(String advice_id) {
		this.advice_id = advice_id;
	}


	public String getTime_start() {
		return time_start;
	}


	public void setTime_start(String time_start) {
		this.time_start = time_start;
	}


	public String getTime_end() {
		return time_end;
	}


	public void setTime_end(String time_end) {
		this.time_end = time_end;
	}


	public MakeAppointment getMake_appointment() {
		return make_appointment;
	}


	public void setMake_appointment(MakeAppointment make_appointment) {
		this.make_appointment = make_appointment;
	}
}
