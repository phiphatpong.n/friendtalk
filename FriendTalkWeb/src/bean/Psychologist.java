package bean;

import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "psychologist")
public class Psychologist {
	@Id
	@Column(name = "username", length = 16)
	private String username;

	@OneToOne(cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private Login login;

	@Column(name = "title", length = 10)
	private String title;

	@Column(name = "fristname", length = 30)
	private String fristname;

	@Column(name = "lastname", length = 30)
	private String lastname;

	@Column(name = "gender", length = 10)
	private String gender;
	
	@Column(name = "birthday")
	private Calendar birthday;

	@Column(name = "phone", length = 10)
	private String phone;

	@Column(name = "email", length = 50)
	private String email;

	@Column(name = "address", length = 225)
	private String address;

	@Column(name = "img_p", length = 50)
	private String img_p;

	@Column(name = "work_history", length = 225)
	private String work_history;

	@Column(name = "mentor_type", length = 225)
	private String mentor_type;

	@Column(name = "certificate", length = 50)
	private String certificate;

	@Column(name = "goodness_score", length = 5)
	private double goodness_score;

	@Column(name = "educationl_history", length = 225)
	private String educationl_history;
	
	@Column(name = "passwords", length = 16)
	private String passwords;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "pk.psychologist")
	private List<PsychologistExpertise> px = new Vector<PsychologistExpertise>();
	
	 @OneToMany(cascade = CascadeType.ALL, mappedBy="pwkey.psychologist") 
	 private List<SetWorkingTime> working = new Vector<SetWorkingTime>();

	public Psychologist() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Psychologist(String username, String title, String fristname, String lastname, String gender,
			Calendar birthday, String phone, String email, String address,String educationl_history, 
			String work_history,String mentor_type, String certificate, String img_p, 
			double goodness_score,String passwords) {
		super();
		this.username = username;
		this.title = title;
		this.fristname = fristname;
		this.lastname = lastname;
		this.gender = gender;
		this.birthday = birthday;
		this.phone = phone;
		this.email = email;
		this.address = address;
		this.img_p = img_p;
		this.work_history = work_history;
		this.mentor_type = mentor_type;
		this.certificate = certificate;
		this.goodness_score = goodness_score;
		this.educationl_history = educationl_history;
		this.passwords = passwords;
	}

	public String getPasswords() {
		return passwords;
	}

	public void setPasswords(String passwords) {
		this.passwords = passwords;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFristname() {
		return fristname;
	}

	public void setFristname(String fristname) {
		this.fristname = fristname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Calendar getBirthday() {
		return birthday;
	}

	public void setBirthday(Calendar birthday) {
		this.birthday = birthday;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getImg_p() {
		return img_p;
	}

	public void setImg_p(String img_p) {
		this.img_p = img_p;
	}

	public String getWork_history() {
		return work_history;
	}

	public void setWork_history(String work_history) {
		this.work_history = work_history;
	}

	public String getMentor_type() {
		return mentor_type;
	}

	public void setMentor_type(String mentor_type) {
		this.mentor_type = mentor_type;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public double getGoodness_score() {
		return goodness_score;
	}

	public void setGoodness_score(double goodness_score) {
		this.goodness_score = goodness_score;
	}

	public String getEducationl_history() {
		return educationl_history;
	}

	public void setEducationl_history(String educationl_history) {
		this.educationl_history = educationl_history;
	}

	public List<PsychologistExpertise> getPx() {
		return px;
	}

	public void setPx(List<PsychologistExpertise> px) {
		this.px = px;
	}

	public List<SetWorkingTime> getWorking() {
		return working;
	}

	public void setWorking(List<SetWorkingTime> working) {
		this.working = working;
	}
	 
	 

}
