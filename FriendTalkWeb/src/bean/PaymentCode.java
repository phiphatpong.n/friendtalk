package bean;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "payment_code")
public class PaymentCode {
	@Id
	@Column(name = "code", length = 10 , unique = false)
	private String code ;
	
	@Column(name = "issue_date")
	private Date issue_date ;
	
	@Column(name = "chat_minute", length = 16)
	private String chat_minute ;

	@Column(name = "call_minute", length = 16)
	private String call_minute ;
	
	@Column(name = "usage_status", length = 16)
	private String usage_status ;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="username", nullable = false)
	private Organization organization ;
	
	

	public PaymentCode() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getUsage_status() {
		return usage_status;
	}

	public PaymentCode(String code, Date issue_date, String chat_minute, String call_minute, String usage_status) {
		super();
		this.code = code;
		this.issue_date = issue_date;
		this.chat_minute = chat_minute;
		this.call_minute = call_minute;
		this.usage_status = usage_status;
	}


	public void setUsage_status(String usage_status) {
		this.usage_status = usage_status;
	}


	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public Date getIssue_date() {
		return issue_date;
	}



	public void setIssue_date(Date issue_date) {
		this.issue_date = issue_date;
	}



	public String getChat_minute() {
		return chat_minute;
	}



	public void setChat_minute(String chat_minute) {
		this.chat_minute = chat_minute;
	}



	public String getCall_minute() {
		return call_minute;
	}



	public void setCall_minute(String call_minute) {
		this.call_minute = call_minute;
	}



	public Organization getOrganization() {
		return organization;
	}



	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	
}
