package bean;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "stress_test")
public class StressTest {
	@Id
	@Column(name = "stress_id" , length = 20)
	private int stress_id;
	
	@Column(name = "test_date")
	private Date test_date;
	
	@Column(name = "total_score" , length = 20)
	private int total_score;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="username" ,  nullable = false )
	private Consultant consultant ;

	public StressTest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StressTest(int stress_id, Date test_date, int total_score) {
		super();
		this.stress_id = stress_id;
		this.test_date = test_date;
		this.total_score = total_score;
	}

	public int getStress_id() {
		return stress_id;
	}

	public void setStress_id(int stress_id) {
		this.stress_id = stress_id;
	}

	public Date getTest_date() {
		return test_date;
	}

	public void setTest_date(Date test_date) {
		this.test_date = test_date;
	}

	public int getTotal_score() {
		return total_score;
	}

	public void setTotal_score(int total_score) {
		this.total_score = total_score;
	}

	public Consultant getConsultant() {
		return consultant;
	}

	public void setConsultant(Consultant consultant) {
		this.consultant = consultant;
	}
	


}
