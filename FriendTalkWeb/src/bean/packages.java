package bean;

import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "packages")
public class packages {
	@Id
	@Column(name = "package_size", length = 16, nullable = false)
	private String package_size;
	
	@Column(name = "price", length = 10)
	private double price;
	
	@Column(name = "chat_hr", length = 20)
	private int chat_hr;
	
	@Column(name = "call_hr", length = 20)
	private int call_hr;

	public packages() {
		super();
		// TODO Auto-generated constructor stub
	}

	public packages(String package_size, double price, int chat_hr, int call_hr) {
		super();
		this.package_size = package_size;
		this.price = price;
		this.chat_hr = chat_hr;
		this.call_hr = call_hr;
	}

	public String getPackage_size() {
		return package_size;
	}

	public void setPackage_size(String package_size) {
		this.package_size = package_size;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getChat_hr() {
		return chat_hr;
	}

	public void setChat_hr(int chat_hr) {
		this.chat_hr = chat_hr;
	}

	public int getCall_hr() {
		return call_hr;
	}

	public void setCall_hr(int call_hr) {
		this.call_hr = call_hr;
	}
	

}
