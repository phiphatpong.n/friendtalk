package bean;

import javax.persistence.*;

@Entity
@Table(name="psychologist_expertise")
public class PsychologistExpertise {
	@EmbeddedId
	private PsychologistExpertiseKey pk;
 
	public PsychologistExpertise() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PsychologistExpertise(PsychologistExpertiseKey pk) {
		super();
		this.pk = pk;
	}

	public PsychologistExpertiseKey getPk() {
		return pk;
	}

	public void setPk(PsychologistExpertiseKey pk) {
		this.pk = pk;
	}
	
	

}
