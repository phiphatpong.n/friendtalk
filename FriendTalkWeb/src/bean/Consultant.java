package bean;

import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Entity
@Table(name = "consultant")
public class Consultant {
	@Id
	@Column(name = "username", length = 16)
	private String username;

	@OneToOne(cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private Login login;

	@Column(name = "title", length = 10)
	private String title;

	@Column(name = "fristname", length = 30)
	private String fristname;

	@Column(name = "lastname", length = 30)
	private String lastname;

	@Column(name = "gender", length = 15)
	private String gender;
	
	@Column(name = "birthday")
	private Calendar birthday;

	@Column(name = "phone", length = 10)
	private String phone;

	@Column(name = "email", length = 50)
	private String email;

	@Column(name = "address", length = 225)
	private String address;

	@Column(name = "food_allergy", length = 225)
	private String food_allergy;

	@Column(name = "drug_allergy", length = 225)
	private String drug_allergy;

	@Column(name = "congenital_disease", length = 225)
	private String congenital_disease;

	@Column(name = "img_c", length = 50)
	private String img_c;


	public Consultant() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Consultant(String username, String title, String fristname, String lastname, String gender,
			Calendar birthday, String phone, String email, String address, String food_allergy, String drug_allergy,
			String congenital_disease, String img_c) {
		super();
		this.username = username;
		this.title = title;
		this.fristname = fristname;
		this.lastname = lastname;
		this.gender = gender;
		this.birthday = birthday;
		this.phone = phone;
		this.email = email;
		this.address = address;
		this.food_allergy = food_allergy;
		this.drug_allergy = drug_allergy;
		this.congenital_disease = congenital_disease;
		this.img_c = img_c;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public Login getLogin() {
		return login;
	}


	public void setLogin(Login login) {
		this.login = login;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getFristname() {
		return fristname;
	}


	public void setFristname(String fristname) {
		this.fristname = fristname;
	}


	public String getLastname() {
		return lastname;
	}


	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public Calendar getBirthday() {
		return birthday;
	}


	public void setBirthday(Calendar birthday) {
		this.birthday = birthday;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getFood_allergy() {
		return food_allergy;
	}


	public void setFood_allergy(String food_allergy) {
		this.food_allergy = food_allergy;
	}


	public String getDrug_allergy() {
		return drug_allergy;
	}


	public void setDrug_allergy(String drug_allergy) {
		this.drug_allergy = drug_allergy;
	}


	public String getCongenital_disease() {
		return congenital_disease;
	}


	public void setCongenital_disease(String congenital_disease) {
		this.congenital_disease = congenital_disease;
	}


	public String getImg_c() {
		return img_c;
	}


	public void setImg_c(String img_c) {
		this.img_c = img_c;
	}




}
