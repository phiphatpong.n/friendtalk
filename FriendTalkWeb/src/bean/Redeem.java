package bean;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "redeem")
public class Redeem {
	@Id
	@Column(name = "redeem_id", length = 20 , nullable = false)
	private String redeem_id;
	
	@Column(name = "redeem_date")
	private Date redeem_date;
	
	@Column(name = "delivery_status")
	private String  delivery_status;
	
	@Column(name = "redeem_score", length = 16)
	private int redeem_score;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="username" , nullable = false)
	private Psychologist psychologist ;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="award_id", nullable = false)
	private Award award ;
	
	public Redeem() {
		super();
		// TODO Auto-generated constructor stub
	}
	


	public Redeem(String redeem_id, Date redeem_date, String delivery_status, int redeem_score) {
		super();
		this.redeem_id = redeem_id;
		this.redeem_date = redeem_date;
		this.delivery_status = delivery_status;
		this.redeem_score = redeem_score;
	}



	public String getDelivery_status() {
		return delivery_status;
	}

	public void setDelivery_status(String delivery_status) {
		this.delivery_status = delivery_status;
	}

	public String getRedeem_id() {
		return redeem_id;
	}

	public void setRedeem_id(String redeem_id) {
		this.redeem_id = redeem_id;
	}

	public Date getRedeem_date() {
		return redeem_date;
	}

	public void setRedeem_date(Date redeem_date) {
		this.redeem_date = redeem_date;
	}

	public int getRedeem_score() {
		return redeem_score;
	}

	public void setRedeem_score(int redeem_score) {
		this.redeem_score = redeem_score;
	}

	public Psychologist getPsychologist() {
		return psychologist;
	}

	public void setPsychologist(Psychologist psychologist) {
		this.psychologist = psychologist;
	}

	public Award getAward() {
		return award;
	}

	public void setAward(Award award) {
		this.award = award;
	}
	
}
