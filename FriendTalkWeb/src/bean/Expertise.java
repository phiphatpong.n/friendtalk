package bean;

import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="expertise")
public class Expertise {
	@Id
	@Column(name = "expertise_id", length = 150)
	private int expertise_id ;

	@Column(name = "expertise_type", length = 150)
	private String expertise_type ;
	
	@OneToMany(cascade = CascadeType.ALL ,mappedBy="pk.expertise")
	private List<PsychologistExpertise> px= new Vector<PsychologistExpertise>(); 

	public Expertise() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Expertise( String expertise_type) {
		super();
		this.expertise_type = expertise_type;
	}

	public String getExpertise_type() {
		return expertise_type;
	}

	public void setExpertise_type(String expertise_type) {
		this.expertise_type = expertise_type;
	}

	public List<PsychologistExpertise> getPx() {
		return px;
	}

	public void setPx(List<PsychologistExpertise> px) {
		this.px = px;
	}
	public int getExpertise_id() {
		return expertise_id;
	}

	public void setExpertise_id(int expertise_id) {
		this.expertise_id = expertise_id;
	}

}
