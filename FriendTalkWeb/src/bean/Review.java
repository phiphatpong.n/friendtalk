package bean;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "review")
public class Review {
	@Id
	@Column(name = "review_id" , length = 20 , nullable = false)
	private String review_id; 
	
	@Column(name = "review_date"  )
	private Date review_date ;
	
	@Column(name = "comment" , length = 225 )
	private String comment ;
	
	@Column(name = "satisfaction_score" , length = 20 )
	private double satisfaction_score ;
	
	public Review() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Review(String review_id, Date review_date, String comment, double satisfaction_score) {
		super();
		this.review_id = review_id;
		this.review_date = review_date;
		this.comment = comment;
		this.satisfaction_score = satisfaction_score;
	}


	public String getReview_id() {
		return review_id;
	}


	public void setReview_id(String review_id) {
		this.review_id = review_id;
	}


	public Date getReview_date() {
		return review_date;
	}


	public void setReview_date(Date review_date) {
		this.review_date = review_date;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	public double getSatisfaction_score() {
		return satisfaction_score;
	}


	public void setSatisfaction_score(double satisfaction_score) {
		this.satisfaction_score = satisfaction_score;
	}
	

}
