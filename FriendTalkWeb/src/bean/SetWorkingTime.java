package bean;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "set_working_time")
public class SetWorkingTime {
	
	 //@Id
	 //@Column(name = "working_date")
	 //private String working_date;
	 
	 
	@EmbeddedId
	private PsychologistWorkingKey pwkey ; 

	
	@Column(name = "time_start" , length = 20)
	private String time_start;
	
	@Column(name = "time_end", length = 20)
	private String time_end;

	public SetWorkingTime() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SetWorkingTime(PsychologistWorkingKey pwkey, String time_start, String time_end) {
		super();
		this.pwkey = pwkey;
		this.time_start = time_start;
		this.time_end = time_end;
	}

	public PsychologistWorkingKey getPwkey() {
		return pwkey;
	}

	public void setPwkey(PsychologistWorkingKey pwkey) {
		this.pwkey = pwkey;
	}

	public String getTime_start() {
		return time_start;
	}

	public void setTime_start(String time_start) {
		this.time_start = time_start;
	}

	public String getTime_end() {
		return time_end;
	}

	public void setTime_end(String time_end) {
		this.time_end = time_end;
	}
	

}
