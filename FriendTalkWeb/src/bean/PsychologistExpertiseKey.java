package bean;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Embeddable
public class PsychologistExpertiseKey implements  Serializable {
	
	@ManyToOne
	protected  Psychologist psychologist;
	@ManyToOne
	protected  Expertise expertise;
	
	public PsychologistExpertiseKey() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PsychologistExpertiseKey(bean.Psychologist psychologist, Expertise expertise) {
		super();
		this.psychologist = psychologist;
		this.expertise = expertise;
	}

	public Psychologist getPsychologist() {
		return psychologist;
	}

	public void setPsychologist(Psychologist psychologist) {
		this.psychologist = psychologist;
	}

	public Expertise getExpertise() {
		return expertise;
	}

	public void setExpertise(Expertise expertise) {
		this.expertise = expertise;
	}


	

}
