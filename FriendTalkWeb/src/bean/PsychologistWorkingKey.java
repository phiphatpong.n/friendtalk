package bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Embeddable
public class PsychologistWorkingKey implements  Serializable {
	
	@ManyToOne
	@JoinColumn(name="username")
	private Psychologist psychologist ;
	
	@Column(name = "working_date")
	private String working_date;

	public PsychologistWorkingKey() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PsychologistWorkingKey(Psychologist psychologist, String working_date) {
		super();
		this.psychologist = psychologist;
		this.working_date = working_date;
	}

	public Psychologist getPsychologist() {
		return psychologist;
	}

	public void setPsychologist(Psychologist psychologist) {
		this.psychologist = psychologist;
	}

	public String getWorking_date() {
		return working_date;
	}

	public void setWorking_date(String working_date) {
		this.working_date = working_date;
	}
	
	
	

}
