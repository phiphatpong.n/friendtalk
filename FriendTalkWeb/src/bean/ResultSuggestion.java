package bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "result_suggestion")
public class ResultSuggestion {
	@Id
	@Column(name = "result_id", length = 16)
	private int result_id;
	
	@Column(name = "max_score", length = 10)
	private int max_score;
	
	@Column(name = "min_score", length = 10)
	private int min_score;
	
	@Column(name = "result_suggestion", length = 100)
	private String result_suggestion;

	
	
	public ResultSuggestion() {
		super();
		// TODO Auto-generated constructor stub
	}



	public ResultSuggestion(int result_id, int max_score, int min_score, String result_suggestion) {
		super();
		this.result_id = result_id;
		this.max_score = max_score;
		this.min_score = min_score;
		this.result_suggestion = result_suggestion;
	}



	public int getResult_id() {
		return result_id;
	}



	public void setResult_id(int result_id) {
		this.result_id = result_id;
	}



	public int getMax_score() {
		return max_score;
	}



	public void setMax_score(int max_score) {
		this.max_score = max_score;
	}



	public int getMin_score() {
		return min_score;
	}



	public void setMin_score(int min_score) {
		this.min_score = min_score;
	}



	public String getResult_suggestion() {
		return result_suggestion;
	}



	public void setResult_suggestion(String result_suggestion) {
		this.result_suggestion = result_suggestion;
	}
	
}
