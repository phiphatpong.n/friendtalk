package bean;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "payment")
public class Payment {
	@Id
	@Column(name = "payment_id", length = 12 , unique = false)
	private String payment_id ;
	
	@Column(name = "amount", length = 12 )
	private int amount ;
	
	@Column(name = "payment_date" )
	private Date payment_date ;

	@Column(name = "receipt_status", length = 20 )
	private int receipt_status ;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="advice_id" , nullable = false, unique = false )
	private Advice advice ;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="code", unique = false)
	private PaymentCode paymentCode ; 
	
	public Payment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Payment(String payment_id, int amount, Date payment_date, int receipt_status) {
		super();
		this.payment_id = payment_id;
		this.amount = amount;
		this.payment_date = payment_date;
		this.receipt_status = receipt_status;
	}

	public String getPayment_id() {
		return payment_id;
	}

	public void setPayment_id(String payment_id) {
		this.payment_id = payment_id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Date getPayment_date() {
		return payment_date;
	}

	public void setPayment_date(Date payment_date) {
		this.payment_date = payment_date;
	}

	public int getReceipt_status() {
		return receipt_status;
	}

	public void setReceipt_status(int receipt_status) {
		this.receipt_status = receipt_status;
	}

	public Advice getAdvice() {
		return advice;
	}

	public void setAdvice(Advice advice) {
		this.advice = advice;
	}

	public PaymentCode getPaymentCode() {
		return paymentCode;
	}

	public void setPaymentCode(PaymentCode paymentCode) {
		this.paymentCode = paymentCode;
	}
	
}
