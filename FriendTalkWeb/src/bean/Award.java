package bean;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "award")
public class Award {
	@Id
	@Column(name = "award_id", length = 20, nullable = false)
	private String award_id ;
	
	@Column(name = "award_name", length = 50)
	private String award_name ;
	
	@Column(name = "award_detail", length = 225)
	private String award_detail ;
	
	@Column(name = "score", length = 10)
	private int score ;
	
	@Column(name = "img_award", length = 50)
	private String img_award;
	
	@Column(name = "available_from")
	private Calendar available_from ;
	
	@Column(name = "expiration_date")
	private Calendar expiration_date ;
	
	
	public Award() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Award(String award_id, String award_name, String award_detail, int score, String img_award,
			Calendar available_from, Calendar expiration_date) {
		super();
		this.award_id = award_id;
		this.award_name = award_name;
		this.award_detail = award_detail;
		this.score = score;
		this.img_award = img_award;
		this.available_from = available_from;
		this.expiration_date = expiration_date;
	}


	public Calendar getAvailable_from() {
		return available_from;
	}


	public void setAvailable_from(Calendar available_from) {
		this.available_from = available_from;
	}


	public Calendar getExpiration_date() {
		return expiration_date;
	}


	public void setExpiration_date(Calendar expiration_date) {
		this.expiration_date = expiration_date;
	}


	public String getAward_id() {
		return award_id;
	}


	public void setAward_id(String award_id) {
		this.award_id = award_id;
	}


	public String getAward_name() {
		return award_name;
	}


	public void setAward_name(String award_name) {
		this.award_name = award_name;
	}


	public String getAward_detail() {
		return award_detail;
	}


	public void setAward_detail(String award_detail) {
		this.award_detail = award_detail;
	}


	public int getScore() {
		return score;
	}


	public void setScore(int score) {
		this.score = score;
	}


	public String getImg_award() {
		return img_award;
	}


	public void setImg_award(String img_award) {
		this.img_award = img_award;
	}
	

}
