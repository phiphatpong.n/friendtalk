package bean;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="article")
public class Article {
	@Id
	@Column(name="article_id", nullable=false)
	private String article_id;
	
	
	@Column(name="article_name", nullable=false)
	private String article_name;
	
	@Column(name="article_detail", nullable=false)
	private String article_detail;
	
	@Column(name="add_date")
	private Calendar add_date;
	
	@Column(name = "img_articles", length = 100)
	private String img_articles;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="username" , nullable = false)
	private Psychologist psychologist ;

	public Article() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Article(String article_id, String article_name, String article_detail, Calendar add_date,
			String img_articles) {
		super();
		this.article_id = article_id;
		this.article_name = article_name;
		this.article_detail = article_detail;
		this.add_date = add_date;
		this.img_articles = img_articles;
	}

	public String getArticle_id() {
		return article_id;
	}

	public void setArticle_id(String article_id) {
		this.article_id = article_id;
	}

	public String getArticle_name() {
		return article_name;
	}

	public void setArticle_name(String article_name) {
		this.article_name = article_name;
	}

	public String getArticle_detail() {
		return article_detail;
	}

	public void setArticle_detail(String article_detail) {
		this.article_detail = article_detail;
	}

	public Calendar getAdd_date() {
		return add_date;
	}

	public void setAdd_date(Calendar add_date) {
		this.add_date = add_date;
	}

	public String getImg_articles() {
		return img_articles;
	}

	public void setImg_articles(String img_articles) {
		this.img_articles = img_articles;
	}

	public Psychologist getPsychologist() {
		return psychologist;
	}

	public void setPsychologist(Psychologist psychologist) {
		this.psychologist = psychologist;
	}

}
