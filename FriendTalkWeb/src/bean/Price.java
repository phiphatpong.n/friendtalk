package bean;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "price")
public class Price {
	@Id
	@Column(name = "contact_type", length = 20)
	private String contact_type;
	
	@Column(name = "price", length = 10)
	private double price;
	
	@Column(name = "total_minute", length = 10)
	private int total_minute;

	public Price() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Price(String contact_type, double price, int total_minute) {
		super();
		this.contact_type = contact_type;
		this.price = price;
		this.total_minute = total_minute;
	}

	public String getContact_type() {
		return contact_type;
	}

	public void setContact_type(String contact_type) {
		this.contact_type = contact_type;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getTotal_minute() {
		return total_minute;
	}

	public void setTotal_minute(int total_minute) {
		this.total_minute = total_minute;
	}
	
}
