package bean;

import java.util.List;
import java.util.Vector;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "organization")
public class Organization {
	@Id
	@Column(name = "username", length = 16)
	private String username;
	
	@OneToOne(cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private Login login;
	
	@Column(name = "Company_name", length = 50)
	private String Companyname;
	
	@Column(name = "phone", length = 10)
	private String phone;

	@Column(name = "email", length = 50)
	private String email;

	@Column(name = "address", length = 225)
	private String address;
	
	@Column(name = "employee_number", length = 60)
	private String employee_number ;
	
	@Column(name = "Company_registration_certificate", length = 20)
	private String Company_registration_certificate ;
	
	@Column(name = "total_chat", length = 20)
	private int total_chat ;
	
	@Column(name = "total_call", length = 20)
	private int total_call ;
	
	@Column(name = "minute_code_call", length = 16)
	private int minute_code_call ;
	
	@Column(name = "minute_code_chat", length = 16)
	private int minute_code_chat ;
	/*
	@OneToMany(cascade = CascadeType.ALL )
	@JoinColumn(name="buy_package_history_id")
	List<BuyPackageHistory> buy_package_history = new Vector<>() ;
	 */
	public Organization() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Organization(String username, String companyname, String phone, String email, String address,
			String employee_number, String company_registration_certificate, int total_chat, int total_call,
			int minute_code_call, int minute_code_chat) {
		super();
		this.username = username;
		this.Companyname = companyname;
		this.phone = phone;
		this.email = email;
		this.address = address;
		this.employee_number = employee_number;
		this.Company_registration_certificate = company_registration_certificate;
		this.total_chat = total_chat;
		this.total_call = total_call;
		this.minute_code_call = minute_code_call;
		this.minute_code_chat = minute_code_chat;
	}

	public int getMinute_code_call() {
		return minute_code_call;
	}

	public void setMinute_code_call(int minute_code_call) {
		this.minute_code_call = minute_code_call;
	}

	public int getMinute_code_chat() {
		return minute_code_chat;
	}

	public void setMinute_code_chat(int minute_code_chat) {
		this.minute_code_chat = minute_code_chat;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Login getLogin() {
		return login;
	}
	public void setLogin(Login login) {
		this.login = login;
	}
	public String getCompanyname() {
		return Companyname;
	}
	public void setCompanyname(String companyname) {
		Companyname = companyname;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmployee_number() {
		return employee_number;
	}
	public void setEmployee_number(String employee_number) {
		this.employee_number = employee_number;
	}
	public String getCompany_registration_certificate() {
		return Company_registration_certificate;
	}
	public void setCompany_registration_certificate(String company_registration_certificate) {
		this.Company_registration_certificate = company_registration_certificate;
	}
	public int getTotal_chat() {
		return total_chat;
	}
	public void setTotal_chat(int total_chat) {
		this.total_chat = total_chat;
	}
	public int getTotal_call() {
		return total_call;
	}
	public void setTotal_call(int total_call) {
		this.total_call = total_call;
	}
}
