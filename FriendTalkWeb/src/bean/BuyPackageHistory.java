package bean;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "buy_package_history")
public class BuyPackageHistory {
	@Id
	@Column(name = "buy_package_history_id", length = 16 , nullable = false)
	private String buy_package_history_id;
	
	@Column(name = "buy_date")
	private Date buy_date;
	
	@Column(name = "chat_hr", length = 20 )
	private int chat_hr;
	
	@Column(name = "call_hr", length = 20)
	private int call_hr;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="username" , nullable = false)
	private Organization organization ;
	
	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public BuyPackageHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BuyPackageHistory(String buy_package_history_id, Date buy_date, int chat_hr, int call_hr) {
		super();
		this.buy_package_history_id = buy_package_history_id;
		this.buy_date = buy_date;
		this.chat_hr = chat_hr;
		this.call_hr = call_hr;
	}

	public String getBuy_package_history_id() {
		return buy_package_history_id;
	}

	public void setBuy_package_history_id(String buy_package_history_id) {
		this.buy_package_history_id = buy_package_history_id;
	}

	public Date getBuy_date() {
		return buy_date;
	}

	public void setBuy_date(Date buy_date) {
		this.buy_date = buy_date;
	}

	public int getChat_hr() {
		return chat_hr;
	}

	public void setChat_hr(int chat_hr) {
		this.chat_hr = chat_hr;
	}

	public int getCall_hr() {
		return call_hr;
	}

	public void setCall_hr(int call_hr) {
		this.call_hr = call_hr;
	}
	

}
