package conn;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import bean.*;

public class HibernateConnection {
	public static SessionFactory sessionFactory;
	static String url = "jdbc:mysql://localhost:3306/friendTalk?characterEncoding=UTF-8"; 
	static String uname = "root";
	static String pwd = "6126";
	  
	public static SessionFactory doHibernateConnection(){
		Properties database = new Properties();
		//database.setProperty("hibernate.hbm2ddl.auto", "create");
		database.setProperty("hibernate.connection.driver_class","com.mysql.jdbc.Driver");
		database.setProperty("hibernate.connection.username",uname);
		database.setProperty("hibernate.connection.password",pwd);
		database.setProperty("hibernate.connection.url",url);
		database.setProperty("hibernate.dialect","org.hibernate.dialect.MySQL5InnoDBDialect");
		Configuration cfg = new Configuration()
							.setProperties(database)
							.addPackage("bean")
							.addAnnotatedClass(Advice.class)
							.addAnnotatedClass(Award.class)
							.addAnnotatedClass(BuyPackageHistory.class)
							.addAnnotatedClass(Consultant.class)
							.addAnnotatedClass(MakeAppointment.class)
							.addAnnotatedClass(Login.class)
							.addAnnotatedClass(Payment.class)
							.addAnnotatedClass(PaymentCode.class)
							.addAnnotatedClass(Price.class)
							.addAnnotatedClass(Psychologist.class)
							.addAnnotatedClass(Redeem.class)
							.addAnnotatedClass(ResultSuggestion.class)
							.addAnnotatedClass(Review.class)
							.addAnnotatedClass(SetWorkingTime.class)
							.addAnnotatedClass(StressQuestion.class)
							.addAnnotatedClass(StressTest.class)
							.addAnnotatedClass(Expertise.class)
							.addAnnotatedClass(PsychologistExpertise.class)
							.addAnnotatedClass(PsychologistExpertiseKey.class)
							.addAnnotatedClass(packages.class)
							.addAnnotatedClass(Organization.class)
							.addAnnotatedClass(Article.class)
							.addAnnotatedClass(PsychologistWorkingKey.class)
							.addAnnotatedClass(User.class);
		StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties());
		sessionFactory = cfg.buildSessionFactory(ssrb.build());
		return sessionFactory;
	}
}
