package controllerreact;
import managerreact.SearchPsychologistManager;
import managerreact.loginmanager;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import conn.ResponseObj;
import bean.*;

import java.text.ParseException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

@Controller
public class logincontroller {
	
	private static String SALT = "123456";

	//---------------------Login member----------------------------------------------------------------------------------
	@RequestMapping(value = "/member/signin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody ResponseObj do_login(@RequestParam  Map<String, String> map) {
		Login result = null;
		Login login = null;
//		List<Psychologist> Listpsy = new ArrayList<Psychologist>();
	//	System.out.println(message);
		try {
			String username = map.get("user");
			String password = map.get("pass");
		//	String type = map.get("type");
			
		//	password = PasswordUtil.getInstance().createPassword(password, SALT);
			
			login = new Login(username,password,"");
			loginmanager rm = new loginmanager();
			result = rm.doHibernateLogin(login);
			System.out.println("Code :: "+ result);
//			SearchPsychologistManager sp = new SearchPsychologistManager();
////			Listpsy = sp.getlistPsychologist();
//			Psychologist psy1 = new Psychologist();
//			psy1.setFristname("Phichet");
//			Psychologist psy2 = new Psychologist();
//			psy2.setFristname("PhichetManori");
//			Listpsy.add(psy1);
//			Listpsy.add(psy2);
//			System.out.println(" "+Listpsy.get(0).getFristname());
			if (result != null) {
				return new ResponseObj(200, result);	
			}else {
				return new ResponseObj(200, null);
			}
			
		} catch (Exception e) { 
			e.printStackTrace();
			return new ResponseObj(500, "0");
		}
	}
	


}
