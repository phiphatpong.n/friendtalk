package controllerreact;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import bean.Psychologist;
import conn.ResponseObj;
import managerreact.SearchPsychologistManager;

@Controller
public class SearchPsychologist {

	private static String SALT = "123456";

//-------------------------------------------------------------------------------------------------------------------------------

	@RequestMapping(value = "/dolistpsy", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody ResponseObj do_Listpsy(@RequestParam Map<String, String> map) {
		List<Psychologist> Listpsy = null;
		System.out.println(map);
		try {
			SearchPsychologistManager sp = new SearchPsychologistManager();
			Listpsy = sp.getlistPsy();
			System.out.println("" + Listpsy.size());
			return new ResponseObj(200, Listpsy);

		} catch (Exception e) {
			System.out.println("Test");
			return new ResponseObj(500, null);
		}
	}

//	@RequestMapping(value = "/dolistpsy", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
//	@ResponseStatus(value = HttpStatus.OK)
//	public @ResponseBody ResponseObj App_getTypechallenge(@RequestParam Map<String, String> map) throws ParseException {
//		List<Psychologist> Listpsy = null;
//	System.out.println(map);
//	try {
//	
//		SearchPsychologistManager sp = new SearchPsychologistManager();
//		Listpsy = sp.getlistPsychologist();
//		for(int i=0;i<Listpsy.size();i++) {
//			System.out.println(" "+(i+1)+" :: "+Listpsy.get(i).getFristname());
//		}
//
//	return new ResponseObj(200,Listpsy);
//
//	} catch (Exception e) {
//		System.out.println("Test");
//	return new ResponseObj(500, null);
//	}
//	}
}
