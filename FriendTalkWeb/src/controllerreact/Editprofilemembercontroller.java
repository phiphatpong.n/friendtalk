package controllerreact;

import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import conn.ResponseObj;
import bean.*;

import java.text.SimpleDateFormat;
import java.util.*;
import managerreact.EditProfilemembermanager;
import javax.servlet.http.HttpServletRequest;
@Controller
public class Editprofilemembercontroller {
	
	private static String SALT = "123456";
	
	@RequestMapping(value = "/member/profile", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody ResponseObj do_profile(@RequestParam  Map<String, String> map) {
		Consultant user = null;
		System.out.println("mapgetpro:: "+map);
		try {
			String id = map.get("id");
			
			EditProfilemembermanager rm = new EditProfilemembermanager();
			user = rm.getProfileconsultant(id);
			System.out.println("Fname :: "+user.getFristname());
			
			if (user != null) {
				return new ResponseObj(200, user);
			}else {
				return new ResponseObj(200, null);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseObj(500, null);
		}
	}
	
	@RequestMapping(value = "/member/editprofile", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody ResponseObj do_editprofile(@RequestParam  Map<String, String> map) {
		Consultant member = null;
	
		System.out.println("map:: "+map);
		try {
			String id = map.get("username");
			String title = map.get("title");
			String fname = map.get("fristname");
			String lname = map.get("lastname");
			String gender = map.get("gender");
			String birthday = map.get("birthday");
			String phone = map.get("phone");
			String email = map.get("email");
			String address = map.get("address");
			String food_allergy = map.get("food_allergy");
			String drug_allergy = map.get("drug_allergy");
			String congenital_disease = map.get("congenital_disease");
			String username = map.get("username");
			String password = map.get("password");
			
			
			SimpleDateFormat dfm = new SimpleDateFormat("DD-MM-YYYY");
			Calendar cal = Calendar.getInstance();
			
			Date date = dfm.parse(birthday);
			cal.setTime(date);
			
			System.out.println("cal ::::::::  "+cal);
			member = new Consultant();
			member.setTitle(title);
			member.setFristname(fname);
			member.setLastname(lname);
			member.setGender(gender);
			member.setBirthday(cal);
			member.setPhone(phone);
			member.setEmail(email);
			member.setAddress(address);
			member.setFood_allergy(food_allergy);
			member.setDrug_allergy(drug_allergy);
			member.setCongenital_disease(congenital_disease);
			member.setUsername(id);
			member.getLogin().setUsername(username);
			member.getLogin().setPassword(password);
			member.getLogin().setType("consultant");
			
			
			EditProfilemembermanager rm = new EditProfilemembermanager();
			int result = rm.Editconsultant(member);
			
			
				return new ResponseObj(200, result);
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseObj(500, null);
		}
	}


}
