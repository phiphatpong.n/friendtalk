package managerreact;

import bean.*;
import conn.HibernateConnection;
import controllerreact.ConnectionDB;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*; 
public class EditProfilemembermanager {
	
	private static String SALT = "123456";
	
	public Consultant getProfileconsultant(String id) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
			Session session = sessionFactory.openSession();
			
			session.beginTransaction();
			List<Consultant> users = session.createQuery("From Consultant where username = '" + id+"'").list();
			session.close();
			
		return users.get(0);
			
		} catch (Exception e) {
			return null;
		}
	}
	
	
	public int Editconsultant (Consultant user) {
		try {
			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();

			Session session = sessionFactory.openSession();
			Transaction t = session.beginTransaction();
			session.update(user);
			t.commit();
			session.close();
			return 1;
		} catch (Exception e) {
			return 0;
		}
	}
	
	
	
	public int updateProfile(Consultant c) {
		ConnectionDB conDB = new ConnectionDB();
		Connection con = conDB.getConnectionDB();
		int result = 0;
		
		try {
			
			CallableStatement stmt = con.prepareCall("{call updatemember(?,?,?,?,?,?,?,?,?,?,?,?)}");
			stmt.setString(1,c.getUsername());
			stmt.setString(2,c.getTitle());
			stmt.setString(3,c.getFristname());
			stmt.setString(4,c.getLastname());
			stmt.setString(5,c.getGender());
			((Query) stmt).setCalendar(6,c.getBirthday());
			stmt.setString(7,c.getPhone());
			stmt.setString(8,c.getEmail());
			stmt.setString(9,c.getAddress());
			stmt.setString(10,c.getFood_allergy());
			stmt.setString(11,c.getDrug_allergy());
			stmt.setString(12,c.getCongenital_disease());
			
			stmt.execute();
			result=1;
			stmt.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result ;

	}

}
