package managerreact;


import java.util.*;


import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.sql.*;
import java.sql.Date;

import bean.Psychologist;
import conn.HibernateConnection;
import controllerreact.ConnectionDB;

public class SearchPsychologistManager {

	private static String SALT = "123456";

//	public List<Psychologist> getlistPsychologist() {
//		try {
//			SessionFactory sessionFactory = HibernateConnection.doHibernateConnection();
//			Session session = sessionFactory.openSession();
//
//			session.beginTransaction();
//			List<Psychologist> psychologists = session.createQuery("From Psychologist").list();
//			session.close();
//
//			return psychologists;
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//	}

	public List<Psychologist> getlistPsy() {
		ConnectionDB conDB = new ConnectionDB();
		Connection con = conDB.getConnectionDB();
		Statement stmt = null;
		List<Psychologist> Listpsy = new Vector<>();
		Psychologist psy = new Psychologist();
		try {

			stmt = con.createStatement();
			String sql = "SELECT fristname,lastname,gender,email,birthday,address,title,mentor_type,img_p FROM psychologist";
			stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				psy = new Psychologist();
				psy.setFristname(rs.getString(1));
				psy.setLastname(rs.getString(2));
				psy.setGender(rs.getString(3));
				psy.setEmail(rs.getString(4));
				String s[] =  rs.getString(5).split(" ");
				String D[] =  s[0].split("-");
				
				System.out.println("D "+rs.getString(5).split("-"));
				Calendar c = Calendar.getInstance(); //This to obtain today's date in our Calendar var.
//				java.sql.Date date = new Date (c.getTimeInMillis());
				c.set(Integer.parseInt(D[0]),Integer.parseInt(D[1]),Integer.parseInt(D[2]));
				psy.setBirthday(c);
				psy.setAddress(rs.getString(6));
				psy.setTitle(rs.getString(7));
				psy.setMentor_type(rs.getString(8));
				psy.setImg_p(rs.getString(9));
				Listpsy.add(psy);
			}
			

			if (Listpsy != null) {
				return Listpsy;
			} else {
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
