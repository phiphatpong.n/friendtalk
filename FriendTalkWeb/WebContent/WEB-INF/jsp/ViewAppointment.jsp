<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*, java.text.*"%>
<%
	String user = (String) session.getAttribute("user");
	String type = (String) request.getAttribute("Mtype");
	List<MakeAppointment> listMA =(List<MakeAppointment>) request.getAttribute("listMA");
	List<PsychologistExpertise> listPEK =(List<PsychologistExpertise>) request.getAttribute("listPEK");
	
	List<Advice> listadvice = (List<Advice>)request.getAttribute("listadvice");
	List<Payment> listpay= (List<Payment>)request.getAttribute("listpay");
	
	String datee = "" ;
	int h = 0;
	int m = 0;
	int s = 0;
	
	int altR = -1;
	try {
		altR = (Integer) request.getAttribute("altR");
	} catch (Exception e) {
	}
	
	int altC = -1;
	try {
		altC = (Integer) request.getAttribute("altC");
	} catch (Exception e) {
	}
	
	int altCan = -1;
	try {
		altCan = (Integer) request.getAttribute("altCan");
	} catch (Exception e) {
	}
%>
<%
	if (1 == altR) {
%>
<script type="text/javascript">
	alert("บันทึกสำเร็จ");
</script>
<%
	} else if (0 == altR) {
%>
<script type="text/javascript">
	alert("บันทึกไม่สำเร็จ");
</script>
<%
	}
	if (1 == altC) {
%>
<script type="text/javascript">
	alert("ยันยันนัดสำเร็จ");
</script>
<%
	} else if (0 == altC) {
%>
<script type="text/javascript">
	alert("ยืนยันนัดไม่สำเร็จ");
</script>
<%
	}
	if (1 == altCan) {
%>
<script type="text/javascript">
	alert("ยกเลิกนัดสำเร็จ");
</script>
<%
	} else if (0 == altCan) {
%>
<script type="text/javascript">
	alert("ยกเลิกนัดไม่สำเร็จ");
</script>
<%
	}
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

<!-- ModelPopup -->
<script src="./js/PopupModel/ModelPopup.js"></script>
<link rel="stylesheet" href="./js/PopupModel/ModelPopup.css">
<script type="text/javascript">
	function getvalue(selectObject) {
		var value = selectObject.value;
			
			if(value == "เลือกสถานะ"){
				document.getElementById('dis1').style.display = "inline";
				document.getElementById('dis2').style.display = "none";
				document.getElementById('dis3').style.display = "none";
				document.getElementById('dis4').style.display = "none";
				document.getElementById('dis5').style.display = "none";
				document.getElementById('dis6').style.display = "none";
			}else if(value == "รอการยืนยัน"){
				document.getElementById('dis1').style.display = "none";
				document.getElementById('dis2').style.display = "inline";
				document.getElementById('dis3').style.display = "none";
				document.getElementById('dis4').style.display = "none";
				document.getElementById('dis5').style.display = "none";
				document.getElementById('dis6').style.display = "none";
			}else if(value == "ยืนยันการนัด"){
				document.getElementById('dis1').style.display = "none";
				document.getElementById('dis2').style.display = "none";
				document.getElementById('dis3').style.display = "inline";
				document.getElementById('dis4').style.display = "none";
				document.getElementById('dis5').style.display = "none";
				document.getElementById('dis6').style.display = "none";
			}else if(value == "ค้างชำระ"){
				document.getElementById('dis1').style.display = "none";
				document.getElementById('dis2').style.display = "none";
				document.getElementById('dis3').style.display = "none";
				document.getElementById('dis4').style.display = "inline";
				document.getElementById('dis5').style.display = "none";
				document.getElementById('dis6').style.display = "none";
			}else if(value == "ยกเลิกนัด"){
				document.getElementById('dis1').style.display = "none";
				document.getElementById('dis2').style.display = "none";
				document.getElementById('dis3').style.display = "none";
				document.getElementById('dis4').style.display = "none";
				document.getElementById('dis5').style.display = "none";
				document.getElementById('dis6').style.display = "inline";
			}else if(value == "สำเร็จ"){
				document.getElementById('dis1').style.display = "none";
				document.getElementById('dis2').style.display = "none";
				document.getElementById('dis3').style.display = "none";
				document.getElementById('dis4').style.display = "none";
				document.getElementById('dis5').style.display = "inline";
				document.getElementById('dis6').style.display = "none";
			}
		
	}

</script>
</head>
<body class="clinic_version">

	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
						
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div id="service" class="services2 wow fadeIn">
	    <div class="">
	       <div class="row center">
	          <div class="col-lg-16 col-md-16 col-sm-16 col-xs-16">
	             <div class="appointment-form">
	                <h3>ตารางนัดหมาย</h3>
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="row">
                         <div class="form-group">
							<select name="selectStatus" class="form-control" OnChange="return getvalue(this)" style="width: 100% ;">
								<option value="เลือกสถานะ">เลือกสถานะ</option>
								<option value="รอการยืนยัน">รอการยืนยัน</option>
								<option value="ยืนยันการนัด">ยืนยันการนัด</option>
								<%if(!type.equals("psychologist")){ %>
								<option value="ค้างชำระ">ค้างชำระ</option>
								<% } %>
								<option value="ยกเลิกนัด">ยกเลิกนัด</option>
								<option value="สำเร็จ">สำเร็จ</option>
							</select>
						</div>
					</div>
					</div>
					 <div id="dis1" style="display: inline;">
	                	<table border="1" class="table table-bordered   table-hover">
							<thead>
								<tr>
									<th>ลำดับ</th>
									<th>ชื่อ-นามสกุล</th>
									<th>วันเวลาที่นัด</th>
									<th>ไอดีไลน์</th>
									<th>ประเภทการให้คำปรึกษา</th>
									<th>สถานะ</th>
									<% if(type.equals("psychologist")){ %>	
									<th>ดูข้อมูล</th>
									<%} %>
									<th></th>
									<th>ยกเลิก</th>
								</tr>
							</thead>
							<tbody>
							<% if(listMA.size() >= 1){ %>
							<% for(int i=0 ; i<listMA.size() ; i++){ %>
								<tr>
								
									<% boolean a  = false ;
									if(type.equals("psychologist")){
										a = listMA.get(i).isAnonymous();
										System.out.println("anonymous ="+a);
										}%>
										
									<td><label><%= i+1 %></label></td>
									<%if(a){ %>
										<td><label>ปกปิดข้อมูล</label></td>
									<% }else{ %>
										<td><label><%=listMA.get(i).getConsultant().getTitle()+" "+ listMA.get(i).getConsultant().getFristname() +" "+ listMA.get(i).getConsultant().getLastname()%></label></td>
									<% } %>
									<%
										String ds = listMA.get(i).getTime_start().toInstant().toString().substring(0, 10);
										System.out.println(ds );
										 h = listMA.get(i).getTime_start().getTime().getHours();
										 m = listMA.get(i).getTime_start().getTime().getMinutes();
										 s = listMA.get(i).getTime_start().getTime().getSeconds();
										
										String[] d = ds.split("-");
									    int day = Integer.parseInt(d[2]);
									    int month = Integer.parseInt(d[1]);
									    int year = Integer.parseInt(d[0]);
									    
									    String time = String.valueOf(h)+":"+String.valueOf(m)+":"+String.valueOf(s);
									    DateFormat sdf = new SimpleDateFormat("HH:mm:ss");
									    Date date = sdf.parse(time);
									    
									    //datee
									    String subtime = date.toString() ;
									    String strT1 = subtime.replaceAll("Thu Jan 01 ", "");
									    String strT2 = strT1.replaceAll("ICT 1970", "");
									    
									    String appdate = ds +" "+ strT2;
									    System.out.println(ds +" "+ strT2 );
									    System.out.println(type );
									%>
									
									<td><label><%= appdate %></label></td>
									<td><label><%= listMA.get(i).getConsultant().getPhone() %></label></td>
									<td><label><%= listMA.get(i).getPrice().getContact_type() %></label></td>
									<td><label><%= listMA.get(i).getStatus_appoint() %></label></td>
									
									<% if(type.equals("psychologist")){
										a = listMA.get(i).isAnonymous();
										System.out.println("anonymous ="+a);
										%>							
									<td><input type="button" name="showdata" id="showdata" value="ดูข้อมูล" class="btn btn-primary"
									onclick="return btnshowdataclick(<%=i%>)" class="form-control-submit-button">
									
									<div id="datatestitem<%=i%>" class="modal">
										<!-- Modal content -->
										<div class="modal-content">
											<span class="close" onclick="return spanclick(<%=i%>)">&times;</span>
											<br>
											<%  if(a){ %>
												<table class="table table-bordered   table-hover">
												<tr>
													<tr>
														<td><label>ผู้ขอรับคำปรึกษาต้องการปิดบังความเป็นส่วนตัว</label></td>
													</tr>
													<tr>
													<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
														<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
													<%}else if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ %>
														<td><input type="hidden" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา" id="submitButton" class="form-control-submit-button"></td>
													<% }else {%>
														<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
													<% } %>													
													</tr>
												</tr>
												</table>
											<%}else{ %>
												<table class="table  table-hover">
											<tr>
												<tr>
												<td>
													<img alt="" align="center" src="./img/<%=listMA.get(i).getPsychologist().getImg_p()%>" width="150" height="150">
													<label style="font-size: 20px;"><%=listMA.get(i).getConsultant().getTitle()+" "+listMA.get(i).getConsultant().getFristname()+" "+listMA.get(i).getConsultant().getLastname()%></label>
													<br>
													
												</td>
												</tr>
												<tr>
													<td><label>เบอร์โทร : </label>
													<label><%= listMA.get(i).getConsultant().getPhone() %></label></td>
												</tr>
												<tr>
													<td><label>ประวัติการแพ้ยา</label>
													<label><%=listMA.get(i).getConsultant().getDrug_allergy()%></label></td>
												</tr>
												<tr>
													<td><label>ประวัติการแพ้อาหาร</label>
													<label><%=listMA.get(i).getConsultant().getFood_allergy()%></label></td>
												</tr>
												<tr>
													<td><label>ประวัติโรคประจำตัว</label>
													<label><%=listMA.get(i).getConsultant().getCongenital_disease()%></label></td>
												</tr>
												<tr>
													<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
														<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
													<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
														<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
													<% }else{ %>
														<td><input type="hidden" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></td>
													<% } %>	
												</tr>
											</tr>
											</table>
											<%} %>
										</div>
									</div>
								</td>
								<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
									<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
								<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
									<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
								<% }else{ %>
									<td><input type="hidden" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></td>
								<% } %>	
								<!-- ปุ่มยืนยันนัด -->
								<%if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ %>				
									<td><input type="hidden" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button"></td>
								<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
									<td><a href="/WebService/Cancelapppsy?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" onclick="return confirm('คุณต้องการยกเลิกนัดใช่หรือไม่?');"></a></td>
								<% }else{ %>
									<td><input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" disabled="disabled" style="background: gray;"></td>
								<%} %>
								<!-- ปุ่มยกเลิกนัด -->
								<% }else{ %><!-- ปุ่มผู้ขอรับคำปรึกษา -->
									<% if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ 
											
											for(int j=0 ; j<listadvice.size() ; j++){
												if(listMA.get(i).getAppointment_id().equals(listadvice.get(j).getMake_appointment().getAppointment_id())){
													boolean ck = true ;
													for(int n=0 ; n<listpay.size() ; n++){
														if(listadvice.get(j).getAdvice_id().equals(listpay.get(n).getAdvice().getAdvice_id())){%>
															<td><a href="/WebService/openMakePayment?Mid=<%= listMA.get(i).getAppointment_id() %>">
															<input type="button" name="Paymant" value="ชำระเงิน" id="submitButton" class="form-control-submit-button" disabled="disabled" style="background: gray;"></a></td>		
														<% ck=false; break;}
													}
													if(ck){%>
														<td><a href="/WebService/openMakePayment?Mid=<%= listMA.get(i).getAppointment_id() %>">
															<input type="button" name="Paymant" value="ชำระเงิน" id="submitButton" class="form-control-submit-button"></a></td>								
													<%}
												}
											} 
									}else{ %>
										<td></td>
										<%if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
											<td><a href="/WebService/Cancelappcon?Mid=<%= listMA.get(i).getAppointment_id() %>">
											<input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" onclick="return confirm('คุณต้องการยกเลิกนัดใช่หรือไม่?');"></a></td>
										<% }else{ %>
											<td><input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button"  disabled="disabled" style="background: gray;"></a></td>
										<% } %>
									<% } %>
								<% } %><%-- else in if(type.equals("psychologist")) --%>
						<%	} %> <%-- for(listMA.size) --%>
							<%	}else{%><%-- if(listMA.size > 1) --%>
								<% if(type.equals("psychologist")){ %>	
								<% }else{ %>
									<td colspan="8"><label style="font-size: 24px;"><a href="/WebService/openListPsychologistent?user=<%= user%>">สร้างนัดหมาย</a></label><td>
								<% } %>
								</tr>
							</tbody>
							<% } %>
					</table>
					</div>
					
					<!--  ทั้งหมด -->
					<div id="dis2" style="display: none;">
						<table border="1" class="table table-bordered   table-hover">
							<thead>
								<tr>
									<th>ลำดับ</th>
									<th>ชื่อ-นามสกุล</th>
									<th>วันเวลาที่นัด</th>
									<th>ไอดีไลน์</th>
									<th>ประเภทการให้คำปรึกษา</th>
									<th>สถานะ</th>
									<% if(type.equals("psychologist")){ %>	
									<th>ดูข้อมูล</th>
									<%} %>
									<th></th>
									<th>ยกเลิก</th>
								</tr>
							</thead>
							<tbody>
							<% if(listMA.size() >= 1){ %>
							<% for(int i=0 ; i<listMA.size() ; i++){ %>
							<% if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){%>
								<tr>
								
									<% boolean a  = false ;
									if(type.equals("psychologist")){
										a = listMA.get(i).isAnonymous();
										System.out.println("anonymous ="+a);
										}%>
										
									<td><label><%= i+1 %></label></td>
									<%if(a){ %>
										<td><label>ปกปิดข้อมูล</label></td>
									<% }else{ %>
										<td><label><%=listMA.get(i).getConsultant().getTitle()+" "+ listMA.get(i).getConsultant().getFristname() +" "+ listMA.get(i).getConsultant().getLastname()%></label></td>
									<% } %>
									<%
										String ds = listMA.get(i).getTime_start().toInstant().toString().substring(0, 10);
										System.out.println(ds );
										 h = listMA.get(i).getTime_start().getTime().getHours();
										 m = listMA.get(i).getTime_start().getTime().getMinutes();
										 s = listMA.get(i).getTime_start().getTime().getSeconds();
										
										String[] d = ds.split("-");
									    int day = Integer.parseInt(d[2]);
									    int month = Integer.parseInt(d[1]);
									    int year = Integer.parseInt(d[0]);
									    
									    String time = String.valueOf(h)+":"+String.valueOf(m)+":"+String.valueOf(s);
									    DateFormat sdf = new SimpleDateFormat("HH:mm:ss");
									    Date date = sdf.parse(time);
									    
									    //datee
									    String subtime = date.toString() ;
									    String strT1 = subtime.replaceAll("Thu Jan 01 ", "");
									    String strT2 = strT1.replaceAll("ICT 1970", "");
									    
									    String appdate = ds +" "+ strT2;
									    System.out.println(ds +" "+ strT2 );
									    System.out.println(type );
									%>
									
									<td><label><%= appdate %></label></td>
									<td><label><%= listMA.get(i).getConsultant().getPhone() %></label></td>
									<td><label><%= listMA.get(i).getPrice().getContact_type() %></label></td>
									<td><label><%= listMA.get(i).getStatus_appoint() %></label></td>
									
									<% if(type.equals("psychologist")){
										a = listMA.get(i).isAnonymous();
										System.out.println("anonymous ="+a);
										%>							
									<td><input type="button" name="showdata" id="showdata" value="ดูข้อมูล" class="btn btn-primary"
									onclick="return btnshowdataclick(<%=i%>)" class="form-control-submit-button">
									
									<div id="datatestitem<%=i%>" class="modal">
										<!-- Modal content -->
										<div class="modal-content">
											<span class="close" onclick="return spanclick(<%=i%>)">&times;</span>
											<br>
											<%  if(a){ %>
												<table class="table table-bordered   table-hover">
												<tr>
													<tr>
														<td><label>ผู้ขอรับคำปรึกษาต้องการปิดบังความเป็นส่วนตัว</label></td>
													</tr>
													<tr>
													<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
														<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
													<%}else if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ %>
														<td><input type="hidden" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา" id="submitButton" class="form-control-submit-button"></td>
													<% }else {%>
														<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
													<% } %>													
													</tr>
												</tr>
												</table>
											<%}else{ %>
												<table class="table  table-hover">
											<tr>
												<tr>
												<td>
													<img alt="" align="center" src="./img/<%=listMA.get(i).getPsychologist().getImg_p()%>" width="150" height="150">
													<label style="font-size: 20px;"><%=listMA.get(i).getConsultant().getTitle()+" "+listMA.get(i).getConsultant().getFristname()+" "+listMA.get(i).getConsultant().getLastname()%></label>
													<br>
													
												</td>
												</tr>
												<tr>
													<td><label>เบอร์โทร : </label>
													<label><%= listMA.get(i).getConsultant().getPhone() %></label></td>
												</tr>
												<tr>
													<td><label>ประวัติการแพ้ยา</label>
													<label><%=listMA.get(i).getConsultant().getDrug_allergy()%></label></td>
												</tr>
												<tr>
													<td><label>ประวัติการแพ้อาหาร</label>
													<label><%=listMA.get(i).getConsultant().getFood_allergy()%></label></td>
												</tr>
												<tr>
													<td><label>ประวัติโรคประจำตัว</label>
													<label><%=listMA.get(i).getConsultant().getCongenital_disease()%></label></td>
												</tr>
												<tr>
													<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
														<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
													<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
														<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
													<% }else{ %>
														<td><input type="hidden" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></td>
													<% } %>	
												</tr>
											</tr>
											</table>
											<%} %>
										</div>
									</div>
								</td>
								<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
									<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
								<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
									<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
								<% }else{ %>
									<td><input type="hidden" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></td>
								<% } %>	
								<!-- ปุ่มยืนยันนัด -->
								<%if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ %>				
									<td><input type="hidden" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button"></td>
								<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
									<td><a href="/WebService/Cancelapppsy?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" onclick="return confirm('คุณต้องการยกเลิกนัดใช่หรือไม่?');"></a></td>
								<% }else{ %>
									<td><input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" disabled="disabled" style="background: gray;"></td>
								<%} %>
								<!-- ปุ่มยกเลิกนัด -->
								<% }else{ %><!-- ปุ่มผู้ขอรับคำปรึกษา -->
									<% if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ 
											
											for(int j=0 ; j<listadvice.size() ; j++){
												if(listMA.get(i).getAppointment_id().equals(listadvice.get(j).getMake_appointment().getAppointment_id())){
													boolean ck = true ;
													for(int n=0 ; n<listpay.size() ; n++){
														if(listadvice.get(j).getAdvice_id().equals(listpay.get(n).getAdvice().getAdvice_id())){%>
															<td><a href="/WebService/openMakePayment?Mid=<%= listMA.get(i).getAppointment_id() %>">
															<input type="button" name="Paymant" value="ชำระเงิน" id="submitButton" class="form-control-submit-button" disabled="disabled" style="background: gray;"></a></td>		
														<% ck=false; break;}
													}
													if(ck){%>
														<td><a href="/WebService/openMakePayment?Mid=<%= listMA.get(i).getAppointment_id() %>">
															<input type="button" name="Paymant" value="ชำระเงิน" id="submitButton" class="form-control-submit-button"></a></td>								
													<%}
												}
											} 
									}else{ %>
										<td></td>
										<%if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
											<td><a href="/WebService/Cancelappcon?Mid=<%= listMA.get(i).getAppointment_id() %>">
											<input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" onclick="return confirm('คุณต้องการยกเลิกนัดใช่หรือไม่?');"></a></td>
										<% }else{ %>
											<td><input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button"  disabled="disabled" style="background: gray;"></a></td>
										<% } %>
									<% } %>
								<% } %><%-- else in if(type.equals("psychologist")) --%>
								
							<% } %> <!-- if(listMA.get(i).getStatus_appoint().equals("รอการยืนยันนัด")) -->

						<%	} %> <%-- for(listMA.size) --%>
							<%	}else{%><%-- if(listMA.size > 1) --%>
								<% if(type.equals("psychologist")){ %>	
								<% }else{ %>
									<td colspan="8"><label style="font-size: 24px;"><a href="/WebService/openListPsychologistent?user=<%= user%>">สร้างนัดหมาย</a></label><td>
								<% } %>
								</tr>
							</tbody>
							<% } %>
					</table>
					</div>
					<!--  รอการยืนยัน -->
					<div id="dis3" style="display: none;">
						<table border="1" class="table table-bordered   table-hover">
							<thead>
								<tr>
									<th>ลำดับ</th>
									<th>ชื่อ-นามสกุล</th>
									<th>วันเวลาที่นัด</th>
									<th>ไอดีไลน์</th>
									<th>ประเภทการให้คำปรึกษา</th>
									<th>สถานะ</th>
									<% if(type.equals("psychologist")){ %>	
									<th>ดูข้อมูล</th>
									<%} %>
									<th></th>
									<th>ยกเลิก</th>
								</tr>
							</thead>
							<tbody>
							<% if(listMA.size() >= 1){ %>
							<% for(int i=0 ; i<listMA.size() ; i++){ %>
							<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){%>
								<tr>
								
									<% boolean a  = false ;
									if(type.equals("psychologist")){
										a = listMA.get(i).isAnonymous();
										System.out.println("anonymous ="+a);
										}%>
										
									<td><label><%= i+1 %></label></td>
									<%if(a){ %>
										<td><label>ปกปิดข้อมูล</label></td>
									<% }else{ %>
										<td><label><%=listMA.get(i).getConsultant().getTitle()+" "+ listMA.get(i).getConsultant().getFristname() +" "+ listMA.get(i).getConsultant().getLastname()%></label></td>
									<% } %>
									<%
										String ds = listMA.get(i).getTime_start().toInstant().toString().substring(0, 10);
										System.out.println(ds );
										 h = listMA.get(i).getTime_start().getTime().getHours();
										 m = listMA.get(i).getTime_start().getTime().getMinutes();
										 s = listMA.get(i).getTime_start().getTime().getSeconds();
										
										String[] d = ds.split("-");
									    int day = Integer.parseInt(d[2]);
									    int month = Integer.parseInt(d[1]);
									    int year = Integer.parseInt(d[0]);
									    
									    String time = String.valueOf(h)+":"+String.valueOf(m)+":"+String.valueOf(s);
									    DateFormat sdf = new SimpleDateFormat("HH:mm:ss");
									    Date date = sdf.parse(time);
									    
									    //datee
									    String subtime = date.toString() ;
									    String strT1 = subtime.replaceAll("Thu Jan 01 ", "");
									    String strT2 = strT1.replaceAll("ICT 1970", "");
									    
									    String appdate = ds +" "+ strT2;
									    System.out.println(ds +" "+ strT2 );
									    System.out.println(type );
									%>
									
									<td><label><%= appdate %></label></td>
									<td><label><%= listMA.get(i).getConsultant().getPhone() %></label></td>
									<td><label><%= listMA.get(i).getPrice().getContact_type() %></label></td>
									<td><label><%= listMA.get(i).getStatus_appoint() %></label></td>
									
									<% if(type.equals("psychologist")){
										a = listMA.get(i).isAnonymous();
										System.out.println("anonymous ="+a);
										%>							
									<td><input type="button" name="showdata" id="showdata" value="ดูข้อมูล" class="btn btn-primary"
									onclick="return btnshowdataclick(<%=i%>)" class="form-control-submit-button">
									
									<div id="datatestitem<%=i%>" class="modal">
										<!-- Modal content -->
										<div class="modal-content">
											<span class="close" onclick="return spanclick(<%=i%>)">&times;</span>
											<br>
											<%  if(a){ %>
												<table class="table table-bordered   table-hover">
												<tr>
													<tr>
														<td><label>ผู้ขอรับคำปรึกษาต้องการปิดบังความเป็นส่วนตัว</label></td>
													</tr>
													<tr>
													<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
														<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
													<%}else if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ %>
														<td><input type="hidden" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา" id="submitButton" class="form-control-submit-button"></td>
													<% }else {%>
														<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
													<% } %>													
													</tr>
												</tr>
												</table>
											<%}else{ %>
												<table class="table  table-hover">
											<tr>
												<tr>
												<td>
													<img alt="" align="center" src="./img/<%=listMA.get(i).getPsychologist().getImg_p()%>" width="150" height="150">
													<label style="font-size: 20px;"><%=listMA.get(i).getConsultant().getTitle()+" "+listMA.get(i).getConsultant().getFristname()+" "+listMA.get(i).getConsultant().getLastname()%></label>
													<br>
													
												</td>
												</tr>
												<tr>
													<td><label>เบอร์โทร : </label>
													<label><%= listMA.get(i).getConsultant().getPhone() %></label></td>
												</tr>
												<tr>
													<td><label>ประวัติการแพ้ยา</label>
													<label><%=listMA.get(i).getConsultant().getDrug_allergy()%></label></td>
												</tr>
												<tr>
													<td><label>ประวัติการแพ้อาหาร</label>
													<label><%=listMA.get(i).getConsultant().getFood_allergy()%></label></td>
												</tr>
												<tr>
													<td><label>ประวัติโรคประจำตัว</label>
													<label><%=listMA.get(i).getConsultant().getCongenital_disease()%></label></td>
												</tr>
												<tr>
													<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
														<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
													<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
														<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
													<% }else{ %>
														<td><input type="hidden" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></td>
													<% } %>	
												</tr>
											</tr>
											</table>
											<%} %>
										</div>
									</div>
								</td>
								<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
									<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
								<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
									<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
								<% }else{ %>
									<td><input type="hidden" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></td>
								<% } %>	
								<!-- ปุ่มยืนยันนัด -->
								<%if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ %>				
									<td><input type="hidden" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button"></td>
								<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
									<td><a href="/WebService/Cancelapppsy?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" onclick="return confirm('คุณต้องการยกเลิกนัดใช่หรือไม่?');"></a></td>
								<% }else{ %>
									<td><input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" disabled="disabled" style="background: gray;"></td>
								<%} %>
								<!-- ปุ่มยกเลิกนัด -->
								<% }else{ %><!-- ปุ่มผู้ขอรับคำปรึกษา -->
									<% if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ 
											
											for(int j=0 ; j<listadvice.size() ; j++){
												if(listMA.get(i).getAppointment_id().equals(listadvice.get(j).getMake_appointment().getAppointment_id())){
													boolean ck = true ;
													for(int n=0 ; n<listpay.size() ; n++){
														if(listadvice.get(j).getAdvice_id().equals(listpay.get(n).getAdvice().getAdvice_id())){%>
															<td><a href="/WebService/openMakePayment?Mid=<%= listMA.get(i).getAppointment_id() %>">
															<input type="button" name="Paymant" value="ชำระเงิน" id="submitButton" class="form-control-submit-button" disabled="disabled" style="background: gray;"></a></td>		
														<% ck=false; break;}
													}
													if(ck){%>
														<td><a href="/WebService/openMakePayment?Mid=<%= listMA.get(i).getAppointment_id() %>">
															<input type="button" name="Paymant" value="ชำระเงิน" id="submitButton" class="form-control-submit-button"></a></td>								
													<%}
												}
											} 
									}else{ %>
										<td></td>
										<%if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
											<td><a href="/WebService/Cancelappcon?Mid=<%= listMA.get(i).getAppointment_id() %>">
											<input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" onclick="return confirm('คุณต้องการยกเลิกนัดใช่หรือไม่?');"></a></td>
										<% }else{ %>
											<td><input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button"  disabled="disabled" style="background: gray;"></a></td>
										<% } %>
									<% } %>
								<% } %><%-- else in if(type.equals("psychologist")) --%>
								
							<% } %> <!-- if(listMA.get(i).getStatus_appoint().equals("รอการยืนยันนัด")) -->

						<%	} %> <%-- for(listMA.size) --%>
							<%	}else{%><%-- if(listMA.size > 1) --%>
								<% if(type.equals("psychologist")){ %>	
								<% }else{ %>
									<td colspan="8"><label style="font-size: 24px;"><a href="/WebService/openListPsychologistent?user=<%= user%>">สร้างนัดหมาย</a></label><td>
								<% } %>
								</tr>
							</tbody>
							<% } %>
					</table>
					</div>
					<!-- ยืนยันการนัด -->
					<div id="dis4" style="display: none;">
						<table border="1" class="table table-bordered   table-hover">
							<thead>
								<tr>
									<th>ลำดับ</th>
									<th>ชื่อ-นามสกุล</th>
									<th>วันเวลาที่นัด</th>
									<th>ไอดีไลน์</th>
									<th>ประเภทการให้คำปรึกษา</th>
									<th>สถานะ</th>
									<% if(type.equals("psychologist")){ %>	
									<th>ดูข้อมูล</th>
									<%} %>
									<th></th>
									<th>ยกเลิก</th>
								</tr>
							</thead>
							<tbody>
							<% if(listMA.size() >= 1){ %>
							<% for(int i=0 ; i<listMA.size() ; i++){ %>
							<% if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){%>
								<tr>
								
									<% boolean a  = false ;
									if(type.equals("psychologist")){
										a = listMA.get(i).isAnonymous();
										System.out.println("anonymous ="+a);
										}%>
										
									<td><label><%= i+1 %></label></td>
									<%if(a){ %>
										<td><label>ปกปิดข้อมูล</label></td>
									<% }else{ %>
										<td><label><%=listMA.get(i).getConsultant().getTitle()+" "+ listMA.get(i).getConsultant().getFristname() +" "+ listMA.get(i).getConsultant().getLastname()%></label></td>
									<% } %>
									<%
										String ds = listMA.get(i).getTime_start().toInstant().toString().substring(0, 10);
										System.out.println(ds );
										 h = listMA.get(i).getTime_start().getTime().getHours();
										 m = listMA.get(i).getTime_start().getTime().getMinutes();
										 s = listMA.get(i).getTime_start().getTime().getSeconds();
										
										String[] d = ds.split("-");
									    int day = Integer.parseInt(d[2]);
									    int month = Integer.parseInt(d[1]);
									    int year = Integer.parseInt(d[0]);
									    
									    String time = String.valueOf(h)+":"+String.valueOf(m)+":"+String.valueOf(s);
									    DateFormat sdf = new SimpleDateFormat("HH:mm:ss");
									    Date date = sdf.parse(time);
									    
									    //datee
									    String subtime = date.toString() ;
									    String strT1 = subtime.replaceAll("Thu Jan 01 ", "");
									    String strT2 = strT1.replaceAll("ICT 1970", "");
									    
									    String appdate = ds +" "+ strT2;
									    System.out.println(ds +" "+ strT2 );
									    System.out.println(type );
									%>
									
									<td><label><%= appdate %></label></td>
									<td><label><%= listMA.get(i).getConsultant().getPhone() %></label></td>
									<td><label><%= listMA.get(i).getPrice().getContact_type() %></label></td>
									<td><label><%= listMA.get(i).getStatus_appoint() %></label></td>
									
									<% if(type.equals("psychologist")){
										a = listMA.get(i).isAnonymous();
										System.out.println("anonymous ="+a);
										%>							
									<td><input type="button" name="showdata" id="showdata" value="ดูข้อมูล" class="btn btn-primary"
									onclick="return btnshowdataclick(<%=i%>)" class="form-control-submit-button">
									
									<div id="datatestitem<%=i%>" class="modal">
										<!-- Modal content -->
										<div class="modal-content">
											<span class="close" onclick="return spanclick(<%=i%>)">&times;</span>
											<br>
											<%  if(a){ %>
												<table class="table table-bordered   table-hover">
												<tr>
													<tr>
														<td><label>ผู้ขอรับคำปรึกษาต้องการปิดบังความเป็นส่วนตัว</label></td>
													</tr>
													<tr>
													<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
														<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
													<%}else if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ %>
														<td><input type="hidden" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา" id="submitButton" class="form-control-submit-button"></td>
													<% }else {%>
														<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
													<% } %>													
													</tr>
												</tr>
												</table>
											<%}else{ %>
												<table class="table  table-hover">
											<tr>
												<tr>
												<td>
													<img alt="" align="center" src="./img/<%=listMA.get(i).getPsychologist().getImg_p()%>" width="150" height="150">
													<label style="font-size: 20px;"><%=listMA.get(i).getConsultant().getTitle()+" "+listMA.get(i).getConsultant().getFristname()+" "+listMA.get(i).getConsultant().getLastname()%></label>
													<br>
													
												</td>
												</tr>
												<tr>
													<td><label>เบอร์โทร : </label>
													<label><%= listMA.get(i).getConsultant().getPhone() %></label></td>
												</tr>
												<tr>
													<td><label>ประวัติการแพ้ยา</label>
													<label><%=listMA.get(i).getConsultant().getDrug_allergy()%></label></td>
												</tr>
												<tr>
													<td><label>ประวัติการแพ้อาหาร</label>
													<label><%=listMA.get(i).getConsultant().getFood_allergy()%></label></td>
												</tr>
												<tr>
													<td><label>ประวัติโรคประจำตัว</label>
													<label><%=listMA.get(i).getConsultant().getCongenital_disease()%></label></td>
												</tr>
												<tr>
													<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
														<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
													<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
														<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
													<% }else{ %>
														<td><input type="hidden" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></td>
													<% } %>	
												</tr>
											</tr>
											</table>
											<%} %>
										</div>
									</div>
								</td>
								<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
									<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
								<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
									<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
								<% }else{ %>
									<td><input type="hidden" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></td>
								<% } %>	
								<!-- ปุ่มยืนยันนัด -->
								<%if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ %>				
									<td><input type="hidden" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button"></td>
								<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
									<td><a href="/WebService/Cancelapppsy?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" onclick="return confirm('คุณต้องการยกเลิกนัดใช่หรือไม่?');"></a></td>
								<% }else{ %>
									<td><input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" disabled="disabled" style="background: gray;"></td>
								<%} %>
								<!-- ปุ่มยกเลิกนัด -->
								<% }else{ %><!-- ปุ่มผู้ขอรับคำปรึกษา -->
									<% if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ 
											
											for(int j=0 ; j<listadvice.size() ; j++){
												if(listMA.get(i).getAppointment_id().equals(listadvice.get(j).getMake_appointment().getAppointment_id())){
													boolean ck = true ;
													for(int n=0 ; n<listpay.size() ; n++){
														if(listadvice.get(j).getAdvice_id().equals(listpay.get(n).getAdvice().getAdvice_id())){%>
															<td><a href="/WebService/openMakePayment?Mid=<%= listMA.get(i).getAppointment_id() %>">
															<input type="button" name="Paymant" value="ชำระเงิน" id="submitButton" class="form-control-submit-button" disabled="disabled" style="background: gray;"></a></td>		
														<% ck=false; break;}
													}
													if(ck){%>
														<td><a href="/WebService/openMakePayment?Mid=<%= listMA.get(i).getAppointment_id() %>">
															<input type="button" name="Paymant" value="ชำระเงิน" id="submitButton" class="form-control-submit-button"></a></td>								
													<%}
												}
											} 
									}else{ %>
										<td></td>
										<%if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
											<td><a href="/WebService/Cancelappcon?Mid=<%= listMA.get(i).getAppointment_id() %>">
											<input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" onclick="return confirm('คุณต้องการยกเลิกนัดใช่หรือไม่?');"></a></td>
										<% }else{ %>
											<td><input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button"  disabled="disabled" style="background: gray;"></a></td>
										<% } %>
									<% } %>
								<% } %><%-- else in if(type.equals("psychologist")) --%>
								
							<% } %> <!-- if(listMA.get(i).getStatus_appoint().equals("รอการยืนยันนัด")) -->

						<%	} %> <%-- for(listMA.size) --%>
							<%	}else{%><%-- if(listMA.size > 1) --%>
								<% if(type.equals("psychologist")){ %>	
								<% }else{ %>
									<td colspan="8"><label style="font-size: 24px;"><a href="/WebService/openListPsychologistent?user=<%= user%>">สร้างนัดหมาย</a></label><td>
								<% } %>
								</tr>
							</tbody>
							<% } %>
					</table>
					</div>
					<!-- ยกเลิกนัด -->
					<div id="dis6" style="display: none;">
						<table border="1" class="table table-bordered   table-hover">
							<thead>
								<tr>
									<th>ลำดับ</th>
									<th>ชื่อ-นามสกุล</th>
									<th>วันเวลาที่นัด</th>
									<th>ไอดีไลน์</th>
									<th>ประเภทการให้คำปรึกษา</th>
									<th>สถานะ</th>
									<% if(type.equals("psychologist")){ %>	
									<th>ดูข้อมูล</th>
									<%} %>
									<th></th>
									<th>ยกเลิก</th>
								</tr>
							</thead>
							<tbody>
							<% if(listMA.size() >= 1){ %>
							<% for(int i=0 ; i<listMA.size() ; i++){ %>
							<% if(listMA.get(i).getStatus_appoint().equals("ยกเลิกนัด")){%>
								<tr>
								
									<% boolean a  = false ;
									if(type.equals("psychologist")){
										a = listMA.get(i).isAnonymous();
										System.out.println("anonymous ="+a);
										}%>
										
									<td><label><%= i+1 %></label></td>
									<%if(a){ %>
										<td><label>ปกปิดข้อมูล</label></td>
									<% }else{ %>
										<td><label><%=listMA.get(i).getConsultant().getTitle()+" "+ listMA.get(i).getConsultant().getFristname() +" "+ listMA.get(i).getConsultant().getLastname()%></label></td>
									<% } %>
									<%
										String ds = listMA.get(i).getTime_start().toInstant().toString().substring(0, 10);
										System.out.println(ds );
										 h = listMA.get(i).getTime_start().getTime().getHours();
										 m = listMA.get(i).getTime_start().getTime().getMinutes();
										 s = listMA.get(i).getTime_start().getTime().getSeconds();
										
										String[] d = ds.split("-");
									    int day = Integer.parseInt(d[2]);
									    int month = Integer.parseInt(d[1]);
									    int year = Integer.parseInt(d[0]);
									    
									    String time = String.valueOf(h)+":"+String.valueOf(m)+":"+String.valueOf(s);
									    DateFormat sdf = new SimpleDateFormat("HH:mm:ss");
									    Date date = sdf.parse(time);
									    
									    //datee
									    String subtime = date.toString() ;
									    String strT1 = subtime.replaceAll("Thu Jan 01 ", "");
									    String strT2 = strT1.replaceAll("ICT 1970", "");
									    
									    String appdate = ds +" "+ strT2;
									    System.out.println(ds +" "+ strT2 );
									    System.out.println(type );
									%>
									
									<td><label><%= appdate %></label></td>
									<td><label><%= listMA.get(i).getConsultant().getPhone() %></label></td>
									<td><label><%= listMA.get(i).getPrice().getContact_type() %></label></td>
									<td><label><%= listMA.get(i).getStatus_appoint() %></label></td>
									
									<% if(type.equals("psychologist")){
										a = listMA.get(i).isAnonymous();
										System.out.println("anonymous ="+a);
										%>							
									<td><input type="button" name="showdata" id="showdata" value="ดูข้อมูล" class="btn btn-primary"
									onclick="return btnshowdataclick(<%=i%>)" class="form-control-submit-button">
									
									<div id="datatestitem<%=i%>" class="modal">
										<!-- Modal content -->
										<div class="modal-content">
											<span class="close" onclick="return spanclick(<%=i%>)">&times;</span>
											<br>
											<%  if(a){ %>
												<table class="table table-bordered   table-hover">
												<tr>
													<tr>
														<td><label>ผู้ขอรับคำปรึกษาต้องการปิดบังความเป็นส่วนตัว</label></td>
													</tr>
													<tr>
													<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
														<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
													<%}else if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ %>
														<td><input type="hidden" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา" id="submitButton" class="form-control-submit-button"></td>
													<% }else {%>
														<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
													<% } %>													
													</tr>
												</tr>
												</table>
											<%}else{ %>
												<table class="table  table-hover">
											<tr>
												<tr>
												<td>
													<img alt="" align="center" src="./img/<%=listMA.get(i).getPsychologist().getImg_p()%>" width="150" height="150">
													<label style="font-size: 20px;"><%=listMA.get(i).getConsultant().getTitle()+" "+listMA.get(i).getConsultant().getFristname()+" "+listMA.get(i).getConsultant().getLastname()%></label>
													<br>
													
												</td>
												</tr>
												<tr>
													<td><label>เบอร์โทร : </label>
													<label><%= listMA.get(i).getConsultant().getPhone() %></label></td>
												</tr>
												<tr>
													<td><label>ประวัติการแพ้ยา</label>
													<label><%=listMA.get(i).getConsultant().getDrug_allergy()%></label></td>
												</tr>
												<tr>
													<td><label>ประวัติการแพ้อาหาร</label>
													<label><%=listMA.get(i).getConsultant().getFood_allergy()%></label></td>
												</tr>
												<tr>
													<td><label>ประวัติโรคประจำตัว</label>
													<label><%=listMA.get(i).getConsultant().getCongenital_disease()%></label></td>
												</tr>
												<tr>
													<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
														<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
													<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
														<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
													<% }else{ %>
														<td><input type="hidden" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></td>
													<% } %>	
												</tr>
											</tr>
											</table>
											<%} %>
										</div>
									</div>
								</td>
								<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
									<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
								<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
									<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
								<% }else{ %>
									<td><input type="hidden" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></td>
								<% } %>	
								<!-- ปุ่มยืนยันนัด -->
								<%if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ %>				
									<td><input type="hidden" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button"></td>
								<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
									<td><a href="/WebService/Cancelapppsy?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" onclick="return confirm('คุณต้องการยกเลิกนัดใช่หรือไม่?');"></a></td>
								<% }else{ %>
									<td><input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" disabled="disabled" style="background: gray;"></td>
								<%} %>
								<!-- ปุ่มยกเลิกนัด -->
								<% }else{ %><!-- ปุ่มผู้ขอรับคำปรึกษา -->
									<% if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ 
											
											for(int j=0 ; j<listadvice.size() ; j++){
												if(listMA.get(i).getAppointment_id().equals(listadvice.get(j).getMake_appointment().getAppointment_id())){
													boolean ck = true ;
													for(int n=0 ; n<listpay.size() ; n++){
														if(listadvice.get(j).getAdvice_id().equals(listpay.get(n).getAdvice().getAdvice_id())){%>
															<td><a href="/WebService/openMakePayment?Mid=<%= listMA.get(i).getAppointment_id() %>">
															<input type="button" name="Paymant" value="ชำระเงิน" id="submitButton" class="form-control-submit-button" disabled="disabled" style="background: gray;"></a></td>		
														<% ck=false; break;}
													}
													if(ck){%>
														<td><a href="/WebService/openMakePayment?Mid=<%= listMA.get(i).getAppointment_id() %>">
															<input type="button" name="Paymant" value="ชำระเงิน" id="submitButton" class="form-control-submit-button"></a></td>								
													<%}
												}
											} 
									}else{ %>
										<td></td>
										<%if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
											<td><a href="/WebService/Cancelappcon?Mid=<%= listMA.get(i).getAppointment_id() %>">
											<input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" onclick="return confirm('คุณต้องการยกเลิกนัดใช่หรือไม่?');"></a></td>
										<% }else{ %>
											<td><input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button"  disabled="disabled" style="background: gray;"></a></td>
										<% } %>
									<% } %>
								<% } %><%-- else in if(type.equals("psychologist")) --%>
								
							<% } %> <!-- if(listMA.get(i).getStatus_appoint().equals("รอการยืนยันนัด")) -->

						<%	} %> <%-- for(listMA.size) --%>
							<%	}else{%><%-- if(listMA.size > 1) --%>
								<% if(type.equals("psychologist")){ %>	
								<% }else{ %>
									<td colspan="8"><label style="font-size: 24px;"><a href="/WebService/openListPsychologistent?user=<%= user%>">สร้างนัดหมาย</a></label><td>
								<% } %>
								</tr>
							</tbody>
							<% } %>
					</table>
					</div>
					<!-- ค้างชำระ -->
					<div id="dis5" style="display: none;">
						<table border="1" class="table table-bordered   table-hover">
							<thead>
								<tr>
									<th>ลำดับ</th>
									<th>ชื่อ-นามสกุล</th>
									<th>วันเวลาที่นัด</th>
									<th>ไอดีไลน์</th>
									<th>ประเภทการให้คำปรึกษา</th>
									<th>สถานะ</th>
									<% if(type.equals("psychologist")){ %>	
									<th>ดูข้อมูล</th>
									<%} %>
									<th></th>
									<th>ยกเลิก</th>
								</tr>
							</thead>
							<tbody>
							<% if(listMA.size() >= 1){ %>
							<% for(int i=0 ; i<listMA.size() ; i++){ %>
							<% if(listMA.get(i).getStatus_appoint().equals("สำเร็จ")){%>
								<tr>
								
									<% boolean a  = false ;
									if(type.equals("psychologist")){
										a = listMA.get(i).isAnonymous();
										System.out.println("anonymous ="+a);
										}%>
										
									<td><label><%= i+1 %></label></td>
									<%if(a){ %>
										<td><label>ปกปิดข้อมูล</label></td>
									<% }else{ %>
										<td><label><%=listMA.get(i).getConsultant().getTitle()+" "+ listMA.get(i).getConsultant().getFristname() +" "+ listMA.get(i).getConsultant().getLastname()%></label></td>
									<% } %>
									<%
										String ds = listMA.get(i).getTime_start().toInstant().toString().substring(0, 10);
										System.out.println(ds );
										 h = listMA.get(i).getTime_start().getTime().getHours();
										 m = listMA.get(i).getTime_start().getTime().getMinutes();
										 s = listMA.get(i).getTime_start().getTime().getSeconds();
										
										String[] d = ds.split("-");
									    int day = Integer.parseInt(d[2]);
									    int month = Integer.parseInt(d[1]);
									    int year = Integer.parseInt(d[0]);
									    
									    String time = String.valueOf(h)+":"+String.valueOf(m)+":"+String.valueOf(s);
									    DateFormat sdf = new SimpleDateFormat("HH:mm:ss");
									    Date date = sdf.parse(time);
									    
									    //datee
									    String subtime = date.toString() ;
									    String strT1 = subtime.replaceAll("Thu Jan 01 ", "");
									    String strT2 = strT1.replaceAll("ICT 1970", "");
									    
									    String appdate = ds +" "+ strT2;
									    System.out.println(ds +" "+ strT2 );
									    System.out.println(type );
									%>
									
									<td><label><%= appdate %></label></td>
									<td><label><%= listMA.get(i).getConsultant().getPhone() %></label></td>
									<td><label><%= listMA.get(i).getPrice().getContact_type() %></label></td>
									<td><label><%= listMA.get(i).getStatus_appoint() %></label></td>
									
									<% if(type.equals("psychologist")){
										a = listMA.get(i).isAnonymous();
										System.out.println("anonymous ="+a);
										%>							
									<td><input type="button" name="showdata" id="showdata" value="ดูข้อมูล" class="btn btn-primary"
									onclick="return btnshowdataclick(<%=i%>)" class="form-control-submit-button">
									
									<div id="datatestitem<%=i%>" class="modal">
										<!-- Modal content -->
										<div class="modal-content">
											<span class="close" onclick="return spanclick(<%=i%>)">&times;</span>
											<br>
											<%  if(a){ %>
												<table class="table table-bordered   table-hover">
												<tr>
													<tr>
														<td><label>ผู้ขอรับคำปรึกษาต้องการปิดบังความเป็นส่วนตัว</label></td>
													</tr>
													<tr>
													<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
														<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
													<%}else if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ %>
														<td><input type="hidden" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา" id="submitButton" class="form-control-submit-button"></td>
													<% }else {%>
														<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
													<% } %>													
													</tr>
												</tr>
												</table>
											<%}else{ %>
												<table class="table  table-hover">
											<tr>
												<tr>
												<td>
													<img alt="" align="center" src="./img/<%=listMA.get(i).getPsychologist().getImg_p()%>" width="150" height="150">
													<label style="font-size: 20px;"><%=listMA.get(i).getConsultant().getTitle()+" "+listMA.get(i).getConsultant().getFristname()+" "+listMA.get(i).getConsultant().getLastname()%></label>
													<br>
													
												</td>
												</tr>
												<tr>
													<td><label>เบอร์โทร : </label>
													<label><%= listMA.get(i).getConsultant().getPhone() %></label></td>
												</tr>
												<tr>
													<td><label>ประวัติการแพ้ยา</label>
													<label><%=listMA.get(i).getConsultant().getDrug_allergy()%></label></td>
												</tr>
												<tr>
													<td><label>ประวัติการแพ้อาหาร</label>
													<label><%=listMA.get(i).getConsultant().getFood_allergy()%></label></td>
												</tr>
												<tr>
													<td><label>ประวัติโรคประจำตัว</label>
													<label><%=listMA.get(i).getConsultant().getCongenital_disease()%></label></td>
												</tr>
												<tr>
													<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
														<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
													<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
														<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
														<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
													<% }else{ %>
														<td><input type="hidden" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></td>
													<% } %>	
												</tr>
											</tr>
											</table>
											<%} %>
										</div>
									</div>
								</td>
								<% if(listMA.get(i).getStatus_appoint().equals("ยืนยันการนัด")){ %>
									<td><a href="/WebService/openGiveAdvice?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="AddConsultationHistory" value="เพิ่มประวัติการให้คำปรึกษา " id="submitButton" class="form-control-submit-button" ></a></td>
								<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
									<td><a href="/WebService/confrimApp?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></a></td>
								<% }else{ %>
									<td><input type="hidden" name="confrim" value="ยืนยันนัด" id="submitButton" class="form-control-submit-button"></td>
								<% } %>	
								<!-- ปุ่มยืนยันนัด -->
								<%if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ %>				
									<td><input type="hidden" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button"></td>
								<%}else if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
									<td><a href="/WebService/Cancelapppsy?Mid=<%= listMA.get(i).getAppointment_id() %>">
									<input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" onclick="return confirm('คุณต้องการยกเลิกนัดใช่หรือไม่?');"></a></td>
								<% }else{ %>
									<td><input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" disabled="disabled" style="background: gray;"></td>
								<%} %>
								<!-- ปุ่มยกเลิกนัด -->
								<% }else{ %><!-- ปุ่มผู้ขอรับคำปรึกษา -->
									<% if(listMA.get(i).getStatus_appoint().equals("ค้างชำระ")){ 
											
											for(int j=0 ; j<listadvice.size() ; j++){
												if(listMA.get(i).getAppointment_id().equals(listadvice.get(j).getMake_appointment().getAppointment_id())){
													boolean ck = true ;
													for(int n=0 ; n<listpay.size() ; n++){
														if(listadvice.get(j).getAdvice_id().equals(listpay.get(n).getAdvice().getAdvice_id())){%>
															<td><a href="/WebService/openMakePayment?Mid=<%= listMA.get(i).getAppointment_id() %>">
															<input type="button" name="Paymant" value="ชำระเงิน" id="submitButton" class="form-control-submit-button" disabled="disabled" style="background: gray;"></a></td>		
														<% ck=false; break;}
													}
													if(ck){%>
														<td><a href="/WebService/openMakePayment?Mid=<%= listMA.get(i).getAppointment_id() %>">
															<input type="button" name="Paymant" value="ชำระเงิน" id="submitButton" class="form-control-submit-button"></a></td>								
													<%}
												}
											} 
									}else{ %>
										<td></td>
										<%if(listMA.get(i).getStatus_appoint().equals("รอการยืนยัน")){ %>
											<td><a href="/WebService/Cancelappcon?Mid=<%= listMA.get(i).getAppointment_id() %>">
											<input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button" onclick="return confirm('คุณต้องการยกเลิกนัดใช่หรือไม่?');"></a></td>
										<% }else{ %>
											<td><input type="button" name="cancel" value="ยกเลิกนัด" id="submitButton" class="form-control-submit-button"  disabled="disabled" style="background: gray;"></a></td>
										<% } %>
									<% } %>
								<% } %><%-- else in if(type.equals("psychologist")) --%>
								
							<% } %> <!-- if(listMA.get(i).getStatus_appoint().equals("รอการยืนยันนัด")) -->

						<%	} %> <%-- for(listMA.size) --%>
							<%	}else{%><%-- if(listMA.size > 1) --%>
								<% if(type.equals("psychologist")){ %>	
								<% }else{ %>
									<td colspan="8"><label style="font-size: 24px;"><a href="/WebService/openListPsychologistent?user=<%= user%>">สร้างนัดหมาย</a></label><td>
								<% } %>
								</tr>
							</tbody>
							<% } %>
					</table>
					</div>
					
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	

<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>