<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	String user = (String) session.getAttribute("user");
	Award award = new Award();
	award = (Award) session.getAttribute("award");
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

    <script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>

	<script src="./js/CheckScriptInformation/JSAwaed.js"></script>

</head>
<script type="text/javascript">

	$.validator.addMethod('filesize', function(value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	}, 'ขนาดไฟล์ต้องไม่เกิน 2 MB และต้องเป็น  jpg,jpeg เท่านั้น {0}');

	jQuery(function($) {
		"use strict";
		$('#myformAward').validate({
			rules : {
				FirstName : {
					required : true,
					maxlength : 20
				},
				image : {
					required : true,
					extension : "jpg,jpeg",
					filesize : 2000,
				}
			},
		});
	});
</script>	
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
						<li><a href="doctor.html">รายชื่อหมอ</a></li>
						<li><a href="appointment.html">ตารางการนัด</a></li>
						<li><a href="packet.html">แพ็กเกจ</a></li>
						<li><a href="contact-us.html">ติดต่อเจ้าหน้าที่</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-9 col-md-4 col-sm-6 col-xs-9">
	             <div class="appointment-form">
	                <h3>แก้ไขของรางวัล</h3>
	                <div class="form">
	                  <form name="frm" id="myformAward" action="/WebService/EditAward" method="POST" enctype="multipart/form-data"  novalidate>
	                      <fieldset>
	                      	<input type="hidden" name="award_id" value="<%= award.getAward_id() %>">
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="fname" class="col-sm-2 control-label">ขื่อของรางวัล </label>
	                               <div class="form-group">
	                               		<input type="text" class="form-control" name="award_name" id="award_name" autocomplete="off" value="<%= award.getAward_name()  %>" pattern="^[A-Za-zก-์]{1,40}$"  required >
	                               </div>
	                            </div>
	                         </div>
	                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="detail" class="col-sm-2 control-label">รายละเอียด</label>
	                               <div class="form-group">
						               <textarea class="form-control" name="award_detail" id="award_detail" autocomplete="off"
											rows="8" cols="50" pattern="^[A-Za-zก-์]$"  required ><%= award.getAward_detail()  %> </textarea>
	                               </div>
	                            </div>
	                         </div>
	                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="detail" class="col-sm-6 control-label">คะแนนในการแลก</label>
	                               <div class="form-group">
						               <input type="number" class="form-control" name="score" id="score" autocomplete="off" value="<%= award.getScore()  %>"  pattern="^[0-9]$"  required >
	                               </div>
	                            </div>
	                         </div>
	                         <%
	                         	if(award.getAvailable_from() != null && award.getExpiration_date() != null){
	                         	String date1 = award.getAvailable_from().getTime().toInstant().toString().replaceAll("T17:00:00Z", "").trim();
								System.out.println("day = "+date1);
								
								String[] d = date1.split("-");
							    int day = Integer.parseInt(d[2])+1 ;
							    String date = "";
							    if(day < 10){
							    	date = "0"+day + "-"+ d[1]  +"-" + d[0] ;
							    }else{
							    	date = d[2] + "-"+ d[1]  +"-" + d[0] ;
							    }
							    String datee = award.getExpiration_date().getTime().toInstant().toString().replaceAll("T17:00:00Z", "").trim();
								System.out.println("day = "+datee);
								
								String[] d2 = datee.split("-");
							    int day2 = Integer.parseInt(d2[2])+1 ;
							    String date2 = "";
							    if(day2 < 10){
							    	date2 = "0"+day2 + "-"+ d2[1]  +"-" + d2[0] ;
							    }else{
							    	date2 = day2 + "-"+ d2[1]  +"-" + d2[0] ;
							    }
	                         
	                         %>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="detail" class="col-sm-6 control-label">ใช้ได้ตั้งแต่</label>
	                               <div class="form-group">
						               <input type="text" class="date form-control" name="Available_from" id="Available_from" autocomplete="off" value="<%= date  %>" pattern="^[0-9]$"  required >
	                               </div>
	                            </div>
	                         </div>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="detail" class="col-sm-6 control-label">วันหมดอายุ</label>
	                               <div class="form-group">
						               <input type="text" class="date form-control" name="Expiration_date" id="Expiration_date" autocomplete="off" value="<%= date2  %>" pattern="^[0-9]$"  required >
	                               </div>
	                            </div>
	                         </div>
	                         <script type="text/javascript">
									$(".date").datepicker({
										format : "dd-mm-yyyy",
										autoclose : true
									});
							</script>
							<%}else{ %>
							 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="detail" class="col-sm-6 control-label">ใช้ได้ตั้งแต่</label>
	                               <div class="form-group">
						               <input type="text" class="date form-control" name="Available_from" id="Available_from" autocomplete="off" value="" pattern="^[0-9]$"  required >
	                               </div>
	                            </div>
	                         </div>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="detail" class="col-sm-6 control-label">วันหมดอายุ</label>
	                               <div class="form-group">
						               <input type="text" class="date form-control" name="Expiration_date" id="Expiration_date" autocomplete="off" value="" pattern="^[0-9]$"  required >
	                               </div>
	                            </div>
	                         </div>
							<% } %>
<!-- 	                         <div class="form-group row"> -->
<!-- 								<label class="col-sm-6 col-form-label text-right">รูปหน้าปก </label> -->
<!-- 								<div class="col col-5"> -->
<%-- 									<img alt="" style="align-items: center;" src="./img/<%= award.getImg_award() %>" width="200" height="200"> --%>
<!-- 								</div> -->
<!-- 							</div> -->
			    			 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="pwd" class="col-sm-6 control-label">อัพโหลดรูปภาพประกอบ</label>
                                 <div class="form-group">
                                      <input type="file" name="img_awards" id="customFile"accept="image/*">
                                 </div>
                              </div>
                           </div>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                               <div class="form-group">
	                                  <div class="center">
	                                  	<button type="submit" value="อัพเดทข้อมูล" name="submit"
	                                      class="btn" style="text-align: center; ">
	                                      <img alt="" src="./img/diskette.png" width="30px" height="30px"> อัพเดทข้อมูล</button>
	                                  </div>
	                               </div>
	                            </div>
	                         </div>
	                         
	                      </fieldset>
	                   </form>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	

<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>