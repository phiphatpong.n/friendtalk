<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*, java.text.*"%>
<%
	List<MakeAppointment> list_histroy =(List<MakeAppointment>) request.getAttribute("list_histroy");
	String user = (String) session.getAttribute("user");
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->
</head>
<body class="clinic_version">
	
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	             <div class="appointment-form">
	                <h3>ประวัติการให้คำปรึกษา</h3>
	                	<table border="1" class="table table-bordered   table-hover">
	                		<thead>
								<tr>
								<th>ชื่อ นามสกุล</th>
								<th>วันที่นัด</th>
								<th>เวลาเริ่มต้น</th>
								<th>เวลาสิ้นสุด</th>
								<th>ประเภทการติดต่อ</th>
								<th>สถานะการให้คำปรึกษา</th>
								</tr>
							</thead>
							<tbody>
							<%
								if (list_histroy.size() >= 1) {
							%>
							<% for(int i=0 ; i< list_histroy.size() ; i++){ %>
							
								<%
										String ds = list_histroy.get(i).getTime_start().toInstant().toString().substring(0, 10);
										System.out.println("ds = "+ds );
										String[] StrD = ds.split("-");
										String d = StrD[2] +"-"+StrD[1]+"-"+StrD[0];
										
										int hs = list_histroy.get(i).getTime_start().getTime().getHours();
										int ms = list_histroy.get(i).getTime_start().getTime().getMinutes();
										int ss = list_histroy.get(i).getTime_start().getTime().getSeconds();
										 
										String de = list_histroy.get(i).getTime_end().toInstant().toString().substring(0, 10);
										System.out.println("de = "+de );
										int	 he = list_histroy.get(i).getTime_end().getTime().getHours();
										int	 me = list_histroy.get(i).getTime_end().getTime().getMinutes();
										int	 se = list_histroy.get(i).getTime_end().getTime().getSeconds();
											 

										String[] s = ds.split("-");
									    int day = Integer.parseInt(s[2]);
									    int month = Integer.parseInt(s[1]);
									    int year = Integer.parseInt(s[0]);
									    
									    String[] e = de.split("-");
									    int daye = Integer.parseInt(e[2]);
									    int monthe = Integer.parseInt(e[1]);
									    int yeare = Integer.parseInt(e[0]);
									    
									    String times = String.valueOf(hs)+":"+String.valueOf(ms)+":"+String.valueOf(ss);
									    String timee = String.valueOf(he)+":"+String.valueOf(me)+":"+String.valueOf(se);
									    DateFormat sdf = new SimpleDateFormat("HH:mm:ss");
									    Date date1 = sdf.parse(times);
									    Date date2 = sdf.parse(timee);
									    System.out.println("date1 "+ date1 );
									    System.out.println("date2 "+ date2 );
									    //datee
									    String subtimeS = date1.toString().substring(11,16);
									    System.out.println("subtimeS "+ subtimeS );
									    String subtimeE = date2.toString().substring(11,16);
									    System.out.println("subtimeE "+ subtimeE );
									    
									%>
									
								<tr>
								<td><label><%= list_histroy.get(i).getConsultant().getFristname()%></label></td>
								<td><label><%= d %></label></td>
								<td><label><%= subtimeS %></label></td>
								<td><label><%= subtimeE %></label></td>
								<td><label><%= list_histroy.get(i).getPrice().getContact_type()%></label></td>
								<td><label><%= list_histroy.get(i).getStatus_appoint() %></label></td>
								</tr>
								<br>
							<% } }%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	

<jsp:include page="navbar/footer.jsp" />
		
		
</body>
</html>