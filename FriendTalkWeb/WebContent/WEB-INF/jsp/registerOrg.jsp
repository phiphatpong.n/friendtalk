<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ page import="bean.*, java.util.*" %>
	<%
		String mtype =(String) session.getAttribute("mtype");
	%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->
</head>

		<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>

<!-- chack data script -->
<script src="./js/CheckScriptInformation/JSorg.js"></script>

<script type="text/javascript">

	$.validator.addMethod('filesize', function(value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	}, 'ขนาดไฟล์ต้องไม่เกิน 2 MB และต้องเป็น  jpg,jpeg,pdf เท่านั้น {0}');

	jQuery(function($) {
		"use strict";
		$('#myformorg').validate({
			rules : {
				FirstName : {
					required : true,
					maxlength : 20
				},
				image : {
					required : true,
					extension : "jpg,jpeg,pdf",
					filesize : 2000,
				}
			},
		});
	});
</script>	
</head>
<body class="clinic_version">
	
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
                     <li><a class="active" href="openindex">หน้าแรก</a></li>
                     <li><a href="/WebService/openListPsychologistent">รายชื่อหมอ</a></li>
                     <li><a href="/WebService/openPackage">แพ็กเกจ</a></li>
                     <li><a href="/WebService/openlistArticle">รายการบทความ</a></li>
                     <li><a href="contact-us.html">ติดต่อเจ้าหน้าที่</a></li>
                     <li><a href="/WebService/openlogin">เข้าสู่ระบบ</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
		<div id="service" class="services2 wow fadeIn">
	      <div class="container">
	         <div class="row center">
	            <div class="col-lg-9 col-md-4 col-sm-6 col-xs-9">
	               <div class="appointment-form">
	                  <h3>สมัครสมาชิก</h3>
	                  <div class="form">
	                    <form name="frm" id="myformorg" action="/WebService/registerorg" method="POST" enctype="multipart/form-data" onsubmit="return myFunction();"  novalidate>
	                        <fieldset>
	                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                              <div class="row">
	                              	<label for="fname" class="col-sm-2 control-label">ชื่อบริษัท</label>
	                              		<p class="col-sm-6 control-label" id="demo1" style="color: red; font-size: 16px"></p>
	                                 <div class="form-group">
	                                 	<input type="text" class="form-control" name="cname" id="cname" autocomplete="off" value="" pattern="^[A-Za-zก-์]{1,40}$"  required >
	                                 </div>
	                              </div>
	                           </div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                              <div class="row">
	                              	<label for="email" class="col-sm-2 control-label">อีเมล</label>
	                              		<p class="col-sm-6 control-label" id="demo2" style="color: red; font-size: 16px"></p>
	                                 <div class="form-group">
	                                     <input type="text" class="form-control" name="email" id="email"
												pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" autocomplete="off" value="" required>
	                                 </div>
	                              </div>
	                           </div>
	                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                              <div class="row">
	                              	<label for="tel" class="col-sm-2 control-label">เบอร์มือถือ</label>
	                              		<p class="col-sm-6 control-label" id="demo3" style="color: red; font-size: 16px"></p>
	                                 <div class="form-group">
	                                    <input type="text" class="form-control" pattern="^0([8|9|6])([0-9]{8}$)" name="tel" id="tel"
											autocomplete="off" value="" required>
	                                 </div>
	                              </div>
	                           </div>
	                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                              <div class="row">
	                              	<label for="address" class="col-sm-2 control-label">ที่อยู่</label>
	                              		<p class="col-sm-6 control-label" id="demo4" style="color: red; font-size: 16px"></p>
	                                 <div class="form-group">
	                                     <textarea class="form-control" name="address" id="address" rows="3" autocomplete="off" required></textarea>
	                                 </div>
	                              </div>
	                           </div>
	                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                              <div class="row">
	                              	<label for="congenital-disease" class="col-sm-2 control-label">จำนวนพนักงาน</label>
	                              		<p class="col-sm-6 control-label" id="demo5" style="color: red; font-size: 16px"></p>
	                                 <div class="form-group">
	                                     <input type="number" class="form-control" name="employee_number" id="employee_number"
											autocomplete="off" value="" required>
	                                 </div>
	                              </div>
	                           </div>
	                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                              <div class="row">
	                              	<label for="uname" class="col-sm-3 control-label">ชื่อผู้ใช้</label>
	                              		<p class="col-sm-6 control-label" id="demo6" style="color: red; font-size: 16px"></p>
	                                 <div class="form-group">
	                                     <input type="text" class="form-control" name="uname" id="uname" autocomplete="off" pattern="^[A-Za-z0-9]{1,40}$" required>
	                                 </div>
	                              </div>
	                           </div>
	                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                              <div class="row">
	                              	<label for="pwd" class="col-sm-2 control-label">รหัสผ่าน</label>
	                              		<p class="col-sm-6 control-label" id="demo7" style="color: red; font-size: 16px"></p>
	                                 <div class="form-group">
	                                      <input type="password" class="form-control" name="pwd" id="pwd" autocomplete="off" value="" pattern="^[A-Za-z0-9]{1,40}$" required>
	                                 </div>
	                              </div>
	                           </div>
	                           
	                           <input type="hidden" name="mtype" value="<%= mtype %>" readonly>
	                           
	                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                              <div class="row">
	                              	<label for="pwd" class="col-sm-6 control-label">อัพโหลดสำเนาเอกสารการจดทะเบียนบริษัท(pdf)</label>
	                              		<p class="col-sm-6 control-label" id="demo7" style="color: red; font-size: 16px"></p>
	                                 <div class="form-group">
	                                      <input type="file" name="crc" id="customFile"accept="application/pdf">
	                                 </div>
	                              </div>
	                           </div>
	                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                              <div class="row">
	                                 <div class="form-group">
	                                    <div class="center">
	                                    	<input type="submit" value="สมัครสมาชิก" name="submit" id="submitButton" 
	                						 class="form-control-submit-button" title="สมัครสมาชิก" >
	                					</div>
	                                 </div>
	                              </div>
	                           </div>
	                           
	                        </fieldset>
	                     </form>
	                  </div>
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>	


<jsp:include page="navbar/footer.jsp" />
 </body>
</html>