<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	Psychologist psy = (Psychologist) session.getAttribute("psy");
	List<Award> awList = (List<Award>) request.getAttribute("awList");
	List<Redeem> lredeem = (List<Redeem>) request.getAttribute("lredeem");
	
	int alertredeem = -1;
	try {
		alertredeem = (Integer) request.getAttribute("alertredeem");
	} catch (Exception e) {
	}
	session.removeAttribute("alertredeem");
%>
<%
	if (1 == alertredeem) {
%>
<script type="text/javascript">
	alert("แลกของรางวัลสำเร็จ");
</script>
<%
	} else if (0 == alertredeem) {
%>
<script type="text/javascript">
	alert("แลกของรางวัลไม่สำเร็จ/หรือคะแนนของท่านไม่เพียงพอ");
</script>
<%
	}
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->
</head>
<body class="clinic_version">
	
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="/WebService/openHome">หน้าแรก</a></li>
						<li><a href="/WebService/openHistoryRedeem?user=<%= psy.getUsername() %>">ประวัติเเลกของรางวัล</a></li>
						
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	             <div class="appointment-form">
	                <h3>รายการของรางวัล</h3>
	                <li style="font-size: 26px; font-style: italic;">คะแนนสะสม : <%= psy.getGoodness_score() %></li>
	                <table border="1" class="table table-bordered   table-hover">
							<thead>
								<tr>
									<th></th>
									<th>ชื่อของรางวัล</th>
									<th>คะแนนที่ใช้แลก</th>
									<th>ระยะเวลาในการใช้งาน</th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
						<%if (awList.size() >= 1) {
							for (int i = 0; i < awList.size(); i++) {
						%>
						<tr>
							<td><img alt="" src="./img/<%=awList.get(i).getAward_id()%>.jpg" width="150" height="150"></td>
							<td><label><%=awList.get(i).getAward_name()%></label></td>
							<td><label><%=awList.get(i).getScore()%></label></td>
							
							
							 <%
	                         	if(awList.get(i).getAvailable_from() != null && awList.get(i).getExpiration_date() != null){
	                         	String date1 = awList.get(i).getAvailable_from().getTime().toInstant().toString();
								System.out.println("day = "+date1);
								
								String str = date1.substring(0,10);
								System.out.println("day 3 = "+str);
								
								String[] d = str.split("-");
							    int day = Integer.parseInt(d[2])+1 ;
							    String date = "";
							    if(day < 10){
							    	date = "0"+day + "-"+ d[1]  +"-" + d[0] ;
							    }else{
							    	date = d[2] + "-"+ d[1]  +"-" + d[0] ;
							    }
							    String datee = awList.get(i).getExpiration_date().getTime().toInstant().toString().replaceAll("T17:00:00Z", "").trim();
								System.out.println("day = "+datee);
								
								String str1 = datee.substring(0,10);
								System.out.println("day 3 = "+str);
								
								String[] d2 = str1.split("-");
							    int day2 = Integer.parseInt(d2[2])+1 ;
							    String date2 = "";
							    if(day2 < 10){
							    	date2 = "0"+day2 + "-"+ d2[1]  +"-" + d2[0] ;
							    }else{
							    	date2 = day2 + "-"+ d2[1]  +"-" + d2[0] ;
							    }
	                         
	                         %>
	                         	<td><label>ใช้ได้ตั้งแต่วัน :<%= day %> 
									ถึง : <%= day2 %>  เดือน <%= d2[1] %></label></td>
							<%}else{ %>
							 	<td><label>ใช้ได้ตั้งแต่วัน : </label></td>
							<% } %>
							<td align="center">
							<form action="/WebService/openSeeMore"method="POST" name="s1frm">
								<input type="text" name="awid" id="awid" value="<%=awList.get(i).getAward_id()%>" readonly="readonly" hidden="hidden">
								<input type="text" name="psyid" id="psyid" value="<%=psy.getUsername()%>" readonly="readonly" hidden="hidden">
								<input type="text" name="Score" id="Score" value="<%=awList.get(i).getScore()%>" readonly="readonly" hidden="hidden">
								<input type="submit" value="เพิ่มเติม" name="openAwardDetails" id="submitButton" class="form-control-submit-button">
							</form></td>
							<td align="center">
							<form action="/WebService/redeem" method="POST" name="s2frm">
								<input type="text" name="awid" id="awid" value="<%=awList.get(i).getAward_id()%>" readonly="readonly" hidden="hidden">
								<input type="text" name="psyid" id="psyid" value="<%=psy.getUsername()%>" readonly="readonly" hidden="hidden">
								<input type="text" name="Score" id="Score" value="<%=awList.get(i).getScore()%>" readonly="readonly" hidden="hidden">
							<% boolean ck = true ;
								if(lredeem.size() >= 1){
								for(int k=0 ; k < lredeem.size() ; k++){%>
									<%if(awList.get(i).getAward_id().equals(lredeem.get(k).getAward().getAward_id())){ %>
										<input type="submit" value="แลกของรางวัล" name="Redeem" class="form-control-submit-button" id="submitButton" disabled="disabled" style="background: gray;">
									<%ck=false; break;} %>
									<% }%>
									<%if(ck){ %> 
										<input type="submit" value="แลกของรางวัล" name="Redeem" class="form-control-submit-button" id="submitButton" onclick="return confirm('คุณต้องการแลกของรางวัลใช่หรือไม่?');">
								<%}}else{ %>								
									<input type="submit" value="แลกของรางวัล" name="Redeem" class="form-control-submit-button" id="submitButton" onclick="return confirm('คุณต้องการแลกของรางวัลใช่หรือไม่?');">
							<% } %>
							</form>
							</td>
						</tr>
						<% } } %>
						</tbody>	
					</table>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>
	


<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>