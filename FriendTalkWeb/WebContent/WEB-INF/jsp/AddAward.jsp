<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	String user = (String) session.getAttribute("user");
	String maxid = (String) session.getAttribute("Awid");
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->
	
	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
	
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
	
	<script src="./js/CheckScriptInformation/JSAwaed.js"></script>
	
	
<script type="text/javascript">
function checkdate(){
var s = document.getElementById('Available_from').value;
var e = document.getElementById('Expiration_date').value	;
var startDate = new Date(s);
var endDate = new Date(e);

if (startDate > endDate){
	alert("กรุณาเลือกวันที่ให้ถูกต้อง");
	document.getElementById('Expiration_date').value	= s; 
}
}
</script>
<script type="text/javascript">
	$.validator.addMethod('filesize', function(value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	}, 'ขนาดไฟล์ต้องไม่เกิน 2 MB และต้องเป็น  jpg,jpeg เท่านั้น {0}');

	jQuery(function($) {
		"use strict";
		$('#myformAward').validate({
			rules : {
				FirstName : {
					required : true,
					maxlength : 20
				},
				image : {
					required : true,
					extension : "jpg,jpeg",
					filesize : 2000,
				}
			},
		});
	});
</script>	
</head>
<body class="clinic_version">

	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-9 col-md-4 col-sm-6 col-xs-9">
	             <div class="appointment-form">
	                <h3>เพิ่มของรางวัล</h3>
	                <div class="form">
	                  <form name="frm" id="myformAward" action="/WebService/AddAward" method="POST" enctype="multipart/form-data" onsubmit="return myfunction();"  novalidate>
	                      <fieldset>
	                      	<input type="hidden" name="award_id" value="<%= maxid %>">
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="fname" class="col-sm-2 control-label">ขื่อของรางวัล </label>
	                                <p class="col-sm-6 control-label" id="demo1" style="color: red; font-size: 16px"></p>
	                               <div class="form-group">
	                               		<input type="text" class="form-control" name="award_name" id="award_name" autocomplete="off" value="" pattern="^[A-Za-zก-์]{1,40}$"  required >
	                               </div>
	                            </div>
	                         </div>
	                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="detail" class="col-sm-2 control-label">รายละเอียด</label>
	                                <p class="col-sm-6 control-label" id="demo4" style="color: red; font-size: 16px"></p>
	                               <div class="form-group">
						               <textarea class="form-control" name="award_detail" id="award_detail" autocomplete="off"
											rows="8" cols="50"  required ></textarea>
	                               </div>
	                            </div>
	                         </div>
	                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="detail" class="col-sm-6 control-label">คะแนนในการแลก</label>
	                                <p class="col-sm-6 control-label" id="demo2" style="color: red; font-size: 16px"></p>
	                               <div class="form-group">
						               <input type="number" class="form-control" name="score" id="score" autocomplete="off" value=""  required >
	                               </div>
	                            </div>
	                         </div>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="detail" class="col-sm-6 control-label">ใช้ได้ตั้งแต่</label>
	                               <div class="form-group">
	                               <p style="color: red; font-size: 16px">*ในกรณีเป็นบัตรส่วนลดหรือบัตรกำนันต่างๆให้ใส่วันที่สามารถใช้งานเเละวันหมดอายุ</p>
	                                <p style="color: red; font-size: 16px">*แต่ถ้าเป็นสิ่งอื่นๆที่ไม่มีวันหมดอายุให้ใส่วันที่เพิ่มทั้งช่องวันเริ่มต้นและวันหมดอายุ</p>
						               <input type="date" class="form-control" name="Available_from" id="Available_from" autocomplete="off" >
	                               </div>
	                            </div>
	                         </div>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="detail" class="col-sm-6 control-label">วันหมดอายุ</label>
	                                
	                               <div class="form-group">
						               <input type="date" class="form-control" name="Expiration_date" id="Expiration_date" autocomplete="off"onchange="return checkdate(this)"  >
	                               </div>
	                            </div>
	                         </div>
			    			 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="pwd" class="col-sm-6 control-label">อัพโหลดรูปภาพประกอบ</label>
                              	<p class="col-sm-6 control-label" id="demo3" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                      <input type="file" name="img_awards" id="customFile"accept="image/*">
                                 </div>
                              </div>
                           </div>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                               <div class="form-group">
	                                  <div class="center">
	                                      <button type="submit" value="เพิ่มรางวัล" name="submit"
	                                      class="btn" style="text-align: center; ">
	                                      <img alt="" src="./img/add.png" width="30px" height="30px"> เพิ่มรางวัล</button>
	                                  </div>
	                               </div>
	                            </div>
	                         </div>
	                         
	                      </fieldset>
	                   </form>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	

<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>