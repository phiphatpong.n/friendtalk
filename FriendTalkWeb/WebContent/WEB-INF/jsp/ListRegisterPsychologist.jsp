<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	List<Psychologist> list = (List<Psychologist>) request.getAttribute("chackListpsy");

	int alertVertify = 3;
	try {
		alertVertify = (Integer) session.getAttribute("alertVertify");
	} catch (Exception e) {
	}
	session.removeAttribute("alertVertify");
	
	int alertDe = 5;
	try {
		alertDe = (Integer) session.getAttribute("alt");
	} catch (Exception e) {
	}
	session.removeAttribute("alertDe");
%>
<% if (1 == alertVertify) {%>
<script type="text/javascript">
	alert("ยืนยันสำเร็จ");
</script>
<%}else if(0 == alertVertify){%>
<script type="text/javascript">
	alert("ยืนยันไม่สำเร็จ");
</script>
<% } %>
<% if (1 == alertDe) {%>
<script type="text/javascript">
	alert("ยกเลิกการสมัครสำเร็จ");
</script>
<%}else if(0 == alertDe){%>
<script type="text/javascript">
	alert("ยกเลิกการสมัครไม่สำเร็จ");
</script>
<% } %>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="clinic_version">
	<!-- LOADER -->
	<div id="preloader">
		<img class="preloader" src="./img/loaders/heart-loading2.gif" alt="">
	</div>
	<!-- END LOADER -->
	<div class="header-top wow fadeIn">
		<div class="container">
			<a class="navbar-brand" href="openHome"><img
				src="./img/logo3.png" alt="image"></a>
			<div class="right-header">
				<div class="header-info">
					<div class="info-inner">
						<span class="icontop"><img src="./img/phone-icon.png"
							alt="#"></span> <span class="iconcont"><a
							href="tel:800 123 456">800 123 456</a></span>
					</div>
					<div class="info-inner">
						<span class="icontop"><i class="fa fa-envelope"
							aria-hidden="true"></i></span> <span class="iconcont"><a
							data-scroll href="mailto:info@yoursite.com">info@Lifecare.com</a></span>
					</div>
					<div class="info-inner">
						<span class="icontop"><i class="fa fa-clock-o"
							aria-hidden="true"></i></span> <span class="iconcont"><a
							data-scroll href="#">Daily: 7:00am - 8:00pm</a></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
						<li><a href="/WebService/ViewHistoryverifyregister">รายชื่อหมอที่ทำการยืนยันไปแล้ว</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

<br>
	<div id="service" class="services2 wow fadeIn">
	    <div class="">
	       <div class="row center">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	             <div class="appointment-form">
	                <h3>รายชื่อผู้สมัครเป็นผู้ให้คำปรึกษา</h3>
	                <div class="form">
	                  <form name="frm" id="myform" action="/WebService/verifyregisterPsy" method="POST">
	                      <fieldset>
	                         	<table border="2" style="width: 100% ;" class="table table-bordered table-hover">
									<thead>
									<tr>
									<th>ชื่อ - นามสกุล </th>
									<th>เมล์</th>
									<th>ประเภทของผู้สมัคร</th>
									<th>ประวัติการศึกษา</th>
									<th>ใบรับรอง/วุฒิการศึกษา</th>
									<th>ยืนยัน</th>
									<th>ยกเลิก</th>
									</tr>
									</thead>
									<tbody>
										<% if(list.size() >= 1){ %>
										<% for(int i=0 ; i<list.size() ; i++){ %>
											<tr>
											<% String name = list.get(i).getTitle()+" "+list.get(i).getFristname()+" "+list.get(i).getLastname(); %>
											<td><label><%= name %></label></td>
											<td><label><%= list.get(i).getEmail() %></label></td>
											<td><label><%= list.get(i).getMentor_type()%></label></td>
											<td style="width: 45% ;"><label><%= list.get(i).getEducationl_history() %></label></td>
											<td><a href="./img/<%= list.get(i).getCertificate() %>">เปิดไฟล์</a></td>
											
											<input type="hidden" name="username" value="<%=list.get(i).getUsername() %>">
											
											<td><button type="submit" class="btn" name="confirm" style="background-color: #33CCFF ;"><img alt="" src="./img/checkbox.png" width="20px" height="20px"> ยืนยัน</button></td>
											
											<td><a href="/WebService/cancelconfirm?user=<%= list.get(i).getUsername()%>">
											<button type="button" class="btn" name="confirm" onclick="return confirm('คุณต้องการยกเลิกการสมัครของผู้ให้คำปรึกษาคนนี้ใช่หรือไม่?'); " style="background-color: #33CCCC;">
											<img alt="" src="./img/trash.png" width="20px" height="20px">ยกเลิก</button></a></td>
											</tr>
											
										<%} }%>
										</tbody>
								</table>
							</fieldset>
	                   </form>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	

<jsp:include page="navbar/footer.jsp" />
		
    
</body>
</html>