<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	String user = (String) session.getAttribute("user");
	List<PaymentCode> listpay =(List<PaymentCode>) request.getAttribute("listpay");
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

</head>
<body class="clinic_version">
	<!-- LOADER -->
	<div id="preloader">
		<img class="preloader" src="./img/loaders/heart-loading2.gif" alt="">
	</div>
	<!-- END LOADER -->
	<div class="header-top wow fadeIn">
		<div class="container">
			<a class="navbar-brand" href="openHome"><img
				src="./img/logo3.png" alt="image"></a>
			<div class="right-header">
				<div class="header-info">
					<div class="info-inner">
						<span class="icontop"><img src="./img/phone-icon.png"
							alt="#"></span> <span class="iconcont"><a
							href="tel:800 123 456">800 123 456</a></span>
					</div>
					<div class="info-inner">
						<span class="icontop"><img src="./img/email-icon.png"
							alt="#"></span></span> <span class="iconcont"><a
							data-scroll href="phiphatpong.n@gmail.com">phiphatpong.n@gmail.com</a></span>
					</div>
					<div class="info-inner">
						<span class="icontop"><img src="./img/oClock-icon.png" width="60" height="30"
							alt="#"></span></span> <span class="iconcont"><a
							data-scroll href="#">Daily: 09:00am - 06:00pm</a></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
						<li><a href="/WebService/openPaymantCode?user=<%= user%>">พิมพ์รหัสชำระเงิน</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	             <div class="appointment-form">
	                <h3>ประวัติการใช้งาน</h3>
	                <table border="1" class="table table-bordered   table-hover">
							<thead>
								<tr>
									<th>รหัสชำระเงิน</th>
									<th>จำนวนเวลาแชท(นาที)</th>
									<th>จำนวนเวลาคุย(นาที)</th>
									<th>วันที่ออก</th>
									<th>สถานะการใช้งาน</th>
								</tr>
							</thead>
							<tbody>
							<% if(listpay.size() >= 1){ %>
							<% for(int i=0 ; i<listpay.size() ; i++){ %>
								<tr>
								<td><label><%= listpay.get(i).getCode() %></label></td>
								<td><label><%= listpay.get(i).getCall_minute() %></label></td>
								<td><label><%= listpay.get(i).getChat_minute() %></label></td>
								<%
									String time = listpay.get(i).getIssue_date().toString().substring(0, 10).trim();
									String[] StrT = time.split("-");
									String t = StrT[2] +"-"+StrT[1]+"-"+StrT[0];
								%>
								<td><label><%= t %></label></td>
								<td><label><%= listpay.get(i).getUsage_status() %></label></td>
								</tr>
				
							<% } }%>
						</tbody>
					</table>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>
	

	
<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>