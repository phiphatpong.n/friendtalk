<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*, java.text.*"%>
<%
	Article  art = new Article();
	art = (Article) session.getAttribute("article");
	
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<script src="./js/Tiny/all_plugins.js"></script>
<script src="./js/Tiny/patch.js"></script>
<script src="./js/Tiny/rte.js"></script>
<link rel="stylesheet" href="./js/Tiny/rte_theme_default.css">
<link rel="stylesheet" href="./js/Tiny/style.css">

<link rel="stylesheet" href="./js/Tiny/tiny.css">
<script src="./js/Tiny/tiny.js"></script>
	 
<!-- chack data script -->
<script src="./js/CheckScriptInformation/CheckScript-Article.js"></script>

	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>

<script type="text/javascript">

	$.validator.addMethod('filesize', function(value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	}, 'ขนาดไฟล์ต้องไม่เกิน 2 MB และต้องเป็น  jpg,jpeg เท่านั้น {0}');

	jQuery(function($) {
		"use strict";
		$('#myformArti').validate({
			rules : {
				FirstName : {
					required : true,
					maxlength : 20
				},
				image : {
					required : true,
					extension : "jpg,jpeg",
					filesize : 2000,
				}
			},
		});
	});
</script>	
</head>
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
						<li><a href="doctor.html">รายชื่อหมอ</a></li>
						<li><a href="appointment.html">ตารางการนัด</a></li>
						<li><a href="packet.html">แพ็กเกจ</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	             <div class="appointment-form">
	                <h3>เพิ่มบทความ</h3>
	                <div class="form">
	                  <form name="frm" id="myformArti" action="/WebService/EditArticle" method="POST" enctype="multipart/form-data"  novalidate>
	                      <fieldset>
	                      	<input type="hidden" name="article_id" value="<%= art.getArticle_id() %>">
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="fname" class="col-sm-2 control-label">หัวเรื่องบทความ </label>
	                               <div class="form-group">
	                               		<input type="text" class="form-control" name="article_name" id="article_name" 
	                               			autocomplete="off" value="<%= art.getArticle_name() %>" pattern="^[A-Za-zก-์]{1,40}$"  required >
	                               </div>
	                            </div>
	                         </div>
	                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label class="col-sm-2 control-label">รายละเอียด</label><br>
	                                <p class="col-sm-6 control-label" id="demo2" style="color: red; font-size: 16px"></p>
	                                
<!-- 	                               		<div id="div_editor1"></div> -->
	                               		<textarea name="article_detail" id="div_editor1" rows="50" cols="20"><%= art.getArticle_detail() %></textarea>
										<script type="text/javascript">
											var editor1 = new RichTextEditor("#div_editor1");
										</script>
	                            </div>
	                         </div>
	                         <%
	                         	String date1 = art.getAdd_date().getTime().toInstant().toString().replaceAll("T17:00:00Z", "").trim() ;
								System.out.println("day = "+date1);
								
								String[] d = date1.split("-");
							    int day = Integer.parseInt(d[2])+1 ;
							    String date = "";
							    if(day < 10){
							    	date = "0"+day + "-"+ d[1]  +"-" + d[0] ;
							    }else{
							    	date = d[2] + "-"+ d[1]  +"-" + d[0] ;
							    }
	                         
	                         %>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="tel" class="col-sm-2 control-label">วันที่เพิ่ม</label>
	                               <div class="form-group">
	                                 <input type="text" class="date datepicker" name="Dateadded" id="Dateadded" 
	                                 	autocomplete="off" value="<%= date %>" required>
	                               </div>
	                            </div>
	                         </div>
	                         <script type="text/javascript">
			      				$(".date").datepicker({
			        				format: "dd-mm-yyyy",
			        				autoclose: true
			      				});
			    			</script>
			    			 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="pwd" class="col-sm-2 control-label">อัพโหลดรูปหน้าปก</label>
                                 <div class="form-group">
                                      <input type="file" name="img_A" id="customFile"accept="image/*" >
                                 </div>
                              </div>
                           </div>
	                         <input type="hidden" name="uname" value="<%= art.getPsychologist().getUsername() %>">
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                               <div class="form-group">
	                                  <div class="center">
	                                      <button type="submit" value="อัพเดทข้อมูล" name="submit"
	                                      class="btn" style="text-align: center; ">
	                                      <img alt="" src="./img/diskette.png" width="30px" height="30px"> อัพเดทข้อมูล</button>
	                                  </div>
	                               </div>
	                            </div>
	                         </div>
	                         
	                      </fieldset>
	                   </form>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	
	
<jsp:include page="navbar/footer.jsp" />

</body>
</html>