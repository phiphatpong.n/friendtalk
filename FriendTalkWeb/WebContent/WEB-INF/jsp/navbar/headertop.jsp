<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="bean.*, java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%
	String user = (String) session.getAttribute("uname");	
	String type = (String) session.getAttribute("Mtype");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Friends Talk</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
</head>
<body>
<!-- LOADER -->
	<!-- LOADER -->
	<div id="preloader">
		<img class="preloader" src="./img/loaders/heart-loading2.gif" alt="">
	</div>
	<!-- END LOADER -->
	<div class="header-top wow fadeIn">
		<div class="container">
			<a class="navbar-brand" href="openHome"><img
				src="./img/logo3.png" alt="image"></a>
			<div class="right-header">
				<div class="header-info">
					<div class="info-inner">
						<span class="icontop"><img src="./img/phone-icon.png"
							alt="#"></span> <span class="iconcont"><a
							href="tel:054451998">054 451 998</a></span>
					</div>
					<div class="info-inner">
						<span class="icontop"><img src="./img/email.png"
							alt="#"></span></span> <span class="iconcont"><a
							data-scroll href="phiphatpong.n@gmail.com">phiphatpong.n@gmail.com</a></span>
					</div>
					<div class="info-inner">
						<span class="icontop"><img src="./img/oClock-icon.png" width="60" height="30"
							alt="#"></span></span> <span class="iconcont"><a
							data-scroll href="#">Daily: 09:00am - 06:00pm</a></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>