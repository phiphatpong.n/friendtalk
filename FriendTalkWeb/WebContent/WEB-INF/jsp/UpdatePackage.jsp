<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	packages  pack = new packages();
	pack = (packages) session.getAttribute("pack");
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

    <script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
    
    	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>

</head>	
<script type="text/javascript">
function myFunction() {
	
    const call_hr = document.getElementById("call_hr")
    if (!call_hrcheckValidity()) {
      document.getElementById("demo1").innerHTML = "*กรุณากรอกข้อความรีวิว";
      return false;
    } else {
      document.getElementById("demo1").innerHTML = "";
    }
    
    const chat_hr = document.getElementById("chat_hr")
    if (!chat_hr.checkValidity()) {
      document.getElementById("demo2").innerHTML = "*กรุณากรอกคะแนนเป็นตัวเลข";
      return false;
    } else {
      document.getElementById("demo2").innerHTML = "";
    }
    const price = document.getElementById("price")
    if (!price.checkValidity()) {
      document.getElementById("demo2").innerHTML = "*กรุณากรอกคะแนนเป็นตัวเลข";
      return false;
    } else {
      document.getElementById("demo2").innerHTML = "";
    }

  }
</script>
<body class="clinic_version">
	
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-9 col-md-4 col-sm-6 col-xs-9">
	             <div class="appointment-form">
	                <h3>แก้ไขแพ็คเกจ</h3>
	                <div class="form">
	                     <fieldset>
	                       <form name="frm" id="myform1" action="/WebService/editPackage" method="POST" enctype="multipart/form-data" novalidate="novalidate">
							<table class="center">
								<div class="">
		                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                            <div class="row">
		                            	<label for="input_name" class="col-sm-6 control-label">ขนาดแพ็คเกจ </label>
										<div class="form-group ">
												<input type="text" class="form-control" name="package_size" id="package_size" autocomplete="off" value="<%= pack.getPackage_size() %>" pattern="^[A-Za-zก-์]{1,40}$"  readonly="readonly" >
										</div>
									</div>
		                         </div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                            <div class="row">
		                            <label for="input_name" class="col-sm-6 control-label">จำนวนชั่วโมงในการคุย </label>
										<div class="form-group">
												<input type="text" class="form-control" name="call_hr" id="call_hr" autocomplete="off" value="<%= pack.getCall_hr() %>" pattern="^[0-9]$"  required >
										</div>
									</div>
		                         </div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                            <div class="row">
		                            <label for="input_name" class="col-sm-6 control-label">จำนวนชั่วโมงในการแชท </label>
										<div class="form-group ">
												<input type="text" class="form-control" name="chat_hr" id="chat_hr" autocomplete="off" value="<%= pack.getChat_hr() %>" pattern="^[0-9]$"  required >
										</div>
									</div>
		                         </div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                            <div class="row">
		                            <label for="input_name" class="col-sm-6 control-label">ราคา</label>
										<div class="form-group ">
												<input type="text" class="form-control" name="price" id="price" autocomplete="off" value="<%= pack.getPrice() %>" pattern="^[0-9]$"  required >
										</div>
									</div>
								</div>
		                     
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			                         <div class="row">
										<div class="form-group ">
											<div class="col-sm-6 center"  >
												<button type="submit" value="อัพเดทแพ็คเกจ" name="submit" class="btn" style="text-align: center; " onclick="return myfunction();">
		                                      	<img alt="" src="./img/diskette.png" width="30px" height="30px"> อัพเดทแพ็คเกจ</button>
											</div>
										</div>	
									</div>
			                    </div>
			                   </div>
							</table>
						</form>
						</fieldset>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	

	

<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>