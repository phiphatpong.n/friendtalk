<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	Organization org = new Organization();
		 org = (Organization) request.getAttribute("org");

%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	
	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
	

</head>
<body class="clinic_version">
	<!-- LOADER -->
	<!-- LOADER -->
	<div id="preloader">
		<img class="preloader" src="./img/loaders/heart-loading2.gif" alt="">
	</div>
	<!-- END LOADER -->
	<div class="header-top wow fadeIn">
		<div class="container">
			<a class="navbar-brand" href="openHome"><img
				src="./img/logo3.png" alt="image"></a>
			<div class="right-header">
				<div class="header-info">
					<div class="info-inner">
						<span class="icontop"><img src="./img/phone-icon.png"
							alt="#"></span> <span class="iconcont"><a
							href="tel:800 123 456">800 123 456</a></span>
					</div>
					<div class="info-inner">
						<span class="icontop"><i class="fa fa-envelope"
							aria-hidden="true"></i></span> <span class="iconcont"><a
							data-scroll href="mailto:info@yoursite.com">info@Lifecare.com</a></span>
					</div>
					<div class="info-inner">
						<span class="icontop"><i class="fa fa-clock-o"
							aria-hidden="true"></i></span> <span class="iconcont"><a
							data-scroll href="#">Daily: 7:00am - 8:00pm</a></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
						
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	    <h1 style="font-size: 30px;">จำนวนแพ็คเกจคงเหลือ</h1>
	       <div class="row center">
	                  
	                        <table class="table  table-hover">
								<thead>
									<tr>
									<th>ชื่อบริษัท</th>
									<th>จำนวนพนักงาน</th>
									<th>จำนวนเวลาคุยคงเหลือ</th>
									<th>จำนวนเวลาแชทคงเหลือ</th>
									<th>จำนวนนาทีที่ออกรหัส</th>
									<th>จำนวนนาทีที่ออกรหัส</th>
									<th></th>
									</tr>
								</thead>
								<tbody>
											 <tr>
												<td><label><%= org.getCompanyname()%></label></td>
												<td><label><%= org.getEmployee_number()%></label></td>
												<td><label><%= org.getTotal_call() %></label></td>
												<td><label><%= org.getTotal_chat() %></label></td>
												<td><label><%= org.getMinute_code_call() %></label></td>
												<td><label><%= org.getMinute_code_chat() %></label></td>
												<td></td>
											</tr>
								</tbody>
							
	                        </table>
	                </div>
	             </div>
	          </div>

<footer id="footer" class="footer-area wow fadeIn">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="logo padding">
						<a href=""><img src="./img/logo3.png" alt=""></a>
						<p>Locavore pork belly scen ester pine est chill wave
							microdosing pop uple itarian cliche artisan.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="footer-info padding">
						<h3>CONTACT US</h3>
						<p>
							<i class="fa fa-map-marker" aria-hidden="true"></i> PO Box 16122
							Collins Street West Victoria 8007 Australia
						</p>
						<p>
							<i class="fa fa-paper-plane" aria-hidden="true"></i>
							info@gmail.com
						</p>
						<p>
							<i class="fa fa-phone" aria-hidden="true"></i> (+1) 800 123 456
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="subcriber-info">
						<h3>SUBSCRIBE</h3>
						<p>Get healthy news, tip and solutions to your problems from
							our experts.</p>
						<div class="subcriber-box">
							<form id="mc-form" class="mc-form">
								<div class="newsletter-form">
									<input type="email" autocomplete="off" id="mc-email"
										placeholder="Email address" class="form-control" name="EMAIL">
									<button class="mc-submit" type="submit">
										<i class="fa fa-paper-plane"></i>
									</button>
									<div class="clearfix"></div>
									<!-- mailchimp-alerts Start -->
									<div class="mailchimp-alerts">
										<div class="mailchimp-submitting"></div>
										<!-- mailchimp-submitting end -->
										<div class="mailchimp-success"></div>
										<!-- mailchimp-success end -->
										<div class="mailchimp-error"></div>
										<!-- mailchimp-error end -->
									</div>
									<!-- mailchimp-alerts end -->
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="copyright-area wow fadeIn">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="footer-text">
						<p>© 2021 Friends Talk. All Rights Reserved.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="social">
						<ul class="social-links">
							<li><a href=""><i class="fa fa-rss"></i></a></li>
							<li><a href=""><i class="fa fa-facebook"></i></a></li>
							<li><a href=""><i class="fa fa-twitter"></i></a></li>
							<li><a href=""><i class="fa fa-google-plus"></i></a></li>
							<li><a href=""><i class="fa fa-youtube"></i></a></li>
							<li><a href=""><i class="fa fa-pinterest"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end copyrights -->
	<a href="#home" data-scroll class="dmtop global-radius"><i
		class="fa fa-angle-up"></i></a>
		
			<!-- all js files -->
	<script src="./js/all.js"></script>
	<!-- all plugins -->
	<script src="./js/custom.js"></script>
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNUPWkb4Cjd7Wxo-T4uoUldFjoiUA1fJc&callback=myMap"></script>
	

    
</body>
</html>