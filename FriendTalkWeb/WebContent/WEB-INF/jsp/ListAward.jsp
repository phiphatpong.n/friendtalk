<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*,Manager.*"%>
<%
	String user = (String) session.getAttribute("user");
	List<Award> awList = (List<Award>) request.getAttribute("awList");
	List<Redeem> listredeem = (List<Redeem>) request.getAttribute("listredeem");
	
	int alert = -1;
	try {
		alert = (Integer) session.getAttribute("alt");
	} catch (Exception e) {
	}
%>
<%
	if (1 == alert) {
%>
<script type="text/javascript">
	alert("ลบสำเร็จ");
</script>
<%
	} else if (0 == alert) {
%>
<script type="text/javascript">
	alert("ลบไม่สำเร็จ");
</script>
<%
	}
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->
</head>
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
						<li><a href="openAddAward">เพิ่มของรางวัล</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	             <div class="appointment-form">
	                <h3>รายการของรางวัล</h3>
	                <table  class="table  table-hover">
							<thead>
								<tr>
									<th></th>
									<th>ชื่อของรางวัล</th>
									<th>คะแนนที่ใช้แลก</th>
									<th>แก้ไข</th>
									<th>ลบ</th>
								</tr>
							</thead>
							<tbody>
							<% if(awList.size() >= 1){ %>
							<% for(int i=0 ; i<awList.size() ; i++){ %>
								<tr>
								<td style="align-content: center;"><img alt="" src="./img/<%= awList.get(i).getImg_award()%>" width="150" height="150"></td>
								<td style="font-size: 20px;"><label><%= awList.get(i).getAward_name() %></label></td>
								<td><label><%= awList.get(i).getScore() %></label></td>
								
								<td align="center" ><a href="/WebService/openEditAward?Awid=<%= awList.get(i).getAward_id() %>">
									<img alt="" src="./img/edit.png" width="30px" height="30px"></a></td>
								
								<% boolean ck = true ;
								System.out.println("listredeem = "+listredeem.size());
								if(listredeem.size() >= 1){	
								for(int k=0 ; k < listredeem.size() ; k++){%>
									<%if(awList.get(i).getAward_id().equals(listredeem.get(k).getAward().getAward_id())){ %>
										<td align="center"><img alt="" src="./img/trash.png" width="30px" height="30px" style="color: gray;"></td>
									<%ck=false; break;} %>
									<% }%>
									<%if(ck){ %> 
										<td align="center"><a href="/WebService/DeleteAward?Awid=<%= awList.get(i).getAward_id() %>">
										<img alt="" src="./img/trash.png" width="30px" height="30px" onclick="return confirm('คุณต้องการลบใช่หรือไม่?');" ></a></td>
								<%}}else{ %>								
										<td align="center"><a href="/WebService/DeleteAward?Awid=<%= awList.get(i).getAward_id() %>">
										<img alt="" src="./img/trash.png" width="30px" height="30px" onclick="return confirm('คุณต้องการลบใช่หรือไม่?');"></a></td>
								<% } %>
								</tr>
								<br>
							<% } }%>
							</tbody>
						</table>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>

	
<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>