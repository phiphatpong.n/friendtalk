<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	List<StressQuestion> liststress = (List<StressQuestion>) session.getAttribute("liststress");
	Consultant con = (Consultant) session.getAttribute("con");
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	
	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
	
	<script src="./js/CheckScriptInformation/JSStreeTest.js"></script>
	

</head>
<body class="clinic_version">
	
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
					
					<%if(con != null){ %>
						<li><a class="active" href="openHome">หน้าแรก</a></li>
						<li><a href="/WebService/openHistoryStressTest?user=<%= con.getUsername() %>">ประวัติคะแนนการทำแบบทดสอบ</a></li>
						<% }else{ %>
						<li><a class="active" href="openindex">หน้าแรก</a></li>
						<% } %>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
	 	<img alt="" src="./img/การให้คะแนน.png">
	 </div>

	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-10 col-md-6 col-sm-9 col-xs-10">
	             <div class="appointment-form">
	                <h3>แบบทดสอบดัชนีชี้วัดสุขภาพจิต</h3>
	                <div class="form">
	                  <form name="frm" id="myformAcheckStressTest" action="/WebService/AddStressTestScore" method="POST"  novalidate>
	                      <fieldset>
	                         <table border="1" class="table table-bordered   table-hover">
								<thead>
								<tr>
									<th></th>
									<th>ไม่เลย</th>
									<th>เล็กน้อย</th>
									<th>มาก</th>
									<th>มากที่สุด</th>
								</tr>
								</thead>
								<tbody>
								<% for(int i=0 ; i<liststress.size() ; i++){ %>
									<tr>
										<td><%=i+1%>.<%= liststress.get(i).getQuestion_detail() %></td>
										<% if(i == 4 ){ %>
											<td><input type="radio" class="chosie" id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
										<% }else if(i == 5){ %>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
										<% }else if(i == 6){ %>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
										<% }else if(i == 7){ %>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
										<% }else if(i == 8){ %>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
										<% }else if(i == 9){ %>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
										<% }else if(i == 10){ %>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
										<% }else if(i == 11){ %>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
										<% }else if(i == 12){ %>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
										<% }else if(i == 24){ %>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
										<% }else if(i == 25){ %>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
										<% }else if(i == 26){ %>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
										<% }else if(i == 27){ %>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
										<% }else{ %>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="1" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="2" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="3" required></td>
											<td><input type="radio" class="chosie"id="ch<%= i %>" name="ch<%= i %>" value="4" required></td>
										<% } %>
										
									</tr>
								<% } %>
								</tbody>
							</table>
	                      </fieldset>
	                      	<div align="center">
	                      	<%if(con != null){ %>
	                      	<input type="hidden" name="user" value="<%= con.getUsername() %>">
	                      	<% } %>
	                      	<input type="submit" value="ส่งบบทดสอบ" name="submit" id="submitButton" onclick="return myFunction();" class="form-control-submit-button"title="Seand" >
	                      	</div>
	                   </form>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>

<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>