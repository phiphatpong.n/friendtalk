<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	Psychologist psy = (Psychologist) session.getAttribute("psy");
	Award award = (Award) request.getAttribute("award");
	List<Redeem> lredeem = (List<Redeem>) request.getAttribute("lredeem");
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->
</head>
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<%if(award != null){ %>
							<li><a class="active" href="openHome">หน้าแรก</a></li>
						<%}else{ %>
							<li><a class="active" href="openindex">หน้าแรก</a></li>
						<%} %>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
<br>
    <div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-9 col-md-4 col-sm-6 col-xs-9">
	             <div class="appointment-form">
	                <form action="/WebService/redeem" method="POST">
						<table class="center">
							<tr>
							<td>
								<img alt="" src="./img/<%=award.getAward_id() %>.jpg" width="300" height="300"><br>
							</td>
							</tr>
							<br>
							<tr>
								<td>
									<label for="AwardName">Award Name : <%=award.getAward_name()%></label><br>
				
								</td>
							</tr>
							<tr>
								<td>
									<label for="AwardDetails">Award Details : <%=award.getAward_detail()%></label><br>
				
								</td>
							</tr>
							<tr>
								<td >
									<label for="Score">Score : <%=award.getScore()%></label><br>
				
								</td>
							</tr>
							<tr>
								<td>
									<input type="hidden" name="awid" value="<%= award.getAward_id()%>">
									<input type="hidden" name="psyid" value="<%= psy.getUsername()%>">
								</td>
							</tr>
							<tr>
							<% 	if(lredeem.size() > 0){
								for(int k=0 ; k < lredeem.size() ; k++){ 
									if(lredeem.get(k).getAward().getAward_id().equals(award.getAward_id())){ %>							
									<td><input type="submit"  value="แลกของรางวัล" name="Redeem" title="แลกของรางวัล"id="submitButton" class="form-control-submit-button" disabled="disabled"></td>
							<% }else{ %>
									<td><input type="submit" value="แลกของรางวัล" name="Redeem" class="form-control-submit-button" id="submitButton" ></td>
							<% } } }else{ %>								
									<td><input type="submit" value="แลกของรางวัล" name="Redeem" class="form-control-submit-button" id="submitButton" ></td>
							<% } %>
							</tr>
						</table>
					</form>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>
    
	
<jsp:include page="navbar/footer.jsp" />
</body>
</html>