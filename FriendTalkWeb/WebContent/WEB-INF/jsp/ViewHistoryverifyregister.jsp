<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	List<Psychologist> list = (List<Psychologist>) request.getAttribute("list");
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	
	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
	

</head>
<body class="clinic_version">
	
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	    <h1 style="font-size: 30px;">ประวัติการยืนยันสมาชิก</h1>
	       <div class="row center">
	                  
	                        <table class="table  table-hover">
								<thead>
									<tr>
									<th>ชื่อ - นามสกุล</th>
									<th>เมล์</th>
									<th>ประเภทของผู้สมัคร</th>
									<th>ประวัติการศึกษา</th>
									<th></th>
									</tr>
								</thead>
								<tbody>
									<% if(list.size() >= 1){ %>
										<% for(int i=0 ; i<list.size() ; i++){ %>
										<tr>
											<td><label><%= list.get(i).getTitle()+" "+list.get(i).getFristname()+" "+list.get(i).getLastname() %></label></td>
											<td><label><%= list.get(i).getEmail() %></label></td>
											<td><label><%= list.get(i).getMentor_type()%></label></td>
											<td><label><%= list.get(i).getEducationl_history() %></label></td>
											<td></td>
										</tr>
									<%}} %>
								</tbody>
							
	                        </table>
	                </div>
	             </div>
	          </div>

<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>