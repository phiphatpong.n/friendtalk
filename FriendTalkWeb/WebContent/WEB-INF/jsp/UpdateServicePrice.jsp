<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%

	List<Price> prices = (List<Price>) request.getAttribute("prices");

	int alert = 5;
	try {
		alert = (Integer) request.getAttribute("alt");
	} catch (Exception e) {
	}
	session.removeAttribute("alert");
%>
<%
	if (1 == alert) {
%>
<script type="text/javascript">
	alert("บันทึกสำเร็จ");
</script>
<%
	} else if (0 == alert) {
%>
<script type="text/javascript">
	alert("บันทึกไม่สำเร็จ");
</script>
<%
	}
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

    <script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
    
    	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
<style>
* {
  box-sizing: border-box;
}

/* Create three unequal columns that floats next to each other */
.column {
  float: left;
  padding: 10px;
  height: 300px; /* Should be removed. Only for demonstration */
}

.left {
  width: 15%;
}
.right {
  width: 42%;
}

.middle {
  width: 42%;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>
<script type="text/javascript">
	function myFunction() {

		const call30 = document.getElementById("call30")
		if (!call30.checkValidity()) {
			document.getElementById("demo1").innerHTML = "*กรุณากรอกราคาให้ถูกต้อง";
			return false;
		} else {
			document.getElementById("demo1").innerHTML = "";
		}

		const call60 = document.getElementById("call60")
		if (!call60.checkValidity()) {
			document.getElementById("demo2").innerHTML = "*กรุณากรอกราคาให้ถูกต้อง";
			return false;
		} else {
			document.getElementById("demo2").innerHTML = "";
		}

		const chat30 = document.getElementById("chat30")
		if (!chat30.checkValidity()) {
			document.getElementById("demo3").innerHTML = "*กรุณากรอกราคาให้ถูกต้อง";
			return false;
		} else {
			document.getElementById("demo3").innerHTML = "";
		}

		const chat60 = document.getElementById("chat60")
		if (!chat60.checkValidity()) {
			document.getElementById("demo4").innerHTML = "*กรุณากรอกราคาให้ถูกต้อง";
			return false;
		} else {
			document.getElementById("demo4").innerHTML = "";
		}

	}
</script>

</head>	
<body class="clinic_version">
	
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>


<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-12 col-md-4 col-sm-6 col-xs-12">
	             <div class="appointment-form">
	                <h3>อัพเดทราคาในการให้คำปรึกษา</h3>
	                <div class="form">
	                  <form name="frm" id="UpdatePrice" action="/WebService/UpdatePrice" method="POST" novalidate>
	                      <fieldset>
	                      
									<div class="row">
										<div class="column left" >
											<br>
											<h2>call30 :</h2>
											<input type="hidden" name="type1" value="call 30">
											<br>
											<h2>call60 :</h2>
											<input type="hidden" name="type2" value="call 60">
											<br>
											<h2>chat30 :</h2>
											<input type="hidden" name="type3" value="chat 30">
											<br>
											<h2>chat60 :</h2>
											<input type="hidden" name="type4" value="chat 60">
										
										</div>
										<div class="column middle" >
										<h2>ราคา</h2>
							                   <input type="text" class="form-control" name="call30" id="call30"
												autocomplete="off" value="<%= prices.get(0).getPrice() %>" pattern="^[0-9_.-]$"  min="1">
												<p class="col-sm-6 control-label" id="demo1" style="color: red; font-size: 16px"></p>
												<br>
												<input type="text" class="form-control" name="call60" id=call60  
												autocomplete="off" value="<%= prices.get(1).getPrice() %>" pattern="^[0-9_.-]$" min="1" >
												<p class="col-sm-6 control-label" id="demo2" style="color: red; font-size: 16px"></p>
												<br>
												<input type="text" class="form-control" name="chat30" id="chat30" 
												autocomplete="off" value="<%= prices.get(2).getPrice() %>" pattern="^[0-9_.-]$" min="1" >
												<p class="col-sm-6 control-label" id="demo3" style="color: red; font-size: 16px"></p>
												<br>
												<input type="text" class="form-control" name="chat60" id="chat60" 
												autocomplete="off" value="<%= prices.get(3).getPrice() %>" pattern="^[0-9_.-]$"  min="1">
												<p class="col-sm-6 control-label" id="demo4" style="color: red; font-size: 16px"></p>
												<br>
										</div>

										<div class="column right" >
										<h2>จำนวนเวลา</h2>
							                    <input type="text" class="form-control" name="Tcall30" id="Tcall30"
												autocomplete="off" value="<%= prices.get(0).getTotal_minute() %>" readonly >
												<br>
												<input type="text" class="form-control" name="Tcall60" id="Tcall60" 
												autocomplete="off" value="<%= prices.get(1).getTotal_minute() %>" readonly >
												<br>
												<input type="text" class="form-control" name="Tchat30" id="Tchat30" 
												autocomplete="off" value="<%= prices.get(2).getTotal_minute() %>" readonly >
												<br>
												<input type="text" class="form-control" name="Tchat60" id="Tchat60" 
												autocomplete="off" value="<%= prices.get(3).getTotal_minute() %>" readonly="readonly" >
												<br>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
		                              <div class="row">
		                                 <div class="form-group">
		                                    <div class="center" >
			                                    <button type="submit" value="อัพเดทราคา" name="submit" class="btn" style="text-align: center; " onclick="return myfunction();">
		                                      	<img alt="" src="./img/diskette.png" width="30px" height="30px"> อัพเดทราคา</button>
		                					</div>
		                                 </div>
		                              </div>
		                           </div>
					                
	                      </fieldset>
	                   </form>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	
	

<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>