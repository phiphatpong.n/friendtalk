<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	List<Organization> listO =(List<Organization>) request.getAttribute("list");
	String Orgname = "" ;
int alert = -1;
	try {
		alert = (Integer) session.getAttribute("alert");
	} catch (Exception e) {
	}
%>
<%
	if (1 == alert) {
%>
<script type="text/javascript">
	alert("อัพเดทสำเร็จ");
</script>
<%
	} else if (0 == alert) {
%>
<script type="text/javascript">
	alert("อัพเดทไม่สำเร็จ");
</script>
<%
	}
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>

	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
	
	<script src="./js/CheckScriptInformation/JSAddPackageOrg.js"></script>

<script type="text/javascript">
	function getvalue(selectObject) {
		var value = selectObject.value;
		<%for (int i = 0 ; i < listO.size() ;i++) {%>
			
			if(value == <%= i %>){
				document.getElementById('dis<%= i %>').style.display = "inline";
			}else{
				document.getElementById('dis<%= i %>').style.display = "none";
			}
		<%}%>
		
	}

</script>
</head>
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
						<li><a href="/WebService/openViewUpdatePackageOrganization">ประวัติการอัพเดทแพ็คเกจ</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-9 col-md-4 col-sm-6 col-xs-9">
	             <div class="appointment-form">
	                <h3>อัพเดทแพ็คเกจ</h3>
	                <div class="form">
	                  <form name="frm" id="myform" action="/WebService/UpdatetotalHR" method="POST"  novalidate>
	                      <fieldset>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="fname" class="col-sm-2 control-label">รายขื่อบริษัท </label>
	                                 <p class="col-sm-6 control-label" id="demo3" style="color: red; font-size: 16px"></p>
	                               <div class="form-group">
	                               <select name="companyname" id="companyname"  OnChange="return getvalue(this)">
	                              		 <option value="choose">เลือก</option>
	                               		<% if(listO.size() >= 1){
	                               			for(int i=0 ; i<listO.size() ; i++){ %>
	                               			<option value="<%= i %>"><%= listO.get(i).getCompanyname() %></option>
	                               		<%}}%>
	                               	</select>
	                               </div>
	                            </div>
	                         </div>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="dis">
	                            <div class="row">
	                               <div class="form-group">
	                               <% if(listO.size() >= 1){
	                               			for(int i=0 ; i<listO.size() ; i++){ %>
	                               			    	<div id="dis<%= i %>" style="display: none;">
	                               			    		<label >ชื่อบริษัท : <%= listO.get(i).getCompanyname() %></label><br>
	                               			    		<label >จำนวนนาทีคุยคงเหลือ : <%= listO.get(i).getTotal_call() %></label><br>
	                               			    		<label >จำนวนนาทีแชทคงเหลือ : <%= listO.get(i).getTotal_chat() %></label><br>
	                               			    		<label >จำนวนพนักงาน : <%= listO.get(i).getEmployee_number()%></label><br>
	                               			    	</div>
	                               		<%}}%>
	                               </div>
	                            </div>
	                         </div>
	                         
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="fname" class="col-sm-6 control-label">เพิ่มจำนวนนาทีคุย </label>
	                                <p class="col-sm-6 control-label" id="demo" style="color: red; font-size: 16px"></p>
	                               <div class="form-group">
	                               		<div id="selected_value"></div>
	                               		<input type="text" class="form-control" name="totalcall" id="totalcall" autocomplete="off" value="" pattern="^[0-9]{1,40}$" min="1"  required >
	                               </div>
	                            </div>
	                         </div>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="fname" class="col-sm-6 control-label">เพิ่มจำนวนนาทีแชท </label>
	                                <p class="col-sm-6 control-label" id="demo2" style="color: red; font-size: 16px"></p>
	                               <div class="form-group">
	                               		<input type="text" class="form-control" name="totalchat" id="totalchat" autocomplete="off" value="" pattern="^[0-9]{1,40}$" min="1"  required >
	                               </div>
	                            </div>
	                         </div>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                               <div class="form-group">
	                                  <div class="center">
	                                  		<button type="submit" value="เพิ่มแพ็คเกจ" name="submit" class="btn" style="text-align: center; " onclick="return myFunction();">
		                                     <img alt="" src="./img/diskette.png" width="30px" height="30px"> เพิ่มแพ็คเกจ</button>
	                                  </div>
	                               </div>
	                            </div>
	                         </div>
	                         
	                      </fieldset>
	                   </form>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	

		


<jsp:include page="navbar/footer.jsp" />
</body>
</html>