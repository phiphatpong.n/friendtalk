<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ page import="bean.*, java.util.*" %>
	<%
		String mtype =(String) session.getAttribute("mtype");
	%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->
    
    <script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<script src="./js/bootstrap-datepicker-thai.js"></script>
	<script src="./js/locales/bootstrap-datepicker.th.js"></script>
	<script src="./js/bootstrap-datepicker.js"></script>

	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
	
	<script src="./js/CheckScriptInformation/JSpsy.js"></script>
	
	<script type="text/javascript">
	$.validator.addMethod('filesize', function(value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	}, 'ขนาดไฟล์ต้องไม่เกิน 2 MB และต้องเป็น  jpg,jpeg เท่านั้น {0}');

	jQuery(function($) {
		"use strict";
		$('#myform1').validate({
			rules : {
				FirstName : {
					required : true,
					maxlength : 20
				},
				image : {
					required : true,
					extension : "jpg,jpeg",
					filesize : 2000,
				}
			},
		});
	});
</script>
<script type="text/javascript">  
function ValidatePetSelection()  
{  	var i = 0 ;
    var checkboxes = document.getElementsByName("expertise");  
    var numberOfCheckedItems = 0;  
    var numChecked = $("input[type=checkbox]:checked").length;
    for(i= 0; i < checkboxes.length; i++)  
    {  
        if(checkboxes[i].checked)  
            numberOfCheckedItems++;  
    }  
    if(numberOfCheckedItems > 5)  
    {  
        alert("สามารถเลือกความเชียวชาญได้ ไม่ เกิน 5 อย่าง");  
        return false;  
    }
    
}
$(document).ready(function() {
	$('input[type="checkbox"]').change(function(){
	var total_checked=  $("input[type='checkbox']:checked").length 
	$("#d1").html( total_checked );
	});
	/////////////
	var total_checked=  $("input[type='checkbox']:checked").length 
	$("#d1").html( total_checked );
	///////
	});
</script>
</head>

<body class="clinic_version">
	
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
                     <li><a class="active" href="openindex">หน้าแรก</a></li>
                     <li><a href="/WebService/openListPsychologistent">รายชื่อหมอ</a></li>
                     <li><a href="/WebService/openPackage">แพ็กเกจ</a></li>
                     <li><a href="/WebService/openlistArticle">รายการบทความ</a></li>
                     <li><a href="contact-us.html">ติดต่อเจ้าหน้าที่</a></li>
                     <li><a href="/WebService/openlogin">เข้าสู่ระบบ</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<div id="service" class="services2 wow fadeIn">
      <div class="container">
         <div class="row center">
            <div class="col-lg-9 col-md-4 col-sm-6 col-xs-9">
               <div class="appointment-form">
                  <h3>สมัครสมาชิก</h3>
                  <div class="form">
                    <form id="myformpsy" name="frm" action="/WebService/registerpsy" method="POST" enctype="multipart/form-data" onsubmit="return myFunction();" novalidate>
                        <fieldset>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	 <label for="title" class="col-sm-2 control-label">คำนำหน้า</label>
                                 <div>
                                 	<input type="radio" name="title" id="title" value="Mr" required>
                    				<label for="title1">นาย</label> 
                    
                    				<input type="radio" name="title" id="title1" value="Mrs" required>
                   					<label for="title2">นาง</label>
                    
                    				<input type="radio" name="title" id="title2" value="Ms" required>	
                    				<label for="title3"> นางสาว</label>
                                 </div>
                              </div>
                              <p id="demo" style="color: red; font-size: 16px"></p>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="fname" class="col-sm-2 control-label">ชื่อ</label>
                              		<p class="col-sm-6 control-label" id="demo1" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                    <input type="text" class="form-control" name="fname" id="fname" pattern="^[A-Za-zก-์]{1,40}$"  required >
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="lname" class="col-sm-2 control-label">นามสกุล</label>
                              		<p class="col-sm-6 control-label" id="demo2" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                     <input type="text" class="form-control" name="lname" id="lname" pattern="^[A-Za-zก-์]{1,40}$" required>
                                 </div>
                              </div>
                           </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	 <label for="gender" class="col-sm-2 control-label">เพศ</label>
                                 <div>
                                 	 <input type="radio" name="gender" id="gender" value="male" required>
                    				<label for="title1">ชาย</label> 
                    
                    				<input type="radio" name="gender" id="gender2" value="female" required>
                   					<label for="title2">หญิง</label>
                    
                                 </div>
                              </div>
                              <p id="demo3" style="color: red; font-size: 16px"></p>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="birthday" class="col-sm-2 control-label">วัน/เดือน/ปี เกิด</label>
                                 <div class="form-group">
                                    	<input type="text" class="date form-control" name="birthday" id="birthday" >
                                 </div>
                              </div>
                           </div>
								<script type="text/javascript">
									$(".date").datepicker({
										format : "dd-mm-yyyy",
										autoclose : true
									});
								</script>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="email" class="col-sm-2 control-label">อีเมล</label>
                              		<p class="col-sm-6 control-label" id="demo5" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                     <input type="text" class="form-control" name="email" id="email"
                   	 					pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" autocomplete="off" value="" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="tel" class="col-sm-2 control-label">เบอร์มือถือ</label>
                              		<p class="col-sm-6 control-label" id="demo6" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                     <input type="text" class="form-control" pattern="^0([8|9|6])([0-9]{8}$)" name="tel" id="tel"
                    						autocomplete="off" value="" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="address" class="col-sm-2 control-label">ที่อยู่</label>
                              		<p class="col-sm-6 control-label" id="demo7" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                     <textarea class="form-control" name="address" id="address" rows="3" required></textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="educationl" class="col-sm-2 control-label">ประวัติการศึกษา</label>
                              		<p class="col-sm-6 control-label" id="demo8" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                     <textarea class="form-control" name="educationl-history" id="educationl" rows="3"  required></textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="working" class="col-sm-2 control-label">ประวัติการทำงาน</label>
                              		<p class="col-sm-6 control-label" id="demo9" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                     <textarea class="form-control" name="working-history" id="working" rows="3" required></textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="mentortype" class="col-sm-6 control-label">เลือกต่ำแหน่งที่ตนเองจบมา เช่น จิตแพทย์</label>
                              		<p class="col-sm-6 control-label" id="demo10" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
										<select class="form-control" name="mentor-type" id="mentortype" required>
												<option value="">เลือกประเภท</option>
												<option value="psychiatrist">จิตแพทย์</option>
												<option value="psychologist">จิตวิทยา</option>
										</select>
								</div>
                              </div>
                           </div>            
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="uname" class="col-sm-3 control-label">ชื่อผู้ใช้</label>
                              		<p class="col-sm-6 control-label" id="demo11" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                     <input type="text" class="form-control" name="uname" id="uname" autocomplete="off" pattern="^[A-Za-z]{1,40}$" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="pwd" class="col-sm-2 control-label">รหัสผ่าน</label>
                              		<p class="col-sm-6 control-label" id="demo12" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                      <input type="password" class="form-control" name="pwd" id="pwd" autocomplete="off" value="" pattern="^[A-Za-z0-9]{1,40}$" required>
                                 </div>
                              </div>
                           </div>
                           
                           <input type="hidden" name="mtype" value="<%= mtype %>" readonly>
                           
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="pwd" class="col-sm-2 control-label">อัพโหลดรูปโปรไฟล์</label>
                              		<p class="col-sm-6 control-label" id="demo13" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                      <input type="file" name="img_c" id="customFile"accept="image/*">
                                 </div>
                              </div>
                           </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="pwd" class="col-sm-6 control-label">ใบรับรอง/ใบจบการศึกษา(ให้ทำเป็นไฟล์ PDF)</label>
                              		<p class="col-sm-6 control-label" id="demo14" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                      <input type="file" name="certificate" id="customFile2"accept="application/pdf">
                                 </div>
                              </div>
                           </div>
                           <div>
                              	<label for="expertise" class="col-sm-6 control-label">ความเชี่ยวชาญ (อย่างน้อย 3 อย่าง)</label>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
									<div>
						                <input   type="checkbox"name="expertise" id="hobby1" value="การเลี้ยงลูก" onclick="return ValidatePetSelection()" > 
						                <label class="form-check-label" for="hobby1"> การเลี้ยงลูก </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby2" value="การเรียนรู้" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby2"> การเรียนรู้ </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby3" value="การไปโรงเรียน" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby3"> การไปโรงเรียน</label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby4" value="ปัญหาความเครียด" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby4"> ปัญหาความเครียด </label>
						            </div>
						            <div>
						                <input   type="checkbox" name="expertise" id="hobby5" value="ซึมเศร้า" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby5"> ซึมเศร้า </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby6" value="ปัญหาครอบครัว" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby6"> ปัญหาครอบครัว </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby7" value="ปัญหาความรัก" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby7"> ปัญหาความรัก </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby8" value="ปัญหาในการทำงาน" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby8"> ปัญหาในการทำงาน </label>
						            </div>
						            <div>
						                <input   type="checkbox" name="expertise" id="hobby9" value="อารมณ์แปรปรวน" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby9">อารมณ์แปรปรวน </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby10" value="ภาวะตื่นตระหนก" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby210"> ภาวะตื่นตระหนก </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby11" value="มีความหมกหมุ่น/เสพติด" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby11"> มีความหมกหมุ่น/เสพติด </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby12" value="สูญเสียบุคคล/สิ่งของอันเป็นที่รัก"onclick="return ValidatePetSelection()" >
						                <label class="form-check-label" for="hobby12"> สูญเสียบุคคล/สิ่งของอันเป็นที่รัก </label>
						            </div>
						            <div>
						                <input   type="checkbox" name="expertise" id="hobby13" value="นอนไม่หลับ"onclick="return ValidatePetSelection()" >
						                <label class="form-check-label" for="hobby13"> นอนไม่หลับ </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;						
						                <input   type="checkbox" name="expertise" id="hobby14" value="อารมณ์ฉุนเฉียว" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby14"> อารมณ์ฉุนเฉียว </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;						
						                <input   type="checkbox" name="expertise" id="hobby15" value="ปัญหาความสัมพันธ์"onclick="return ValidatePetSelection()" >
						                <label class="form-check-label" for="hobby15"> ปัญหาความสัมพันธ์ </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby16" value="ปัญหาเพศสัมพันธ์" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby16"> ปัญหาเพศสัมพันธ์ </label>
						            </div>
						            <div>
						                <input   type="checkbox" name="expertise" id="hobby17" value="ปัญหาเพศทางเลือก" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby17"> ปัญหาเพศทางเลือก </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby18" value="สมาธิสั้น" >
						                <label class="form-check-label" for="hobby18">สมาธิสั้น </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby19" value="ติดเกม&อินเตอร์เนท"onclick="return ValidatePetSelection()" >
						                <label class="form-check-label" for="hobby19"> ติดเกม&อินเตอร์เนท </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby20" value="การเงิน" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby20"> การเงิน </label>
						            </div>
						            <div>
						                <input   type="checkbox" name="expertise" id="hobby21" value="ยาเสพติด" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby21"> ยาเสพติด </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby22" value="พัฒนาการช้า" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby22">พัฒนาการช้า </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby23" value="เรื่องในที่ทำงาน" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby23">เรื่องในที่ทำงาน </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby24" value="การสร้างวินัยเชิงบวกให้ลูก" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby24"> การสร้างวินัยเชิงบวกให้ลูก</label>
						            </div>
						            <div>
						                <input   type="checkbox" name="expertise" id="hobby25" value="การลดน้ำหนัก" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby25"> การลดน้ำหนัก </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby26" value="การปรับตัว" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby26">การปรับตัว </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby27" value="ความวิตกกังวล" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby27"> ความวิตกกังวล </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby28" value="Bipolar" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby28"> Bipolar </label>
						            </div>
						            <div>
						                <input   type="checkbox" name="expertise" id="hobby29" value="โควิด-19" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby29"> โควิด-19</label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby30" value="ทำงานจากที่บ้าน" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby30"> ทำงานจากที่บ้าน </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby31" value="กักกันตัวเอง" onclick="return ValidatePetSelection()" >
						                <label class="form-check-label" for="hobby31"> กักกันตัวเอง </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby32" value="ย้ำคิดย้ำทำ" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby32"> ย้ำคิดย้ำทำ </label>
						            </div> 
						            <div>
						                <input   type="checkbox" name="expertise" id="hobby33" value="ปัญหาการตัดสินใจ" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby33"> ปัญหาการตัดสินใจ</label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby34" value="ปัญหาในการเรียน" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby34"> ปัญหาในการเรียน </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby35" value="จิตบำบัด" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby35"> จิตบำบัด </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby36" value="CBT" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby36"> CBT </label>
						            </div> 
						            <div>
						                <input   type="checkbox" name="expertise" id="hobby37" value="ซาเทียร์" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby37"> ซาเทียร์</label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby38" value="MBTI" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby38"> MBTI </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby39" value="EMDR" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby39"> EMDR </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby40" value="นพลักษณ์" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby40"> นพลักษณ์ </label>
						            </div> 
						            <div>
						                <input   type="checkbox" name="expertise" id="hobby41" value="ทฤษฎีเผชิญความจริง" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby41">ทฤษฎีเผชิญความจริง</label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby42" value="ทฤษฎีภวนิยม" >
						                <label class="form-check-label" for="hobby42"> ทฤษฎีภวนิยม </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby43" value="การปรึกษาแบบยึดบุคคลเป็นศูนย์กลาง" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby43"> การปรึกษาแบบยึดบุคคลเป็นศูนย์กลาง </label>
						                &nbsp;&nbsp;&nbsp;&nbsp;
						                <input   type="checkbox" name="expertise" id="hobby44" value="การให้การปรึกษาแบบผสมผสาน" onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby44"> การให้การปรึกษาแบบผสมผสาน </label>
						            </div> 
						            <div >
						                <input   type="checkbox" name="expertise" id="hobby45" value="ผู้ป่วยนอก"onclick="return ValidatePetSelection()">
						                <label class="form-check-label" for="hobby45">ผู้ป่วยนอก</label>
						                
						            </div> 
								</div>
							</div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <div class="form-group">
                                    <div class="center">
                                    	<input type="submit" id="submitButton" class="form-control-submit-button" title="สมัครสมาชิก" >
<!--                 						  <button type="submit" name="countnum"class="btn btn-secondary"  id="d1"></button> -->
                					</div>
                                 </div>
                              </div>
                           </div>
                          
                        </fieldset>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>	


<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>