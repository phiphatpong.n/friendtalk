<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	Psychologist psy = new Psychologist();
	psy =  (Psychologist) session.getAttribute("Pid");
	
	String ds1 =(String) session.getAttribute("ds1");
	String ds2 =(String) session.getAttribute("ds2");
	String ds3 =(String) session.getAttribute("ds3");
	String ds4 =(String) session.getAttribute("ds4");
	String ds5 =(String) session.getAttribute("ds5");
	String ds6 =(String) session.getAttribute("ds6");
	String ds7 =(String) session.getAttribute("ds7");
	
	String de1 =(String) session.getAttribute("de1");
	String de2 =(String) session.getAttribute("de2");
	String de3 =(String) session.getAttribute("de3");
	String de4 =(String) session.getAttribute("de4");
	String de5 =(String) session.getAttribute("de5");
	String de6 =(String) session.getAttribute("de6");
	String de7 =(String) session.getAttribute("de7");
	
	int alertsettime = 5;
	try {
		alertsettime = (Integer) session.getAttribute("alertsettime");
	} catch (Exception e) {
	}
	session.removeAttribute("alertsettime");
%>
<%
	if (1 == alertsettime) {
%>
<script type="text/javascript">
	alert("บันทึกสำเร็จ");
</script>
<%
	} else if (0 == alertsettime) {
%>
<script type="text/javascript">
	alert("บันทึกไม่สำเร็จ");
</script>
<%
	}
%>

<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

    <script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
    
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
    
<!--     JSsetWokingTime.js -->
<script src="./js/CheckScriptInformation/JSsetWokingTime.js"></script>
    
<style>
* {
  box-sizing: border-box;
}

/* Create three unequal columns that floats next to each other */
.column {
  float: left;
  padding: 10px;
  height: 300px; /* Should be removed. Only for demonstration */
}

.left {
  width: 15%;
}
.right {
  width: 42%;
}

.middle {
  width: 42%;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>
    
</head>
<body class="clinic_version">
	
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="/WebService/openHome">หน้าแรก</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
    <br>
	
	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-12 col-md-4 col-sm-6 col-xs-12">
	             <div class="appointment-form">
	                <h3>อัพเดทเวลาทำงาน</h3>
	                <div class="form">
	                  <form name="frm" id="myformsettime" action="/WebService/SetWorkingTime" method="POST" novalidate>
	                      <fieldset>
									<div class="row">
										<div class="column left" >
											<br>
											<h2>วันอาทิตย์ :</h2>
											<input type="hidden" name="day7" value="วันอาทิตย์">
											<br>
											<h2>วันจันทร์ :</h2>
											<input type="hidden" name="day1" value="วันจันทร์">
											<br>
											<h2>วันอังคาร :</h2>
											<input type="hidden" name="day2" value="วันอังคาร">
											<br>
											<h2>วันพุธ :</h2>
											<input type="hidden" name="day3" value="วันพุธ">
											<br>
											<h2>วันพฤหัสบดี :</h2>
											<input type="hidden" name="day4" value="วันพฤหัสบดี">
											<br>
											<h2>วันศุกร์ :</h2>
											<input type="hidden" name="day5" value="วันศุกร์">
											<br>
											<h2>วันเสาร์ :</h2>
											<input type="hidden" name="day6" value="วันเสาร์">
											<br>
											<h2>ว่างทุกวัน :</h2>
										</div>
										<div class="column middle" >
										<h2>เวลาเริ่ม</h2>
											<%if(!ds7.equals("")){ %>
							                   	 <input type="text" class="form-control" name="day1start" id="day1start" placeholder="09.00" 
												autocomplete="off" value="<%= ds7 %>" pattern="^([0-9]{2})[.-]([0-9]{2}$)$" >
											<%}else{ %>
												<input type="text" class="form-control" name="day1start" id="day1start" placeholder="09.00" 
												autocomplete="off" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%} %>
											<p id="demo" style="color: red; font-size: 16px"></p>
											<br>
											<%if(!ds1.equals("")){ %>
					                            <input type="text" class="form-control" name="day2start" id="day2start" placeholder="09.00" 
												autocomplete="off" value="<%= ds1 %>" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%}else{ %>
												<input type="text" class="form-control" name="day2start" id="day2start" placeholder="09.00" 
												autocomplete="off" pattern="^[0-9_.-]$" >
											<%} %>
											<p id="demo2" style="color: red; font-size: 16px"></p>
											<br>
											<%if(!ds2.equals("")){ %>
					                             <input type="text" class="form-control" name="day3start" id="day3start" placeholder="09.00" 
												autocomplete="off" value="<%= ds2 %>" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%}else{ %>
												<input type="text" class="form-control" name="day3start" id="day3start" placeholder="09.00" 
												autocomplete="off" pattern="^[0-9_.-]$" >
											<%} %>
											<p id="demo3" style="color: red; font-size: 16px"></p>
											<br>
											<%if(!ds3.equals("")){ %>
					                             <input type="text" class="form-control" name="day4start" id="day4start" placeholder="09.00" 
												autocomplete="off" value="<%= ds3 %>" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%}else{ %>
												<input type="text" class="form-control" name="day4start" id="day4start" placeholder="09.00" 
												autocomplete="off" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%} %>
											<p id="demo4" style="color: red; font-size: 16px"></p>
											<br>
											<%if(!ds4.equals("")){ %>
					                            <input type="text" class="form-control" name="day5start" id="day5start" placeholder="09.00" 
												autocomplete="off" value="<%= ds4 %>" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%}else{ %>
												<input type="text" class="form-control" name="day5start" id="day5start" placeholder="09.00" 
												autocomplete="off" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%} %>
											<p id="demo5" style="color: red; font-size: 16px"></p>
											<br>
											<%if(!ds5.equals("")){ %>
					                            <input type="text" class="form-control" name="day6start" id="day6start" placeholder="09.00" 
												autocomplete="off" value="<%= ds5%>" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%}else{ %>
												<input type="text" class="form-control" name="day6start" id="day6start" placeholder="09.00" 
												autocomplete="off" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%} %>
											<p id="demo6" style="color: red; font-size: 16px"></p>
											<br>
											<%if(!ds6.equals("")){ %>
					                            <input type="text" class="form-control" name="day7start" id="day7start" placeholder="09.00" 
												autocomplete="off" value="<%= ds6 %>" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%}else{ %>
												<input type="text" class="form-control" name="day7start" id="day7start" placeholder="09.00" 
												autocomplete="off" pattern="^[0-9_.-]$" >
											<%} %>
											<p id="demo7" style="color: red; font-size: 16px"></p>
											<br>
											<input type="checkbox" class="form-control" name="allday" id="allday" value="1" >
										</div>
										
										
										<div class="column right" >
										<h2>เวลาหยุด</h2>
											<%if(!de7.equals("")){ %>
							                      <input type="text" class="form-control" name="day1end" id="day1end" placeholder="18.00" 
												  autocomplete="off" value="<%= de7 %>" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%}else{ %>
												 <input type="text" class="form-control" name="day1end" id="day1end" placeholder="18.00" 
												autocomplete="off" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%} %>
											<p id="demo8" style="color: red; font-size: 16px"></p>
											<br>
											<%if (!de1.equals("")) {%>
											<input type="text" class="form-control" name="day2end"
												id="day2end" placeholder="18.00" autocomplete="off"
												value="<%=de1%>" pattern="^([0-9]{2})[.-]([0-9]{2}$)">
											<%} else {%>
											<input type="text" class="form-control" name="day2end"
												id="day2end" placeholder="18.00" autocomplete="off"
												pattern="^([0-9]{2})[.-]([0-9]{2}$)">
											<%}%>
											<p id="demo9" style="color: red; font-size: 16px"></p>
											<br>
											<%if(!de2.equals("")){ %>
					                             <input type="text" class="form-control" name="day3end" id="day3end" placeholder="18.00" 
												 autocomplete="off" value="<%= de2 %>" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%}else{ %>
												<input type="text" class="form-control" name="day3end" id="day3end" placeholder="18.00" 
												autocomplete="off" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%} %>
											<p id="demo10" style="color: red; font-size: 16px"></p>
											<br>
											<%if(!de3.equals("")){ %>
					                            <input type="text" class="form-control" name="day4end" id="day4end" placeholder="18.00" 
												autocomplete="off" value="<%= de3 %>" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%}else{ %>
												<input type="text" class="form-control" name="day4end" id="day4end" placeholder="18.00" 
												autocomplete="off" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%} %>
											<p id="demo11" style="color: red; font-size: 16px"></p>
											<br>
											<%if(!de4.equals("")){ %>
					                             <input type="text" class="form-control" name="day5end" id="day5end" placeholder="18.00" 
												autocomplete="off" value="<%= de4 %>" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%}else{ %>
												<input type="text" class="form-control" name="day5end" id="day5end" placeholder="18.00" 
												autocomplete="off" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%} %>
											<p id="demo12" style="color: red; font-size: 16px"></p>
											<br>
											<%if(!de5.equals("")){ %>
					                            <input type="text" class="form-control" name="day6end" id="day6end" placeholder="18.00" 
												autocomplete="off" value="<%= de5 %>" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%}else{ %>
												<input type="text" class="form-control" name="day6end" id="day6end" placeholder="18.00" 
												autocomplete="off" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%} %>
											<p id="demo13" style="color: red; font-size: 16px"></p>
											<br>
											<%if(!de6.equals("")){ %>
					                            <input type="text" class="form-control" name="day7end" id="day7end" placeholder="18.00" 
												autocomplete="off" value="<%= de6 %>" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%}else{ %>
												<input type="text" class="form-control" name="day7end" id="day7end" placeholder="18.00" 
												autocomplete="off" pattern="^([0-9]{2})[.-]([0-9]{2}$)" >
											<%} %>
											<p id="demo14" style="color: red; font-size: 16px"></p>
										</div>
										<input type="hidden" name="user" value="<%= psy.getUsername() %>">
									</div>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
		                              <div class="row">
		                                 <div class="form-group">
		                                    <div class="center" >
		                                    <button type="submit" value="อัพเดทเวลาการทำงาน" name="submit" class="btn" style="text-align: center; " onclick="return myFunction();">
		                                     <img alt="" src="./img/diskette.png" width="30px" height="30px"> อัพเดทเวลาการทำงาน</button>
		                					</div>
		                                 </div>
		                              </div>
		                           </div>
					                
	                      </fieldset>
	                   </form>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	
	
<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>