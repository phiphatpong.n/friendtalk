<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	int score = (int) session.getAttribute("sum");
	Consultant con = (Consultant) session.getAttribute("con");
	List<ResultSuggestion> lsitRS = (List<ResultSuggestion>) session.getAttribute("lsitRS");
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	
	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
	

</head>
<body class="clinic_version">
	
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
					<%if(con != null){ %>
						<li><a class="active" href="openHome">หน้าแรก</a></li>
					<% }else{ %>
						<li><a class="active" href="openindex">หน้าแรก</a></li>
						<% } %>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-9 col-md-4 col-sm-6 col-xs-9">
	             <div class="appointment-form">
	                <h3>ผลการทดสอบดัชนีชี้วัดสุขภาพจิต</h3>
	                <div class="form">
	                      <fieldset>
	                         <table border="1" class="table table-bordered   table-hover">
	                         	<% for(int i=0 ; i<lsitRS.size() ; i++){
	                         		int min = lsitRS.get(i).getMin_score() ;
	                         		int max = lsitRS.get(i).getMax_score() ;
		                         		if(score >= min && score <= max){%>
		                         			<tr>
		                         				<td><label>คะแนนที่ได้ : <%= score %></label></td>
		                         				<td><label>ผลการประเมิน : <%= lsitRS.get(i).getResult_suggestion() %></label></td>
		                         			</tr>
	                         	<%} } %>
							 </table>
							 
								<table border="1" class="table table-bordered   table-hover">
								<h3>การแปลผลการประเมิน</h3>
								
								<tr>	
									<br>
									<h4>ดัชนีชี้วัดสุขภาพจิตคนไทยฉบับสมบูรณ์ 55 ข้อ มีคะแนนเต็มทั้งหมด 220 คะแนน เมื่อผู้ตอบได้ประเมินตนเองแล้ว</h4>
									<h4> และรวมคะแนนทุกข้อได้คะแนนเท่าไร สามารถนำมาเปรียบเทียบกับเกณฑ์ปกติที่กำหนดดังนี้</h4>
									
									<% for(int j=0 ; j<lsitRS.size() ; j++){ %>
										<td><label><%= lsitRS.get(j).getMin_score() +"-"+ lsitRS.get(j).getMax_score() %></label></td>
										<td><label>คะแนน</label></td>
										<td><label><%= lsitRS.get(j).getResult_suggestion() %></label></td>
										
									</tr>
									<%} %>
								</table>
							</fieldset>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	

<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>