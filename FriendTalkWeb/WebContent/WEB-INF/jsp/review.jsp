<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*, java.text.*"%>
<%
	MakeAppointment ma = (MakeAppointment)request.getAttribute("ma");
	
	Date date = new Date();
	
	String datee = "" ;
	int h = 0;
	int m = 0;
	int s = 0;
	
	int alert = -1;
	try {
		alert = (Integer) session.getAttribute("alt");
	} catch (Exception e) {
	}
%>
<%
	if (1 == alert) {
%>
<script type="text/javascript">
	alert("ลบสำเร็จ");
</script>
<%
	} else if (0 == alert) {
%>
<script type="text/javascript">
	alert("ลบไม่สำเร็จ");
</script>
<%
	}
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
	
	<link rel="stylesheet" href="./js/Star/Star.css">
	<script src="./js/Star/Star.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>
    
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
    

</head>
<script type="text/javascript">
function myFunction() {
	
    const comment = document.getElementById("comment")
    if (comment == "") {
      document.getElementById("demo1").innerHTML = "*กรุณากรอกข้อความรีวิว";
      return false;
    } else {
      document.getElementById("demo1").innerHTML = "";
    }
    
    const socre = document.getElementById("socre")
    if (!socre.checkValidity()) {
      document.getElementById("demo2").innerHTML = "*กรุณากรอกคะแนนเป็นตัวเลข";
      return false;
    } else {
      document.getElementById("demo2").innerHTML = "";
    }

  }
</script>
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
					</ul>
				</div>
			</nav>
			<form action="/WebService/searchArticle" method="POST">
				<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg" name="keyword" id="myInput" placeholder="ค้นหาชื่อบทความ" /> 
							<span class="input-group-btn">
								<button class="btn btn-info btn-lg" type="submit"> <i class="fa fa-search" aria-hidden="true"></i></button>
							</span>
					</div>
				</div>
				</div>
			</form>
		</div>
	</div>
	
	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	             <div class="appointment-form">
	                <h3>รีวิว</h3>
	                <div class="form">
	                  <form name="frm" id="AddReview" action="/WebService/AddReview" method="POST" onsubmit="return myfunction();"  novalidate>
	                      <fieldset>
	                      	<table class="table  table-hover">
	                      		<thead>
	                      			<tr>
	                      				<th>ชื่อ - นามสกุล</th>
	                      				<th>ประเภทการติดต่อ</th>
	                      				<th>ราคา</th>
	                      				<th>สถานะ</th>
	                      				<th>วันที่ในการให้คำปรึกษา</th>
	                      			</tr>
	                      		</thead>
	                      		<tbody>
	                      			<tr>
	                      				<td><label><%= ma.getPsychologist().getTitle()+" "+ma.getPsychologist().getFristname() +" "+ ma.getConsultant().getLastname() %></label></td>
	                      				<td><label><%= ma.getPrice().getContact_type() %></label></td>
	                      				<td><label><%= ma.getPrice().getPrice() %></label></td>
	                      				<td><label><%= ma.getStatus_appoint() %></label></td>
	                      				<%
										String ds = ma.getTime_start().toInstant().toString().substring(0, 10);
										System.out.println(ds );
										 h = ma.getTime_start().getTime().getHours();
										 m = ma.getTime_start().getTime().getMinutes();
										 s = ma.getTime_start().getTime().getSeconds();
										
										String[] d = ds.split("-");
									    int day = Integer.parseInt(d[2]);
									    int month = Integer.parseInt(d[1]);
									    int year = Integer.parseInt(d[0]);
									    
									    String time = String.valueOf(h)+":"+String.valueOf(m)+":"+String.valueOf(s);
									    DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
									    Date date2 = sdf.parse(time);
									    
									    //datee
									    String strT2 = date2.toString().substring(11,16);
									    System.out.println("subtimeS "+ strT2 );
									    String appdate = ds +" "+ strT2;
									    System.out.println(ds +" "+ strT2 );
									%>
	                      				<td><label><%= appdate %></label></td>
	                      			</tr>
	                      		</tbody>
	                      	</table>
	                      	<input type="hidden" name="mid" value="<%= ma.getAppointment_id() %>">
						    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="fname" class="col-sm-6 control-label">แสดงความคิดเห็น</label>
	                                <p class="col-sm-6 control-label" id="demo1" style="color: red; font-size: 16px"></p>
	                               <div class="form-group">
	                               		<textarea cols="30" name="comment" id="comment"  placeholder="แสดงความคิดเห็น.."></textarea>
	                               </div>
	                            </div>
	                         </div>
						    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="fname" class="col-sm-6 control-label">ให้คะแนน</label>
	                                <p class="col-sm-6 control-label" id="demo2" style="color: red; font-size: 16px"></p>
	                               <div class="form-group">
	                               		<input type="text" name="score" id="socre" pattern="^[0-5_.-]$" step="0.1" max="5">
	                               </div>
	                            </div>
	                         </div>
							<input type="hidden" class="date form-control" name="review_date" id="review_date"  value="<%= date.toInstant().toString().substring(0, 10).trim() %>" readonly="readonly"required>
	                      	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                               <div class="form-group">
	                                  <div class="center">
	                                      <input type="submit" value="Review" name="submit" id="submitButton" 
	                                      class="form-control-submit-button"title="Review" >
	                                  </div>
	                               </div>
	                            </div>
	                         </div>
	                      </fieldset>
	                   </form>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	    </div>
	

<jsp:include page="navbar/footer.jsp" />
</body>
</html>