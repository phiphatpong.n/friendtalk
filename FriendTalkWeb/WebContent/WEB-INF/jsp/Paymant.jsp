<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
    Price price =  (Price) request.getAttribute("price");
	Consultant con = (Consultant) session.getAttribute("con");
	Advice advice = (Advice) request.getAttribute("advice");
	MakeAppointment makeapp = (MakeAppointment) request.getAttribute("ma");
	
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->
	
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
	
	<script type="text/javascript">
	function getvalue(selectObject) {
		var value = selectObject.value;
			
			if(value == "เลือกการชำระ"){
				document.getElementById('dis1').style.display = "none";
				document.getElementById('dis2').style.display = "none";
			}else if(value == "ชำระด้วยรหัสองค์กร"){
				document.getElementById('dis1').style.display = "inline";
				document.getElementById('dis2').style.display = "none";
				
			}else if(value == "จ่ายผ่านบัตร"){
				document.getElementById('dis1').style.display = "none";
				document.getElementById('dis2').style.display = "inline";
			}
		
	}

</script>
<script type="text/javascript">
function myFunction() {
	
    const code = document.getElementById("code")
    if (code == "") {
      document.getElementById("demo1").innerHTML = "*กรุณากรอกรหัสชำระเงิน";
      return false;
    } else {
      document.getElementById("demo1").innerHTML = "";
    }
  }
</script>
</head>
  
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

<br>
	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	             <div class="appointment-form">
	                <h3>ชำระเงิน</h3>
	                <div class="form">
	                      <fieldset>
	                      		<table class="table  table-hover">
	                      			<thead>
	                      				<tr>
	                      					<th>ชื่อผู้ให้คำปรึกษา</th>
	                      					<th>สถานะการรับคำปรึกษา</th>
	                      					<th>ประเภทการติดต่อ</th>
	                      					<th>ราคา</th>
	                      					<th>จำนวนเวลาในการรับคำปรึกษา</th>
	                      				</tr>
	                      			</thead>
	                      			<tbody>
	                      				<tr>
	                      					<td><label><%= 	advice.getMake_appointment().getPsychologist().getTitle() +" "+ 
	                      									advice.getMake_appointment().getPsychologist().getFristname() +" "+ 
	                      									advice.getMake_appointment().getPsychologist().getLastname() %></label></td>
	                      					<td><label><%= advice.getMake_appointment().getStatus_appoint() %></label></td>
	                      					<td><label><%= price.getContact_type() %></label></td>
	                      					<td><label><%= price.getPrice() %></label></td>
	                      					<td><label><%= price.getTotal_minute()%></label></td>
	                      				</tr>
	                      			</tbody>
	                      			</table>
	                      			<h3>จำนวนเงินที่ต้องชำระ <%= price.getPrice() %></h3>
	                      			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				                         <div class="row">
				                            <label for="fname" class="col-sm-6 control-label">เลือกประเภทการชำระเงิน</label>
				                             <div class="form-group">
				                               	<select name="selectTypePay" OnChange="return getvalue(this)">
				                               		<option value="เลือกการชำระ">เลือกการชำระ</option>
				                               		<option value="ชำระด้วยรหัสองค์กร">ชำระด้วยรหัสองค์กร</option>
				                               		<option value="จ่ายผ่านบัตร">จ่ายผ่านบัตร</option>		
				                               	</select>
				                            </div>
				                         </div>
				                    </div>
	                      			<div id="dis1" style="display: none;">
	                      			<form name="checkoutForm" id="checkoutForm" action="/WebService/ChackMakePayment" method="POST">
		                      				<input type="hidden" name="Mid" value="<%= makeapp.getAppointment_id() %>" >
		                      				<input type="hidden" name=conid value="<%= con.getUsername() %>" >
		                      				<input type="hidden" name=adviceid value="<%= advice.getAdvice_id() %>" >
		                      				<input type="hidden" name=price value="<%= price.getPrice() %>" >
		                      			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				                            <div class="row">
				                                <label for="fname" class="col-sm-6 control-label">รหัสชำระเงิน(เฉพาะองค์กร) </label>
				                                <p class="col-sm-6 control-label" id="demo1" style="color: red; font-size: 16px"></p>
				                               <div class="form-group">
				                               		<input type="text" class="form-control" name="code" id="code" autocomplete="off" value="" pattern="^[A-Za-z]{1,40}$"  required >
				                               </div>
				                            </div>
				                         </div>
		                      			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				                            <div class="row">
				                               <div class="form-group">
				                                  <div class="center">
				                                      <input type="submit" value="ชำระเงิน" name="submit" id="submitButton" 
				                                       class="form-control-submit-button" title="ชำระเงิน" onclick="return myFunction();" >
				                                  </div>
				                               </div>
				                            </div>
				                         </div>
	                      			</form>
	                      			</div>
	                      		<br>
	                      		<div id="dis2" style="display: none; margin-left: 150px ; align-items: center;">
	                      		<form name="checkoutForm" id="checkoutForm" action="/WebService/ChackMakePayment" method="POST">
		                      	<input type="hidden" name="Mid" value="<%= makeapp.getAppointment_id() %>" >
		                      	<input type="hidden" name=conid value="<%= con.getUsername() %>" >
		                      	<input type="hidden" name=adviceid value="<%= advice.getAdvice_id() %>" >
		                      	<input type="hidden" name=price value="<%= price.getPrice() %>" >
								<script type="text/javascript"
									src="https://cdn.omise.co/omise.js.gz"
									data-key="pkey_test_5pw5gnd2yz79xvngc89"
									data-image="http://yimwhanfamily.com/wp-content/uploads/2017/05/logo_Yimwhan.png"
									data-frame-label="Friend Talk" data-button-label="ชำระเงิน"
									data-submit-label="ชำระเงิน" data-location="no" data-amount="<%= price.getPrice()*100 %>"
									data-currency="บาท" >
								</script>
								</form>
								</div>
	                      </fieldset>
	                   
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	

<jsp:include page="navbar/footer.jsp" />
		
    
</body>
</html>