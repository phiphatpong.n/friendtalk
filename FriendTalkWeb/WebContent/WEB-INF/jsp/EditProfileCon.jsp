<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ page import="bean.*, java.util.* , java.text.*" %>
	<%
	Consultant con = new Consultant();
	con =(Consultant) request.getAttribute("Cid");
	
	String date1 = new SimpleDateFormat("dd-mm-yyyy").format(con.getBirthday().getTime());
	int mon = con.getBirthday().getTime().getMonth();
	
	String[] d = date1.split("-");
    int month = mon + 1 ;
    String date = "";
    if(month < 10){
    	date = d[0] + "-0"+ String.valueOf(month)  +"-" + d[2] ;
    }else{
    	date = d[0] + "-"+ String.valueOf(month)  +"-" + d[2] ;
    }
    
	%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">

<!-- [if lt IE 9] -->
    
	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<script src="./js/bootstrap-datepicker-thai.js"></script>
	<script src="./js/locales/bootstrap-datepicker.th.js"></script>
	<script src="./js/bootstrap-datepicker.js"></script>
	
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
	
	 <!-- Bootstrap core CSS -->
 <link href="./js/test/css/bootstrap.min.css" rel="stylesheet">
 <link href="./js/test/css/bootstrap.min.css" rel="stylesheet">
 <link href="./js/test/css/bootstrap-theme.min.css" rel="stylesheet">
 
<!--  <link href="theme.css" rel="stylesheet"> -->
<!--   <script src="jquery.min.js"></script> -->
  <script src="./js/test/js/bootstrap.min.js"></script>
  
  <link href="./js/test/validator.css" rel="stylesheet">
 <script src="./js/test/jquery.form.validator.min.js"></script>
 <script src="./js/test/security.js"></script>
 <script src="./js/test/file.js"></script>

	<!-- chack data script -->
	<script src="./js/JScon.js"></script>

<script>
function validateForm(){
var x=document.forms[""]["fname"].value;
if (x==null || x=="")
  {  alert("First name must be filled out");
  return false;  }
}
</script>


<script type="text/javascript">

	$.validator.addMethod('filesize', function(value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	}, 'ขนาดไฟล์ต้องไม่เกิน 2 MB และต้องเป็น  jpg,jpeg เท่านั้น {0}');

	jQuery(function($) {
		"use strict";
		$('#myform1').validate({
			rules : {
				FirstName : {
					required : true,
					maxlength : 20
				},
				image : {
					required : true,
					extension : "jpg,jpeg",
					filesize : 2000,
				}
			},
		});
	});
</script>	

</head>
<body class="clinic_version">
	
	<jsp:include page="navbar/header.jsp" />

	<div id="service" class="services2 wow fadeIn">
      <div class="container">
         <div class="row center">
            <div class="col-lg-9 col-md-4 col-sm-6 col-xs-9">
               <div class="appointment-form">
                  <h3>แก้ไขข้อมูลส่วนตัว</h3>
                  <div class="form">
                  	<div class="form-group row" align="center">
						<div class="col col-5" >
							<img alt="" src="./img/<%= con.getImg_c() %>" width="200" height="200">
						</div>
					</div>
					<div class="form-group row">
						<div class="col col-5" align="center">
							<p1>รูปโปรไฟล์</p1>
						</div>
					</div>
                    <form name="frm" id="myform1" action="/WebService/editprofilecon" method="POST" enctype="multipart/form-data"  novalidate>
                        <fieldset>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	 <label for="title" class="col-sm-2 control-label">คำนำหน้า</label>
                                 <div>
                                 	<div class="col col-5">
										<% if(con.getTitle().equals("นาย")) {%>
										<div class="form-check">
												<input type="radio" name="title" id="title" value="Mr" checked="checked">
				                    			<label for="title1" >นาย</label> 
				                    
				                    			<input type="radio" name="title" id="title" value="Mrs" >
				                   				<label for="title2">นาง</label>
				                    
				                    			<input type="radio" name="title" id="title" value="Ms" >	
				                    			<label for="title3"> นางสาว</label>
										</div>
										<% }else if(con.getTitle().equals("นาง")){ %>	
										<div class="form-check">
											<input type="radio" name="title" id="title" value="Mr" >
				                    		<label for="title1">นาย</label> 
				                    
				                    		<input type="radio" name="title" id="title" value="Mrs" checked="checked">
				                   			<label for="title2">นาง</label>
				                    
				                    		<input type="radio" name="title" id="title" value="Ms" >	
				                    		<label for="title3"> นางสาว</label>
										</div>
										<% }else{ %>
										<div class="form-check">
											<input type="radio" name="title" id="title" value="Mr" >
				                    		<label for="title1">นาย</label> 
				                    
				                    		<input type="radio" name="title" id="title" value="Mrs" >
				                   			<label for="title2">นาง</label>
				                    
				                    		<input type="radio" name="title" id="title" value="Ms" checked="checked">	
				                    		<label for="title3"> นางสาว</label>
										</div>
										<%} %>
										
									</div>
				                       <p id="demo" style="color: red; font-size: 16px"></p>           	
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="fname" class="col-sm-2 control-label">ชื่อ</label>
                              	<p class="col-sm-6 control-label" id="demo1" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                    <input type="text" class="form-control" name="fname" id="fname" 
                                    	pattern="^[A-Za-zก-์]{1,40}$" value="<%= con.getFristname()  %>"  required >
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="lname" class="col-sm-2 control-label">นามสกุล</label>
                              	<p class="col-sm-6 control-label" id="demo2" style="color: red; font-size: 16px"></p>
                                 <div class="form-group">
                                     <input type="text" class="form-control" name="lname" id="lname" value="<%= con.getLastname() %>" 
                                     	pattern="^[A-Za-zก-์]{1,40}$" required>
                                 </div>
                              </div>
                           </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	 <label for="gender" class="col-sm-2 control-label">เพศ</label>
                                 <div>
                                 	<% if(con.getGender().equals("ชาย")){ %>
                                 		<input type="radio" name="gender" id="gender" value="male" checked="checked">
                    					<label for="title1">ชาย</label> 
                    					<input type="radio" name="gender" id="gender" value="female" >
                   						<label for="title2">หญิง</label>
                    				<%}else{ %>
                    					<input type="radio" name="gender" id="gender" value="male" >
                    					<label for="title1">ชาย</label>
                    					<input type="radio" name="gender" id="gender" value="female" c>
                   						<label for="title2">หญิง</label>
                    				<% } %>
                    
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="birthday" class="col-sm-2 control-label">วัน/เดือน/ปี เกิด</label>
                                 <div class="form-group">
                                    	<input type="text" class="date form-control" name="birthday" id="birthday" value="<%= date %>">
                                 </div>
                              </div>
                           </div>
								<script type="text/javascript">
									$(".date").datepicker({
										format : "dd-mm-yyyy",
										autoclose : true
									});
								</script>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="email" class="col-sm-2 control-label">อีเมล</label>
                                 <div class="form-group">
                                     <input type="text" class="form-control" name="email" id="email"
                   	 					pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" autocomplete="off" value="<%= con.getEmail() %>" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="tel" class="col-sm-2 control-label">เบอร์มือถือ</label>
                                 <div class="form-group">
                                     <input type="text" class="form-control" pattern="^0([8|9|6])([0-9]{8}$)" name="tel" id="tel"
                    						autocomplete="off" value="<%= con.getPhone() %>" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="address" class="col-sm-2 control-label">ที่อยู่</label>
                                 <div class="form-group">
                                     <textarea class="form-control" name="address" id="address" rows="3" required><%= con.getAddress() %></textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="congenital-disease" class="col-sm-2 control-label">โรคประจำตัว</label>
                                 <div class="form-group">
                                     <input type="text" class="form-control" name="congenital-disease" id="congenital-disease" 
                                     	autocomplete="off" pattern="^[A-Za-zก-์_.-]$" value="<%= con.getCongenital_disease() %>" >
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="Drug-allergy" class="col-sm-2 control-label">แพ้ยา</label>
                                 <div class="form-group">
                                     <input type="text" class="form-control" name="Drug-allergy" id="Drug-allergy" 
                                     	autocomplete="off" pattern="^[A-Za-zก-์_.-]$" value="<%= con.getDrug_allergy() %>" >
                                 </div>
                              </div>
                           </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="Drug-allergy" class="col-sm-2 control-label">แพ้อาหาร</label>
                                 <div class="form-group">
                                     <input type="text" class="form-control" name="Food-allergy" id="Food-allergy" 
                                     	autocomplete="off" pattern="^[A-Za-zก-์_.-]{1,40$" value="<%= con.getFood_allergy() %>" >
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="uname" class="col-sm-3 control-label">ชื่อผู้ใช้</label>
                                 <div class="form-group">
                                     <input type="text" class="form-control" name="uname" id="uname" 
                                     	autocomplete="off" pattern="^[A-Za-z]{1,40}$" value="<%= con.getUsername() %>" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="pwd" class="col-sm-2 control-label">รหัสผ่าน</label>
                                 <div class="form-group">
                                      <input type="password" class="form-control" name="pwd" id="pwd" autocomplete="off"
                                      	 pattern="^[A-Za-z0-9]{1,40}$" value="<%= con.getLogin().getPassword() %>" required>
                                 </div>
                              </div>
                           </div>
                           
                           <input type="hidden" name="mtype" value="<%= con.getLogin().getType() %>" readonly>
                           
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="pwd" class="col-sm-2 control-label">ไฟล์อัพโหลด</label>
                                 <div class="form-group">
                                      <input type="file" name="img_c" id="customFile"accept="image/*" value="<%= con.getImg_c()%>">
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <div class="form-group">
                                    <div class="center">
                                    	<button type="submit" value="อัพเดทข้อมูล" name="submit"
	                                      class="btn" style="text-align: center; ">
	                                      <img alt="" src="./img/diskette.png" width="30px" height="30px"> อัพเดทข้อมูล</button>
                					</div>
                                 </div>
                              </div>
                           </div>
                           
                        </fieldset>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>	
	
	<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>