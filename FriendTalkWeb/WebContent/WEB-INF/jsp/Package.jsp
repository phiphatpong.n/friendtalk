<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@ page import="bean.*, java.util.*,Manager.*"%>
    <%
    List<packages> list_pack = (List<packages>) session.getAttribute("list_pack");
    String type = (String) session.getAttribute("type");	
    %>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="images/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="js/modernizer.js"></script>
<!-- [if lt IE 9] -->
</head>
<body class="clinic_version">
   <jsp:include page="navbar/headertop.jsp" />
	
        <div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
							
						<%if(type != null){ %>
							<li><a class="active" href="/WebService/openHome">หน้าแรก</a></li>
						<%}else{ %>
							<li><a class="active" href="openindex">หน้าแรก</a></li>
						<%} %>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
    
		<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	    <h1 style="font-size: 30px;">แพ็คเกจ</h1>
	       <div class="row center">
	                  
	                        <table class="table  table-hover">
								<thead>
									<tr>
									<th>ขนาดแพ็คเกจ</th>
									<th>จำนวนชั่วโมงคุย</th>
									<th>จำนวนชั่วโมงแชท</th>
									<th>ราคา</th>
									<th></th>
									</tr>
								</thead>
								<tbody>
									<% if(list_pack.size() >= 1){ %>
										<% for(int i=0 ; i<list_pack.size() ; i++){ %>
											<tr>
											<td><label><%= list_pack.get(i).getPackage_size() %></label></td>
											<td><label><%= list_pack.get(i).getCall_hr() %></label></td>
											<td><label><%= list_pack.get(i).getChat_hr() %></label></td>
											<td><label><%= list_pack.get(i).getPrice() %></label></td>
											
											<% if(type != null){
											if(type.toLowerCase().equals("admin".toLowerCase())){ %>
												<td><a href="openEditPackage?size=<%= list_pack.get(i).getPackage_size() %>"><img alt="" src="./img/edit.png" width="30px" height="30px"></a></td>
											<% }else { %>
												<td><a href="openEditPackage?size=<%= list_pack.get(i).getPackage_size() %>"><input type="hidden" name="Edit" value="แก้ไข" title="แก้ไข"></a></td>
											<% }} %>
											<tr>
											<br>
										<% }}%>
								</tbody>
							
	                        </table>
	                </div>
	             </div>
	          </div>


<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>