<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	List<StressTest> stest = (List<StressTest>) request.getAttribute("stest");
	List<ResultSuggestion> listr =(List<ResultSuggestion>) request.getAttribute("lsitRS");
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	
	<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
	

</head>
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	             <div class="appointment-form">
	                <h3>ประวัติการทดสอบดัชนีชี้วัดสุขภาพจิต</h3>
	                <div class="form">
	                  <form name="frm" id="myformAcheckStressTest" action="/WebService/AddStressTestScore" method="POST"  novalidate>
	                      <fieldset>
	                         <table border="1" class="table table-bordered   table-hover">
								<thead>
								<tr>
									<th>ครั้งที่</th>
									<th>วันที่ทำ</th>
									<th>คะแนน</th>
									<th>ผลที่ได้</th>
								</tr>
								</thead>
								<tbody>
								<% if(stest.size() >= 1) { %>
								<% for(int i=0 ; i<stest.size() ; i++){ %>
									<tr>
										<td><%= i+1 %></td>
										<td><label><%= stest.get(i).getTest_date().toString().replaceAll(" 00:00:00.0", "") %></label></td>
										<td><label><%= stest.get(i).getTotal_score()%></label></td>
										<%
											int totalscore = 0 ;
											int resultscoremax = 0;
											int resultscoremin = 0;
											List<String> listresulttest = new ArrayList<>();
												for(int r=0 ; r<listr.size() ; r++) {
													totalscore = stest.get(i).getTotal_score();
													resultscoremax = listr.get(r).getMax_score();
													resultscoremin = listr.get(r).getMin_score();
														if(totalscore >= resultscoremin && totalscore <= resultscoremax) {
															%>
															<td><label><%= listr.get(r).getResult_suggestion() %></label></td>
												<%} }%>
									</tr>	
								<% } } %>
								</tbody>
							</table>
	                      </fieldset>
	                   </form>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	

<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>