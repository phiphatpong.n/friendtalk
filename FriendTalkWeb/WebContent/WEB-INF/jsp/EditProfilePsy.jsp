<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ page import="bean.*, java.util.* , java.text.*" %>
	<%
	Psychologist psy = new Psychologist();
	psy =(Psychologist) request.getAttribute("Pid");

	String date1 = new SimpleDateFormat("dd-mm-yyyy").format(psy.getBirthday().getTime());
	int mon = psy.getBirthday().getTime().getMonth();
	
	String[] d = date1.split("-");
    int day = Integer.parseInt(d[0]);
    int month = mon + 1 ;
    int year = Integer.parseInt(d[2]);
    String date = "";
    if(month < 10){
    	date = d[0] + "-0"+ String.valueOf(month)  +"-" + d[2] ;
    }else{
    	date = d[0] + "-"+ String.valueOf(month)  +"-" + d[2] ;
    }
	%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->


    <script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	<script src="./js/bootstrap-datepicker-thai.js"></script>
	<script src="./js/locales/bootstrap-datepicker.th.js"></script>
	<script src="./js/bootstrap-datepicker.js"></script>
	
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>

	<!-- chack data script -->
	<script src="./js/Jspsy.js"></script>
	
	
	<script type="text/javascript">

	$.validator.addMethod('filesize', function(value, element, param) {
		return this.optional(element) || (element.files[0].size <= param)
	}, 'ขนาดไฟล์ต้องไม่เกิน 2 MB และต้องเป็น  jpg,jpeg เท่านั้น {0}');

	jQuery(function($) {
		"use strict";
		$('#myform1').validate({
			rules : {
				FirstName : {
					required : true,
					maxlength : 20
				},
				image : {
					required : true,
					extension : "jpg,jpeg",
					filesize : 2000,
				}
			},
		});
	});
</script>	
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openHome">หน้าแรก</a></li>
						<li><a href="doctor.html">รายชื่อหมอ</a></li>
						<li><a href="appointment.html">ตารางการนัด</a></li>
						<li><a href="packet.html">แพ็กเกจ</a></li>
						<li><a href="contact-us.html">ติดต่อเจ้าหน้าที่</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="service" class="services2 wow fadeIn">
      <div class="container">
         <div class="row center">
            <div class="col-lg-9 col-md-4 col-sm-6 col-xs-9">
               <div class="appointment-form">
                  <h3>แก้ไขข้อมูล</h3>
                  <div class="form">
                  	<div class="form-group row" align="center">
						<div class="row">
							<img alt="" src="./img/<%= psy.getImg_p()%>" width="200" height="200">
						</div>
					</div>
					<div class="form-group row">
						<div class="col col-5" align="center">
							<p1>รูปโปรไฟล์</p1>
						</div>
					</div>
                    <form name="frm" id="myform1" action="/WebService/editprofilepsy" method="POST" enctype="multipart/form-data"  novalidate>
                        <fieldset>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	 <label for="title" class="col-sm-2 control-label">คำนำหน้า</label>
                              	 
                                 <div>
                                 	<div class="col col-5">
										<% if(psy.getTitle().equals("นาย")) {%>
										<div class="form-check">
												<input type="radio" name="title" id="title" value="Mr" checked="checked">
				                    			<label for="title1" >นาย</label> 
				                    
				                    			<input type="radio" name="title" id="title" value="Mrs" >
				                   				<label for="title2">นาง</label>
				                    
				                    			<input type="radio" name="title" id="title" value="Ms" >	
				                    			<label for="title3"> นางสาว</label>
										</div>
										<% }else if(psy.getTitle().equals("นาง")){ %>	
										<div class="form-check">
											<input type="radio" name="title" id="title" value="Mr" >
				                    		<label for="title1">นาย</label> 
				                    
				                    		<input type="radio" name="title" id="title" value="Mrs" checked="checked">
				                   			<label for="title2">นาง</label>
				                    
				                    		<input type="radio" name="title" id="title" value="Ms" >	
				                    		<label for="title3"> นางสาว</label>
										</div>
										<% }else{ %>
										<div class="form-check">
											<input type="radio" name="title" id="title" value="Mr" >
				                    		<label for="title1">นาย</label> 
				                    
				                    		<input type="radio" name="title" id="title" value="Mrs" >
				                   			<label for="title2">นาง</label>
				                    
				                    		<input type="radio" name="title" id="title" value="Ms" checked="checked">	
				                    		<label for="title3"> นางสาว</label>
										</div>
										<%} %>
										 
									</div>
				                                 	
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="fname" class="col-sm-2 control-label">ชื่อ</label>
                                 <div class="form-group">
                                    <input type="text" class="form-control" name="fname" id="fname" 
                                    	pattern="^[A-Za-zก-์]{1,40}$" value="<%= psy.getFristname()  %>"  required >
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="lname" class="col-sm-2 control-label">นามสกุล</label>
                                 <div class="form-group">
                                     <input type="text" class="form-control" name="lname" id="lname" value="<%= psy.getLastname() %>" 
                                     	pattern="^[A-Za-zก-์]{1,40}$" required>
                                 </div>
                              </div>
                           </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	 <label for="gender" class="col-sm-2 control-label">เพศ</label>
                                 <div>
                                 	<% if(psy.getGender().equals("ชาย")){ %>
                                 		<input type="radio" name="gender" id="gender" value="male" checked="checked">
                    					<label for="title1">ชาย</label> 
                    					<input type="radio" name="gender" id="gender" value="female" >
                   						<label for="title2">หญิง</label>
                    				<%}else{ %>
                    					<input type="radio" name="gender" id="gender" value="male" >
                    					<label for="title1">ชาย</label>
                    					<input type="radio" name="gender" id="gender" value="female" checked="checked">
                   						<label for="title2">หญิง</label>
                    				<% } %>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="birthday" class="col-sm-6 control-label">วัน/เดือน/ปี เกิด</label>
                                 <div class="form-group">
                                    	<input type="text" class="date form-control" name="birthday" id="birthday" value="<%= date %>">
                                 </div>
                              </div>
                           </div>
								<script type="text/javascript">
									$(".date").datepicker({
										format : "dd-mm-yyyy",
										autoclose : true
									});
								</script>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="email" class="col-sm-2 control-label">อีเมล</label>
                                 <div class="form-group">
                                     <input type="text" class="form-control" name="email" id="email"
                   	 					pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" autocomplete="off" value="<%= psy.getEmail() %>" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="tel" class="col-sm-2 control-label">เบอร์มือถือ</label>
                                 <div class="form-group">
                                     <input type="text" class="form-control" pattern="^0([8|9|6])([0-9]{8}$)" name="tel" id="tel"
                    						autocomplete="off" value="<%= psy.getPhone() %>" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="address" class="col-sm-2 control-label">ที่อยู่</label>
                                  <div class="form-group">
                                     <textarea class="form-control" name="address" id="address" rows="3" required><%= psy.getAddress() %></textarea>
                                 </div>
                              </div>
                           </div>
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                         	<div class="row">
								<label for="textarea_address" class="col-sm-6 control-label">ประวัติการศึกษา</label>
								 <div class="form-group">
									<textarea class="form-control" name="educationl-history" id="educationl-history" rows="3"  required><%= psy.getEducationl_history() %></textarea>
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="row">
								<label for="textarea_address" class="col-sm-6 control-label">ประวัติการทำงาน</label>
								 <div class="form-group">
									<textarea class="form-control" name="working-history" id="working-history" rows="3" required><%= psy.getWork_history() %></textarea>
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="row">
							<label for="select_province" class="col-sm-6 control-label">ประเภทที่ปรึกษา(ตามที่จบมา เช่น จิตแพทย์)</label>
							 <div class="form-group">
								<% if(psy.getMentor_type().equals("psychiatrist")){ %>
								<select class="custom-select" name="mentor-type" id="mentortype" required>
									<option value="">เลือกประเภท</option>
									<option value="psychiatrist" selected>จิตแพทย์</option>
									<option value="psychologist">จิตวิทยา</option>
								</select>
								<%}else{ %>
								<select class="custom-select" name="mentor-type" id="mentortype" required>
									<option value="">เลือกประเภท</option>
									<option value="psychiatrist">จิตแพทย์</option>
									<option value="psychologist" selected>จิตวิทยา</option>
								</select>
								<% } %>
							</div>
						</div>
						</div>
						 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="uname" class="col-sm-2 control-label">ชื่อผู้ใช้</label>
                                  <div class="form-group">
                                     <input type="text" class="form-control" name="uname" id="uname" 
                                     	autocomplete="off" pattern="^[A-Za-z0-9]{1,40}$" value="<%= psy.getUsername() %>" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="pwd" class="col-sm-2 control-label">รหัสผ่าน</label>
                                 <div class="form-group">
                                      <input type="password" class="form-control" name="pwd" id="pwd" autocomplete="off"
                                      	 pattern="^[A-Za-z0-9]{1,40}$" value="<%= psy.getLogin().getPassword() %>" required>
                                 </div>
                              </div>
                           </div>
                           
                           <input type="hidden" name="mtype" value="<%= psy.getLogin().getType() %>" readonly>
                           
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                              	<label for="pwd" class="col-sm-6 control-label">อัพโหลดรูปโปรไฟล์</label>
                                 <div class="form-group">
                                      <input type="file" name="img_c" id="customFile"accept="image/*">
                                 </div>
                              </div>
                           </div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">     
							<div class="row">   
								<label class="col-sm-6 control-label">ใบรับรอง/ใบจบการศึกษา(ให้ทำเป็นไฟล์ PDF)</label>	  
								 <div class="form-group">
                                      <input type="file" name="img_crc" id="customFile2"accept="application/pdf">
                                 </div>                            
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-6 col-form-label text-right">ไฟล์ใบรับรอง/ใบจบการศึกษา</label>
							<div class="col col-5">
								<embed src="./img/<%=psy.getCertificate()%>" type="application/pdf" width="800px" height="800px" />
							</div>
						</div>
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="row">
                                 <div class="form-group">
                                    <div class="center">
                                    	<button type="submit" value="อัพเดทข้อมูล" name="submit"
	                                      class="btn" style="text-align: center; ">
	                                      <img alt="" src="./img/diskette.png" width="30px" height="30px"> อัพเดทข้อมูล</button>
                					</div>
                                 </div>
                              </div>
                           </div>
                           
                        </fieldset>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>	
   
<jsp:include page="navbar/footer.jsp" />

</body>
</html>