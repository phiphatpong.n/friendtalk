<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->
<script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
<link rel="stylesheet" href="./js/test/main.css">
<script src="./js/test/main.js"></script>

<script type="text/javascript">
	function check(frmlogin) {
		var user = /^[A-Za-z]{1,255}$/;
		var h = "";
		var p = /^[A-Za-zก-์]{1,40}$/;

		if (!user.test(frmlogin.uname.value)) {
			alert("กรุณากรอกชื่อผู้ใช้");
			frm.uname.focus();
			return false;
		}
		if (h.test(frmlogin.pwd.value)) {
			alert("กรุณากรอก รหัสผ่าน");
			frm.pwd.focus();
			return false;
		}
	}
</script>
</head>
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="openindex">หน้าแรก</a></li>
                     <li><a href="/WebService/openPackage">แพ็กเกจ</a></li>
                     <li><a href="/WebService/openStressQuestion">แบบทดสอบ</a></li>
                     <li><a href="/WebService/openlistArticle">รายการบทความ</a></li>
                     <li><a href="contact-us.html">ติดต่อเจ้าหน้าที่</a></li>
						<li><a href="contact-us.html">ติดต่อเจ้าหน้าที่</a></li>

					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<br>
	<div id="service" class="services2 wow fadeIn" >
	    <div class="container">
	       <div class="row center" >
	          <div class="col-lg-9 col-md-4 col-sm-6 col-xs-9">
	             <div class="appointment-form">
	                <h3>เข้าสู่ระบบ</h3>
	                <div class="form">
	                  <form name="frmlogin" id="frmlogin" action="/WebService/verifylogin" method="POST" novalidate>
	                      <fieldset>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="fname" class="col-sm-2 control-label">ชื่อผู้ใช้</label>
	                                <p class="col-sm-6 control-label" id="demo1" style="color: red; font-size: 16px"></p>
	                               <div class="form-group">
	                               		<input type="text" class="form-control" name="uname"placeholder="username" required="required">
	                               </div>
	                            </div>
	                         </div>
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="fname" class="col-sm-2 control-label">รหัสผ่าน </label>
	                                <p class="col-sm-6 control-label" id="demo1" style="color: red; font-size: 16px"></p>
	                               <div class="form-group">
	                               		<input type="password" class="form-control" name="pwd" placeholder="password" required="required">
	                               </div>
	                            </div>
	                         </div>
	                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                                <label for="detail" class="col-sm-6 control-label">ประเภทผู้ใช้งาน</label>
	                                <p class="col-sm-6 control-label" id="demo4" style="color: red; font-size: 16px"></p>
	                               <div class="form-group">
						               <select class="custom-select" name="type">
												<option value="consultant">ผู้ใช้ทั่วไป</option>
												<option value="organization">องค์กร</option>
												<option value="psychologist">ผู้ให้คำปรึกษา</option>
												<option value="admin">แอดมิน</option>
										</select>
	                               </div>
	                            </div>
	                         </div>
	                         
	                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                            <div class="row">
	                               <div class="form-group">
	                                  <div class="center">
	                                      <input type="submit" value="เข้าสู่ระบบ" name="submit"
											id="submitButton" class="form-control-submit-button"  title="เข้าสู่ระบบ" onclick="return frmlogin();">
	                                  </div>
	                               </div>
	                            </div>
	                         </div>
	                         
	                      </fieldset>
	                   </form>
	                   <form action="/WebService/openregister" method="GET">
						<!-- popup -->
						<div class="container" style="margin-left: 300px; width: ">
							<!-- Trigger the modal with a button -->
							<button type="button" class="btn btn-info btn-lg"  data-toggle="modal"
								data-target="#myModal">สมัครสมาชิก</button>
				
							<!-- Modal -->
							<div class="modal fade" id="myModal" role="dialog">
								<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title">Member Type</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											
										</div>
				
										<div class="modal-body">
											<select class="custom-select" name="Mtype">
												<option value="consultant">ผู้ใช้ทั่วไป</option>
												<option value="organization">องกรค์</option>
												<option value="psychologist">ผู้ให้คำปรึกษา</option>
											</select>
										</div>
										<div class="modal-footer">
											<td><input type="submit" name="next"
												id="submitButton"class="form-control-submit-button" ></td>
										</div>
									</div>
				
								</div>
							</div>
						</div>
					</form>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	

	


<jsp:include page="navbar/footer.jsp" />

</body>

</html>