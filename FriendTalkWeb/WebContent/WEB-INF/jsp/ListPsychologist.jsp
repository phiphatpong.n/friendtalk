<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.* , java.text.*"%>
<%
	String user = (String) session.getAttribute("user");
	List<Psychologist> list = (List<Psychologist>) request.getAttribute("list");
	List<PsychologistExpertise> listPEK = (List<PsychologistExpertise>) request.getAttribute("listPEK");
	List<SetWorkingTime> listswt = (List<SetWorkingTime>) request.getAttribute("listswt");
	
	List<Review> listreReviews = (List<Review>) request.getAttribute("listreReviews");
	List<Advice> liAdvices = (List<Advice>) request.getAttribute("liAdvices");
	
	
	
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

<!-- ModelPopup -->
<script src="./js/PopupModel/ModelPopup.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.checked {
  color: orange;
}
</style>
<style>
/* The Modal (background) */
.modalpop {
	display: none; /* Hidden by default */
	position: fixed; /* Stay in place */
	z-index: 1; /* Sit on top */
	padding-top: 35px; /* Location of the box */
	left: 0;
	top: 0;
	width: 100%; /* Full width */
	height: 100%; /* Full height */
	overflow: auto; /* Enable scroll if needed */
	background-color: rgb(0, 0, 0); /* Fallback color */
	background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
	background-color: #fefefe;
	margin: auto;
	padding: 20px;
	border: 1px solid #888;
	width: 65%;
	overflow: auto; /* Enable scroll if needed */
}
.img {
	margin-left: auto;
	margin-right: auto;
	width: 50%;
}

/* The Close Button */
.close {
	color: #FF0000;
	float: right;
	font-size: 28px;
	font-weight: bold;
 	background-color: #FF0000; 
}

.close:hover, .close:focus {
	color: #FF0000;
	text-decoration: none;
	cursor: pointer;
}

.pagination {
	margin: auto;
	display: inline-block;
}

.pagination a {
	color: black;
	float: left;
	padding: 8px 16px;
	text-decoration: none;
	transition: background-color .3s;
}

.pagination a.active {
	background-color: #4CAF50;
	color: white;
}

.pagination a :hover :not (.active ){
	background-color: #ddd;
}
</style>

</head>
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
					
					<% if(user != null){ %>
						<li><a class="active" href="openHome">หน้าแรก</a></li>
						<%}else{ %>
						<li><a class="active" href="openindex">หน้าแรก</a></li>
						<%} %>
					</ul>
				</div>
			</nav>
			<form action="/WebService/searchpsychologist" method="POST">
				<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg" name="keyword" id="myInput" placeholder="ค้นหา" /> 
							<span class="input-group-btn">
								<button class="btn btn-info btn-lg" type="submit"> <i class="fa fa-search" aria-hidden="true"></i></button>
							</span>
					</div>
				</div>
				</div>
			</form>
		</div>
	</div>
	
		<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	             <div class="appointment-form">
	                <h3>รายชือผู้ให้คำปรึกษา</h3>
	                <table class="table  table-hover">
							<thead>
								<tr>
									<th></th>
									<th>ชื่อ-ประเภทที่ปรึกษา</th>
									<th>ความเชียวชาญเฉพาะ</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<%
									if (list.size() >= 1) {
								%>
								<%
									for (int i = 0; i < list.size(); i++) {
								%>
							<tr>
								<td><img alt="" src="./img/<%=list.get(i).getImg_p()%>" width="150" height="150"></td>
								<td><label><%=list.get(i).getTitle()+" "+ list.get(i).getFristname() + " " + list.get(i).getLastname()%></label><br>
								
								<%if(list.get(i).getMentor_type().equals("psychiatrist")){ %>
									<label style="color: blue;">จิตแพทย์</label>
								<%}else{ %>
									<label style="color: blue;">นักจิตวิทยา</label>
								<% } %>
								<br>
								<label>
								<% int x=0  ; Double sumscore = 0.0 ;
												for(int j=0 ; j<liAdvices.size() ; j++){ 
													 if(liAdvices.get(j).getMake_appointment().getPsychologist().getUsername().equals(list.get(i).getUsername())){
														for(int n=0 ; n<listreReviews.size() ; n++){
															 if(liAdvices.get(j).getReview() != null){
															   if(liAdvices.get(j).getReview().getReview_id().equals(listreReviews.get(n).getReview_id())){
																   sumscore += listreReviews.get(n).getSatisfaction_score()  ;
																  	x++;
																   sumscore = sumscore/x ;
																}
															}
														}
													}
												} %>
											<% DecimalFormat df = new DecimalFormat("#.#"); %>
													<% if(sumscore >= 5){ %>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<%}else if(sumscore  >= 4){ %>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<%} else if(sumscore >= 3){ %>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<% }else if(sumscore >= 2){ %>  
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<% }else if(sumscore >= 1){ %>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<% }else{ %>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<% } %>
													     <%=  df.format(sumscore) %>
								
								</label>
								</td>
								<td>
								<%
									for (int j = 0; j < listPEK.size(); j++) {
								%>
								<%
								if (listPEK.get(j).getPk().getPsychologist().getUsername().equals(list.get(i).getUsername())) {
								%>
								<label><ul><li><%=listPEK.get(j).getPk().getExpertise().getExpertise_type()%></li></ul></label><br>
								<%
									}
											}
								%>
								</td>
								<td>
									<label>วีดิโอคอลหรือคอล</label><br>
									<label>800/1 ชม.</label><br>
									<label>400/30 นาที </label><br>
									<label>คุยแชท</label><br>
									<label>400/1 ชม. </label><br>
									<label>200/30 นาที </label>
								</td>
								
								<td><input type="button" name="showdata" id="showdata" value="ดูข้อมูล" class="btn  btn-primary"
									onclick="return btnshowdataclick(<%=i%>)" class="form-control-submit-button">

									<div id="datatestitem<%=i%>" class="modalpop">
										<!-- Modal content -->
										<div class="modal-content">
											<span class="close" onclick="return spanclick(<%=i%>)">&times;</span>
											<br>
											<table class="table table-hover">
											<tr>
												<tr>
												<td>
													<img alt="" src="./img/<%=list.get(i).getImg_p()%>" width="100" height="100">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<label style="font-size: 18px;"><%=list.get(i).getTitle() + "." + list.get(i).getFristname() + "   " + list.get(i).getLastname()%></label>
													<br>
													<br>
													<%if(list.get(i).getMentor_type().equals("psychiatrist")){ %>
														<label style="color: blue;">จิตแพทย์</label>
													<%}else{ %>
														<label style="color: blue;">นักจิตวิทยา</label>
													<% } %>
													<br>
													<%for (int j = 0; j < listPEK.size(); j++) {%>
													<%if (listPEK.get(j).getPk().getPsychologist().getUsername().equals(list.get(i).getUsername())) {%>
														<label><ul><li><%=listPEK.get(j).getPk().getExpertise().getExpertise_type()%></li></ul></label>
													<%}}%>
												</td> 
												</tr>
												<tr>
													<td>
														<label>ประวัติการศึกษา</label>
														<br>
														<label><%=list.get(i).getEducationl_history()%></label>
													</td>
												</tr>
												<tr>
													<td>
														<label>ประวัติการทำงาน</label>
														<br>
														<label><%=list.get(i).getWork_history()%></label>
													</td> 
												</tr>
												<tr>
												<td>
													<label>เวลาในการทำงาน</label>
													<br>
													<% if(listswt.size() >=1){
														for(int s=0 ; s<listswt.size() ; s++){ 
														if(listswt.get(s).getPwkey().getPsychologist().getUsername().equals(list.get(i).getUsername())){%>
																<label><ul><li><%=listswt.get(s).getPwkey().getWorking_date() %></li></ul></label>
																<label><%= listswt.get(s).getTime_start() +" -"  %></label>
																<label><%= listswt.get(s).getTime_end() %></label><br>
													<%}}} %>
												</td>
												</tr>
												<tr>
												<% 
//												int x=0  ; Double sumscore = 0.0 ;
// 												for(int j=0 ; j<liAdvices.size() ; j++){ 
// 													 if(liAdvices.get(j).getMake_appointment().getPsychologist().getUsername().equals(list.get(i).getUsername())){
// 														for(int n=0 ; n<listreReviews.size() ; n++){
// 															 if(liAdvices.get(j).getReview() != null){
// 															   if(liAdvices.get(j).getReview().getReview_id().equals(listreReviews.get(n).getReview_id())){
// 																   sumscore += listreReviews.get(n).getSatisfaction_score()  ;
// 																  	x++;
// 																   sumscore = sumscore/x ;
// 																}
// 															}
// 														}
// 													}
//												} 
												%>
												<td>
													<label>รีวิว</label>
												</td>
												<td><label>
													<% if(sumscore >= 5){ %>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<%}else if(sumscore  >= 4){ %>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<%} else if(sumscore >= 3){ %>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<% }else if(sumscore >= 2){ %>  
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<% }else if(sumscore >= 1){ %>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<% }else{ %>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<span class="fa fa-star" style="color:LightGray;"></span>
													<% } %>
													     <%=  df.format(sumscore) %>
												</label>
												</td>
												</tr>
												<tr>
												
												</tr>
												<%for(int j=0 ; j<liAdvices.size() ; j++){ 
													if(liAdvices.get(j).getMake_appointment().getPsychologist().getUsername().equals(list.get(i).getUsername())){
														for(int n=0 ; n<listreReviews.size() ; n++){
															if(liAdvices.get(j).getReview() != null){
															if(liAdvices.get(j).getReview().getReview_id().equals(listreReviews.get(n).getReview_id())){
												%>
												<tr>
													<td>
														<label><%= listreReviews.get(n).getComment() %></label>
													</td>
													<td><label><%= listreReviews.get(n).getSatisfaction_score() %></label></td>
												  </tr>
													
												<%
															}
															}
														}
													}
												} %>
												
												<tr>
												<%if(user != null){ %>
											<form action="/WebService/openMakeAppointment" name="frm" method="POST">
												<input type="hidden" name="user" value="<%=user%>">
												<input type="hidden" name="userpsy"value="<%=list.get(i).getUsername()%>">
												<td colspan="2" align="center"><input type="submit" class="form-control-submit-button" id="submitButton"  name="appiontment" value="นัดหมาย"></td>
											</form>
											<%} %>
											</tr>
											
										</table>
									</div>
								</div>
							</td>
							</tr>
								<%}}%>
							</tbody>
						</table>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>




	<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>