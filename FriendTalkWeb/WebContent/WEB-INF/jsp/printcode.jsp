<%@page import="org.hibernate.query.criteria.internal.expression.function.SubstringFunction"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	PaymentCode pc =(PaymentCode) session.getAttribute("paymentCode");


	String datee = pc.getIssue_date().toString().replaceAll(" 00:08:00.0", "").trim();
	
	int alertpaymant = 0;
	try {
		alertpaymant = (Integer) session.getAttribute("alertPC");
	} catch (Exception e) {
	}

	session.removeAttribute("alertPC");
%>
<%
	if (1 == alertpaymant) {
%>
<script type="text/javascript">
	alert("พิมพ์โค้ดชำระเงินสำเร็จ");
</script>
<%} %>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->
<link rel="stylesheet" href="./css/print.css">
</head>
<style>
	@page {
	size: A4 ;
	margin: 0 ;
	}
</style>
<script type="text/javascript">
	function PrintDiv() {
		var divToPrint = document.getElementById('container'); // เลือก div id ที่เราต้องการพิมพ์
		var html = '<html>'
				+ // 
				'<head>'
				+ '<link href="css/print.css" rel="stylesheet" type="text/css">'
				+ '</head>' + '<body onload="window.print(); window.close();">'
				+ divToPrint.innerHTML  + '</body>' + '</html>';

		var popupWin = window.open();
		popupWin.document.open();
		popupWin.document.write(html); //โหลด print.css ให้ทำงานก่อนสั่งพิมพ์
		popupWin.document.close();
	}
</script>
<body class="clinic_version">
	
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="/WebService/openHome">หน้าแรก</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</div>

	<div class="container">
		<table border="1" class="table table-bordered   table-hover">
			<thead>
				<tr>
					<th>รหัสชำระเงิน</th>
					<th>จำนวนเวลาแชท(นาที)</th>
					<th>จำนวนเวลาคุย(นาที)</th>
					<th>วันที่ออก</th>
					<th>พิมพ์รหัส</th>
				</tr>
			</thead>
			<tbody>

				<tr>
					<td><label style="font-size: 18px;"><%= pc.getCode() %></label></td>
					<td><label><%= pc.getCall_minute() %></label></td>
					<td><label><%= pc.getChat_minute() %></label></td>
					<td><label><%= datee %></label></td>
					<td>
						<button type="submit" value="พิมรหัสชำระเงิน" name="submit" class="btn" style="text-align: center; ">
	                     <img alt="" src="./img/shuffle.png" width="30px" height="30px" onclick="window.print();"> พิมรหัสชำระเงิน</button>
					</td>
				</tr>

			</tbody>
		</table>
	</div>
   
	<!-- end copyrights -->
	<a href="#home" data-scroll class="dmtop global-radius"><i
		class="fa fa-angle-up"></i></a>
	<!-- all js files -->
	<script src="./js/all.js"></script>
	<!-- all plugins -->
	<script src="./js/custom.js"></script>
	<!-- map -->
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNUPWkb4Cjd7Wxo-T4uoUldFjoiUA1fJc&callback=myMap"></script>
    
</body>
</html>