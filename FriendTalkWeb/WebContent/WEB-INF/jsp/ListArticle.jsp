<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*"%>
<%
	String user = (String) session.getAttribute("user");
	String type = (String) session.getAttribute("type");
	List<Article> arList = (List<Article>) request.getAttribute("arList");
	
	int alert = -1;
	try {
		alert = (Integer) session.getAttribute("alt");
	} catch (Exception e) {
	}
%>
<%
	if (1 == alert) {
%>
<script type="text/javascript">
	alert("ลบสำเร็จ");
</script>
<%
	} else if (0 == alert) {
%>
<script type="text/javascript">
	alert("ลบไม่สำเร็จ");
</script>
<%
	}
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

</head>
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						
						<% 
						if(type != null){
							if(type.toLowerCase().equals("psychologist".toLowerCase())){ %>
								<li><a class="active" href="openHome">หน้าแรก</a></li>
								<li><a href="/WebService/openAddArticle?user=<%=user%>">เพิ่มบทความ</a></li>
							<%}else{%>
								<li><a class="active" href="openHome">หน้าแรก</a></li>
							<%}
						}else{ %>
							<li><a class="active" href="openindex">หน้าแรก</a></li>
						<%} %>
					</ul>
				</div>
			</nav>
			<form action="/WebService/searchArticle" method="POST">
				<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg" name="keyword" id="myInput" placeholder="ค้นหาชื่อบทความ" /> 
							<span class="input-group-btn">
								<button class="btn btn-info btn-lg" type="submit"> <i class="fa fa-search" aria-hidden="true"></i></button>
							</span>
					</div>
				</div>
				</div>
			</form>
		</div>
	</div>
	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	             <div class="appointment-form">
	                <h3>รายการบทความ</h3>
	                <table border="1" class="table table-bordered   table-hover">
						<thead>
						<tr>
						<th>รูป</th>
						<th>ชื่อบทความ</th>
						<th>วันที่เพิ่มบทความ</th>
						<%if(type != null){ 
						if(type.toLowerCase().equals("psychologist".toLowerCase())){ %>
						<th></th>
						<th></th>
						<th></th>
						<%}else{ %>
						<th></th>		
						<%}}%>
						
						</tr>
						</thead>
						<tbody>
							<% if(arList.size() >= 1){ %>
							<% for(int i=0 ; i<arList.size() ; i++){ %>
								<tr>
								<td><img alt="" src="./img/<%= arList.get(i).getImg_articles()%>" width="100" height="100"></td>
								<td><label><%= arList.get(i).getArticle_name() %></label></td>
								<%
									String date1 = arList.get(i).getAdd_date().getTime().toInstant().toString().replaceAll("T17:00:00Z", "").trim() ;
									System.out.println("day = "+date1);
									
									String[] d = date1.split("-");
								    int day = Integer.parseInt(d[2]) +1 ;
								    String date = "";
								    if(day < 10){
								    	date = "0"+day + "-"+ d[1]  +"-" + d[0] ;
								    }else{
								    	date = day + "-"+ d[1]  +"-" + d[0] ;
								    }
									
								%>
								<td><label><%= date %></label></td>
								
								<% if(type != null){
									if(type.toLowerCase().equals("psychologist".toLowerCase())){ %>
									<td align="center"><a href="/WebService/openEditArticle?Aid=<%= arList.get(i).getArticle_id() %>">
										<img alt="" src="./img/edit.png" width="30px" height="30px"></a></td>
									<td align="center"><a href="/WebService/DeleteArticle?Aid=<%= arList.get(i).getArticle_id() %>">
										<img alt="" src="./img/trash.png" width="30px" height="30px" onclick="return confirm('คุณต้องการลบใช่หรือไม่?');"></a></td>
									<td align="center"><a href="/WebService/openArticleDetils?Aid=<%= arList.get(i).getArticle_id() %>">
										<input type="button" name="SeeMore" value="เพิ่มเติม" id="submitButton" title="เพิ่มเติม" class="form-control-submit-button"></a></td>
									<%}else{ %>
										<td align="center"><a href="/WebService/openArticleDetils?Aid=<%= arList.get(i).getArticle_id() %>">
										<input type="button" name="SeeMore" value="เพิ่มเติม" id="submitButton" title="เพิ่มเติม" class="form-control-submit-button"></a></td>
									<tr>
									<%} 
								}else{%>
								<td align="center"><a href="/WebService/openArticleDetils?Aid=<%= arList.get(i).getArticle_id() %>">
									<input type="button" name="SeeMore" value="เพิ่มเติม" id="submitButton" title="เพิ่มเติม" class="form-control-submit-button"></a></td>
							<%} } }%>
							</tbody>
						</table>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	


<jsp:include page="navbar/footer.jsp" />
</body>
</html>