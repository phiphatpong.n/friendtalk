<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script type="text/javascript">
var count = 0;
function result(){
	if(count != 0){
		document.getElementById('result').innerHTML = 
		"<h4>Rating: <label class='text-primary'>" + count + "</label></h4>"
		+ "<h4>Review</h4>"
		+ "<p>"+document.getElementById("review").value+"</p>";
	}else{
 
	}
}
function startRating(item){
	count=item.id[0];
	sessionStorage.star = count;
	for(var i=0;i<5;i++){
		if(i < count){
			document.getElementById((i+1)).style.color="yellow";
		}
		else{
			document.getElementById((i+1)).style.color="black";
		}
	}
}
</script>
</head>
<body>
<nav class="navbar navbar-default">
		<div class="continer-fluid">
			<a class="navbar-brand" href="https://sourcecodester.com" target="_blank">Sourcecodester</a>
		</div>
	</nav>
	<div class="col-md-3"></div>
	<div class="col-md-6 well">
		<h3 class="text-primary">JavaScript - Simple Star Rating</h3>
		<hr style="border-top:1px dotted #ccc;"/>
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div>
				<h3>Rating:</h3>
				<span id="1" style="font-size:45px; cursor:pointer;"  class="fa fa-star" onmouseover="startRating(this)" startRating="starmark(this)" ></span>
				<span id="2"  style="font-size:45px; cursor:pointer;" class="fa fa-star" onmouseover="startRating(this)" startRating="starmark(this)"></span>
				<span id="3"  style="font-size:45px; cursor:pointer;" class="fa fa-star" onmouseover="startRating(this)" startRating="starmark(this)"></span>
				<span id="4"  style="font-size:45px; cursor:pointer;" class="fa fa-star" onmouseover="startRating(this)" startRating="starmark(this)"></span>
				<span id="5"  style="font-size:45px; cursor:pointer;" class="fa fa-star" onmouseover="startRating(this)" startRating="starmark(this)"></span>
			</div>
			<br />
			<div class="form-group">
				<h3>Review:</h3>
				<textarea id="review" class="form-control" style="resize:none; height:100px;"></textarea>
			</div>
			<center><button class="btn btn-success" onclick="result()">SUBMIT</button></center>
			<div id="result"></div>
		</div>
	</div>
</body>
</html>