<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*,Manager.*"%>
<%
	String user = (String) session.getAttribute("uname");	
	String type = (String) session.getAttribute("Mtype");
%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->

</head>
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
		<div class="header-bottom wow fadeIn">
			<div class="container">
				<nav class="main-menu">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse" data-target="#navbar"
							aria-expanded="false" aria-controls="navbar">
							<i class="fa fa-bars" aria-hidden="true"></i>
						</button>
					</div>

					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<% if(type.equals("psychologist")){ %>
								<li><a class="active" href="openHome">หน้าแรก</a></li>
								<li><a href="/WebService/openViewMentoringHistory?user=<%=user%>">ประวัติการให้คำปรึกษา</a></li>
								<li><a href="/WebService/openViewAppointment?user=<%= user %>">ตารางการนัด</a></li>
								<li><a href="/WebService/openEditprofile?user=<%=user%>">ข้อมูลส่วนตัว</a></li>
								<li><a href="/WebService/openlistArticle?user=<%=user%>">บทความ</a></li>
								<li><a href="/WebService/openRedeem?user=<%=user%>">รายการของรางวัล</a></li>
								<li><a href="/WebService/openTimeWorking?user=<%=user%>">เวลาทำงาน</a></li>
								<li><a href="/WebService/logout?user=<%=user%>">ออกจากระบบ</a></li>
							<% }else if(type.equals("consultant")){ %>
								<li><a class="active" href="openHome">หน้าแรก</a></li>
								<li><a href="/WebService/openListPsychologistent?user=<%= user %>">รายชื่อหมอ</a></li>
								<li><a href="/WebService/openStressQuestion?user=<%= user %>">แบบทดสอบ</a></li>
								<li><a href="/WebService/openlistArticle?user=<%= user %>">รายการบทความ</a></li>
								<li><a href="/WebService/openViewAppointment?user=<%= user %>">ตารางการนัด</a></li>
								<li><a href="/WebService/openPackage?user=<%=user%>">แพ็กเกจ</a></li>
								<li><a href="contact-us.html">ติดต่อเจ้าหน้าที่</a></li>
								<li><a href="/WebService/openEditprofile?user=<%=user%>">ข้อมูลส่วนตัว</a></li>
								<li><a href="/WebService/logout?user=<%=user%>">ออกจากระบบ</a></li>
							<% }else if(type.equals("organization")){%>
								<li><a class="active" href="openHome">หน้าแรก</a></li>
								<li><a href="/WebService/openEditprofile?user=<%=user%>">ข้อมูลส่วนตัว</a></li>
								<li><a href="doctor.html">รายชื่อหมอ</a></li>
								<li><a href="/WebService/openPackage?user=<%=user%>">แพ็กเกจ</a></li>
								<li><a href="/WebService/getListGiveAdvice?user=<%=user%>">รายการรหัสชำระเงิน</a></li>
								<li><a href="/WebService/openViewBuyPackageOrg?user=<%=user%>">ประวัติการซื้อแพ็คเกจ</a></li>
								<li><a href="/WebService/openViewPackageOrg?user=<%=user%>">แพ็กเกจคงเหลือ</a></li>
								<li><a href="/WebService/logout?user=<%=user%>">ออกจากระบบ</a></li>
							<%} else{%>
								<li><a class="active" href="openHome">หน้าแรก</a></li>
								<li><a href="/WebService/openPackage?user=<%=user%>">แพ็กเกจ</a></li>
								<li><a href="/WebService/openListAward">ของรางวัล</a></li>
								<li><a href="/WebService/openPrice">อัพเดทราคา</a></li>
								<li><a href="/WebService/openAppPackage">อัพเดทแพ็คเกจ</a></li>
								<li><a href="/WebService/openListregisterPsy">รายชื่อผู้สมัครผู้ให้คำปรึกษา</a></li>
								<li><a href="/WebService/logout?user=<%=user%>">ออกจากระบบ</a></li>
							<% } %>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</header>
	<div id="home" class="parallax first-section wow fadeIn"
		data-stellar-background-ratio="0.4"
		style="background-image: url('./img/banner3.jpg');">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="text-contant">
						<h2>
							<span class="center"><span class="icon"><img
									src="./img/icon-logo.png" alt="#" /></span></span> <a href=""
								class="typewrite" data-period="2000"
								data-type='[ "Welcome to Friends Talk", "We Care Your Health", "We are Expert!" ]'>
								<span class="wrap"></span>
							</a>
						</h2>
					</div>
				</div>
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end section -->
	<div id="time-table" class="time-table-section">
		<div class="container">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<div class="row">
					<div class="service-time one" style="background: #2895f1;">
						<span class="info-icon"><i class="fa fa-ambulance"
							aria-hidden="true"></i></span>
						<h3>Emergency Case</h3>
						<p>Dignissimos ducimus qui blanditii sentium volta tum
							deleniti atque cori as quos dolores et quas mole.</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<div class="row">
					<div class="service-time middle" style="background: #0071d1;">
						<span class="info-icon"><i class="fa fa-clock-o"
							aria-hidden="true"></i></span>
						<h3>Working Hours</h3>
						<div class="time-table-section">
							<ul>
								<li><span class="left">Monday - Friday</span><span
									class="right">8.00 – 18.00</span></li>
								<li><span class="left">Saturday</span><span class="right">8.00
										– 16.00</span></li>
								<li><span class="left">Sunday</span><span class="right">8.00
										– 13.00</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<div class="row">
					<div class="service-time three" style="background: #0060b1;">
						<span class="info-icon"><i class="fa fa-hospital-o"
							aria-hidden="true"></i></span>
						<h3>Clinic Timetable</h3>
						<p>Dignissimos ducimus qui blanditii sentium volta tum
							deleniti atque cori as quos dolores et quas mole.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="about" class="section wow fadeIn">
		<div class="container">
			<div class="heading">
				<span class="icon-logo"><img src="./img/icon-logo.png"
					alt="#"></span>
				<h2>The Specialist Clinic</h2>
			</div>
			<!-- end title -->
			<div class="row">
				<div class="col-md-6">
					<div class="message-box">
						<h4>What We Do</h4>
						<h2>Clinic Service</h2>
						<p class="lead">Quisque eget nisl id nulla sagittis auctor
							quis id. Aliquam quis vehicula enim, non aliquam risus. Sed a
							tellus quis mi rhoncus dignissim.</p>
						<p>Integer rutrum ligula eu dignissim laoreet. Pellentesque
							venenatis nibh sed tellus faucibus bibendum. Sed fermentum est
							vitae rhoncus molestie. Cum sociis natoque penatibus et magnis
							dis parturient montes, nascetur ridiculus mus.</p>
						<a href="#services" data-scroll
							class="btn btn-light btn-radius btn-brd grd1 effect-1">Learn
							More</a>
					</div>
					<!-- end messagebox -->
				</div>
				<!-- end col -->
				<div class="col-md-6">
					<div class="post-media wow fadeIn">
						<img src="./img/about_03.jpg" alt="" class="img-responsive">
						<a href="http://www.youtube.com/watch?v=nrJtHemSPW4"
							data-rel="prettyPhoto[gal]" class="playbutton"> <i
							class="flaticon-play-button"></i>
						</a>
					</div>
					<!-- end media -->
				</div>
				<!-- end col -->
			</div>
			<!-- end row -->
			<hr class="hr1">
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="service-widget">
						<div class="post-media wow fadeIn">
							<a href="./img/clinic_01.jpg" data-rel="prettyPhoto[gal]"
								class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
							<img src="./img/clinic_01.jpg" alt="" class="img-responsive">
						</div>
						<h3>Digital Control Center</h3>
					</div>
					<!-- end service -->
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="service-widget">
						<div class="post-media wow fadeIn">
							<a href="./img/clinic_02.jpg" data-rel="prettyPhoto[gal]"
								class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
							<img src="./img/clinic_02.jpg" alt="" class="img-responsive">
						</div>
						<h3>Hygienic Operating Room</h3>
					</div>
					<!-- end service -->
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="service-widget">
						<div class="post-media wow fadeIn">
							<a href="./img/clinic_03.jpg" data-rel="prettyPhoto[gal]"
								class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
							<img src="./img/clinic_03.jpg" alt="" class="img-responsive">
						</div>
						<h3>Specialist Physicians</h3>
					</div>
					<!-- end service -->
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="service-widget">
						<div class="post-media wow fadeIn">
							<a href="./img/clinic_01.jpg" data-rel="prettyPhoto[gal]"
								class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
							<img src="./img/clinic_01.jpg" alt="" class="img-responsive">
						</div>
						<h3>Digital Control Center</h3>
					</div>
					<!-- end service -->
				</div>
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<div id="service" class="services wow fadeIn">
		<div class="container">
			<h1>พวกสโลแกนต่างๆ</h1>
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
					<div class="inner-services">
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<div class="serv">
								<span class="icon-service"><img
									src="./img/service-icon1.png" alt="#" /></span>
								<h4>PREMIUM FACILITIES</h4>
								<p>Lorem Ipsum is simply dummy text of the printing.</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<div class="serv">
								<span class="icon-service"><img
									src="./img/service-icon2.png" alt="#" /></span>
								<h4>LARGE LABORATORY</h4>
								<p>Lorem Ipsum is simply dummy text of the printing.</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<div class="serv">
								<span class="icon-service"><img
									src="./img/service-icon3.png" alt="#" /></span>
								<h4>DETAILED SPECIALIST</h4>
								<p>Lorem Ipsum is simply dummy text of the printing.</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<div class="serv">
								<span class="icon-service"><img
									src="./img/service-icon4.png" alt="#" /></span>
								<h4>CHILDREN CARE CENTER</h4>
								<p>Lorem Ipsum is simply dummy text of the printing.</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<div class="serv">
								<span class="icon-service"><img
									src="./img/service-icon5.png" alt="#" /></span>
								<h4>FINE INFRASTRUCTURE</h4>
								<p>Lorem Ipsum is simply dummy text of the printing.</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<div class="serv">
								<span class="icon-service"><img
									src="./img/service-icon6.png" alt="#" /></span>
								<h4>ANYTIME BLOOD BANK</h4>
								<p>Lorem Ipsum is simply dummy text of the printing.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="appointment-form">
						<h3>
							<span>+</span> Book Appointment
						</h3>
						<div class="form">
							<form action="index.jsp">
								<fieldset>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="row">
											<div class="form-group">
												<input type="text" id="name" placeholder="Your Name" />
											</div>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="row">
											<div class="form-group">
												<input type="email" placeholder="Email Address" id="email" />
											</div>
										</div>
									</div>
									<div
										class="col-lg-12 col-md-12 col-sm-12 col-xs-12 select-section">
										<div class="row">
											<div class="form-group">
												<select class="form-control">
													<option>Day</option>
													<option>Sunday</option>
													<option>Monday</option>
												</select>
											</div>
											<div class="form-group">
												<select class="form-control">
													<option>Time</option>
													<option>AM</option>
													<option>PM</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="row">
											<div class="form-group">
												<select class="form-control">
													<option>Doctor Name</option>
													<option>Mr.XYZ</option>
													<option>Mr.ABC</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="row">
											<div class="form-group">
												<textarea rows="4" id="textarea_message"
													class="form-control" placeholder="Your Message..."></textarea>
											</div>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="row">
											<div class="form-group">
												<div class="center">
													<button type="submit">Submit</button>
												</div>
											</div>
										</div>
									</div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end section -->

	<!-- doctor -->

	<div id="doctors" class="parallax section db"
		data-stellar-background-ratio="0.4" style="background: #fff;"
		data-scroll-id="doctors" tabindex="-1">
		<div class="container">

			<div class="heading">
				<span class="icon-logo"><img src="./img/icon-logo.png"
					alt="#"></span>
				<h2>The Specialist Clinic</h2>
			</div>

			<div class="row dev-list text-center">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 wow fadeIn"
					data-wow-duration="1s" data-wow-delay="0.2s"
					style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s; animation-name: fadeIn;">
					<div class="widget clearfix">
						<img src="./img/doctor_01.jpg" alt=""
							class="img-responsive img-rounded">
						<div class="widget-title">
							<h3>Soren Bo Bostian</h3>
							<small>Clinic Owner</small>
						</div>
						<!-- end title -->
						<p>Hello guys, I am Soren from Sirbistana. I am senior art
							director and founder of Violetta.</p>

						<div class="footer-social">
							<a href="#" class="btn grd1"><i class="fa fa-facebook"></i></a> <a
								href="#" class="btn grd1"><i class="fa fa-github"></i></a> <a
								href="#" class="btn grd1"><i class="fa fa-twitter"></i></a> <a
								href="#" class="btn grd1"><i class="fa fa-linkedin"></i></a>
						</div>
					</div>
					<!--widget -->
				</div>
				<!-- end col -->

				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 wow fadeIn"
					data-wow-duration="1s" data-wow-delay="0.4s"
					style="visibility: visible; animation-duration: 1s; animation-delay: 0.4s; animation-name: fadeIn;">
					<div class="widget clearfix">
						<img src="./img/doctor_02.jpg" alt=""
							class="img-responsive img-rounded">
						<div class="widget-title">
							<h3>Bryan Saftler</h3>
							<small>Internal Diseases</small>
						</div>
						<!-- end title -->
						<p>Hello guys, I am Soren from Sirbistana. I am senior art
							director and founder of Violetta.</p>

						<div class="footer-social">
							<a href="#" class="btn grd1"><i class="fa fa-facebook"></i></a> <a
								href="#" class="btn grd1"><i class="fa fa-github"></i></a> <a
								href="#" class="btn grd1"><i class="fa fa-twitter"></i></a> <a
								href="#" class="btn grd1"><i class="fa fa-linkedin"></i></a>
						</div>
					</div>
					<!--widget -->
				</div>
				<!-- end col -->

				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 wow fadeIn">
					<div class="widget clearfix">
						<img src="./img/doctor_03.jpg" alt=""
							class="img-responsive img-rounded">
						<div class="widget-title">
							<h3>Matthew Bayliss</h3>
							<small>Orthopedics Expert</small>
						</div>
						<!-- end title -->
						<p>Hello guys, I am Soren from Sirbistana. I am senior art
							director and founder of Violetta.</p>

						<div class="footer-social">
							<a href="#" class="btn grd1"><i class="fa fa-facebook"></i></a> <a
								href="#" class="btn grd1"><i class="fa fa-github"></i></a> <a
								href="#" class="btn grd1"><i class="fa fa-twitter"></i></a> <a
								href="#" class="btn grd1"><i class="fa fa-linkedin"></i></a>
						</div>
					</div>
					<!--widget -->
				</div>
				<!-- end col -->

			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>


	<div id="price" class="section pr wow fadeIn"
		style="background-image: url('./img/price-bg.png');">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="tab-content">
						<div class="tab-pane active fade in" id="tab1">
							<div class="row text-center">

								<div class="col-md-6">
									<div class="pricing-table pricing-table-highlighted">
										<div class="pricing-table-header grd1">
											<h2>แพ็กเกจ1</h2>
											<h3>$59/year</h3>
										</div>
										<div class="pricing-table-space"></div>
										<div class="pricing-table-text">
											<p>This is a perfect choice for small businesses and
												startups.</p>
										</div>
										<div class="pricing-table-features">
											<p>
												<i class="fa fa-envelope-o"></i> <strong>150</strong> Email
												Addresses
											</p>
											<p>
												<i class="fa fa-rocket"></i> <strong>65GB</strong> of
												Storage
											</p>
											<p>
												<i class="fa fa-database"></i> <strong>60</strong> Databases
											</p>
											<p>
												<i class="fa fa-link"></i> <strong>30</strong> Domains
											</p>
											<p>
												<i class="fa fa-life-ring"></i> <strong>24/7
													Unlimited</strong> Support
											</p>
										</div>
										<div class="pricing-table-sign-up">
											<a href="#contact" data-scroll=""
												class="btn btn-light btn-radius btn-brd grd1 effect-1">สมัครแพ็กเกจ</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="pricing-table pricing-table-highlighted">
										<div class="pricing-table-header grd1">
											<h2>แพ็กเกจ2</h2>
											<h3>$59/year</h3>
										</div>
										<div class="pricing-table-space"></div>
										<div class="pricing-table-text">
											<p>This is a perfect choice for small businesses and
												startups.</p>
										</div>
										<div class="pricing-table-features">
											<p>
												<i class="fa fa-envelope-o"></i> <strong>150</strong> Email
												Addresses
											</p>
											<p>
												<i class="fa fa-rocket"></i> <strong>65GB</strong> of
												Storage
											</p>
											<p>
												<i class="fa fa-database"></i> <strong>60</strong> Databases
											</p>
											<p>
												<i class="fa fa-link"></i> <strong>30</strong> Domains
											</p>
											<p>
												<i class="fa fa-life-ring"></i> <strong>24/7
													Unlimited</strong> Support
											</p>
										</div>
										<div class="pricing-table-sign-up">
											<a href="#contact" data-scroll=""
												class="btn btn-light btn-radius btn-brd grd1 effect-1">สมัครแพ็กเกจ</a>
										</div>
									</div>
								</div>
							</div>
							<!-- end row -->
						</div>
						<!-- end pane -->
						<div class="tab-pane fade" id="tab2">
							<div class="row text-center">
								<div class="col-md-6">
									<div class="pricing-table">
										<div class="pricing-table-header">
											<h2>Dedicated Server</h2>
											<h3>$85/month</h3>
										</div>
										<div class="pricing-table-space"></div>
										<div class="pricing-table-features">
											<p>
												<i class="fa fa-envelope-o"></i> <strong>250</strong> Email
												Addresses
											</p>
											<p>
												<i class="fa fa-rocket"></i> <strong>125GB</strong> of
												Storage
											</p>
											<p>
												<i class="fa fa-database"></i> <strong>140</strong>
												Databases
											</p>
											<p>
												<i class="fa fa-link"></i> <strong>60</strong> Domains
											</p>
											<p>
												<i class="fa fa-life-ring"></i> <strong>24/7
													Unlimited</strong> Support
											</p>
										</div>
										<div class="pricing-table-sign-up">
											<a href="#contact" data-scroll=""
												class="btn btn-dark btn-radius btn-brd">Order Now</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="pricing-table pricing-table-highlighted">
										<div class="pricing-table-header grd1">
											<h2>VPS Server</h2>
											<h3>$59/month</h3>
										</div>
										<div class="pricing-table-space"></div>
										<div class="pricing-table-text">
											<p>This is a perfect choice for small businesses and
												startups.</p>
										</div>
										<div class="pricing-table-features">
											<p>
												<i class="fa fa-envelope-o"></i> <strong>150</strong> Email
												Addresses
											</p>
											<p>
												<i class="fa fa-rocket"></i> <strong>65GB</strong> of
												Storage
											</p>
											<p>
												<i class="fa fa-database"></i> <strong>60</strong> Databases
											</p>
											<p>
												<i class="fa fa-link"></i> <strong>30</strong> Domains
											</p>
											<p>
												<i class="fa fa-life-ring"></i> <strong>24/7
													Unlimited</strong> Support
											</p>
										</div>
										<div class="pricing-table-sign-up">
											<a href="#contact" data-scroll=""
												class="btn btn-light btn-radius btn-brd grd1 effect-1">Order
												Now</a>
										</div>
									</div>
								</div>
							</div>
							<!-- end row -->
						</div>
						<!-- end pane -->
					</div>
					<!-- end content -->
				</div>
				<!-- end col -->
			</div>
		</div>
	</div>

	<!-- end doctor section -->


	<div id="getintouch" class="section wb wow fadeIn"
		style="padding-bottom: 0;">
		<div class="container">
			<div class="heading">
				<span class="icon-logo"><img src="./img/icon-logo.png"
					alt="#"></span>
				<h2>Get in Touch</h2>
			</div>
		</div>
		<div class="contact-section">
			<div class="form-contant">
				<form id="ajax-contact" action="assets/mailer.php" method="post">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group in_name">
								<input type="text" class="form-control" placeholder="Name"
									required="required">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group in_email">
								<input type="email" class="form-control" placeholder="E-mail"
									required="required">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group in_email">
								<input type="tel" class="form-control" id="phone"
									placeholder="Phone" required="required">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group in_email">
								<input type="text" class="form-control" id="subject"
									placeholder="Subject" required="required">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group in_message">
								<textarea class="form-control" id="message" rows="5"
									placeholder="Message" required="required"></textarea>
							</div>
							<div class="actions">
								<input type="submit" value="Send Message" name="submit"
									id="submitButton" class="btn small"
									title="Submit Your Message!">
							</div>
						</div>
					</div>
				</form>
			</div>
			<div id="googleMap" style="width: 100%; height: 450px;"></div>
		</div>
	</div>
	
<jsp:include page="navbar/footer.jsp" />
    
</body>
</html>