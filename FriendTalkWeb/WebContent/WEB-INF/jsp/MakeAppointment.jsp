<%@ page language="java" contentType="text/html; charset=UTF-8" 
	pageEncoding="UTF-8"%>
<%@ page import="bean.*, java.util.*,java.text.*"%>
	<%
		String user = (String) session.getAttribute("user");
		Psychologist psy =(Psychologist) request.getAttribute("psy");
		String maxid = (String) request.getAttribute("maxid");
		List<Price> list_price = (List<Price>)request.getAttribute("list_Price");		
		List<MakeAppointment> listtimeapp = (List<MakeAppointment>)request.getAttribute("listtimeapp");
		List<SetWorkingTime> listswt = (List<SetWorkingTime>) request.getAttribute("listswt");
		
		List<SetWorkingTime> Twork = (List<SetWorkingTime>)request.getAttribute("Twork");
		String dayS = (String) request.getAttribute("day");
		String time = (String) request.getAttribute("minute");
		String appdate = (String) request.getAttribute("AppDate");
		String contact = (String) request.getAttribute("contact");
		
		String datee = "" ;
		int h = 0;
		int m = 0;
		int s = 0;

	%>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<!-- Site Metas -->
<title>Friends Talk</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="./img/icon-ft.png" image/x-icon" />
<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="./css/style.css">
<!-- Colors CSS -->
<link rel="stylesheet" href="./css/colors.css">
<!-- ALL VERSION CSS -->
<link rel="stylesheet" href="./css/versions.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="./css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="./css/custom.css">
<!-- Modernizer for Portfolio -->
<script src="./js/modernizer.js"></script>
<!-- [if lt IE 9] -->
     <script src="https://unpkg.com/jquery@3.3.1/dist/jquery.min.js"></script>
	 <link rel="stylesheet" href="//apps.bdimg.com/libs/jqueryui/1.10.4/css/jquery-ui.min.css">
   	 <script src="//apps.bdimg.com/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
  	 <link rel="stylesheet" href="jqueryui/style.css">
  	 <script src="./ja/locales/bootstrap-datepicker.th.js"></script>
  	 
  	 
  	 	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
	
  	  <script src="./js/JSMakeApp.js"></script>
  	 
<style type="text/css">
.btnlike{
  display : none;
}

input.btnlike + span{
  background-color : #00BFFF;
  color : #000000;
  display : block;
  height : 30px;
  width : 60px;
  margin : 20px;
  text-align: center;
}

input.btnlike:checked + span{
  background-color : #E0FFFF;
  color : #000000;
}
.date{
	display: inline;
}
input.btnlike:disabled + span{
	background-color : #778899;
}
</style>
</head>
<body class="clinic_version">
	<jsp:include page="navbar/headertop.jsp" />
	
	<div class="header-bottom wow fadeIn">
		<div class="container">
			<nav class="main-menu">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</button>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a class="active" href="/WebService/openHome">หน้าแรก</a></li>
					</ul>
				</div>
			</nav>
			<div class="serch-bar">
				<div id="custom-search-input">
					<div class="input-group col-md-12">
						<input type="text" class="form-control input-lg"
							placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
				
	<div id="service" class="services2 wow fadeIn">
	    <div class="container">
	       <div class="row center">
	          <div class="col-lg-9 col-md-4 col-sm-6 col-xs-9">
	             <div class="appointment-form">
	                <h3>ทำนัด</h3>
	                 <div class="form">
	                 <form action="/WebService/openTime" method="POST">
	                 		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									<label class="col-sm-6 control-label">ข้อมูลของผู้ให้คำปรึกษา</label>
									<div class="form-group">
										<table>
											<tr>
												<td><label>ชื่อ : <%= psy.getTitle() + " "+ psy.getFristname() +" "+ psy.getLastname() %></label>
												<br>
												<%if(psy.getMentor_type().equals("psychiatrist")){ %>
													<label style="color: blue;">ต่ำแหน่ง : จิตแพทย์</label><br>
												<%}else{ %>
													<label style="color: ;">ต่ำแหน่ง : นักจิตวิทยา</label><br>
												<% } %>
												<label>การศึกษา : <%= psy.getWork_history()%></label><br>
													<label>เวลาในการทำงาน</label>
													<br>
													<% if(listswt.size() >=1){
														for(int i=0 ; i<listswt.size() ; i++){ 
															if(listswt.get(i).getPwkey().getPsychologist().getUsername().equals(psy.getUsername())){%>
														
																<label><ul><li><%=listswt.get(i).getPwkey().getWorking_date() %></li></ul></label>
																<label><%= listswt.get(i).getTime_start() %></label>
																<label><%= listswt.get(i).getTime_end() %></label><br>
													<%}}} %>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
	                 		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									<label class="col-sm-6 control-label">เลือกวันนัดหมาย</label>
									<div class="form-group">
										<% if(appdate == null) {%>
											<input type="text" class="datepicker" id="datepicker" name="appointment_date" >
										<%}else{ %>
											<input type="text" class="datepicker" id="datepicker" name="appointment_date" value="<%= appdate %>">
										<% } %>
									</div>
								</div>
							</div>
							<script type="text/javascript">
								$(".datepicker").datepicker({
									dateFormat : 'dd-mm-yy',
									altField : "#alternate",
									altFormat : "DD",
									sideBySide : true,
									minDate : +0,
									language : "th-TH",
									autoclose : true
								});
							</script>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="row" id="time">
									<div class="form-group">
											<input type="hidden" name="appid" value="<%= maxid %>">
											<input type="hidden" name="psyid" value="<%= psy.getUsername() %>">
											<input type="hidden" name="conid" value="<%= user %>">
											<%if(dayS == null){ %>
												<input type="hidden" name="selectday" id="alternate" > 
											<%}else{ %>
												<input type="hidden" name="selectday" id="alternate" value="<%= dayS %>"> 
											<% } %>
											<% for(int i=0 ; i<list_price.size() ; i++){ %>
												<div class="col-md-6">
													<input type="submit" id="submitButton" class="form-control-submit-button" name="butt" 
													value="<%= list_price.get(i).getContact_type() %>">
												</div>
											<% } %>
									</div>
								</div>
							</div>
						</form>

						<form name="frm" id="myformArti" action="/WebService/MakeAppointment" method="POST" onsubmit="return myFunction();"  novalidate>	                
	                     	<fieldset>
								<input type="hidden" name="cancel_appointment" value="0">
								<input type="hidden" name="status_appoint" value="รอการยืนยัน">
 							
<!-- 							Select Time  -->
	                         	 <div class="container">
									<div class="row">
									<% 
										if(time != null){
											for(int j=0; j<listtimeapp.size() ; j++){
												//เวลานัด
													 h = listtimeapp.get(j).getTime_start().getTime().getHours();
													 m = listtimeapp.get(j).getTime_start().getTime().getMinutes();
													 s = listtimeapp.get(j).getTime_start().getTime().getSeconds();
													
													String time1 = String.valueOf(h)+":"+String.valueOf(m)+":"+String.valueOf(s);
												    DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
												    Date date = sdf.parse(time1);
												    
												    System.out.println("Time  = "+date.toString() );
												    
												    String subtime = date.toString() ;
												    String strT1 = subtime.substring(10);
												    String strT2 = strT1.substring(1, 9);
													//วันที่นัด
												    String ds = listtimeapp.get(j).getTime_start().toInstant().toString().substring(0, 10);
													String[] d = ds.split("-");
												    datee = d[2]+"-"+d[1]+"-"+d[0] ;
												    
												    System.out.println(listtimeapp.get(j).getAppointment_id()+" = "+ds );
												    System.out.println("check Time  = "+strT2 );
												    System.out.println("Date  = "+ datee + "appdate" + appdate );
												    String checkdate2 = ds +" "+ strT2;
												    System.out.println("check date time = "+ds +" "+ strT2 );
												    System.out.println("check = "+checkdate2 );
												}
											boolean l = true ;
											String[] d2 = appdate.split("-");
											String[] d1 = {""} ;
											String[] d3 = {""} ;
											if(listtimeapp.size() > 0){
												for(int k=0 ; k<listtimeapp.size() ; k++){
													String status = listtimeapp.get(k).getStatus_appoint();
													if(!status.equals("สำเร็จ")){
													String ds = listtimeapp.get(k).getTime_start().toInstant().toString().substring(0, 10);
													d3 = ds.split("-");
													System.out.println("Check d2 = "+d2[0]+" d3 = "+d3[2]);
														if(d2[0].equals(d3[2])){
															d1 = ds.split("-");
															System.out.println("d2 = "+d2[0]+" d1 = "+d1[2]);
															if(d1[2].equals(d2[0])){
																h = listtimeapp.get(k).getTime_start().getTime().getHours();
																m = listtimeapp.get(k).getTime_start().getTime().getMinutes();
																l = false ;
																System.out.println("l = "+ l); 
																break ;
															}
														}

													}
												}
												if(l){
													String ds = "00-00-0000";
													d1 = ds.split("-"); 
												}
											}else{
												String ds = "00-00-0000";
												d1 = ds.split("-"); 
											}
									
									%>
										<input type="hidden" name="appid" value="<%= maxid %>">
										<input type="hidden" name="psyid" value="<%= psy.getUsername() %>">
										<input type="hidden" name="appdate" value="<%= appdate %>">
										<input type="hidden" name="contact" value="<%= contact %>">
										<input type="hidden" name="conid" value="<%= user %>">
										
									<% if(time.equals("30")){ %>
											
	                               		<div class="col-md-6 float-left">
	                               			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					                            <div class="row">
					                                <label class="col-sm-6 control-label">ต้องการปิดบังตัวตนหรือไม่</label>
					                               		<input type="checkbox" name="anonymous" id="anonymous" value="1" >
								               </div>
					                         </div>
					                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					                            <div class="row">
					                                <label class="col-sm-6 control-label">รับการแจ้งเตือน </label>
					                               		<input type="checkbox" name="notify" id="notify" value="1">
					                             </div>
					                         </div>
					                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					                            <div class="row">
<!-- 					                                <label class="col-sm-6 control-label">ราคา</label> -->
					                                	<%
					                                	System.out.println("Contact = "+contact);
					                                	if(contact.equals("call 30")){ %>
					                                		<h3>ราคา : 400</h3>
					                                	<%}else{ %>
					                                		<h3>ราคา : 200</h3>
					                                	<% } %>
					                             </div>
					                         </div>
					                         <p class="col-sm-6 control-label" id="demo1" style="color: red; font-size: 16px"></p>
											<%for(int i=0 ; i < Twork.size() ; i++){ 

													if(!Twork.get(i).getTime_start().equals("") || !Twork.get(i).getTime_end().equals("")){
													String HS = Twork.get(i).getTime_start().substring(0, 2);
													String MS = Twork.get(i).getTime_start().substring(3);
													String HE = Twork.get(i).getTime_end().substring(0, 2);
													String ME = Twork.get(i).getTime_start().substring(3);
													
													int Hrs = Integer.parseInt(HS); 
													int Mins = Integer.parseInt(MS);
													int timeEnd = Integer.parseInt(HE); 

													if(Twork.get(i).getPwkey().getWorking_date().equals(dayS)){ 
														int sumMS = Mins; int sumHS = Hrs; 
													%>
													<input type="hidden" name="day" value="<%=Twork.get(i).getPwkey().getWorking_date().toString() %>">
														<label><%= Twork.get(i).getPwkey().getWorking_date().toString() %></label>
														<br>
													
												<% do{ 
														if(sumMS / 60 == 1.5){
															sumHS = sumHS + 1 ;
															sumMS = 30 ;
															
															int hs = 0 ;
															int ms = 0 ;
															if(d1[2].equals(d2[0])){
																for(int n=0 ; n<listtimeapp.size(); n++){
																	if(!listtimeapp.get(n).getStatus_appoint().equals("สำเร็จ")){
																	String ds = listtimeapp.get(n).getTime_start().toInstant().toString().substring(0, 10);
																	d3 = ds.split("-");
																	System.out.println("Check d2 = "+d2[0]+" d3 = "+d3[2]);
																		if(d2[0].equals(d3[2])){
																			d1 = ds.split("-");
																			System.out.println("d2 = "+d2[0]+" d1 = "+d1[2]);
																			if(!listtimeapp.get(n).getStatus_appoint().equals("สำเร็จ")){
																				hs = listtimeapp.get(n).getTime_start().getTime().getHours();
																				ms = listtimeapp.get(n).getTime_start().getTime().getMinutes();
																				if(hs ==  sumHS){
																					if(ms == sumMS){
																						break;
																					}
																					break;
																				}
																			}
																		}
																	}
																}
																if(h ==  sumHS){	
												%>			
															<tr>
																<td style="background-color:rad;"><label ><input class="btnlike" type="radio" name="timeS" value="<%=sumHS+":"+sumMS%>" disabled="disabled" ><span ><%=sumHS+":"+sumMS%></span></label></td>
															</tr>
														<% }else{ %>
															<tr>
																<td><label><input class="btnlike" type="radio" name="timeS" id="minute" value="<%=sumHS+":"+sumMS%>"><span><%=sumHS+":"+sumMS%></span></label></td>
															</tr>
														<% } %>	
													<%}else{%>
														<tr>
															<td><label><input class="btnlike" type="radio" name="timeS" id="minute" value="<%=sumHS+":"+sumMS%>"><span><%=sumHS+":"+sumMS%></span></label></td>
														</tr>
													<%} 
													}else if(sumMS / 60 == 1){
														sumHS = sumHS + 1 ;
														sumMS = 0 ;
														
														int hs = 0 ;
														int ms = 0 ;
														if(d1[2].equals(d2[0])){
															for(int n=0 ; n<listtimeapp.size(); n++){
																if(!listtimeapp.get(n).getStatus_appoint().equals("สำเร็จ")){
																String ds = listtimeapp.get(n).getTime_start().toInstant().toString().substring(0, 10);
																d3 = ds.split("-");
																System.out.println("Check d2 = "+d2[0]+" d3 = "+d3[2]);
																	if(d2[0].equals(d3[2])){
																		d1 = ds.split("-");
																		System.out.println("d2 = "+d2[0]+" d1 = "+d1[2]);
																		if(!listtimeapp.get(n).getStatus_appoint().equals("สำเร็จ")){
																			hs = listtimeapp.get(n).getTime_start().getTime().getHours();
																			ms = listtimeapp.get(n).getTime_start().getTime().getMinutes();
																			if(hs ==  sumHS){
																				if(ms == sumMS){
																					break;
																				}
																				break;
																			}
																		}
																	}
																}
															}
															if(hs ==  sumHS ){		
													%>
															<tr>
																<td ><label ><input class="btnlike" type="radio" name="timeS" value="<%=sumHS+":00"%>" disabled="disabled" ><span style="background-color:rad;"><%=sumHS+":00"%></span></label></td>
															</tr>
														<% }else{ %>
															<tr>
																<td><label><input class="btnlike" type="radio" name="timeS" id="minute" value="<%=sumHS+":00"%>"><span><%=sumHS+":00"%></span></label></td>
															</tr>
														<%} }else{%>
														<tr>
															<td><label><input class="btnlike" type="radio" name="timeS" id="minute" value="<%=sumHS+":00"%>"><span><%=sumHS+":00"%></span></label></td>
														</tr>
													<% }
													}else{
														if(sumMS == 0){%>
														<tr>
															<td><label><input class="btnlike" type="radio" name="timeS" id="minute" value="<%=sumHS+":00"%>"><span><%=sumHS+":00"%></span></label></td>
														</tr>
													<%}else{%>
														<tr>
															<td><label><input class="btnlike" type="radio" name="timeS" id="minute" value="<%=sumHS+":"+sumMS%>"><span><%=sumHS+":"+sumMS%></span></label></td>
														</tr>
													<%}}
												sumMS = sumMS + 30 ;
												}while(timeEnd != sumHS);  
											  }
											}
											}%><!-- do while if=day if!="" for -->
										</div>
										<!-- ***************************** Time 60 ***************************** -->
										<%}else if(time.equals("60")){ %>
										<div class="col-md-6 float-left">	
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					                            <div class="row">
					                                <label class="col-sm-6 control-label">ต้องการปิดบังตัวตนหรือไม่</label>
					                               		<input type="checkbox" name="anonymous" id="anonymous" value="0" >
								               </div>
					                         </div>
					                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					                            <div class="row">
					                                <label class="col-sm-6 control-label">รับการแจ้งเตือน </label>
					                               		<input type="checkbox" name="notify" id="notify" value="0">
					                             </div>
					                         </div>
					                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					                            <div class="row">
					                                	<%
					                                	System.out.println("Contact = "+contact);
					                                	if(contact.equals("call 60")){ %>
					                                		<h3>ราคา : 800</h3>
					                                	<%}else{ %>
					                                		<h3>ราคา : 400</h3>
					                                	<% } %>
					                             </div>
					                         </div>
					                         <p class="col-sm-6 control-label" id="demo1" style="color: red; font-size: 16px"></p>
											<%for(int i=0 ; i < Twork.size() ; i++){ 
												if(!Twork.get(i).getTime_start().equals("") || !Twork.get(i).getTime_end().equals("")){
												System.out.println(Twork.size());
												String HS = Twork.get(i).getTime_start().substring(0, 2);
												String MS = Twork.get(i).getTime_start().substring(3);
												String HE = Twork.get(i).getTime_end().substring(0, 2);
												String ME = Twork.get(i).getTime_start().substring(3);
												
												int Hrs = Integer.parseInt(HS); 
												int Mins = Integer.parseInt(MS);
												int timeEnd = Integer.parseInt(HE); 
											
												if(Twork.get(i).getPwkey().getWorking_date().equals(dayS)){ 
													double sumMS = Mins; int sumHS = Hrs; 
											%>
												<input type="hidden" name="dayapp" value="<%=Twork.get(i).getPwkey().getWorking_date().toString() %>">
												<p><%= Twork.get(i).getPwkey().getWorking_date().toString() %></p>
											<%do{ 
											 
												if(sumMS / 60 == 1.5){
															sumHS = sumHS + 1 ;
															sumMS = 30 ;
															
															int hs = 0 ;
															int ms = 0 ;
															if(d1[2].equals(d2[0])){
																for(int n=0 ; n<listtimeapp.size(); n++){
																	if(!listtimeapp.get(n).getStatus_appoint().equals("สำเร็จ")){
																	String ds = listtimeapp.get(n).getTime_start().toInstant().toString().substring(0, 10);
																	d3 = ds.split("-");
																	System.out.println("Check d2 = "+d2[0]+" d3 = "+d3[2]);
																		if(d2[0].equals(d3[2])){
																			d1 = ds.split("-");
																			System.out.println("d2 = "+d2[0]+" d1 = "+d1[2]);
																			if(!listtimeapp.get(n).getStatus_appoint().equals("สำเร็จ")){
																				hs = listtimeapp.get(n).getTime_start().getTime().getHours();
																				ms = listtimeapp.get(n).getTime_start().getTime().getMinutes();
																				if(hs ==  sumHS){
																					if(ms == sumMS){
																						break;
																					}
																					break;
																				}
																			}
																		}
																	}
																}
															if(h ==  sumHS && m == sumMS){		
													%>
															<tr>
																<td ><label ><input class="btnlike" type="radio" name="timeS" id="minute" value="<%=sumHS+":"+sumMS%>" disabled="disabled" ><span style="background-color:rad;"><%=sumHS+":"+sumMS%></span></label></td>
															</tr>
														<% }else{ %>
															<tr>
																<td><label><input class="btnlike" type="radio" name="timeS" id="minute" value="<%=sumHS+":"+sumMS%>"><span><%=sumHS+":"+sumMS%></span></label></td>
															</tr>
														<% } %>	
													<%}else{%>
														<tr>
															<td><label><input class="btnlike" type="radio" name="timeS" id="minute" value="<%=sumHS+":"+sumMS%>"><span><%=sumHS+":"+sumMS%></span></label></td>
														</tr>
													<%}}else if(sumMS / 60 == 1){
														sumHS = sumHS + 1 ;
														sumMS = 0 ;
														int hs = 0 ;
														int ms = 0 ;
														if(d1[2].equals(d2[0])){
															for(int n=0 ; n<listtimeapp.size(); n++){
																if(!listtimeapp.get(n).getStatus_appoint().equals("สำเร็จ")){
																String ds = listtimeapp.get(n).getTime_start().toInstant().toString().substring(0, 10);
																d3 = ds.split("-");
																System.out.println("Check d2 = "+d2[0]+" d3 = "+d3[2]);
																	if(d2[0].equals(d3[2])){
																		d1 = ds.split("-");
																		System.out.println("d2 = "+d2[0]+" d1 = "+d1[2]);
																		if(!listtimeapp.get(n).getStatus_appoint().equals("สำเร็จ")){
																			hs = listtimeapp.get(n).getTime_start().getTime().getHours();
																			ms = listtimeapp.get(n).getTime_start().getTime().getMinutes();
																			if(hs ==  sumHS){
																				if(ms == sumMS){
																					break;
																				}
																				break;
																			}
																		}
																	}
																}
															}
															if(hs ==  sumHS && ms == sumMS){		
													%>
															<tr>
																<td ><label ><input class="btnlike" type="radio" name="timeS" value="<%=sumHS+":00"%>" disabled="disabled" ><span style="background-color:rad;"><%=sumHS+":00"%></span></label></td>
															</tr>
														<% }else{ %>
															<tr>
																<td><label><input class="btnlike" type="radio" name="timeS" id="minute" value="<%=sumHS+":00"%>"><span><%=sumHS+":00"%></span></label></td>
															</tr>
														<% } %>	
													<%}else{%>
														<tr>
															<td><label><input class="btnlike" type="radio" name="timeS" id="minute" value="<%=sumHS+":00"%>"><span><%=sumHS+":00"%></span></label></td>
														</tr>
													<% }
													}else{
														if(sumMS == 0){%>
														<tr>
															<td><label><input class="btnlike" type="radio" name="timeS" id="minute" value="<%=sumHS+":00"%>"><span><%=sumHS+":00"%></span></label></td>
														</tr>
													<%}else{%>
														<tr>
															<td><label><input class="btnlike" type="radio" name="timeS" id="minute" value="<%=sumHS+":"+sumMS%>"><span><%=sumHS+":"+sumMS%></span></label></td>
														</tr>
													<%}}
													sumMS = sumMS + 60 ;
													}while(timeEnd > sumHS); 
												   } 
												}
											 } %><!--  if while if=day if!="" for-->
										 
									<%	} %><!-- if == 60 -->
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				                            <div class="row">
				                               <div class="form-group">
				                                  <div class="col-lg-4" style="align-items: center;">
				                                      <input type="submit" name="ทำนัดหมาย" id="submitButton" 
				                                       class="form-control-submit-button" title="ทำนัดหมาย" value="ทำนัดหมาย">
				                                  </div>
				                               </div>
				                            </div>
				                         </div>
								<% } %> <!-- if == null -->
	                               	</div>
	                            </div>
	                         </div>
	                       </fieldset>
	                   </form>
	                </div>
	             </div>
	          </div>
	       </div>
	    </div>
	 </div>	
	 	<jsp:include page="navbar/footer.jsp" />

</body>
</html>