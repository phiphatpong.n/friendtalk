$(function() {
    $("#frmPC").on("submit",function(){
    var message_init = '<div class="alert alert-danger fade in"><span class="glyphicon glyphicon-triangle-top"></span><a href="#" class="close" data-dismiss="alert">×</a>';
    var message_ends = '</div>';

    var fcall_minute = $('#call_minute').val();
    if (fcall_minute >= 30 && fcall_minute <=60) { 
      var message = message_init + 'กรุณากรอกเวลา 30 และ  60 นาที เท่านนั้น' + message_ends;
      $('#call_minute').focus();
      if($('input#call_minute + div.alert.alert-danger').length == 0) {
        $('#call_minute').after(message);
      }
      return false;
    }else{
      $('input#award_name + div.alert.alert-danger').remove();
    }
    var fchat_minute = $('#chat_minute').val();
    if (fchat_minute >= 30 && fchat_minute <=60){ 
        var message = message_init + 'กรุณากรอกเวลา 30 และ  60 นาที เท่านนั้น' + message_ends;
        $('#chat_minute').focus();
        if($('input#chat_minute + div.alert.alert-danger').length == 0) {
            $('#chat_minute').after(message);
        }
        return false;
    }else{
        $('input#chat_minute + div.alert.alert-danger').remove();
    }
  

  return true;

    });

});