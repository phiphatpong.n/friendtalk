function myFunction() {
    const t1 = document.getElementById('title').checked  == false;
    const t2 = document.getElementById('title1').checked  == false;
    const t3 = document.getElementById('title2').checked  == false;
    
    //(document.getElementById('title').checked  == false && document.getElementById('title1').checked  == false) && document.getElementById('title2').checked  == false
    if ((t1&&t2)&&t3) {
      document.getElementById("demo").innerHTML = "*กรุณาระบุคำนำหน้า";
      return false;
    } else {
      document.getElementById("demo").innerHTML = "";

    } 
    const fname = document.getElementById("fname")
    if (!fname.checkValidity()) {
      document.getElementById("demo1").innerHTML = "*กรุณากรอกชื่อ เป็นภาษาไทยหรือภาษาอังกฤษ";
      return false;
    } else {
      document.getElementById("demo1").innerHTML = "";

    } 
    const lname = document.getElementById("lname");
    if (!lname.checkValidity()) {
      document.getElementById("demo2").innerHTML = "*กรุณากรอกนามสกุล เป็นภาษาไทยหรือภาษาอังกฤษ";
      return false;
    } else {
      document.getElementById("demo2").innerHTML = "";

    } 
    const g1 = document.getElementById('gender').checked  == false;
    const g2 = document.getElementById('gender2').checked  == false;
    if (g1&&g2) {
      document.getElementById("demo3").innerHTML = "*กรุณาเลือกเพศ";
      return false;
    } else {
      document.getElementById("demo3").innerHTML = "";

    } 
    const birthday = document.getElementById('birthday');
    if (birthday == "") {
      document.getElementById("demo4").innerHTML = "*กรุณากรอกวันเดือนปีเกิด";
      return false;
    } else {
      document.getElementById("demo4").innerHTML = "";
    } 
    const email = document.getElementById('email');
    if (!email.checkValidity()) {
      document.getElementById("demo5").innerHTML = "*กรุณากรอกอีเมลให้ถูกต้อง";
      return false;
    } else {
      document.getElementById("demo5").innerHTML = "";

    } 
    const tel = document.getElementById('tel');
    if (!tel.checkValidity()) {
      document.getElementById("demo6").innerHTML = "*กรุณากรอกเบอร์มือถือตัวเลข 10 หลัก";
      return false;
    } else {
      document.getElementById("demo6").innerHTML = "";

    } 
    if (document.getElementById('address').value == "" ){
      document.getElementById("demo7").innerHTML = "*กรุณากรอกที่อยู่ เป็นภาษาไทยหรือภาษาอังกฤษ";
      return false;
    } else {
      document.getElementById("demo7").innerHTML = "";

    } 
    if (document.getElementById('educationl').value == "" ) {
      document.getElementById("demo8").innerHTML = "*กรุณากรอกประวัติการศึกษา เป็นภาษาไทยหรือภาษาอังกฤษ ";
      return false;
    } else {
      document.getElementById("demo8").innerHTML = "";

    } 
    if (document.getElementById('working').value == "" ) {
      document.getElementById("demo9").innerHTML = "*กรุณากรอกประวัติการทำงาน เป็นภาษาไทยหรือภาษาอังกฤษ";
      return false;
    } else {
      document.getElementById("demo9").innerHTML = "";

    } 
    const mentortype = document.getElementById('mentortype').value  == "0"  
    if (!mentortype.checkValidity()) {
      document.getElementById("demo10").innerHTML = "*กรุณาเลือกเลือกประเภท";
      return false;
    } else {
      document.getElementById("demo10").innerHTML = "";

    } 
    const uname = document.getElementById('uname');
    if (!uname.checkValidity()) {
      document.getElementById("demo11").innerHTML = "*กรุณากรอกชื่อผู้ใช้เป็นภาษาอังกฤษ";
      return false;
    } else {
      document.getElementById("demo11").innerHTML = "";

    } 
    const pwd = document.getElementById('pwd');
    if (pwd == "") {
      document.getElementById("demo12").innerHTML = "*กรุณากรอกรหัสผ่าน";
      return false;
    } else {
      document.getElementById("demo12").innerHTML = "";

    } 
    const customFile = document.getElementById('customFile');
    if (customFile == "") {
      document.getElementById("demo13").innerHTML = "*กรุณาอัพโหลดรูปภาพโปรไฟล์";
      return false;
    } else {
      document.getElementById("demo13").innerHTML = "";

    } 
    const customFile2 = document.getElementById('customFile2');
    if (customFile2 == "") {
      document.getElementById("demo14").innerHTML = "*กรุณาอัพโหลดไฟล์";
      return false;
    } else {
      document.getElementById("demo14").innerHTML = "";

    } 
  }