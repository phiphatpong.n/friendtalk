function myfunction() {


    const award_name = document.getElementById("award_name")
    if (!award_name.checkValidity()) {
      document.getElementById("demo1").innerHTML = "*กรุณากรอกชื่อของรางวัล เป็นภาษาไทยหรือภาษาอังกฤษ";
      return false;
    } else {
      document.getElementById("demo1").innerHTML = "";

    } 
    const award_detail = document.getElementById("award_detail")
    if (award_detail == "") {
      document.getElementById("demo4").innerHTML = "*กรุณากรอกรายละเอียดของรางวัล เป็นภาษาไทยหรือภาษาอังกฤษ";
      return false;
    } else {
      document.getElementById("demo4").innerHTML = "";

    } 
    const score = document.getElementById("score")
    if (score == "") {
      document.getElementById("demo2").innerHTML = "*กรุณากรอกจำนวนคะแนน";
      return false;
    } else {
      document.getElementById("demo2").innerHTML = "";

    } 
    var Available_from = $('#Available_from').val();
    if (Available_from == "") { 
        var message = message_init + 'กรุณาเลือกวันที่' + message_ends;
        $('#Dateadded').focus();
        if($('input#Dateadded + div.alert.alert-danger').length == 0) {
            $('#Dateadded').after(message);
        }
        return false;
    }else{
        $('input#birthday + div.alert.alert-danger').remove();
    }
    const customFile = document.getElementById("customFile")
    if (!customFile.checkValidity()) {
      document.getElementById("demo3").innerHTML = "*กรุณาอัพโหลดรูปภาพ";
      return false;
    } else {
      document.getElementById("demo3").innerHTML = "";

    } 

}