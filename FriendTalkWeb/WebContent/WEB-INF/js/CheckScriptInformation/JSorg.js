function myFunction() {
    const cname = document.getElementById("cname")
    if (!cname.checkValidity()) {
      document.getElementById("demo1").innerHTML = "*กรุณากรอกชื่อบริษัท เป็นภาษาไทยหรือภาษาอังกฤษ";
      return false;
    } else {
      document.getElementById("demo1").innerHTML = "";

    } 
    const email = document.getElementById('email');
    if (!email.checkValidity()) {
      document.getElementById("demo2").innerHTML = "*กรุณากรอกอีเมลให้ถูกต้อง";
      return false;
    } else {
      document.getElementById("demo2").innerHTML = "";

    } 
    const tel = document.getElementById('tel');
    if (!tel.checkValidity()) {
      document.getElementById("demo3").innerHTML = "*กรุณากรอกเบอร์มือถือตัวเลข 10 หลัก";
      return false;
    } else {
      document.getElementById("demo3").innerHTML = "";

    } 
    if (document.getElementById('address').value == ""){
      document.getElementById("dem4").innerHTML = "*กรุณากรอกที่อยู่ เป็นภาษาไทยหรือภาษาอังกฤษ";
      return false;
    } else {
      document.getElementById("demo4").innerHTML = "";

    } 
    const employeenumber = document.getElementById('employee_number');
    if (!employeenumber.checkValidity()) {
      document.getElementById("demo5").innerHTML = "*กรุณากรอกจำนวนพนักงาน ";
      return false;
    } else {
      document.getElementById("demo5").innerHTML = "";

    } 
    const uname = document.getElementById('uname');
    if (!uname.checkValidity()) {
      document.getElementById("demo6").innerHTML = "*กรุณากรอกชื่อผู้ใช้เป็นภาษาอังกฤษ";
      return false;
    } else {
      document.getElementById("demo6").innerHTML = "";

    } 
    const pwd = document.getElementById('pwd').value;
    if (pwd == "") {
      document.getElementById("demo7").innerHTML = "*กรุณากรอกรหัสผ่าน";
      return false;
    } else {
      document.getElementById("demo7").innerHTML = "";

    } 
    const customFile = document.getElementById('customFile').value;
    if (customFile == "") {
      document.getElementById("demo8").innerHTML = "*กรุณาอัพโหลดเอกสารประกอบการ";
      return false;
    } else {
      document.getElementById("demo8").innerHTML = "";

    } 
  }